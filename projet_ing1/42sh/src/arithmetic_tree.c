/**
** \file arithmetic_tree.c
** \brief This file creates an ast from a string
** \author Mazyad Vincent
**
** This file creates an ast and calculate its value
*/

#include <stdlib.h>
#include <stdio.h>
#include "arithmetic.h"
#include "queue.h"

/**
** \fn struct tree* tree_init(int enumeration)
** \brief creates an empty tree
**
** \param int enumeration
** \return return the tree
*/

struct tree* tree_init(int enumeration)
{
   struct tree *tree = malloc(sizeof(struct tree));
   tree->right = NULL;
   tree->left = NULL;
   tree->val = enumeration;
   return tree;
}

/**
** \fn struct tree* create_integer_node(int value)
** \brief creates an empty tree for integer
**
** \param int value
** \return return the tree
*/

static struct tree* create_integer_node(int value)
{
    struct tree *tree = tree_init(1);
    tree->entier = value;
    return tree;
}

/**
** \fn struct tree* create_tree(struct liste *polonaise)
** \brief creates an the whole ast from a string
**
** \param char *key
** \return return the tree
*/

struct tree* create_tree(struct liste *polonaise)
{
   struct liste *copy = polonaise;
   while (copy)
   {
      while (copy && copy->val == 1)
        copy = copy->next;
      if (!copy)
         return NULL;
      copy->head = tree_init(copy->val);
      if (copy->prec && copy->prec->val == 1)
      {
         copy->head->right = create_integer_node(copy->prec->entier);
         polonaise = list_pop(polonaise, copy->prec);
      }
      else if (copy->prec && copy->prec->val != 1)
      {
         copy->head->right = copy->prec->head;
         polonaise = list_pop(polonaise, copy->prec);
      }
      if (copy->prec && copy->prec->val == 1)
      {
         copy->head->left = create_integer_node(copy->prec->entier);
         polonaise = list_pop(polonaise, copy->prec);
      }
      else if (copy->prec && copy->prec->val != 1)
      {
         copy->head->left = copy->prec->head;
         polonaise = list_pop(polonaise, copy->prec);
      }
      if (copy->next)
        copy = copy->next;
      else
        break;
   }
   struct tree *to_return = copy->head;
   while (copy)
   {
      struct liste *free_liste = copy;
      copy = copy->prec;
      free(free_liste);
   }
   return to_return;
}
/**
** \fn int result(struct tree *tree)
** \brief return the operation of an ast
**
** \param struct tree *tree
** \return return the result
*/

int result(struct tree *tree)
{
   if (!tree)
     return 0;
   if (tree->val == 2)  /* + */
     return result(tree->left) + result(tree->right);
   else if (tree->val == 3) /* * */
     return result(tree->left) * result(tree->right);
   else if (tree->val == 4) /* - */
     return result(tree->left) - result(tree->right);
   else if (tree->val == 5) /* / */
     return result(tree->left) / result(tree->right);
   else if (tree->val == 1) /* value */
     return tree->entier;
   return 0;
}

/**
** \fn void free_all_tree(struct tree *tree)
** \brief free the ast
**
** \param struct tree *tree
*/

void free_all_tree(struct tree *tree)
{
  if (!tree)
    return;
  struct tree *to_free = tree;
  free_all_tree(tree->left);
  free_all_tree(tree->right);
  free(to_free);
}
