#include <stdlib.h>
#include <stdio.h>
#include "control.h"
#include "movements.h"
#include "path_finding.h"
#include "queue.h"
#include "list.h"

int is_first = 1;

float find_finish(struct car *car)
{
    enum status stat = NONE;
    struct car *clone = car_clone(car);
    while (stat != END)
    {
        struct car *n_clone = car_clone(clone);
        while (stat == NONE)
            stat = car_move(n_clone, ACCELERATE);
        if (stat == CRASH)
            stat = car_move(clone, TURN_RIGHT);
        car_delete(n_clone);
    }
    float angle = clone->direction_angle;
    car_delete(clone);
    return angle;
}

int get_mingraph(struct sentinelle *senti)
{
    if (!senti)
        return -1;
    int min = senti->head->weight; 
    struct list *cpy = senti->head;
    while(cpy)
    {
        if (cpy->weight < min)
            min = cpy->weight;
        cpy = cpy->next;
    }
    return min;
}

struct graph* get_graph(struct sentinelle *senti, int weight)
{
    if (!senti || weight == -1)
        return NULL;
    struct list *cpy = senti->head;
    while (cpy && cpy->weight != weight)
        cpy = cpy->next;
    return cpy->data;
}

struct car *accelerate(struct car *clone, struct car *n_clone,
                       enum status *stat)
{
    while (*stat == NONE)
    {
        clone = car_clone(n_clone);
        *stat = car_move(n_clone, ACCELERATE);
    }
    return clone;
}

struct queue *find_finish2(struct car *car)
{
    enum status stat = NONE;
    struct car *clone = car_clone(car);
    struct queue *instructions = create_queue(DO_NOTHING);
    while (stat != END)
    {
        struct car *n_clone = car_clone(clone);
        clone = accelerate(clone, n_clone, &stat);
        if (stat == CRASH)
        {
            while (stat == CRASH)
            {
                stat = car_move(clone, TURN_RIGHT);
            }
            
        }
    }
    return instructions;
}

enum move update(struct car *car)
{
    
    static float startx;
    static float starty;
    struct vector2 *vect;
    static float finish_angle;
    if (is_first)
    {
        startx = map_get_start_x(car->map);
        starty = map_get_start_y(car->map);
        vect = vector2_new();
        vect->x = startx;
        vect->y = starty;
        finish_angle= find_finish(car);
        is_first = 0;
    }
    float angle = car->direction_angle - finish_angle;
    if (car->position.x == startx && car->position.y == starty
        && angle != 0)
    {
            return TURN_RIGHT;
    }
    return ACCELERATE;
}
