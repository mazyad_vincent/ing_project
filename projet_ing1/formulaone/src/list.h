#ifndef LIST_H
# define LIST_H

#include <stdlib.h>
#include <stdio.h>
#include "graph.h"
#include "control.h"


struct sentinelle
{
    struct list *head;
};

struct list
{
    struct graph *data;
    struct list *next;
    int weight;
};

struct sentinelle *senti_init();
struct list* list_init();
void push_list(struct sentinelle *senti, struct graph *graph, int weight);
void free_all(struct sentinelle *senti);
#endif
