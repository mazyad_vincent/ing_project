#include <stdio.h>
#include <stdlib.h>
#include "generation_map.h"

void update_fire_map(struct map *map, struct IA *ia, size_t index, int condi)
{
    if (ia->fire[index].x != ia->pos.x && ia->fire[index].y != ia->pos.y)
    /* if fire pos is != pos of ia */
    {
       if (!condi)
         map->pos[ia->fire[index].y][ia->fire[index].x] = NOT;
       else if (condi && map->pos[ia->fire[index].y][ia->fire[index].x == NOT])
         map->pos[ia->fire[index].y][ia->fire[index].x] = FIRE;
    }
 }

void losing_hp(struct map *map, struct player *player)
{
   int x = map->pos_player.x;
   int y = map->pos_player.y;
   if (map->pos[y][x] == FIRE)
      player->life -= 20;
}
