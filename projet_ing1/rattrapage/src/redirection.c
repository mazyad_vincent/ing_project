#include "ast.h"
#include "parse.h"
#include "execute.h"
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>

int right_redirection(struct ast *ast, int type)
{
   int file = 0;
   if (!type) /* simple > redirection */
     file = open(ast->right->key, O_CREAT|O_WRONLY|O_TRUNC, 0666);
   else
     file = open(ast->right->key, O_CREAT|O_WRONLY|O_APPEND, 0666);
   if (file == -1)
   {
      fprintf(stderr, "file can't be opened\n");
      return 1;
   }
   int copy_stdout = dup(1);
   if (dup2(file, 1) == -1)
   {
     perror("error dup2");
     close(file);
     exit(1);
   }
   int res = exec_rec(ast->left);
   if (dup2(copy_stdout, 1) == -1)
   {
     perror("error dup2");
     close(file);
     exit(1);
   }
   close(file);
   close(copy_stdout);
   return res;
}

int left_redirection(struct ast *ast)
{
   int file = open(ast->right->key, O_RDONLY, 0666);
   if (file == -1)
   {
      fprintf(stderr, "file can't be opened\n");
      return 1;
   }
   int copy_stdin = dup(0);
   if (dup2(file, 0) == -1)
   {
     perror("error dup2");
     close(file);
     close(copy_stdin);
     exit(1);
   }
   int res = exec_rec(ast->left);
   if (dup2(copy_stdin, 0) == -1)
   {
     perror("error dup2");
     close(file);
     close(copy_stdin);
     exit(1);
   }
   close(file);
   close(copy_stdin);
   return res;
}

int double_left_redirection(struct ast *ast)
{
  char buff[4096] = { 0 };
  int fd = open("my_file", O_APPEND | O_CREAT | O_WRONLY);
  if (!fd)
  {
    perror("can't open file");
    exit(1);
  }
  while (printf("> ") && fgets(buff, 4096, stdin))
  {
    if (!strncmp(buff, ast->right->key, strlen(ast->right->key)))
      break;
    if (write(fd, buff, strlen(buff)) == -1)
    {
      perror("can't write in file");
      remove("my_file");
    }
  }
  struct ast *copy = copy_ast(ast);
  free(copy->right->key);
  copy->right->key = malloc(8);
  memcpy(copy->right->key, "my_file", 8);
  close(fd);
  int res = left_redirection(copy);
  remove("my_file");
  free_ast(copy);
  return res;
}

int pipe_rule(struct ast *ast)
{
  int fd[2];
  pid_t childpid;
  pid_t lastpid;
  int stat; /* status of exec */
  if (pipe(fd) == -1)
  {
    perror("error");/* error */
    exit(1);
  }
  if ((childpid = fork()) == -1)
  {
    perror("error");/* error */
    exit(1);
  }
  else if (childpid == 0) /* child process */
  {
    close(fd[0]);
    if (dup2(fd[1], 1) == -1) /* connect the write with stdout */
      perror("error dup");
    exit(exec_rec(ast->left));
  }
  else /* parents */
  {
    if ((lastpid = fork()) == -1)
    {
      perror("error"); /*  error */
      exit(1);
    }
    if (lastpid == 0)
    {
      close(fd[1]); /* close write side */
      dup2(fd[0], 0); /* connecte the read with stdin */
      exit(exec_rec(ast->right));
    }
    else
    {
      close(fd[0]);
      close(fd[1]);
      waitpid(childpid, &stat, 0);
      waitpid(lastpid, &stat, 0);
      return WEXITSTATUS(stat);
    }
  }
}
