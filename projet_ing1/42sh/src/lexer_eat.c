/**
** \file lexer_eat.c
** \brief This file is the eat part of the lexer of the 42sh
** \author Odile Guillaume
**
** This file regroups functions that eat the current token in the command.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"
#include "call_commands.h"

extern struct global g_global;

static void quoted(void)
{
  g_global.index += 1;
  for (; g_global.command[g_global.index] != '\0' &&
         g_global.command[g_global.index] != '"'; g_global.index += 1)
    continue;
  if (g_global.command[g_global.index] == '\0')
    g_global.command = NULL;
  else
    g_global.index++; 
}

static void dollar(void)
{
  int nb_par = 0;
  for (; g_global.command[g_global.index] != '\0'; g_global.index++)
  {
    if ((g_global.command[g_global.index] == '\n' ||
         g_global.command[g_global.index] == ' '  ||
         g_global.command[g_global.index] == ';') && !nb_par)
      break;
    if (g_global.command[g_global.index] == '(')
      nb_par++;
    if (g_global.command[g_global.index] == ')')
      nb_par--;
  }
  if (g_global.command[g_global.index] == '\0')
    g_global.command = NULL;
  else
    g_global.index++; 
}

static void normal(void)
{
  while (g_global.command[g_global.index] != '\0' &&
         g_global.command[g_global.index] != ' ' &&
         g_global.command[g_global.index] != '\n' &&
         g_global.command[g_global.index] != '&' &&
         g_global.command[g_global.index] != '|' &&
         g_global.command[g_global.index] != '!' &&
         g_global.command[g_global.index] != '(' &&
         g_global.command[g_global.index] != ')' &&
         g_global.command[g_global.index] != '{' &&
         g_global.command[g_global.index] != '}' &&
         g_global.command[g_global.index] != ';' &&
         g_global.command[g_global.index] != '<' &&
         g_global.command[g_global.index] != '>')
    g_global.index++;

  if (g_global.command[g_global.index] == '\0')
    g_global.command = NULL;
}

/**
** \fn void eat(void)
** \brief increment the global index in command in function of the next token
**
** \param void
** \return void
*/

void eat(void)
{
  if (!g_global.command)
    return;
  while (g_global.command[g_global.index] != '\0' &&
        (g_global.command[g_global.index] == ' ' ||
         g_global.command[g_global.index] == '\n'))
    g_global.index++;
  comments(g_global.command, &(g_global.index));
  if (g_global.command[g_global.index] == '\0')
    return;
  int specials = is_special(g_global.command, g_global.index);
  if (specials)
  {
    g_global.index += specials;
    if (g_global.command[g_global.index] == '\0')
      g_global.command = NULL;
    return;
  }

  if (g_global.command[g_global.index] == '"')
    quoted();
  else if (g_global.command[g_global.index] == '$')
    dollar();
  else
    normal();
}
