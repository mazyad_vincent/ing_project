/**
 ** \file ast/method-call-exp.hxx
 ** \brief Inline methods of ast::MethodCallExp.
 */

#pragma once

#include <ast/method-call-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  inline const Var& MethodCallExp::var_get() const
  {
    return *var_;
  }


  inline Var& MethodCallExp::var_get()
  {
    return *var_;
  }

   inline const typename misc::symbol& MethodCallExp::get_func() const
   {
     return func_;
   }

   inline typename misc::symbol& MethodCallExp::get_func()
   {
     return func_;
   }

   inline const typename std::vector<Exp*>& MethodCallExp::get_vect() const
   {
     return vect_;
   }

   inline typename std::vector<Exp*>& MethodCallExp::get_vect()
   {
     return vect_;
   }

   inline void MethodCallExp::push_front(Exp* exp)
   {
     vect_.insert(vect_.begin(), exp);
   }
} // namespace ast

