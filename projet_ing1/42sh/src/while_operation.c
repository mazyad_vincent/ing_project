#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include "ast.h"
#include "call_commands.h"
#include "opt_commands.h"
#include "builtins.h"

int command_while_operation(struct ast *ast)
{
  int res = select_exec(ast->left);
  if (!res)
  {
     while (!res)
       res = then_if_command(ast);
     return 0;
  }
  if (res == 1)     
    return 0;
  while (!condition_if_command(ast)) /* same operation, exec condi */
  { 
      int new_res = then_if_command(ast);
      if (new_res == -2)
         break;
  }
  return 0;
}

int for_operation(struct ast *ast)
{
  return command_while_operation(ast); /* same than while */
}
