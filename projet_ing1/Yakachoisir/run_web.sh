#!/usr/bin/env bash

pip install -r requirements.txt
cd intra_asso/
python manage.py makemigrations
python manage.py makemigrations intranet
python manage.py migrate
python manage.py loaddata ../data.json
python manage.py runserver 0.0.0.0:8000
