/**
** \file loops_rule.c
** \brief This file regroups all the loops rule function of the parser
** \author Odile Guillaume
**
** This file regroups all the loops rule function of the LL grammar for the
** parser.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"


static struct ast *last_node(struct ast *node)
{
  struct ast *done = node;
  
  while (done && done->left && strcmp(done->left->key, "done"))
    done = done->left;
  if (strcmp(done->left->key, "done"))
    return NULL;
  struct ast *res = done->left;
  done->left = NULL;
  return res;
}

/**
** \fn struct ast *p_rule_until(void)
** \brief reprensents the until rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_rule_until(void)
{
  struct ast *curr = curr_tok();
  if (!curr || curr->type != UNTIL)
    return free_node(curr);
  eat();
  struct ast *node = p_compound_list();
  if (!node)
    return free_node(curr);
  node = ast_create(node, curr, NULL, NULL);
  struct ast *node2 = p_do_group();
  if (!node2)
    return free_node(node);
  node = ast_create(NULL, node, node2, NULL);
  struct ast *done = last_node(node->right);
  node = ast_create(done, node, NULL, NULL);
  return node;
}

/**
** \fn struct ast *p_rule_while(void)
** \brief reprensents the while rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_rule_while(void)
{
  struct ast *curr = curr_tok();
  if (!curr || curr->type != WHILE)
    return free_node(curr);
  eat();
  struct ast *node = p_compound_list();
  if (!node)
    return free_node(curr);
  node = ast_create(node, curr, NULL, NULL);
  struct ast *node2 = p_do_group();
  if (!node2)
    return free_node(node);
  node = ast_create(NULL, node, node2, NULL);
  struct ast *done = last_node(node->right);
  node = ast_create(done, node, NULL, NULL);
  return node;
}

static struct ast *p_newl_for(struct ast *curr)
{
  while (curr && curr->type == NEWL)
  {
    eat();
    ast_free(curr);
    curr = curr_tok();
  }
  return curr;
}

static struct ast *p_rule_for3(struct ast *node, struct ast *curr)
{
  while (curr->type == WORD)
  {
    node = ast_create(curr, node, NULL, NULL);
    eat();
    curr = curr_tok();
    if (!curr)
      return NULL;
  }
  if (curr)
    ast_free(curr);
  return node;
}

static struct ast *p_rule_for2(struct ast *node, struct ast *curr)
{
    curr = p_newl_for(curr);
    if (curr && curr->type == IN)
    {
      node = ast_create(curr, node, NULL, NULL);
      eat();
      curr = curr_tok();
      if (!curr)
        return NULL;
      node = p_rule_for3(node, curr);
      curr = curr_tok();
      if (curr->type == SEMI || curr->type == NEWL)
      {
        eat();
        if (curr->type == SEMI)
          node = ast_create(curr, node, NULL, NULL);
      }
      else
        return free_node(node);
    }
    return node;
}

/**
** \fn struct ast *p_rule_for(void)
** \brief reprensents the for rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_rule_for(void)
{
  struct ast *curr = curr_tok();
  if (!curr || curr->type != FOR)
    return free_node(curr);
  eat();
  struct ast *curr2 = curr_tok();
  if (!curr || curr2->type != WORD)
    return free_node(curr);
  eat();
  struct ast *node = ast_create(curr2, curr, NULL, NULL);
  curr = curr_tok();
  if (!curr)
    return NULL;
  if (curr->type == SEMI)
  {
    eat();
    ast_free(curr);
    curr = curr_tok();
  }
  else
    node = p_rule_for2(node, curr);
  curr = p_newl_for(curr);
  ast_free(curr);
  struct ast *node2 = p_do_group();
  if (!node2)
    return free_node(node);
  node = ast_create(NULL, node, node2, NULL);
  struct ast *done = last_node(node->right);
  node = ast_create(done, node, NULL, NULL);
  return node;
}
