var search = instantsearch({
    searchFunction(helper) {
        if (helper.state.query === '') {
            document.querySelector('#res-users').innerHTML = '';
            return;
        }
        var query = search.helper.state.query;
        search2.helper.setQuery(query);
        search2.helper.search();
        helper.search();
    },
    // Replace with your own values
    appId: '3STPWUWKGQ',
    apiKey: '31bf4d5c8d81a9559c4492f548e74f07', // search only API key, no ADMIN key
    indexName: 'asso_index',
    searchParameters: {
        hitsPerPage: 3
    }
});

var search2 = instantsearch({
    searchFunction(helper) {
        if (helper.state.query === '') {
            document.querySelector('#res-users').innerHTML = '';
            return;
        }
        helper.search();
    },
    // Replace with your own values
    appId: '3STPWUWKGQ',
    apiKey: '31bf4d5c8d81a9559c4492f548e74f07', // search only API key, no ADMIN key
    indexName: 'user_index',
    searchParameters: {
        hitsPerPage: 3
    }
});

search.addWidget(
    instantsearch.widgets.searchBox({
        container: '#search-input',
        autofocus: false,
        poweredBy: true,
        magnifier: false,
        reset: false,
        loadingIndicator: false
    })
);

search.addWidget(
    instantsearch.widgets.hits({
        container: document.querySelector('#res-assos'),
        templates: {
            item: document.getElementById('hit-template').innerHTML,
            empty: ""
        }
    })
);

search2.addWidget(
    instantsearch.widgets.hits({
        container: document.querySelector('#res-users'),
        templates: {
            item: document.getElementById('hit-template2').innerHTML,
            empty: ""
        }
    })
);
search2.start();
search.start();