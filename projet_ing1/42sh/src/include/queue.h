#ifndef QUEUE_H
# define QUEUE_H

#include "arithmetic.h"

struct liste
{
  int entier;
  enum val val;
  struct tree *head;
  struct liste *next;
  struct liste *prec;
};

struct queue
{
  struct liste *list;
  size_t size;
};

struct queue* init_queue();
struct liste* init_liste(int val);
struct queue* queue_push(struct queue *queue, struct liste *list);
struct liste* liste_push(struct liste *head, struct liste *list);
struct liste* queue_pop(struct queue *queue);
struct liste* get_polonaise_list(char *input, struct queue *queue);
struct liste* list_pop(struct liste *list, struct liste *addr);
void free_all_list(struct liste *list);

#endif
