/**
 ** \file ast/field-var.hh
 ** \brief Declaration of ast::FieldVar.
 */

#pragma once

#include <ast/var.hh>
#include <misc/symbol.hh>
#include <ast/exp.hh>
#include <ast/name-ty.hh>
#include <ast/field.hh>

namespace ast
{

  /// FieldVar.
  class FieldVar : public Var
  {
   public:
     FieldVar(const Location& location, Var* var, misc::symbol& symb);
     FieldVar(const FieldVar&) = delete;
     FieldVar& operator=(const FieldVar&) = delete;
     virtual ~FieldVar();

     void accept(ConstVisitor& v) const override;
     void accept(Visitor& v) override;

     const Var& var_get() const;
     Var& var_get();
     const misc::symbol& field_get() const;
     misc::symbol& field_get();

   protected:
     Var* var_;
     misc::symbol symb_;
 };

} // namespace ast

#include <ast/field-var.hxx>

