/**
** \file func_rule.c
** \brief This file contains the funcdec rule of the parser
** \author Odile Guillaume
**
** This contains the funcdec rule for the LL grammar of the parser.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

static struct ast *p_funcdec2(struct ast *node, struct ast *curr,
                              struct ast *curr2)
{
  eat();
  if (curr2)
    node = ast_create(curr, curr2, NULL, NULL);
  else
    node = ast_create(NULL, curr, NULL, NULL);
   curr2 = curr_tok();
  if (!curr2 || curr2->type != LPAR)
    return free_node(node);
  eat();
  curr = curr_tok();
  if (!curr || curr->type != RPAR)
    return free_node(node);
  eat();
  node = ast_create(curr2, node, curr, NULL);
  curr = curr_tok();
  while (curr && curr->type == NEWL)
  {
    eat();
    curr = curr_tok();
  }
  struct ast *node2 = p_shell_command();
  if (!node2)
    return free_node(node);
  node = ast_create(node2, node, NULL, NULL);
  return node;
}

/**
** \fn struct ast *p_funcdec(void)
** \brief reprensents the funcdec rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_funcdec(void)
{
  struct ast *curr = curr_tok();
  if (!curr)
    return NULL;
  struct ast *curr2 = NULL;
  struct ast *node = NULL;
  if (curr->type != WORD)
  {
    if (curr->type == FUNCTION)
    {
      curr2 = curr;
      eat();
      curr = curr_tok();
      if (!curr)
        return NULL;
    }
    else
      return free_node(curr);
  }
  if (curr->type == WORD)
    node = p_funcdec2(node, curr, curr2);
  else
  {
    ast_free(curr);
    return free_node(node);
  }
  return node;
}
