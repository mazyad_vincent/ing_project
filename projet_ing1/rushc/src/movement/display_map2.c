#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <stdbool.h>
#include <SDL2/SDL_image.h>
#include "texture.h"
#include "../generation_map.h"
#include "player_control.h"
#include "../scrollin/scroling.h"

#define SCREEN_WIDTH  810
#define SCREEN_HEIGHT 600

#define PIXEL_BLOCK_SIZE 30

struct state_of_game sog = {true, false};

static void put_end(struct map *map)
{
  int x = map->width;
  int y = map->height;
  int randy = random_two_numb(y, 1);
  map->pos[randy][x] = END;
  map->end.x = x;
  map->end.y = randy;
}

void render_map(struct map *map, SDL_Renderer *renderer)
{
  enum type **m = map->pos;
  struct texture *t = map->ground_texture;
  static int off = 0;
  off+=2;
  for (int y = 0; y < map->height; y++)
  {
    for (int x = 0; x < map->width; x++)
    {
      int k = 1;
      switch (m[y][x])
      {
        case TOP:
         t = map->top_texture;
         break;
        case GROUND:
         t = map->ground_texture;
         break;
        case DECOR:
         t = map->decoy_texture;
         break;
        default:
         k = 0;
         break;
      }
      if (k == 1)
       render(t, x * PIXEL_BLOCK_SIZE - off, y * PIXEL_BLOCK_SIZE, NULL, NULL,
     SDL_FLIP_NONE, renderer);
    }
  }
}

static SDL_Surface* init_surf()
{
  SDL_Surface *background = NULL;
  background = IMG_Load("../texture/space.png");
  if (!background)
  {
     printf("No background image");
     return NULL;
  }
  return background;
}

static SDL_Texture* init_text(SDL_Renderer *renderer,
                              SDL_Surface *background, SDL_Rect *rect)
{
  SDL_Texture *screen_texture = SDL_CreateTextureFromSurface(renderer, background);
  SDL_FreeSurface(background);
  SDL_QueryTexture(screen_texture, NULL, NULL, &rect->w, &rect->h);
  rect->x = 0;
  rect->y = 0;
  SDL_RenderCopy(renderer, screen_texture, NULL, rect);
  return screen_texture;
}

int game(SDL_Renderer *renderer)
{
   SDL_Rect rect;
   SDL_Surface *background = init_surf();
   SDL_Texture *screen_texture = init_text(renderer, background, &rect);

   struct map *map = map_init(); //init the map
   map->ground_texture = texture_init("../texture/bot.png", renderer);
   map->decoy_texture = texture_init("../texture/rock.png", renderer);
   map->top_texture = texture_init("../texture/top.png", renderer);
   put_end(map); // put the end
   struct player *p = init_start(map);
   p->p_texture = texture_init("../texture/btship2.png", renderer);
   p->pos.y = SCREEN_HEIGHT / 2;
   /* While application is running */
   int quit = 0;
   for (int i = 0; i < 4; ++i)
       add_decoy(map);
   generate_IA(map, renderer);
   int begin_time = SDL_GetTicks();
   int curr_time = 0;
   while(quit == 0 && is_win(map, p) == 2) // add the res
   {
     quit = player_update(p, map);
     //Clear screen
     SDL_RenderClear(renderer);
     SDL_RenderCopy(renderer, screen_texture, NULL, &rect);
     render_map(map, renderer);
     curr_time = SDL_GetTicks();
     int timing = curr_time - begin_time;
     update_ia(map, timing);
     rend_all(map, renderer); /* print ia with flames */
     render(p->p_texture, p->pos.x, p->pos.y, NULL, NULL, SDL_FLIP_NONE,
            renderer);
     //Update screen
     SDL_Delay(2000/60);
     SDL_RenderPresent(renderer);
   }
   sog.end = false;
   return 0;
}
