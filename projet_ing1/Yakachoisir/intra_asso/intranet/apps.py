from django.apps import AppConfig
import algoliasearch_django as algolia
from .index import AssociationIndex, UserIndex
from django.conf import settings


class IntranetConfig(AppConfig):
    name = 'intranet'

    def ready(self):
        association = self.get_model('association')
        algolia.register(association, AssociationIndex)
        user = self.get_model('ProxyUser')
        algolia.register(user, UserIndex)
        import intranet.signals

