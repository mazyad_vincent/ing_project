#ifndef BUILTIN_H
# define BUILTIN_H

#include "ast.h"

int is_builtin(const char *key); /* 1 if builtin 0 is not */
int cd_builtin(struct ast *ast); /* cd command */
int echo_command(struct ast *ast); /* echo command */
int call_builtin(struct ast *ast); /* call the right builtin */
int exit_command(struct ast *ast); /* exit command */
int kill_builtin(struct ast *ast); /* kill command */

#endif
