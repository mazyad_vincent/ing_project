#include "parse.h"
#include "builtin.h"

static char *char_to_print(char *c)
{
	char buf[1024] = { 0 };
	size_t i = 0;
	for (size_t index = 0; c[index]; ++c)
	{
		if (c[index] != '\'' && c[index] != '"')
		{
			buf[i] = c[index];
			++i;
		}
	}
	buf[i] = '\0';
	char *ret = malloc(i + 1);
	memcpy(ret, buf, i+1);
	return ret;
}

int echo_command(struct ast *ast)
{
	if (!ast)
	{
		printf("\n");
		return 0;
	}
	int n_opt = 0;
	if (!strcmp(ast->key, "-n"))
	{
		n_opt = 1;
		ast = ast->left;
	}
	for (; ast && strcmp(ast->key, ";"); ast = ast->left)
	{
		if (!strcmp(ast->key, "-n") || !strcmp(ast->key, "-e") || !strcmp(ast->key, "-E"))
			continue;
		char *key = char_to_print(ast->key);
		if (ast->left && strcmp(ast->left->key, ";"))
	  	printf("%s ", key);
		else
			printf("%s", key);
		free(key);
	}
	if (!n_opt)
		printf("\n");
	return 0;
}
