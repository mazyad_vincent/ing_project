#include "env.h"
#include "parse.h"
#include <stdlib.h>

int check_equal(char *key)
{
  for (size_t i = 0; key[i]; ++i)
  {
    if (key[i] == '=')
      return 1;
  }
  return 0;
}

int is_valid_alias(struct ast *ast) /* is valid = 1 */
{
  if (!ast)
    return 1;
  if (check_equal(ast->key))
  {
    if (ast->key[0] == '=')
       return 0;
    if (ast->key[0] >= 48 && ast->key[0] <= 57)
       return 0;
    return 1;
  }
  else
    return 0;
}

char *get_alias(char *key)
{
  char buf[1024] = { 0 };
  size_t i = 0;
  for (; key[i] && key[i] != '='; ++i)
    buf[i] = key[i];
  buf[i] = '\0';
  char *to_ret = malloc(i + 1);
  memcpy(to_ret, buf, i + 1);
  return to_ret;
}

char *get_val(char *key)
{
  size_t i = 0;
  for (; key[i] && key[i] != '='; ++i);
  if (key[i] == '=')
    i += 1;
  if (!key[i])
    return NULL;
  char buf[1024] = { 0 };
  size_t index = 0;
  for (; key[i]; ++i)
  {
    buf[index] = key[i];
    ++index;
  }
  buf[index] = '\0';
  char *to_ret = malloc(index + 1);
  memcpy(to_ret, buf, index + 1);
  return to_ret;
}

int environ_command(struct ast *ast)
{
  char *alias = get_alias(ast->key);
  char *val = get_val(ast->key);
  if (!val)
    return 0;
  if (setenv(alias, val, 1) == -1)
  {
    perror("setenv");
    free(alias);
    free(val);
    exit(1);
  }
  free(alias);
  free(val);
  return 0;
}
