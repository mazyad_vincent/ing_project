/**
** \file replace_value.c
** \brief replace_value.c file to replace the key value node
** \author Mazyad Vincent
**
** File that regroups functions to change the value of a variable to its
** corresponding value
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "ast.h"
#include "hashtable.h"
#include "replace_value.h"
#include "parser.h"
#include "queue.h"
#include "arithmetic.h"

extern struct global g_global;

/**
** \fn static int is_unalias_command(struct ast *ast)
** \brief check if there is an unalias builtin
**
** \param struct ast *ast
** \return true (1) or false (0)
*/

static int is_unalias_command(struct ast *ast)
{
  if (!ast)
    return 0;
  if (!strcmp(ast->key, "unalias"))
    return 1;
  return is_unalias_command(ast->left) || is_unalias_command(ast->right);
}

/**
** \fn int is_variable(char *a)
** \brief check if its a variable called with the $
**
** \param char *
** \return true (1) or false (0)
*/

int is_variable(char *a)
{
  return (a[0] == '$')? 1 : 0;
}

/**
** \fn void replace_node(struct hashtable *hash, struct ast *ast)
** \brief replace the ast node by its corresponding data
**
** \param struct hashtable *hash, struct ast *ast
*/

void replace_node(struct hashtable *hash, struct ast *ast, int unalias)
{
  int isvar = is_variable(ast->key);
  char *copy = ast->key + isvar;
  if (find_hash(hash, copy) && (isvar || !unalias))
  {
    char *data = strdup(find_hash(hash, copy)->data);
    free(ast->key);
    ast->key = data;
  }
}

/**
** \fn int is_arithmetic(struct ast *ast)
** \brief check if it looks like an arithmetic operation
** \param struct ast *ast
** \return 1 if true 0 if false
*/

static int is_arithmetic(struct ast *ast)
{
  if (ast && strlen(ast->key) < 3)
     return 0;
  return !strncmp(ast->key, "$((", 3)? 1 : 0;
}

/**
** \fn void get_environnement(struct ast *ast)
** \brief replace each nodes starting by $ by its env data
** \param struct ast *ast
*/

static void get_environnement(struct ast *ast)
{
  char *data = getenv(ast->key + 1);
  if (!data)
     return;
  free(ast->key);
  char *new_key = malloc(sizeof(char) * (strlen(data) + 1));
  strcpy(new_key, data);
  ast->key = new_key;  
}
/**
** \fn void replace_all_nodes(struct hashtable *hash, struct ast *ast)
** \brief replace each nodes starting by $ by its data
** \param struct hashtable hash, struct ast *ast
*/

void replace_all_nodes(struct hashtable *hash, struct ast *ast)
{
   if (!ast || !hash)
     return;
   int unalias = is_unalias_command(g_global.ast); /* if unalias builtin in ast */
   if (getenv(ast->key + 1))
      get_environnement(ast); 
   if (is_variable(ast->key) && !is_arithmetic(ast) &&
      find_hash(hash, ast->key + 1) && !find_hash(hash, ast->key + 1)->alias)
      replace_node(hash, ast, unalias);
   if (find_hash(hash, ast->key) && find_hash(hash, ast->key)->alias)
      replace_node(hash, ast, unalias);
   if (is_arithmetic(ast))
      do_all_operation(ast);
   if (!find_hash(hash, ast->key) && is_variable(ast->key))
   {
      free(ast->key);
      char *dat = malloc(2);
      dat[0] = ' ';
      dat[1] = '\0'; 
      ast->key = dat;
   }
   replace_all_nodes(hash, ast->left);
   replace_all_nodes(hash, ast->right);
}
