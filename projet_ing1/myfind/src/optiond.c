#include "Threshold.h"

void redirect_opt(char c, char *copy, struct queue *queue, struct stat st)
{
    if (c == 'd')
        print_D(copy, queue, st);
    if (c == 'H')
        print_H(copy, queue, st);
    if (c == 'L')
        print_L(copy, queue, st);
    if (c == 'P')
        print_P(copy, queue, st);
}

void print_D(char *copy, struct queue *queue, struct stat st)
{

    int u = 0;
    int l = my_strlen(copy);
    if (S_ISDIR(st.st_mode)) //directory
    {
        if (copy[my_strlen(copy)-1] != '/')
            u = 1;
        copy = print_dopt(copy, queue); //function to call
        struct queue_list *head = queue_pop(queue);
        (u==1)? printf("%.*s\n", l , head->data) : printf("%s\n", head->data);
        free(head->data);
        free(head);
    }
    else if ((S_ISREG(st.st_mode) || is_symb(copy)))
        printf("%s\n", copy);
    free(copy);
}

void print_H(char *copy, struct queue *queue, struct stat st)
{

    if (is_symb(copy) || S_ISDIR(st.st_mode))
    {
        int u = 0;
        if (copy[my_strlen(copy)-1] != '/')
            u = 1;
        copy = print_dopt(copy, queue); //function to call
        struct queue_list *head = queue_pop(queue);
        (u == 1)? printf("%.*s\n", my_strlen(copy), 
                head->data) : printf("%s\n", head->data);
        free(head->data);
        free(head);
    }
    else if (S_ISREG(st.st_mode))
        printf("%s\n", copy);
    free(copy);
}

void print_L(char *copy, struct queue *queue, struct stat st)
{
    if (is_symb(copy) || S_ISDIR(st.st_mode))
    {
        int u = 0;
        if (copy[my_strlen(copy)-1] != '/')
            u = 1;
        copy = print_dopt_syml(copy, queue); //function to call
        struct queue_list *head = queue_pop(queue);
        (u == 1)? printf("%.*s\n", my_strlen(copy),
                head->data) : printf("%s\n", head->data);
        free(head->data);
        free(head);
    }
    else if (S_ISREG(st.st_mode))
        printf("%s\n", copy);
    free(copy);
}

void print_P(char *copy, struct queue *queue, struct stat st)
{
    if (S_ISDIR(st.st_mode))
    {
        int u = 0;
        if (copy[my_strlen(copy)-1] != '/')
            u = 1;
        copy = print_dopt(copy, queue); //function to call
        struct queue_list *head = queue_pop(queue);
        (u == 1)? printf("%.*s\n", my_strlen(copy),
                head->data) : printf("%s\n", head->data);
        free(head->data);
        free(head);
    }
    else if (S_ISREG(st.st_mode) || is_symb(copy))
        printf("%s\n", copy);
    free(copy);
}
