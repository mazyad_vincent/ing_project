#include <stdlib.h>
#include "ast.h"
#include <stdio.h>
#include <string.h>

struct ast *init_ast(char *key)
{
  if (!key)
    return NULL;
  struct ast *ret = malloc(sizeof(struct ast));
  ret->right = NULL;
  ret->left = NULL;
  ret->key = key;
  return ret;
}

void insert_node(struct ast **ast_main, struct ast *to_insert) /* insert in the left */
{
	if (!(*ast_main))
		return;
	if (!((*ast_main)->left))
	{
		(*ast_main)->left = to_insert;
		return;
	}
	insert_node(&((*ast_main)->left), to_insert);
}

void free_ast(struct ast *ast)
{
  if (!ast)
    return;
  free_ast(ast->left);
  free_ast(ast->right);
	free(ast->key);
  free(ast);
}

void print_all(struct ast *ast)
{
	if (!ast)
		return;
	printf("%s\n", ast->key);
	print_all(ast->left);
	print_all(ast->right);
}

struct ast *copy_ast(struct ast *ast)
{
   if (!ast)
    return NULL;
   struct ast *copy = malloc(sizeof(struct ast));
   char *copy_key = malloc(strlen(ast->key) + 1);
   memcpy(copy_key, ast->key, strlen(ast->key) + 1);
   copy->key = copy_key;
   copy->left = copy_ast(ast->left);
   copy->right = copy_ast(ast->right);
   return copy;
}
