#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "memory_list.h"
#include "tool_parse.h"
#include "parsing.h"

char *get_key(struct mem_list *memory, FILE *torrent, int type_value)
{
  int size_key = type_value - '0';
  int digit = fgetc(torrent);
  while (digit != ':')
  {
    size_key = size_key * 10;
    size_key = size_key + (digit - '0');
    digit = fgetc(torrent);
  }
  size_key++;
  char *key = malloc(size_key * sizeof(char));
  if (!key)
    my_err("Couldn't malloc key");
  push_mem(memory, key);
  if (!(fgets(key, size_key, torrent)))
    my_err("keyfgets fail");
  return key;
}

struct b_data *get_string(struct mem_list *memory,
                          FILE *torrent, int type_value)
{
  size_t size = type_value - '0';
  int digit = fgetc(torrent);
  while (digit != ':')
  {
    size = size * 10;
    size = size + (digit - '0');
    digit = fgetc(torrent);
  }
  char *value = malloc(size * sizeof(char));
  if (!value)
    my_err("Couldn't malloc string value");
  push_mem(memory, value);
  if (fread(value, 1, size, torrent) < size)
    my_err("valuefgets fail");
  struct b_data *data = malloc(sizeof(struct b_data));
  if (!data)
    my_err("Couldn't malloc b_data string");
  push_mem(memory, data);
  data->my_type = CHAR;
  data->my_value = malloc(sizeof(union value));
  if (!data->my_value)
    my_err("Couldn't malloc union string");
  push_mem(memory, data->my_value);
  data->my_value->string = value;
  data->length = size;
  return data;
}

struct b_data *get_integer(struct mem_list *memory, FILE *torrent)
{
  int integer = 0;
  int digit = fgetc(torrent);
  size_t nbr_digit = 0;
  while (digit != 'e')
  {
    integer = integer * 10;
    integer = integer + (digit - '0');
    digit = fgetc(torrent);
    nbr_digit++;
  }
  struct b_data *data = malloc(sizeof(struct b_data));
  if (!data)
    my_err("Couldn't malloc b_data integer");
  push_mem(memory, data);
  data->my_type = INT;
  data->my_value = malloc(sizeof(union value));
  if (!data->my_value)
    my_err("Couldn't malloc union integer");
  push_mem(memory, data->my_value);
  data->my_value->integer = integer;
  data->length = nbr_digit;
  return data;
}

struct b_data *get_list(struct mem_list *memory, FILE *torrent, int type_value)
{
  struct b_list *b_list = b_list_init(memory);
  struct b_data *data_root = malloc(sizeof(struct b_data));
  if (!data_root)
    my_err("Couldn't malloc data_root");
  push_mem(memory, data_root);
  data_root->my_type = B_LIST;
  data_root->my_value = malloc(sizeof(union value));
  if (!data_root->my_value)
    my_err("Couldn't malloc union b_list");
  push_mem(memory, data_root->my_value);
  data_root->my_value->my_b_list = b_list;
  type_value = fgetc(torrent);
  while (type_value != 'e')
  {
    struct b_list_node *node = malloc(sizeof(struct b_list_node));
    if (!node)
      my_err("Couldn't malloc node list");
    push_mem(memory, node);
    if ('0' <= type_value && type_value <= '9')
      node->data = get_string(memory, torrent, type_value);
    else if (type_value == 'i')
      node->data = get_integer(memory, torrent);
    else if (type_value == 'l')
      node->data = get_list(memory, torrent, type_value);
    else if (type_value == 'd')
      node->data = get_dict(memory, torrent, type_value);
    else
      my_err("Wrong type_value in b_list");
    b_list_push(b_list, node);
    type_value = fgetc(torrent);
  }
  return data_root;
}

struct b_data *get_dict(struct mem_list *memory, FILE *torrent, int type_value)
{
  struct b_dict_list *dict = dict_list_init(memory);
  struct b_data *data_root = malloc(sizeof(struct b_data));
  if (!data_root)
    my_err("Couldn't malloc dict_root");
  push_mem(memory, data_root);
  data_root->my_type = B_DICT_LIST;
  data_root->my_value = malloc(sizeof(union value));
  if (!data_root->my_value)
    my_err("Couldn't malloc union dict_list");
  push_mem(memory, data_root->my_value);
  data_root->my_value->my_b_dict_list = dict;
  type_value = fgetc(torrent);
  while (type_value != 'e')
  {
    struct b_dict_list_node *node = malloc(sizeof(struct b_dict_list_node));
    if (!node)
      my_err("Couldn't malloc node list");
    push_mem(memory, node);
    node->my_key = get_key(memory, torrent, type_value);
    type_value = fgetc(torrent);
    if ('0' <= type_value && type_value <= '9')
      node->data = get_string(memory, torrent, type_value);
    else if (type_value == 'i')
      node->data = get_integer(memory, torrent);
    else if (type_value == 'l')
      node->data = get_list(memory, torrent, type_value);
    else if (type_value == 'd')
      node->data = get_dict(memory, torrent, type_value);
    else
      my_err("Wrong type_value in b_dict_list");
    dict_list_push(dict, node);
    type_value = fgetc(torrent);
  }
  return data_root;
}

size_t get_nbr_digits(int value)
{
  size_t i = 0;
  while (value > 0)
  {
    i++;
    value = value / 10;
  }
  return i;
}

void get_info_key(struct b_dict_list_node *node, struct info *info,
                          size_t after_size, size_t mid_size)
{
  info->string = realloc(info->string, 1 + after_size);
  sprintf(info->string + info->size, "%zu:", strlen(node->my_key));
  size_t i = 0;
  while (i < strlen(node->my_key))
  {
    sprintf(info->string + mid_size + i, "%c", *(node->my_key + i));
    i++;
  }
  info->size = after_size;
}

void get_info_string(struct b_data *data, struct info *info,
                      size_t after_size, size_t mid_size)
{
  info->string = realloc(info->string, 1 + after_size);
  sprintf(info->string + info->size, "%zu:", data->length);
  size_t i = 0;
  while (i < data->length)
  {
    sprintf(info->string + mid_size + i, "%c", *(data->my_value->string + i));
    i += 1;
  }
  info->size = after_size;
}

void get_info_int(struct b_data *data, struct info *info,
                          size_t after_size)
{
  info->string = realloc(info->string, 1 + after_size);
  sprintf(info->string + info->size, "i%de", data->my_value->integer);
  info->size = after_size;
}

void get_info_list(struct b_list_node *node, struct info *info)
{
  info->string = realloc(info->string, 1 + info->size);
  sprintf(info->string + info->size, "l");
  info->size += 1;
  while (node)
  {
    if (node->data->my_type == INT)
    {
      size_t new_size = node->data->length + 2 + info->size;
      get_info_int(node->data, info, new_size);
    }
    else if (node->data->my_type == CHAR)
    {
      size_t new_size = get_nbr_digits(node->data->length) + 1 + info->size +
                        node->data->length;
      size_t mid_size = get_nbr_digits(node->data->length) + 1 + info->size;
      get_info_string(node->data, info, new_size, mid_size);
    }
    else if (node->data->my_type == B_LIST)
      get_info_list(node->data->my_value->my_b_list->head, info);
    else if (node->data->my_type == B_DICT_LIST)
      get_info_dict(node->data->my_value->my_b_dict_list->head, info);
    node = node->next;
  }
  info->string = realloc(info->string, 1 + info->size);
  sprintf(info->string + info->size, "e");
  info->size += 1;
}

void get_info_dict(struct b_dict_list_node *node, struct info *info)
{
  info->string = realloc(info->string, 2 + info->size);
  sprintf(info->string + info->size, "d");
  info->size += 1;
  while (node)
  {
    size_t new_size = info->size + get_nbr_digits(strlen(node->my_key)) +
                      1 + strlen(node->my_key);
    size_t mid_size = info->size + 1 + get_nbr_digits(strlen(node->my_key));
    get_info_key(node, info, new_size, mid_size);
    if (node->data->my_type == INT)
    {
      new_size = node->data->length + 2 + info->size;
      get_info_int(node->data, info, new_size);
    }
    else if (node->data->my_type == CHAR)
    {
      new_size = get_nbr_digits(node->data->length) + 1 + info->size +
                 node->data->length;
      mid_size = get_nbr_digits(node->data->length) + 1 + info->size;
      get_info_string(node->data, info, new_size, mid_size);
    }
    else if (node->data->my_type == B_LIST)
      get_info_list(node->data->my_value->my_b_list->head, info);
    else if (node->data->my_type == B_DICT_LIST)
      get_info_dict(node->data->my_value->my_b_dict_list->head, info);
    node = node->next;
  }
  info->string = realloc(info->string, 2 + info->size);
  sprintf(info->string + info->size, "e");
  info->size += 1;
}

struct info *get_info(struct mem_list *memory, struct b_data *data)
{
  struct b_dict_list_node *node = data->my_value->my_b_dict_list->head;
  while (node && strcmp(node->my_key, "info") != 0)
    node = node->next;
  struct info *info = malloc(sizeof(struct info));
  if (!info)
    my_err("Couldn't malloc info");
  info->string = NULL;
  info->size = 0;
  get_info_dict(node->data->my_value->my_b_dict_list->head, info);
  push_mem(memory, info);
  push_mem(memory, info->string);
  return info;
}
