# Yaka Choisir

Yep

## Installation

### Database
* Install postgresql (9.3 or older)
* Setup postgresql :
```
42sh$ echo 'export PGDATA="$HOME/postgres_data"' >> ~/.bashrc
42sh$ source ~/.bashrc
42sh$ initdb --locale "$LANG" -E UTF8
42sh$ postgres -D "$PGDATA" -k /tmp
```
* In the project root `psql -h localhost postgres -f conf.db`

The database is now ready

### Pip and Virtualenv

* Run `pip --version` and make sure it's using Python 3.X
* Then you can directly do `source create_venv.sh`
* If it is not using python 3 :
```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

### Setup Django

* In the `intra_asso` directory do `python manage.py makemigrations intranet`
* Then `python manage.py migrate`

Django is now ready.

To start the server you just have to run `python manage.py runserver`

If you want to have some data in your database run `python manage.py loaddata ../data.json`

## Docker

You can also use Docker to initializes the database and start Django without installing anything.
Make sure you have `docker` and `docker-compose` installed.

*Tips*: run `sudo gpasswd -a $USER docker` to run docker as non-root

To do that:
* In the project root run `docker-compose up` it will start the database and start the development server.
* If you want to run a special command for Django you can run `docker-compose exec web bash`. This will start a bash interpreter in the container that hold Django.

## Built With

* [Bootstrap](https://getbootstrap.com/) - Framework Web
* [VueJS](https://vuejs.org/) - Framework JS
* [Django](https://www.djangoproject.com/) - Framework Python | Base de donnée

## Authors
* **OUVRARD Valentin** - *Front End* - [Zeitlan](https://github.com/Zeitlan)
* **Antoine Léauté** - *Back End* - [Leot78](https://github.com/leot78)
* **Quentin Wentzler** - *Back End* - [Quiwin](https://github.com/Quiwin)
* **Vincent Mazyad** - *Front End / Back End* - [VincentMazyad](https://github.com/vincentmazyad)

See also the list of [contributors](https://github.com/Zeitlan/YakaChoisir/contributors) who participated in this project.
