#ifndef PARSE_H
# define PARSE_H

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "ast.h"

struct global
{
	int index; /* index for parsing input */
	struct ast *ast; /* ast for parsing */
	int res; /* result to return ($?) */
	struct ast *ast_parse; /* to free if exit used */
};

enum type
{
	WORD = 1,
	SPECIAL,
};

struct ast *parse_input(struct ast *ast, char *input); /* create the ast from input */
char *get_next_word(char *command); /* get next word of input */
int check_parse(struct ast *ast); /* fast function which check if input is correct */
int is_file(const char *file); /* check either file is a file */
char *get_input_from_file(char *file); /* get input from file */

#endif
