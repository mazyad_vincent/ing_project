/**
** \file redirection_rule.c
** \brief This file contains the redirection rule for the parser
** \author Odile Guillaume
**
** This file contains the redirection rule for the LL grammar of the parser.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

static struct ast *p_redirection2(struct ast *node, struct ast *curr,
                                  struct ast *curr2)
{
  if (curr->type == GREAT || curr->type == LESS || curr->type == DGREAT || \
      curr->type == GREATAND || curr->type == LESSAND ||                   \
      curr->type == CLOBBER || curr->type == LESSGREAT)
  {
    eat();
    struct ast *curr3 = curr_tok();
    if (!curr3 || curr3->type != WORD)
    {
      ast_free(curr2);
      ast_free(node);
      return free_node(curr);
    }
    eat();
    node = ast_create(curr2, curr, curr3, NULL);
  }
  else if (curr->type == DLESS || curr->type == DLESSDASH)
  {
    eat();
    struct ast *curr3 = curr_tok();
    if (!curr3 || curr3->type != HEREDOC)
    {
      ast_free(curr2);
      ast_free(node);
      return free_node(curr);
    }
    eat();
    node = ast_create(curr2, curr, curr3, NULL);
  }
  else
  {
    ast_free(curr2);
    ast_free(node);
    return free_node(curr);
  }
  return node;
}

/**
** \fn struct ast *p_redirection(void)
** \brief reprensents the redirection rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_redirection(void)
{
  struct ast *curr = curr_tok();
  if (!curr)
    return NULL;
  struct ast *curr2 = NULL;
  struct ast *node = NULL;
  if (curr->type == IONUMBER)
  {
    eat();
    curr2 = curr;
    curr = curr_tok();
    if (!curr)
      return free_node(curr2);
  }
  node = p_redirection2(node, curr, curr2);
  return node;
}
