/**
 ** \file ast/array-exp.hxx
 ** \brief Inline methods of ast::ArrayExp.
 */

#pragma once

#include <ast/array-exp.hh>

namespace ast
{
  inline const NameTy& ArrayExp::name_get() const
  {
    return *name_;
  }

  inline NameTy& ArrayExp::name_get()
  {
    return *name_;
  }

  inline const Exp& ArrayExp::into_get() const
  {
    return *into_;
  }

  inline Exp& ArrayExp::into_get()
  {
    return *into_;
  }

  inline const Exp& ArrayExp::type_get() const
  {
    return *type_;
  }

  inline Exp& ArrayExp::type_get()
  {
    return *type_;
  }

} // namespace ast
