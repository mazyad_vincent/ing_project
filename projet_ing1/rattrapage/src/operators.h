#ifndef OPERATORS_H
# define OPERATORS_H

#include "builtin.h"

int is_operator(const char *key); /* check if this is an operator */
int operator_and(struct ast *ast);
int operator_or(struct ast *ast);
int call_operator(struct ast *ast); /* call the method of the right operator */
int right_redirection(struct ast *ast, int type); /* right redirection + >> */
int left_redirection(struct ast *ast); /* left redirection */
int double_left_redirection(struct ast *ast); /* << */
void swap_ast(struct ast *ast); /* swap if multiple chevrons */
int pipe_rule(struct ast *ast); /* pipe rule */

#endif
