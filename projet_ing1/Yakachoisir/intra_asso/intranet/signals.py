from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver
from .models import Association, DemandeCreationAssociation, ResponsableAsso, IsApplyingTo
from guardian.shortcuts import assign_perm
from django.core.mail import EmailMessage


@receiver(post_save, sender=Association)
def asso_give_perm(sender, instance, **kwargs):
    assign_perm('change_association', instance.login_pres, instance)
    assign_perm('change_association', instance.login_tres, instance)
    assign_perm('change_association', instance.login_secr, instance)


@receiver(post_save, sender=DemandeCreationAssociation)
def crea_asso_notify_respo(sender, instance, **kwargs):
    body = ("Bonjour, une demande de création d'association a été effectuée par " + instance.createur.first_name + " "
            + instance.createur.last_name + ".\nLe nom de l'association proposée est : " + instance.nom_asso + ".\n"
            + "Vous pouvez traiter cette demande à partir de votre compte de responsable des associations.")
    email = EmailMessage("Demande de création d'association", body, to=[ResponsableAsso.objects.first().user.email])
    email.send()


@receiver(post_save, sender=IsApplyingTo)
def is_applying_to_give_perm(sender, instance, **kwargs):
    instance.apply_perm(instance.asso.login_pres)
    instance.apply_perm(instance.asso.login_tres)
    instance.apply_perm(instance.asso.login_secr)


@receiver(post_save, sender=ResponsableAsso)
def give_staff(sender, instance, **kwargs):
    instance.user.is_staff = True
    instance.user.save()


@receiver(pre_delete, sender=ResponsableAsso)
def remove_staff(sender, instance, **kwargs):
    instance.user.is_staff = False
    instance.user.save()


@receiver(pre_save, sender=ResponsableAsso)
def change_staff(sender, instance, **kwargs):
    if instance.id:
        old = ResponsableAsso.objects.get(pk=instance.id)
        old.user.is_staff = False
        old.user.save()
