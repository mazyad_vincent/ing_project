#include "iso9660.h"
#include "my_read_iso.h"

void *cast_void(void *ptr)
{
    return ptr;
}

char *cast_char(void *ptr)
{
    return ptr;
}

void *add_offset(void *ptr, int offset)
{
    char *p = ptr;
    return p + offset;
}

int next_slash(const char *str, int i)
{
    while (str[i] && str[i] != '/')
        i++;
    return str[i];
}

void *get_super_block(void* ptr)
{
    char *p = ptr;
    return p + SUPER_BLOCK_OFFSET;
}
