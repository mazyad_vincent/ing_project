#ifndef SCROLLING_H
# define SCROLLING_H

#include "../generation_map.h"

struct state_of_game
{
  bool debut;
  bool end;
};

void start_game(SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *mario, SDL_Rect *mario_pos, struct map *map, int i);

# endif /* !SCROLLING_H_ */
