/**
 ** \file bind/tasks.hh
 ** \brief Bind module related tasks.
 */

  // FIXME

#pragma once

#include <task/libtask.hh>

namespace bind
{
  namespace tasks
  {

    //extern std::unique_ptr<ast::DecsList> the_program;

    TASK_GROUP("3. Bind");

    TASK_DECLARE("B|bindings-display", "display the addresses",
                 bindings_display, ""); // bound removed

    TASK_DECLARE("b|bindings-compute", "bind the identifiers",
                 bindings_compute, "parse");

    TASK_DECLARE("rename", "rename identifiers to unique names",
                 rename_compute, "bindings-compute");

     DISJUNCTIVE_TASK_DECLARE("bound", 
"default the computation of bindings to Tiger (without objects nor overloading)",
     "bindings_compute object_bindings_compute");
  }
}
