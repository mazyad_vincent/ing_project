from django.shortcuts import render, reverse, redirect
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from algoliasearch_django import raw_search
from guardian.decorators import permission_required
from intranet import chronos, pdf_utils, finance_utils
from django.utils.formats import get_format

import datetime
from django.conf import settings
import requests

from .models import Association, IsMemberOfAssociation, IsApplyingTo, ProxyUser, DemandeCreationAssociation, \
    DemandeDeBudget, ResponsableAsso, DeclarationDepense
from .forms import LoginForm, CreateUserForm, ApplyForm, DemandeCreateAssoForm, ChangeUserForm, ChangeAssoForm, \
    ReunionForm, enacFormSet, EnacBoardForm, DemandeBudgetForm, DeclareSpent, AcceptBudgetForm

from .EnacToPDF import exportENAC


# Create your views here.

@login_required
def home_view(request):
    return render(request, 'temp_accueil.html')


@login_required
def list_asso_view(request):
    assos = Association.objects.all()
    return render(request, 'temp_list_assos.html', locals())


@login_required
def asso_view(request, slug):
    asso = get_object_or_404(Association, slug=slug)
    form = ChangeAssoForm(request.POST or None)
    isBoard = request.user in asso.get_board()
    isMember = request.user in asso.members.all()
    is_enac_open = ResponsableAsso.is_enac_open()
    if request.method == "POST":
        form = ChangeAssoForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            website = form.cleaned_data['website']

            asso.email = email
            asso.website = website
            asso.save()
    else:
        form = ChangeAssoForm()
    return render(request, 'temp_asso.html', locals())


@login_required
def list_user_view(request):
    users = User.objects.all()
    return render(request, 'temp_list_users.html', locals())


@login_required
def user_view(request, login):
    disUser = get_object_or_404(User, username=login)
    form = ChangeUserForm(request.POST or None)

    if request.method == 'POST':
        form = ChangeUserForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            firstname = form.cleaned_data["firstname"]
            lastname = form.cleaned_data["lastname"]

            disUser.email = email
            disUser.first_name = firstname
            disUser.last_name = lastname
            disUser.save()
    else:
        form = ChangeUserForm()

    return render(request, 'temp_user.html', locals())


@login_required
def crea_asso_view(request):
    form = DemandeCreateAssoForm(request.POST or None)
    applys = DemandeCreationAssociation.objects.filter(createur=request.user)
    if request.method == 'POST':
        form = DemandeCreateAssoForm(request.POST, request.FILES)
        if form.is_valid():
            nom_asso = form.cleaned_data['name']
            file = form.cleaned_data['file']
            DemandeCreationAssociation.objects.create(nom_asso=nom_asso, createur=request.user, file=file)
            messages.info(request, "Votre demande de création d'association a bien été prise en compte")

            return redirect(reverse(home_view))
        else:
            for field in form:
                messages.error(request, field.errors)
    else:
        form = DemandeCreateAssoForm()

    return render(request, 'temp_crea_asso.html', locals())


@login_required
def member_view(request, slug):
    asso = get_object_or_404(Association, slug=slug)
    if request.user.is_staff or request.user in {asso.login_pres, asso.login_tres, asso.login_secr}:
        members = asso.members.all()
        return render(request, 'temp_list_members.html', locals())
    else:
        messages.error(request, "vous n'avez pas accès à cette page.")
    return redirect(home_view)


@login_required
def enacs_view(request, slug):
    if (not ResponsableAsso.is_enac_open()):
        messages.error(request, "Les points ENACS ne sont pas ouverts")
        return redirect(home_view)

    asso = get_object_or_404(Association, slug=slug)
    if request.user.is_staff or request.user in {asso.login_pres, asso.login_tres, asso.login_secr}:
        if request.method == 'POST':
            formset = enacFormSet(request.POST, request.FILES, prefix='member',
                                  queryset=asso.ismemberofassociation_set.all())
            form = EnacBoardForm(request.POST, instance=asso)
            if (form.is_valid() and formset.is_valid()):
                formset.save()
                form.save()
                messages.success(request, "Les changements ont bien été effectués.")
            else:
                messages.error(request, "Les notes totales des membres du bureau doivent être entre 0 et 100")
        else:
            formset = enacFormSet(prefix='member', queryset=asso.ismemberofassociation_set.all())
            form = EnacBoardForm(instance=asso)

        members = asso.members.all()
        return render(request, 'enacs.html', locals())
    else:
        messages.error(request, "vous n'avez pas accès à cette page.")
    return redirect(home_view)


@login_required
def export_enac_view(request, slug):
    if (not ResponsableAsso.is_enac_open()):
        messages.error("Les points ENACS ne sont pas ouverts")
        return redirect(home_view)
    asso = get_object_or_404(Association, slug=slug)
    success = exportENAC(asso)
    if (not success):
        messages.error(request, "Une erreur a occuré durant l'export des points ENACS.")
    else:
        messages.success(request, "L'export des point ENACS a bien été effectué.")
        # Handle errror
    return redirect(reverse(asso_view, kwargs={'slug': slug}), locals())


@login_required
def delete_member_view(request, slug, user_id):
    asso = get_object_or_404(Association, slug=slug)
    if request.user.is_staff or request.user in {asso.login_pres, asso.login_tres, asso.login_secr}:
        member = get_object_or_404(IsMemberOfAssociation, asso=asso.name, user=user_id)
        member.delete()
        return redirect(reverse(member_view, kwargs={'slug': slug}))
    else:
        messages.error(request, "vous n'avez pas accès à cette page.")
    return redirect(home_view)


@login_required
def apply_to_asso_view(request, slug):
    asso = get_object_or_404(Association, slug=slug)
    form = ApplyForm(request.POST or None)
    error = not asso.isapplyingto_set.filter(user__username=request.user.username, statut='w').count() == 0
    error_message = ""
    if error == True:
        messages.error(request, "Vous avez déjà une candidature en attente.")
    elif asso.members.filter(username=request.user.username).count() == 1 or request.user in asso.get_board():
        messages.error(request, "Vous faites déjà parti de cette association.")
    elif request.method == 'POST':
        form = ApplyForm(request.POST)
        form.fields['motivation_letter'].required = asso.motivation_letter
        if form.is_valid():
            motivation_letter = form.cleaned_data['motivation_letter']
            application = IsApplyingTo.objects.create(asso=asso, user=request.user, motivation_letter=motivation_letter)
            return redirect(reverse(asso_view, kwargs={'slug': asso.slug}))
        elif asso.motivation_letter:
            messages.error(request, "Cette association demande un texte de motivation.")

    else:
        form = ApplyForm()
    return render(request, 'apply_asso.html', locals())


@login_required
def list_apply_view(request, slug):
    asso = get_object_or_404(Association, slug=slug)
    applying_users = asso.isapplyingto_set.all()
    if request.user not in asso.get_board():
        messages.warning(request, "Vous n'avez pas les droits pour accéder à cet page.")
        return redirect(asso_view, slug)
    return render(request, 'temp_list_apply.html', locals())


@login_required
@permission_required('intranet.view_apply', (IsApplyingTo, 'id', 'apply_id'), return_403=True)
def apply_resolve(request, slug, apply_id, accepted):
    asso = get_object_or_404(Association, slug=slug)
    if request.user in {asso.login_pres, asso.login_tres, asso.login_secr} or request.user.is_staff:
        try:
            apply = IsApplyingTo.objects.get(pk=apply_id)
        except IsApplyingTo.DoesNotExist:
            apply = None
        if apply is not None:
            if apply.statut == 'w':
                if accepted == 1:
                    IsMemberOfAssociation.objects.create(asso=asso, user=apply.user)
                apply.delete()
            else:
                messages.error(request, "Cette candidature a déjà été résolue.")
        else:
            messages.error(request, "Cette candidature n'existe pas.")
        return redirect(reverse(list_apply_view, kwargs={'slug': slug}), locals())
    return redirect(home_view)


def create_user_view(request):
    form = CreateUserForm(request.POST or None)

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            login = form.cleaned_data['login']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            firstname = form.cleaned_data["firstname"]
            lastname = form.cleaned_data["lastname"]

            user = User.objects.create_user(username=login, email=email, first_name=firstname, last_name=lastname)
            user.set_password(password)
            user.save()
            return redirect(reverse(login_view))
    else:
        form = CreateUserForm()

    return render(request, 'account_create_test.html', locals())


def login_view(request):
    error = False

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return redirect(reverse(home_view))
            else:
                error = True
    else:
        form = LoginForm()

    return render(request, 'connexion.html', locals())


@login_required
def disconnect(request):
    logout(request)
    return redirect(reverse(login_view))


@login_required
def result_search_view(request):
    query = request.GET.get('search')
    print(query)
    result_assos = []
    result_user = []
    if query:
        params = {'hitsPerPage': 5}
        response = raw_search(Association, query, params)
        for elt in response['hits']:
            result_assos.append(Association.objects.get(slug=elt['slug']))

        response = raw_search(ProxyUser, query, params)
        for elt in response['hits']:
            result_user.append(User.objects.get(username=elt['username']))

    return render(request, 'temp_list_assos.html', locals())


@login_required
def error_view404(request):
    return render(request, 'error_page404.html', locals())


@login_required
def get_room_view(request, slug):
    asso = get_object_or_404(Association, slug=slug)

    if request.user not in asso.get_board() and not request.user.is_staff:
        messages.error(request, "Vous n'avez pas accés à cette page.")
        return redirect(reverse(asso_view, kwargs={'slug': slug}))

    form = ReunionForm(request.POST or None)

    if request.method == 'POST':
        form = ReunionForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data['date']
            res_id = form.cleaned_data['lieu']
            if chronos.get_availability(res_id, date):
                messages.info(request, "Votre demande a bien été prise en compte.")
            else:
                messages.error(request, "Ce créneau n'est pas disponible pour cette salle.")
            return redirect(reverse(get_room_view, kwargs={'slug': slug}))
        else:
            messages.error(request, "Les champs rentrés sont incorrects.")
    form = ReunionForm()
    r = chronos.connect()
    if not r:
        messages.error(request, 'Une erreur est survenu. Réessayer plus tard ou contacter l\'administrateur.')
    else:
        resources = chronos.get_resources()

    return render(request, 'reservation.html', locals())


@login_required
def respo_view(request):
    if (not request.user.is_staff):
        messages.error(request, "vous n'avez pas accès à cette page.")
        return redirect(reverse(home_view))
    applys = DemandeCreationAssociation.objects.all()
    applys_toList = len(DemandeCreationAssociation.objects.filter(statut__in=['c', 'r']))
    budgets_toList = len(DemandeDeBudget.objects.filter(statut='w'))
    budgets = DemandeDeBudget.objects.all()
    is_enac_open = ResponsableAsso.is_enac_open()

    return render(request, 'respo_asso.html', locals())


@login_required
def acceptBudget_view(request, budget_id, accepted):
    if not request.user.is_staff:
        messages.error(request, "vous n'avez pas accès à cette page.")
        return redirect(reverse(home_view))
    demand = get_object_or_404(DemandeDeBudget, id=budget_id)
    if not accepted:
        demand.statut = 'd'
        demand.save()
        return redirect(reverse(respo_view))
    else:
        demand.statut = 'a'
    form = AcceptBudgetForm(request.POST or None, instance=demand)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect(reverse(respo_view))
    else:
        form = AcceptBudgetForm(instance=demand)
    return render(request, 'acceptBudget.html', locals())


@login_required
def respo_change_status_asso(request, apply_id, accepted):
    if (not request.user.is_staff):
        messages.error(request, "vous n'avez pas accès à cette page.")
        return redirect(reverse(home_view))
    demand = get_object_or_404(DemandeCreationAssociation, id=apply_id)
    print(demand.statut)
    demand.changeStatus(accepted == 1)
    print(demand.statut)
    return redirect(reverse(respo_view))


@login_required
def get_asso_list(request):
    proxy = get_object_or_404(ProxyUser, username=request.user.username)
    if not proxy.is_board() and not request.user.is_staff:
        messages.error(request, "Vous n'avez pas accés cet page.")
        return redirect(reverse(home_view))

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="liste_asso.pdf"'

    assos = Association.objects.all()
    head = ["Nom", "Email"]
    fields = [[asso.name, asso.email] for asso in assos]
    data = [head] + fields

    pdf_utils.create_doc(response, data, "Liste des associations")

    return response


@login_required
def get_board_list(request, slug):
    proxy = get_object_or_404(ProxyUser, username=request.user.username)
    if not proxy.is_board() and not request.user.is_staff:
        messages.error(request, "Vous n'avez pas accés cet page.")
        return redirect(reverse(home_view))

    asso = get_object_or_404(Association, slug=slug)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="bureau_asso_' + slug + '.pdf"'

    pres = asso.login_pres
    secr = asso.login_secr
    tres = asso.login_tres

    data = [
        ['Rôle', 'Login', 'Nom', 'Prénom', 'Email'],
        ['Président', pres.username, pres.last_name, pres.first_name, pres.email],
        ['Secrétaire', secr.username, secr.last_name, secr.first_name, secr.email],
        ['Trésorier', tres.username, tres.last_name, tres.first_name, tres.email],
    ]

    v_pres = asso.login_vice_pres
    if v_pres:
        data.append(['Vice Président', v_pres.username, v_pres.last_name, v_pres.first_name, v_pres.email])

    v_secr = IsMemberOfAssociation.objects.get(asso=asso, poste='s')
    if v_secr:
        data.append(
            ['Vice Secrétaire', v_secr.user.username, v_secr.user.last_name, v_secr.user.first_name, v_secr.user.email])

    v_tres = IsMemberOfAssociation.objects.get(asso=asso, poste='t')
    if v_tres:
        data.append(
            ['Vice Trésorier', v_tres.user.username, v_tres.user.last_name, v_tres.user.first_name, v_tres.user.email])

    pdf_utils.create_doc(response, data, "Bureau de " + asso.name)

    return response


@login_required
def finances_view(request, slug):
    asso = get_object_or_404(Association, slug=slug)

    budgets = DemandeDeBudget.objects.filter(emis=asso, statut='a').order_by('date').values('amount', 'dateAccepted')
    depenses = DeclarationDepense.objects.filter(emis=asso).order_by('date').values('user__username',
                                                                                    'amount',
                                                                                    'date')
    finances = finance_utils.get_list(list(budgets), list(depenses))
    print(finances)

    print(get_format('DATE_FORMAT'))
    if request.user.is_staff or request.user in {asso.login_pres, asso.login_tres, asso.login_secr}:
        members = asso.members.all()

        mb = IsMemberOfAssociation.objects.filter(asso=asso)

        form_declare = DeclareSpent(request.POST or None, request.FILES, prefix='declare', initial={'emis': asso})
        form_declare.fields['user'].queryset = User.objects.filter(pk__in=mb.values_list('user_id'))
        form_demand = DemandeBudgetForm(request.POST or None, request.FILES, initial={'emis': asso}, prefix='demand')
        if request.method == 'POST':
            if 'declarespent' in request.POST:
                if (request.user != asso.login_tres and request.user != asso.login_pres \
                        and not request.user.is_staff):
                    messages.error(request, "Vous n'avez pas les droits pour effectuer cette action.")
                    return redirect(home_view)

                form_declare = DeclareSpent(request.POST, request.FILES, prefix='declare', initial={'emis': asso})
                # form_declare.fields['user'].queryset = User.objects.filter(pk__in=mb.values_list('user_id'))
                if form_declare.is_valid():
                    form_declare.save()
                    messages.success(request, "Les changements ont bien été effectués.")
                    return redirect(reverse(finances_view, kwargs={'slug': slug}))
                else:
                    for field in form_demand:
                        print(field.label, field.value, field.errors)
                    messages.error(request, "Les champs rentrés sont invalides.")
            elif 'demandbudget' in request.POST:
                if request.user != asso.login_tres and not request.user.is_staff:
                    messages.error(request, "Vous n'avez pas les droits pour effectuer cette action.")
                    return redirect(home_view)
                form_demand = DemandeBudgetForm(request.POST, request.FILES, initial={'emis': asso}, prefix='demand')
                if form_demand.is_valid():
                    form_demand.asso = asso
                    new_instance = form_demand.save(commit=False)
                    new_instance.save()
                    messages.info(request, "Votre demande a bien été prise en compte.")
                else:
                    for field in form_demand:
                        print(field.label, field.value, field.errors)
                    messages.error(request, "Les champs rentrés sont invalides.")
        else:
            form_demand = DemandeBudgetForm(initial={'emis': asso}, prefix='demand')
            form_declare = DeclareSpent(prefix='declare', initial={'emis': asso})
            form_declare.fields['user'].queryset = User.objects.filter(pk__in=mb.values_list('user_id'))

        return render(request, 'finance.html', locals())

    messages.error(request, "vous n'avez pas accès à cette page.")
    return redirect(home_view)


@login_required
def demande_open_enac(request):
    if not ResponsableAsso.is_respo(request.user) and not request.user.is_staff:
        messages.error(request, "Vous n'avez pas accés à cette page.")
        return redirect(reverse(home_view))

    error = ResponsableAsso.open_enac()
    if (error is not None):
        messages.error(request, error)
    else:
        messages.success(request, "L'ouverture de la saisie des points ENACS a bien été effectuée")
    return redirect(reverse(respo_view))


@login_required
def demande_close_enac(request):
    if not ResponsableAsso.is_respo(request.user) and not request.user.is_staff:
        messages.error(request, "Vous n'avez pas accés à cette page.")
        return redirect(reverse(home_view))
    error = ResponsableAsso.close_enac()
    if (error is not None):
        messages.error(request, error)
    else:
        messages.success(request, "La fermeture de la saisie des points ENACS a bien été effectuée")
    return redirect(reverse(respo_view))
