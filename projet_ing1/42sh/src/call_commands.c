/**
** \file call_commands.c
** \brief This file regroups the commands to get info about the ast
** \author Mazyad Vincent
**
** This file regroups the informations needed for exec an ast
*/

#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdarg.h>
#include <errno.h>
#include "ast.h"
#include "call_commands.h"
#include "builtins.h"

/**
** \fn size_t nb_ast_elements(struct ast *ast)
** \brief get the number of arg in the ast
**
** \param vast
** \return the numb of elements
*/

size_t nb_ast_elements(struct ast *ast) /* get the numb of args */
{
    if (!ast)
       return 0;
    if (!strcmp(ast->key, ";") || !strcmp(ast->key, "&") ||
        !strcmp(ast->key, "fi") || !strcmp(ast->key, "done"))
       return 0;
    return 1 + nb_ast_elements(ast->left) + nb_ast_elements(ast->right);
}

/**
** \fn void get_ast_args(struct ast *ast, char *tab[], size_t index)
** \brief fill a static tab with the elements in the ast
**
** \param the ast, a statuc tab and an index called first with 0
** \return the corresponding tab
*/

void get_ast_args(struct ast *ast, char *tab[], size_t *index)
{
    if (!ast)
      return;
    if (strcmp(ast->key, "&&"))
    {
      if (strcmp(ast->key, ";") && strcmp(ast->key, "fi") && strcmp(ast->key,
        "done") && strcmp(ast->key, "&"))
      {
        tab[*index] = ast->key;
        *index += 1;
      }
    }
    get_ast_args(ast->left, tab, index);
    get_ast_args(ast->right, tab, index);
}

/**
** \brief count the numb of method + args from a char*
**
** \param the string
** \return the number of elements
*/

size_t nb_elem(char *buffer) /* count the numb of method + args */
{
   size_t i = 0;
   size_t length = 0;
   for (; i < strlen(buffer); i++)
   {
      if (i < strlen(buffer) && buffer[i] != ' ')
      {
          while (i < strlen(buffer) && buffer[i] != ' ')
            i++;
          length ++;
      }
   }
   return length;
}

/**
** \fn  fill_tab(struct ast *ast, int condi)
** \brief get the tab filled from get_ast_args and execute it
**
** \param struct ast, a boolean int
** \return the value of execution (0 if success) 1 fail
*/

int fill_tab(struct ast *ast, int condi)
{
   if (is_builtins(ast))
      return call_builtins(ast);
   if (!condi)
   {
        int res = 0;
        if ((res = select_exec(ast)) == -1 && !is_builtins(ast))
        {
          size_t index = 0;
          size_t nb_args = nb_ast_elements(ast);
          char* tab[nb_args + 1];
          tab[nb_args] = NULL;
          get_ast_args(ast, tab, &index);
          return execute_cmd(tab);
       }
       return res;
   }
   else if (condi) /* exec the if then and else */
      return if_operation(ast);
   return 1;
}

/**
** \fn int execute_cmd(char *tab[])
** \brief execute the tab keys
**
** \param char *tab[]
** \return if exec was success (0) or not (1)
*/

int execute_cmd(char *tab[]) /* execute the cmd with the args */
{
   int status; /* table for exec */
   pid_t pid;
   pid = fork();
   if (pid == -1)
   {
      perror("fail fork");
      exit(EXIT_FAILURE);
   }
   if (pid == 0) /* child process */
   {
      execvp(tab[0], tab);
      perror("err");
      exit(EXIT_FAILURE);
   }
   else
   {
      waitpid(pid, &status, 0);
      return WEXITSTATUS(status);
   }
   return 0;
}
