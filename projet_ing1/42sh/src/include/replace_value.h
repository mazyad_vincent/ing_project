#ifndef REPLACE_VALUE_H
# define REPLACE_VALUE_H

/**
** \file replace_value.h
** \brief file to replace values in ast nodes
**
** \author Mazyad Vincent
*/

struct hashtable *hash;
struct ast *ast;
int is_variable(char *a); /* is it a variable called with $ */
void replace_node(struct hashtable *hash, struct ast *ast, int unalias);
/* replace the variable node in ast by its corresponding value */
void replace_all_nodes(struct hashtable *hash, struct ast *ast);
int is_alias(char *x, struct hashtable *hash); /* check if this is an alias */
int is_equal_operation(struct ast *ast);
int put_string_in_hash(struct hashtable *hash, char *string, int alias);

#endif
