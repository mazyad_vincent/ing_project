#include "builtin.h"
#include <string.h>

int is_builtin(const char *key) /* check if key is a builtin */
{
	if (!strcmp(key, "echo") || !strcmp(key, "cd") || !strcmp(key, "exit") || 
		 !strcmp(key, "kill"))
		return 1; /* is a builtin */
	return 0;
}

int call_builtin(struct ast *ast)
{
	if (!strcmp(ast->key, "echo"))
		return echo_command(ast->left);
	else if (!strcmp(ast->key, "cd"))
		return cd_builtin(ast->left);
	else if (!strcmp(ast->key, "exit"))
		return exit_command(ast->left);
  else if (!strcmp(ast->key, "kill"))
    return kill_builtin(ast);
	return 0;
}
