#ifndef QUEUE_H
# define QUEUE_H

struct queue
{
    enum move action;
    struct queue *next;
};

struct queue *add_queue(struct queue *head, struct queue *elt);
struct queue *pop_queue(struct queue **head);
struct queue *create_queue(enum move action);
void print_queue(struct queue *head);
void destroy_queue(struct queue *head);

#endif
