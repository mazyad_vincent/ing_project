#include "ppm.h"
#include "parsing.h"

struct pixel ambient_lighting(struct lights *lights, struct materials *mat)
{
    struct vector3 *ambient = lights->amb;
    /*if (mat->ka->x == 0 && mat->ka->y == 0 && mat->ka->z == 0)
        mat->ka->x = 1;*/
    struct pixel pixel = init_pixel(ambient->x * mat->ka->x,
                                    ambient->y * mat->ka->y,
                                    ambient->z * mat->ka->z);
    return pixel;
}

struct pixel directionnal_lighting(struct lights *lights, struct materials *mat,
                                   struct vector3 N, struct pixel pixel)
{
    struct list *dlight = lights->dlight_pos->head;
    struct list *dlight_color = lights->dlight_color->head;
    while (dlight && dlight_color)
    {
        struct vector3 *dcolor = dlight_color->data;
        struct vector3 *dvect = dlight->data;
        pixel = init_pixel(
                   pixel.r + dcolor->x * mat->kd->x * (-dvect->x) * N.x,
                   pixel.g + dcolor->y * mat->kd->y * (-dvect->y) * N.y,
                   pixel.b + dcolor->z * mat->kd->z * (-dvect->z) * N.z);
        dlight = dlight->next;
        dlight_color = dlight_color->next;
    }
    return pixel;
}

struct pixel point_lighting(struct lights *lights, struct materials *mat,
                            struct vector3 N, float distance, struct pixel pixel)
{
    struct list *plight = lights->plight_pos->head;
    struct list *plight_color = lights->plight_color->head;
    float attenuation = 1 / distance;
    while (plight && plight_color)
    {
        struct vector3 *pcolor = plight_color->data;
        struct vector3 *pvect = plight->data;
        pixel = init_pixel(
          pixel.r + pcolor->x * mat->kd->x * (attenuation * (-pvect->x)) * N.x,
          pixel.g + pcolor->y * mat->kd->y * (attenuation * (-pvect->y)) * N.y,
          pixel.b + pcolor->z * mat->kd->z * (attenuation * (-pvect->z)) * N.z);
        plight = plight->next;
        plight_color = plight_color->next;
    }
    return pixel;
}
