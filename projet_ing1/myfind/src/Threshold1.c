#include "Threshold.h"

char* print_dopt(char *path, struct queue *queue) //-d 
{ 
    DIR *dir = opendir(path); //open directory of path
    if (!dir)
    {
        fprintf(stderr, "%s %s\n", "myfind: cannot do open_directory:"
        , "the directory may not exist or is not valid");
        queue_destroy(queue);
        exit(1);
    }
    struct dirent *entry = readdir(dir);
    char *g = NULL;
    char *copy_path = new_concatenation("", path);
    queue = queue_push(queue, copy_path);
    if (path[my_strlen(path) - 1] != '/')
        path = concatenation(path, "/");
    for (; entry; entry = readdir(dir))
    {
       while (entry && (my_strmcpy(entry->d_name, ".")
             || my_strmcpy(entry->d_name, "..")))
       {
             entry = readdir(dir);
             continue;   
       }
       if (entry)
           g = new_concatenation(path, entry->d_name);
       if (entry && entry->d_type == DT_DIR)      //directory
       {
            char *print_dire = new_concatenation(path, entry->d_name);
            print_dire = print_dopt(print_dire, queue);
            if (queue->size != 0)
            {
                 struct queue_list *list = queue_pop(queue);
                 printf("%.*s\n", my_strlen(list->data), list->data);
                 free(list->data);
                 free(list);
            }
            free(print_dire);
       }
       else if (entry && (entry->d_type == DT_REG || is_symb(g)))
       {
           char *print_files = new_concatenation(path, entry->d_name);
           printf("%s\n", print_files);
           free(print_files);
       }
       if (entry)
           free(g);
    }
    closedir(dir);
    return path;
}

char* print_dopt_syml(char *path, struct queue *queue) //-d 
{ 
    DIR *dir = opendir(path); //open directory of path
    if (!dir)
    {
        fprintf(stderr, "%s %s\n", "myfind: cannot do open_directory:"
        , "the directory may not exist or is not valid");
        queue_destroy(queue);
        exit(1);
    }
    struct dirent *entry = readdir(dir);
    char *copy_path = new_concatenation("", path);
    queue = queue_push(queue, copy_path);
    if (path[my_strlen(path) - 1] != '/')
        path = concatenation(path, "/");
    char *g = NULL;
    for (; entry; entry = readdir(dir))
    {
       while (entry && (my_strmcpy(entry->d_name, ".")
              || my_strmcpy(entry->d_name, "..")))
       {
         entry = readdir(dir);
         continue;
       }
       if (entry)
          g = new_concatenation(path, entry->d_name);
       if (entry && (entry->d_type == DT_DIR || is_symb(g)))   //directory
       {
            char *print_dire = new_concatenation(path, entry->d_name);
            print_dire = print_dopt_syml(print_dire, queue);
            if (queue->size != 0)
            {
                 struct queue_list *list = queue_pop(queue);
                 printf("%.*s\n", my_strlen(list->data) + 1, list->data);
                 free(list->data);
                 free(list);
             }
            free(print_dire);
       }
       else if (entry && entry->d_type == DT_REG)
       {
           char *print_files = new_concatenation(path, entry->d_name);
           printf("%s\n", print_files);
           free(print_files);
       }
       if (entry)
         free(g);
    }
    closedir(dir);
    return path;
}

char* print_dir_syml(char *path)
{ 
    DIR *dir = opendir(path); //open directory of path
    if (!dir)
    {
        fprintf(stderr, "%s %s\n", "myfind: cannot do open_directory:"
        , "the directory may not exist or is not valid");
        exit(1);
    }
    struct dirent *entry = readdir(dir);
    char *print_dire = NULL;
    char *g = NULL;
    printf("%s\n",path);
    if (path[my_strlen(path) - 1] != '/')
        path = concatenation(path, "/");
    for (; entry; entry = readdir(dir))
    {
       while(entry && (my_strmcpy(entry->d_name, ".")
             || my_strmcpy(entry->d_name, "..")))
       {
          entry = readdir(dir);            
          continue;
       }
       if (entry)
           g = new_concatenation(path, entry->d_name);
       if (entry && ((entry->d_type == DT_DIR) || is_symb(g))) //directory
       { 
            print_dire = new_concatenation(path, entry->d_name);
            print_dire = print_dir_syml(print_dire);
            free(print_dire);
       }
       else if (entry && entry->d_type == DT_REG)
       {
           char *print_files = new_concatenation(path, entry->d_name);
           printf("%s\n", print_files);
           free(print_files);
       }
       if (entry)
         free(g);
    }
    closedir(dir);
    return path;
}
