#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#include "tool_parse.h"
#include "memory_list.h"

struct b_list *b_list_init(struct mem_list *memory)
{
  struct b_list *list = malloc(sizeof(struct b_list));
  if (!list)
    my_err("Couldn't malloc b_list");
  push_mem(memory, list);
  list->size = 0;
  list->head = NULL;
  list->tail = NULL;
  return list;
}

void b_list_push(struct b_list *list, struct b_list_node *node)
{
  if (!list)
    return;
  if (list->size == 0)
  {
    list->head = node;
    list->tail = node;
    node->next = NULL;
    node->prev = NULL;
  }
  else
  {
    struct b_list_node *tmp = list->tail;
    list->tail = node;
    tmp->next = node;
    node->next = NULL;
    node->prev = tmp;
  }
  list->size += 1;
}

struct b_dict_list *dict_list_init(struct mem_list *memory)
{
  struct b_dict_list *list = malloc(sizeof(struct b_dict_list));
  if (!list)
    my_err("Couldn't malloc b_dict_list");
  push_mem(memory, list);
  list->head = NULL;
  list->tail = NULL;
  list->size = 0;
  return list;
}

void dict_list_push(struct b_dict_list *list, struct b_dict_list_node *node)
{
  if (!list)
    return;
  if (list->size == 0)
  {
    list->head = node;
    list->tail = node;
    node->next = NULL;
    node->prev = NULL;
  }
  else
  {
    struct b_dict_list_node *tmp = list->tail;
    list->tail = node;
    tmp->next = node;
    node->next = NULL;
    node->prev = tmp;
  }
  list->size += 1;
}
