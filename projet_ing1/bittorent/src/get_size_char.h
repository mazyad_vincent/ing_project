#ifndef GET_SIZE_CHAR_H
# define GET_SIZE_CHAR_H

# include <stddef.h>

size_t get_size_list(char *b_encode, size_t size);
size_t get_size_dict(char *b_encode, size_t size);
size_t get_size_b_encode(char *b_encode);
FILE* get_tracker_into_file(FILE *file_track, char *b_encode);

#endif
