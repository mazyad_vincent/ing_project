#ifndef THRESHOLD_H
# define THRESHOLD_H
#define _DEFAULT_SOURCE

#include <stdio.h>
#include <sys/stat.h>
#include <err.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <fnmatch.h>
#include "queue.h"
#include "fifo.h"

void redirect_opt(char c, char *copy, struct queue *queue, struct stat st);
void redirect_flag(char *c, char *copy, struct stat st);
void no_arg(struct fifo *options, struct fifo *expressions, 
            struct stat st, int argc, char **argv);
int is_arg(int argc, char **argv);
int my_strlen(char *path);
int my_strmcpy(char *string1, char *string2);
char* create_arg(char *x); //if no arg given
char* print_dir(char *path);
char *new_concatenation(char *a, char *b);
char *copy_string(char *a, char *b);
char *concatenation(char *a, char *b);
char *print_dopt(char *path, struct queue *queue);
int is_symb(char *path); //is sym-link
void empty_size(struct fifo *options, struct fifo *expressions, 
                struct stat st, char *path); //Threshold0
void with_size(struct fifo *options, struct stat st, char *path);
char* print_dir_syml(char *path);
char* print_dopt_syml(char *path, struct queue *queue);
void print_D(char *copy, struct queue *queue, struct stat st); //d
void print_H(char *copy, struct queue *queue, struct stat st); //H
void print_L(char *copy, struct queue *queue, struct stat st); //L
void print_P(char *copy, struct queue *queue, struct stat st); //P
int is_expression(int argc, char **path, struct fifo *options, struct fifo *fi); // if there is an expression name...
int is_expression_arg(int i, char **path); //if the current char is an expr
void put_expression(struct fifo *fifo, struct fifo *opt, 
                    char **path, int argc); //put expr in fi
char* print_dir_expr(char *path, char *name);
int print_name_expr(struct fifo *fifo, char *expr);
char* print_dir_type(char *path, char *type);
void condition_type(char *type, char *path); //parsing type
void err_type(char *c);
int same_type(char *path, char *path2);
void err_args(char **argv, int i, struct fifo *options, struct fifo *expr);
int add_options(struct fifo *options, struct fifo *expressions, 
                char **argv, int i); //add options in fifo
int is_type (char *type, char *path); // type for type expressions
#endif
