#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <stdio.h>

#include "get_image.h"

struct image *get_image(char *path, SDL_Renderer *renderer, int x, int y, int flag_render, int flag_width_end, int flag_height_end)
{
  SDL_Surface *img = IMG_Load(path);
  if (!img)
  {
    printf("Erreur de chargement de l'image : %s", SDL_GetError());
    return NULL;
  }

  SDL_Texture *text = SDL_CreateTextureFromSurface(renderer, img);
  SDL_FreeSurface(img);

  //printf("xy = %d %d\n", x, y);
  SDL_Rect pos;
  pos.x = x;
  pos.y = y;
  SDL_QueryTexture(text, NULL, NULL, &pos.w, &pos.h);
  if (flag_height_end)
    pos.y -= pos.h + flag_height_end;
  if(flag_width_end)
    pos.x -= pos.w + flag_width_end;

  struct image *ret = malloc(sizeof(*ret));
  ret->pos = malloc(sizeof(SDL_Rect));
  ret->pos->x = pos.x;
  ret->pos->y = pos.y;
  ret->pos->h = pos.h;
  ret->pos->w = pos.w;
  ret->texture = text;
  //printf("new xy = %d %d\n", pos.x, pos.y);
  //printf("ret xy = %d %d\n", ret->pos->x, ret->pos->y);
  if (flag_render)
  {
    SDL_RenderCopy(renderer, text, NULL, &pos);
    SDL_RenderPresent(renderer);
  }
  return ret;
}