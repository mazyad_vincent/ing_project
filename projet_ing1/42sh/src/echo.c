#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ast.h"

static int is_echo_options(char *key)
{
  if (!strcmp(key, "-n") || !strcmp(key, "-e") || !strcmp(key, "-E"))
    return 1;
  return 0;
}

static void print_echo_ast(struct ast *ast, int boolean)
{
   while (ast && strcmp(ast->key, ";")) 
   {
     if (ast->key[0] == '\\')
        printf("%s ", ast->key+1);
     else
        printf("%s ", ast->key);
     ast = ast->left; 
   }
   if (boolean)
     printf("\n");
   else
     printf(" ");
}

static void echo_if_options(struct ast *ast)
{
   struct ast *val = ast->left;
   if (!strcmp(val->key, "-n"))  
      print_echo_ast(val->left, 0);
   else if (!strcmp(val->key, "-E"))
      print_echo_ast(val->left, 1);  
}

int echo_builtin(struct ast *ast)
{
   if (!ast->left)
   {
      printf(" \n");
      return 0;
   }
   if (is_echo_options(ast->left->key))
      echo_if_options(ast->left);
   else
   {
      if (!ast->left)
        printf(" \n");
      else
      {
        ast = ast->left;
        print_echo_ast(ast, 1);
      }
   }
   return 0;
}
