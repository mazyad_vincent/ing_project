#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parsing.h"

void fill_struct(struct my_read_svati *parse, char *path)
{
    FILE *file = fopen(path, "r");
    if (!file)
        return;
    ssize_t read;   
    size_t nombre;
    char *lineptr = NULL;
    char *compare_first = NULL;
    while ((read = getline(&lineptr, &nombre, file)) != -1)
    {
        compare_first = strtok(lineptr, " "); 
        if (!strcmp(compare_first, "camera")) //CAMERA
        {
           fill_camera(parse);
           continue;
        }
        else if (!strcmp(compare_first, "a_light") || !strcmp(compare_first,
                 "p_light") || !strcmp(compare_first, "d_light")) //LIGHTS
        {    
            fill_light(parse, lineptr);
            continue;
        }
        
        else if (!strcmp(compare_first, "object")) //OBJECT N
        {
            push_mat(parse->mat, atoi(strtok(NULL, " ")));
            continue;
        }
        else if (!strcmp(compare_first, "Ka") || !strcmp(compare_first, "Kd")
                 || !strcmp(compare_first, "Ks"))  //Materiaux KA KS KD
        {
            fill_materK(parse, lineptr);
            continue;
        }
        else if (!strcmp(compare_first, "Ns") || !strcmp(compare_first, "Nr")
                 || !strcmp(compare_first, "Ni") || !strcmp(compare_first, "d"))
            //Materiaux ns nr ni d
        {
            fill_materN(parse, lineptr);
            continue;
        }
        else if (!strcmp(compare_first, "v") || !strcmp(compare_first, "vn"))
        {
            fill_v(parse, lineptr);
            continue; 
        }
    }
}

void fill_v(struct my_read_svati *parse, char *line)
{ 
    struct materials *mat = get_lastmat(parse->mat);
    char *to_compare = line;
    if (!strcmp(to_compare, "v"))
    {
        struct vector3 *vect = vect_init();
        to_compare = strtok(NULL, " ");
        vect->x = atof(to_compare);
        to_compare = strtok(NULL, " ");
        vect->y = atof(to_compare);
        to_compare = strtok(NULL, " ");
        vect->z = atof(to_compare);
        push_list(mat->vertx->v, vect);
        return;
    }
    
    if (!strcmp(to_compare, "vn"))
    {
        struct vector3 *vect = vect_init();
        to_compare = strtok(NULL, " ");
        vect->x = atof(to_compare);
        to_compare = strtok(NULL, " ");
        vect->y = atof(to_compare);
        to_compare = strtok(NULL, " ");
        vect->z = atof(to_compare);
        push_list(mat->vertx->vn, vect);
        return;
    }
}
void fill_materN(struct my_read_svati *parse, char *line)
{
    struct materials *mat = get_lastmat(parse->mat);
    char *to_compare = line;
    if (!strcmp(to_compare, "Ns"))
    {
       to_compare = strtok(NULL, " ");
       mat->ns = atof(to_compare);
       return;
    }
    if (!strcmp(to_compare, "Ni"))
    {
       to_compare = strtok(NULL, " ");
       mat->ni = atof(to_compare);
       return;
    }
    
    if (!strcmp(to_compare, "Nr"))
    {
       to_compare = strtok(NULL, " ");
       mat->nr = atof(to_compare);
       return;
    }
    
    if (!strcmp(to_compare, "d"))
    {
       to_compare = strtok(NULL, " ");
       mat->d = atof(to_compare);
       return;
    }
}

void fill_materK(struct my_read_svati *parse, char *line)
{
    char *to_compare = line;
    struct materials *mat = get_lastmat(parse->mat);
    if (!strcmp(to_compare, "Ka"))
    {
       to_compare = strtok(NULL, " ");
       mat->ka->x = atof(to_compare);
       to_compare = strtok(NULL, " ");
       mat->ka->y = atof(to_compare);
       to_compare = strtok(NULL, " ");
       mat->ka->z = atof(to_compare);
       return;
    }
    if (!strcmp(to_compare, "Ks"))
    {
       to_compare = strtok(NULL, " ");
       mat->ks->x = atof(to_compare);
       to_compare = strtok(NULL, " ");
       mat->ks->y = atof(to_compare);
       to_compare = strtok(NULL, " ");
       mat->ks->z = atof(to_compare);
       return;
    }
    
    if (!strcmp(to_compare, "Kd"))
    {
       to_compare = strtok(NULL, " ");
       mat->kd->x = atof(to_compare);
       to_compare = strtok(NULL, " ");
       mat->kd->y = atof(to_compare);
       to_compare = strtok(NULL, " ");
       mat->kd->z = atof(to_compare);
       return;
    }
}

void fill_camera(struct my_read_svati *parse)
{
    char *to_compare;
    to_compare = strtok(NULL, " ");
    parse->camera->width = atoi(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->height = atoi(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->pos->x = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->pos->y = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->pos->z = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->ortogonal1->x = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->ortogonal1->y = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->ortogonal1->z = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->ortogonal2->x = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->ortogonal2->y = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->ortogonal2->z = atof(to_compare);
    to_compare = strtok(NULL, " ");
    parse->camera->field = atof(to_compare);
}

void fill_light(struct my_read_svati *parse, char *line)
{
    char *to_compare = line;
    if (!strcmp(to_compare, "a_light"))
      {
         to_compare = strtok(NULL, " ");
         parse->light->amb->x = atof(to_compare);
         to_compare = strtok(NULL, " ");
         parse->light->amb->y = atof(to_compare);
         to_compare = strtok(NULL, " ");
         parse->light->amb->z = atof(to_compare);
      }

    else if (!strcmp(to_compare, "p_light"))
      {
         struct vector3 *vect_pos = vect_init();
         to_compare = strtok(NULL, " ");
         vect_pos->x = atof(to_compare);
         to_compare = strtok(NULL, " ");
         vect_pos->y = atof(to_compare);
         to_compare = strtok(NULL, " ");
         vect_pos->z = atof(to_compare);
         push_list(parse->light->plight_pos, vect_pos);
         struct vector3 *vect_col = vect_init();
         to_compare = strtok(NULL, " ");
         vect_col->x = atof(to_compare);
         to_compare = strtok(NULL, " ");
         vect_col->y = atof(to_compare);
         to_compare = strtok(NULL, " ");
         vect_col->z = atof(to_compare);
         push_list(parse->light->plight_color, vect_col);
      }

     else if (!strcmp(to_compare, "d_light"))
      {
         struct vector3 *vect_pos = vect_init();
         to_compare = strtok(NULL, " ");
         vect_pos->x = atof(to_compare);
         to_compare = strtok(NULL, " ");
         vect_pos->y = atof(to_compare);
         to_compare = strtok(NULL, " ");
         vect_pos->z = atof(to_compare);
         push_list(parse->light->dlight_pos, vect_pos);
         struct vector3 *vect_col = vect_init();
         to_compare = strtok(NULL, " ");
         vect_col->x = atof(to_compare);
         to_compare = strtok(NULL, " ");
         vect_col->y = atof(to_compare);
         to_compare = strtok(NULL, " ");
         vect_col->z = atof(to_compare);
         push_list(parse->light->dlight_pos, vect_pos);
       }
}
/*
int main()
{
   struct my_read_svati *parse = init_struct();
   fill_struct(parse, "../tests/cube.svati");
   printf("%s %d \n", "width is :", parse->camera->width);
   printf("%s %d \n", "height is :", parse->camera->height);
   printf("%s %f %s %f %s %f \n", "pos x:", parse->camera->pos->x, "pos y:",
           parse->camera->pos->y, "pos z:", parse->camera->pos->z);
   printf("%s %f \n", "field is :", parse->camera->field);

   printf("%s %f %f %f\n", "amb is :", parse->light->amb->x, parse->light->amb->y, 
           parse->light->amb->z);
   printf("%s %f \n", "dlight is :", parse->light->dlight_pos->x);
   printf("%s %f \n", " plight is :", parse->light->plight_pos->x);

   struct sentinelle_mat *cpy = parse->mat;
   struct materials *head = cpy->head;
   while (head)
   {
      printf("%d\n", head->nb_elements);
      printf("%s %f %f %f\n", "ka is:", head->ka->x, head->ka->y, head->ka->z);
      printf("------------------------------ LIST:");
      print_list(head->vertx->v);
      head = head->next;
   }
}*/

