#include "metadata.h"

 __attribute__((__visibility__("hidden")))
void *cast_void(size_t off, void *ptr)
{
  char *to_move = ptr;
  return to_move + off;
}

 __attribute__((__visibility__("hidden")))
size_t word_align(size_t val)
{
  return (val + (sizeof(size_t) - 1)) & ~ (sizeof(size_t) - 1);
}

 __attribute__((__visibility__("hidden")))
struct block *init_block() /* init the cur block */ 
{
  if (!cur_block)
  {
    cur_block = mmap(NULL, sysconf(_SC_PAGESIZE), PROT_READ | PROT_WRITE,
                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    if (cur_block == MAP_FAILED)
       return NULL;
    cur_block->next = NULL;
    cur_block->prev = NULL;
    cur_block->size = sysconf(_SC_PAGESIZE); /*- sizeof(struct block)
                      - sizeof(struct metadata);*/
    cur_block->head = cast_void(sizeof(struct block), cur_block);    
    cur_block->head->free = 1; // set the head
    cur_block->head->size = cur_block->size -
                            sizeof(struct block) - sizeof(struct metadata);
    cur_block->head->next = NULL;
    cur_block->head->blk = cur_block;     
    return cur_block;
  }
  return NULL;
}

 __attribute__((__visibility__("hidden")))
struct block *size_block(size_t size)
{
  size_t file_size = sysconf(_SC_PAGESIZE);
  size = (size < 4096 - sizeof(struct block) - sizeof (struct metadata))?
  file_size : (size + sizeof(struct block) + sizeof(struct metadata) + file_size)
              & ~ file_size;
  struct block *block =  mmap(NULL, size, PROT_READ | PROT_WRITE,
                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
  if (block == MAP_FAILED)
     return NULL;
  struct block *last_block = get_last_block(cur_block);
  if (!last_block)
    return NULL;
  last_block->next = block;
  block->size = size; /* - sizeof(struct block) - sizeof(struct metadata);*/
  block->next = NULL;
  block->prev = last_block;
  block->head = cast_void(sizeof(struct block), block);
  block->head->size = size - sizeof(struct block) - sizeof(struct metadata);
  block->head->free = 1;
  block->head->next = NULL;
  block->head->blk = block;
  return block; 
}
