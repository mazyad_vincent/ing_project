/**
 ** \file bind/renamer.cc
 ** \brief Implementation of bind::Renamer.
 */
#include <tuple>
#include <bind/renamer.hh>
#include <misc/symbol.hh>

namespace bind
{

  using namespace ast;

  Renamer::Renamer()
    : super_type(), new_names_()
  {}

  // FIXME

  void Renamer::operator()(ast::VarDecs& e)
  {
    for (auto i = e.decs_get().begin(); i != e.decs_get().end(); ++i)
    {
      ast::VarDec* elm = *i;
      visit(*elm, elm);
      super_type::operator()(elm);
    }
  }

  void Renamer::operator()(ast::TypeDecs& e)
  {
    for (auto i = e.decs_get().begin(); i != e.decs_get().end(); ++i)
    {
      ast::TypeDec* elm = *i;
      visit(*elm, elm);
      super_type::operator()(elm);
    }
  }

  void Renamer::operator()(ast::FunctionDecs& e)
  {
    for (auto i = e.decs_get().begin(); i != e.decs_get().end(); ++i)
    {
        ast::FunctionDec* elm = *i;
        if (elm->body_get() == nullptr)
          return;
        if (elm->name_get() == "_main")
          super_type::operator()(elm);
        else
        {
          visit(*elm, elm);
          super_type::operator()(elm);
        }
    }
  }

  void Renamer::operator ()(ast::CallExp& e)
  {
    if (e.def_get()->body_get() != nullptr)
      visit(e, e.def_get());
    super_type::operator()(e);
  }

  void Renamer::operator ()(ast::NameTy& e)
  {
    // e def ?
      visit(e, e.def_get());
      super_type::operator()(e);
}

  void Renamer::operator ()(ast::SimpleVar& e)
  {
    visit(e, e.def_get());
    super_type::operator()(e);
  }

} // namespace bind
