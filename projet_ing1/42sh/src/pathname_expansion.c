#define _GNU_SOURCE
#include <dirent.h>
#include <fnmatch.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int expansion(char *str, char *path, int nb_sub, char **res)
{
    int i = 0;
    int nb_slash = 0;
    while (str[i] != '\0' && nb_slash <= nb_sub)
    {
        if (str[i] == '/')
            nb_slash += 1;
        i++;
    }
    char *dest = malloc(i);
    dest = strncpy(dest, str, i);
    dest[i] = '\0';
    int match = !fnmatch(dest, path, FNM_PATHNAME);
    if (match && !fnmatch(str, path, FNM_PATHNAME))
    {
        char *temp;
        asprintf(&temp, "%s ", path);
        *res = temp;
    }
    free(dest);
    return match;
}

char *pathname_expansion(char *str, char *path, int nb_sub)
{
    char *res = "";
    DIR *root = opendir(path);
    struct dirent *curr_dir = readdir(root);
    while (curr_dir)
    {
        char *pathname;
        if (strcmp(path, ".") && strcmp(path, ".."))
            asprintf(&pathname, "%s%s", path, curr_dir->d_name);
        else
            asprintf(&pathname, "%s/", curr_dir->d_name);
        if (expansion(str, pathname, nb_sub, &res))
        {
            if (curr_dir->d_type == DT_DIR && strcmp(curr_dir->d_name, ".")
                && strcmp(curr_dir->d_name, ".."))
            {
                char *new_res = pathname_expansion(str, pathname, nb_sub + 1);
                char *temp;
                asprintf(&temp, "%s%s", res, new_res);
                if (strcmp(res, ""))
                    free(res);
                if (strcmp(new_res, ""))
                    free(new_res);
                res = temp;
            }
        }
        free(pathname);
        curr_dir = readdir(root);
    }
    closedir(root);
    return res;
}

int main(void)
{
    char *res = pathname_expansion("*/42sh", ".", 0);
    printf("%s\n", res);
    free(res);
    return 0;
}
