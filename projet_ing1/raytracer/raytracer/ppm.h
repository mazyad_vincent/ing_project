#ifndef PPM_H
# define PPM_H

struct pixel
{
    float r;
    float g;
    float b;
};

struct ppm
{
    int color_store;
    int width;
    int height;
    struct pixel **image;
};

void create_ppmfile(struct ppm *ppm, char* filename);
struct pixel init_pixel(float r, float g, float b);
struct pixel **init_image(int height, int width);
void free_image(struct pixel **image, int height);

#endif
