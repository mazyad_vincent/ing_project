/**
 ** \file ast/break-exp.hh
 ** \brief Declaration of ast::BreakExp.
 */

#pragma once

#include <ast/exp.hh>
#include <iostream>

namespace ast
{

  /// BreakExp.
  class BreakExp : public Exp
  {
  // FIXME: Some code was deleted here.
   public:
    BreakExp(const Location& location);
    BreakExp(const BreakExp&) = delete;
    BreakExp& operator=(const BreakExp&) = delete;
    virtual ~BreakExp() = default;
    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    void def_set(Exp* exp);
    Exp* def_get();
    const Exp* def_get() const;

    protected:
    Exp* exp_ = nullptr; /* for the loop */
  };

} // namespace ast

#include <ast/break-exp.hxx>

