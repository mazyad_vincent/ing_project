/**
 ** \file escapes/escapes-visitor.cc
 ** \brief Implementation for escapes/escapes-visitor.hh.
 */

#include <ast/all.hh>
#include <escapes/escapes-visitor.hh>
#include <misc/contract.hh>

namespace escapes
{
  // FIXME

  void EscapesVisitor::operator()(ast::FunctionDec& e)
  {
    depth_ += 1;
    super_type::operator()(e);
    depth_ -= 1;
  }

  void EscapesVisitor::operator()(ast::SimpleVar& e)
  {
    int is_found = 0;
    auto i = scope.find(e.def_get());
    e.def_get()->is_escap_set(true);
  }

  void EscapesVisitor::operator()(ast::VarDec& e)
  {
    scope.insert({&e, depth_});
  }

} // namespace escapes
