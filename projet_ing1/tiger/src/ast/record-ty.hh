/**
 ** \file ast/record-ty.hh
 ** \brief Declaration of ast::RecordTy.
 */

#pragma once

#include <ast/field.hh>
#include <ast/ty.hh>
#include <ast/fwd.hh>

namespace ast
{

  /// RecordTy.
  class RecordTy : public Ty
  {
  // FIXME: Some code was deleted here.
     public:

      using fields_type = std::vector<Field*>;
      RecordTy(const Location& location, fields_type* vect);
      RecordTy(const RecordTy&) = delete;
      RecordTy& operator=(const RecordTy&) = delete;
      virtual ~RecordTy();
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      const fields_type* get_vect() const;
      fields_type* get_vect();
      void push_front(Field* field);

     protected:
      fields_type* vect_;
  };

} // namespace ast

#include <ast/record-ty.hxx>

