#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "get_size_char.h"

size_t get_size_string(char *b_encode, size_t size)
{
  size_t len = *(b_encode + size) - '0';
  size += 1;
  while (*(b_encode + size) != ':')
  {
    len = len * 10;
    len = len + (*(b_encode + size) - '0');
    size += 1;
  }
  size += len;
  size += 1;
  return size;
}

size_t get_size_int(char *b_encode, size_t size)
{
  while (*(b_encode + size) != 'e')
    size += 1;
  size += 1;
  return size;
}

size_t get_size_list(char *b_encode, size_t size)
{
  while (*(b_encode + size) != 'e')
  {
    if ('0' <= *(b_encode + size) && *(b_encode + size) <= '9')
      size = get_size_string(b_encode, size);
    else if (*(b_encode + size) == 'i')
      size = get_size_int(b_encode, size);
    else if (*(b_encode + size) == 'l')
      size = get_size_list(b_encode, size);
    else if (*(b_encode + size) == 'd')
      size = get_size_dict(b_encode, size);
  }
  size += 1;
  return size;
}

size_t get_size_dict(char *b_encode, size_t size)
{
  while (*(b_encode + size) != 'e')
  {
    if ('0' <= *(b_encode + size ) && *(b_encode + size) <= '9')
      size = get_size_string(b_encode, size);
    else if (*(b_encode + size) == 'i')
      size = get_size_int(b_encode, size);
    else if (*(b_encode + size) == 'l')
      size = get_size_list(b_encode, size);
    else if (*(b_encode + size) == 'd')
      size = get_size_dict(b_encode, size);
  }
  size += 1;
  return size;
}

size_t get_size_b_encode(char *b_encode)
{
  return get_size_dict(b_encode, 1);
}

FILE* get_tracker_into_file(FILE* tracker, char *b_encode)
{
  size_t size = get_size_b_encode(b_encode);
  fwrite(b_encode, 1, size, tracker);
  fseek(tracker, 0, SEEK_SET); 
  return tracker;
}
