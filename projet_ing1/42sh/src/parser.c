/**
** \file parser.c
** \brief This file regroups all the functions of the parser
** \author Odile Guillaume
**
** This file regroups all the functions of the parser of 42sh. It contains the
** functions corresponding to the LL grammar of 42sh
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

/**
** \fn struct ast *free_node(struct ast *node)
** \brief free ast and return NULL
**
** \param node: node to be freed
** \return NULL
*/

struct ast *free_node(struct ast *node)
{
  if (node)
    ast_free(node);
  return NULL;
}

/**
** \fn struct ast *p_and_or(void)
** \brief reprensents the and or rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_and_or(void)
{
  struct ast *node = p_pipeline();
  if (!node)
    return NULL;
  struct ast *curr = curr_tok();
  if (!curr)
    return node;
  while (curr && (curr->type == AND_IF || curr->type == OR_IF))
  {
    eat();
    node = ast_create(node, curr, NULL, NULL);
    curr = curr_tok();
    while (curr && curr->type == NEWL)
    {
      eat();
      ast_free(curr);
      curr = curr_tok();
    }
    struct ast *temp = p_pipeline();
    if (!temp)
    {
      ast_free(curr);
      return free_node(node);
    }
    ast_free(curr);
    curr = curr_tok();
    node = ast_create(NULL, node, temp, NULL);
  }
  ast_free(curr);
  return node;
}

/**
** \fn struct ast *p_list(void)
** \brief reprensents the list rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_list(void)
{
  struct ast *node = p_and_or();
  if (!node)
    return NULL;
  struct ast* curr = curr_tok();
  if (!curr)
    return node;
  while (curr && (curr->type == SEMI || curr->type == AND))
  {
    eat();
    node = ast_create(node, curr, NULL, NULL);
    struct ast *node2 = p_and_or();
    if (!node2)
      break;
    node = ast_create(NULL, node, node2, NULL);
    curr = curr_tok();
  }
  return node;
}

/**
** \fn struct ast *p_input(void)
** \brief reprensents the input rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_input(void)
{
  struct ast *node = p_list();
  if (!node)
    return NULL;
  struct ast *curr = curr_tok();
  if (!curr)
    return node;
  if (curr->type == NEWL || curr->type == END_FILE)
  {
    ast_free(curr);
    return node;
  }
  ast_free(node);
  ast_free(curr);
  return NULL;
}
