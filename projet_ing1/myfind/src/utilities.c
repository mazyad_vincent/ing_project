#include "Threshold.h"

int my_strlen(char *path)
{
  int i = 0;
  while(path[i] != '\0')
    i++;
  return i;
}

int my_strmcpy(char *string1, char *string2)
{
    char *x = string1;
    char *y = string2;
    if (my_strlen(x) != my_strlen(y))
        return 0;    
    while (*x && *y)
    {
        if (*x != *y)
           return 0;
        x++;
        y++;
    }
    return 1;
}

char* create_arg(char *x)
{
    x[0] = '.';
    x[1] = '\0';
    return x;
}

int add_options(struct fifo *options, struct fifo *expressions,
                char **argv, int i)
{
    int is_options = 0;
    if (argv[i][0] == '-')
     {
         is_options = 1;
         err_args(argv, i, options, expressions);
         char *s = malloc(sizeof(char) + 1);
         s[0] = argv[i][1];
         s[1] = '\0';
         fifo_push(options, s);
     }
    return is_options;
}
