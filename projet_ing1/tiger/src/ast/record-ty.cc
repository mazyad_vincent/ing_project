/**
 ** \file ast/record-ty.cc
 ** \brief Implementation of ast::RecordTy.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/record-ty.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  RecordTy::RecordTy(const Location& location, RecordTy::fields_type* vect)
    : Ty(location)
    , vect_(vect)
  {}

  RecordTy::~RecordTy()
  {
    for (auto it = vect_->begin(); it != vect_->end(); ++it)
      delete *it;
    delete vect_;
  }

  void
  RecordTy::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  RecordTy::accept(Visitor& v)
  {
    v(*this);
  }


} // namespace ast

