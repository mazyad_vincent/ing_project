/**
 ** \file ast/type-constructor.hxx
 ** \brief Inline methods of ast::TypeConstructor.
 */

#pragma once

#include <ast/type-constructor.hh>
#include <type/types.hh>

namespace ast
{
  inline void TypeConstructor::created_type_set(type::Type* tp)
  {
    tp_ = tp;
  }

  inline const type::Type* TypeConstructor::created_type_get() const
  {
    return tp_;
  }

} // namespace ast

