from algoliasearch_django import AlgoliaIndex


class AssociationIndex(AlgoliaIndex):
    fields = ('name', 'slug')
    settings = {'searchableAttributes': ['name']}
    index_name = 'asso_index'


class UserIndex(AlgoliaIndex):
    should_index = 'is_board'
    fields = ('username', 'first_name', 'last_name')
    settings = {'searchableAttributes': ['username', 'first_name', 'last_name']}
    index_name = 'user_index'
