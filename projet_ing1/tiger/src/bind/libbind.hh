/**
 ** \file bind/libbind.hh
 ** \brief Interface of the bind module.
 */

  // FIXME

#include<ast/fwd.hh>
#include <misc/error.hh>
#include <bind/binder.hh>
#include <common.hh>

namespace bind
{
  namespace tasks
  {
    misc::error bind(ast::Ast& tree);
  }
}
