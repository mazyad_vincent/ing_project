/**
 ** \file ast/assign-exp.hh
 ** \brief Declaration of ast::AssignExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/var.hh>

namespace ast
{

  /// AssignExp.
  class AssignExp : public Exp
  {
    public:

    AssignExp(const Location& location, Var* lval, Exp* exp);
    AssignExp(const AssignExp&) = delete;
    AssignExp& operator=(const AssignExp&) = delete;
    virtual ~AssignExp();
    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;
    const Exp& exp_get() const;
    Exp& exp_get();
    const Var& lval_get() const;
    Var& lval_get();


    protected:
      Exp* exp_;
      Var* lval_;
  /* FIXME: Some code was deleted here. */
  };

} // namespace ast

#include <ast/assign-exp.hxx>

