/**
 ** \file ast/seq-exp.cc
 ** \brief Implementation of ast::SeqExp.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/seq-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  SeqExp::SeqExp(const Location& location, typename SeqExp::exps_type* vect)
    : Exp(location)
    , vect_(vect)
  {}

  SeqExp::~SeqExp()
  {
    for (auto it = vect_->begin(); it != vect_->end(); ++it)
      delete *it;
    delete vect_; 
  }

  void
  SeqExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  SeqExp::accept(Visitor& v)
  {
    v(*this);
  }
} // namespace ast

