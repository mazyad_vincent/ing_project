#include <stdio.h>
#include <stdlib.h>
#include "parsing.h"

struct sentinelle_mat* sentimat_init()
{
    struct sentinelle_mat *senti = malloc(sizeof(struct sentinelle_mat));
    if (!senti)
        return NULL;
    senti->head = NULL;
    return senti;
}

struct materials* material_init()
{
    struct materials *material = malloc(sizeof(struct materials));
    if (!material)
        return NULL;
    struct vector3 *vect = vect_init();
    vect->x = 0;
    vect->y = 0;
    vect->z = 0;
    material->ka = vect;
    struct vector3 *vect1 = vect_init();
    vect1->x = 0;
    vect1->y = 0;
    vect1->z = 0;
    material->kd = vect1;
    struct vector3 *vect2 = vect_init();
    vect2->x = 0;
    vect2->y = 0;
    vect2->z = 0;
    material->ks = vect2;
    material->ns = 0;
    material->ni = 1.0;
    material->nr = 0.0;
    material->d = 1.0;
    material->vertx = vertice_init();
    material->next = NULL;
    return material;
}

void push_mat(struct sentinelle_mat *senti, int n)
{
    struct materials *cpy = senti->head;
    struct materials *push = material_init();
    push->nb_elements= n;
    if (!cpy)
    {
       senti->head = push; 
       return;
    }
    while (cpy && cpy->next)
        cpy = cpy->next;
    cpy->next = push;
}

void free_all_mat(struct sentinelle_mat *senti)
{
    struct materials *cpy = senti->head;
    while (cpy)
    {
        free(cpy->ka);
        free(cpy->ks);
        free(cpy->kd);
        struct materials *to_pop = cpy;
        free_all(cpy->vertx->v);
        free_all(cpy->vertx->vn);
        cpy = cpy->next;
        free(to_pop);
    }
   free(senti);
}

struct materials* get_lastmat(struct sentinelle_mat *senti)
{
    if (!senti || !senti->head)
        return NULL;
    struct materials *cpy = senti->head;
    while (cpy && cpy->next)
        cpy = cpy->next;
    return cpy;
}

void print_mat(struct my_read_svati *svat)
{
    for (struct materials *mat = svat->mat->head; mat; mat = mat->next)
    {
        printf("VECTORS V:\n");
        print_list(mat->vertx->v);
        printf("VECTORS VN:\n");
        print_list(mat->vertx->vn);
    }
}
