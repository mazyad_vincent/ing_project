#include "metadata.h"

static pthread_mutex_t thread_m = PTHREAD_MUTEX_INITIALIZER;

void *realloc(void *ptr, size_t size)
{
  if (!size)
  {
    free(ptr);
    return NULL;
  }
  void *new_mall = malloc(size);
  if (!ptr)
    return new_mall;
  pthread_mutex_lock(&thread_m);
  struct metadata *meta = cast_void(-sizeof(struct metadata), ptr);
  if (meta->size >= size)
  {
    pthread_mutex_unlock(&thread_m);
    return ptr;
  }
  memmove(new_mall, ptr, meta->size);
  pthread_mutex_unlock(&thread_m);
  return new_mall;
}
