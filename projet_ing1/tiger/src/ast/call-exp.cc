/**
 ** \file ast/call-exp.cc
 ** \brief Implementation of ast::CallExp.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/call-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  CallExp::CallExp(const Location& location, const misc::symbol& func,
           typename CallExp::exps_type* vect)
    : Exp(location)
    , vect_(vect)
    , func_(func)
  {}

  CallExp::~CallExp()
  {
    for (auto i = vect_->begin(); i != vect_->end(); ++i)
      delete *i;
    delete vect_;
  }

  void
  CallExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  CallExp::accept(Visitor& v)
  {
    v(*this);
  }
} // namespace ast

