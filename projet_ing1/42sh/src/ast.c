/**
** \file ast.c
** \brief ast.c: file that contains all functions about ast
** \author Odile Guillaume
**
** File that regroups all functions about the ast. It contains function that
** init, free and create the dot file of the ast.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ast.h"

/**
** \fn ast_init(void)
** \brief init the ast (all mallocs)
**
** \return the ast malloc; NULL if malloc failed
*/

struct ast *ast_init(void)
{
  struct ast *ast = malloc(sizeof(struct ast));
  if (!ast)
    return NULL;
  ast->key = NULL;
  ast->type = 0;
  ast->left = NULL;
  ast->right = NULL;
  ast->third = NULL;
  return ast;
}

/**
** \fn struct ast *ast_create(struct ast *node1, struct ast *node2, struct ast
** *node3, struct ast *node4)
**
** \param node1: represent the node that will be the next left child
** \param node2: represent the node that will be the father of both parameters
** \param node3: represent the node that will be the next right child
** \param node4: represent the node that will be the third child
** \return the new node corresponding to the parameters
*/

struct ast *ast_create(struct ast *node1, struct ast *node2, struct ast *node3,
                       struct ast *node4)
{
  struct ast *temp_left = node2->left;
  struct ast *temp_right = node2->right;
  struct ast *temp_third = node2->third;
  if (node1)
  {
    while (temp_left && temp_left->left)
      temp_left = temp_left->left;
    if (temp_left)
      temp_left->left = node1;
    else
      node2->left = node1;
  }
  if (node3)
  {
    while (temp_right && temp_right->left)
      temp_right = temp_right->left;
    if (temp_right)
      temp_right->left = node3;
    else
      node2->right = node3;
  }
  if (node4)
  {
    while (temp_third && temp_third->left)
      temp_third = temp_third->left;
    if (temp_third)
      temp_third->left = node4;
    else
      node2->third = node4;
  }
  return node2;
}

/**
** \fn static char *concat_dot(char *s1, char *s2)
** \brief concat s1 and s2 to match the dot format (s1 -> s2;)
**
** \param s1: first string to be concated
** \param s2: second string to be concated
*/

static char *concat_dot(char *s1, char *s2)
{
  int l1 = strlen(s1);
  int l2 = strlen(s2);
  char *s = malloc((l1 + l2 + 11) * sizeof(char));
  int i = 0;
  s[i++] = '"';
  for (int j = 0; j < l1; i++, j++)
    s[i] = s1[j];
  s[i++] = '"';
  s[i++] = ' ';
  s[i++] = '-';
  s[i++] = '>';
  s[i++] = ' ';
  s[i++] = '"';
  for (int j = 0; j < l2; i++, j++)
    s[i] = s2[j];
  s[i++] = '"';
  s[i++] = ';';
  s[i++] = '\n';
  s[i] = '\0';
  return s;
}

/**
** \fn static void node_dot(struct ast *node, FILE *dot)
** \brief function that will write in the .dot file with the current ast
**
** \param node: represents the ast to be written in the dot file
** \param dot: represent the struct FILE of the dot file
** \return void
*/

static void node_dot(struct ast *node, FILE *dot)
{
  if (!node)
    return;
  if (node->left)
  {
    char *s = concat_dot(node->key, node->left->key);
    fwrite(s, sizeof(char), strlen(s), dot);
    free(s);
  }
  if (node->right)
  {
    char *s = concat_dot(node->key, node->right->key);
    fwrite(s, sizeof(char), strlen(s), dot);
    free(s);
  }
  if (node->third)
  {
    char *s = concat_dot(node->key, node->third->key);
    fwrite(s, sizeof(char), strlen(s), dot);
    free(s);
  }

  node_dot(node->left, dot);
  node_dot(node->right, dot);
  node_dot(node->third, dot);
}

/**
** \fn void to_dot(struct ast *node)
** \brief the function that regroup the calls all function that create dot file
**
** \param node: represents the ast to be written in the dot file
** \return void
*/

void to_dot(struct ast *node)
{
  if (!node)
    return;
  FILE *dot = fopen("ast.dot", "w");
  fwrite("digraph G {\n", sizeof(char), 12, dot);
  if (!node->left && !node->right && !node->third)
  {
    fwrite(node->key, sizeof(char), strlen(node->key), dot);
    fwrite("\n", sizeof(char), 1, dot);
  }
  else
    node_dot(node, dot);
  fwrite("}", sizeof(char), 1, dot);
  fclose(dot);
}

/*
** \fn void ast_free(struct ast *ast)
** \brief free the ast
**
** \param ast: represents the ast the be freed
** \return void
*/

void ast_free(struct ast *ast)
{
  if (!ast)
    return;
  free(ast->key);
  ast_free(ast->left);
  ast_free(ast->right);
  ast_free(ast->third);
  free(ast);
}
