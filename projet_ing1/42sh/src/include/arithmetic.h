/**
** \file arithmetic.h
** \brief This file creates an ast for operations
** \author Mazyad Vincent
**
*/

#ifndef ARITHMETIC_H
# define ARITHMETIC_H

struct liste *list;

enum val
{
  PAR = 0, /* '(' */
  VAL, /* value */
  ADD, /* + */
  MULT, /* * */
  MIN, /* - */
  DIV, /* / */
  PIPE, /* | */
  SEP,  /* & */
};

struct tree
{
  struct tree *right;
  struct tree *left;
  enum val val;
  int entier;
};

struct ast *ast;

struct tree* tree_init(int enumeration);
struct tree* create_tree(struct liste *lis); /* create the ast */
int result(struct tree *tree); /* create the tree */
int do_all_operation(struct ast *ast); /* uploading ast->key with val */
void free_all_tree(struct tree *tree);
char *replace_node_operation(char *key, size_t size); /* replace variable in node */

#endif
