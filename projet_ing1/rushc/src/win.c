#include <stdio.h>
#include <stdlib.h>
#include "generation_map.h"

int update_pos_player(struct map *map, struct player *player, int x, int y)
{
  x=x;
  int res = 0;
  map->pos[map->pos_player.y][map->pos_player.x] = NOT;
  map->pos_player.x += 1;
  map->pos_player.y += y;
  losing_hp(map, player);
  res = is_win(map, player);
  if (res == 0 || res == 1)
    return res;
  map->pos[map->pos_player.y][map->pos_player.x] = PLAYER;
  return 2;
}

int is_win(struct map *map, struct player *player)
{
  if (player->pos.y < 0 || player->pos.y >= map->height * 30)
    return 0;
  if (player->pos.x < 0 || player->pos.x >= map->width * 30)
    return 0;
  if (player->life <= 0)
    return 0;
  int indicex = map->pos_player.x;
  int indicey = map->pos_player.y;
  if (map->pos[indicey][indicex] == DECOR ||
      map->pos[indicey][indicex] == IA ||
      map->pos[indicey][indicex] == GROUND ||
      map->pos[indicey][indicex] == TOP)
    return 0;
  if (map->pos[indicey][indicex] == END)
    return 1;
  return 2;
}
