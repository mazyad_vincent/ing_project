#include "ast.h"
#include "parse.h"
#include "execute.h"
#include "operators.h"
#include "env.h"

extern struct global g_global;

static size_t get_nb_elm(struct ast *ast) /* return the nb of elements to execute */
{
	size_t number = 0;
	while(ast && strcmp(ast->key, ";"))
	{
		number++;
		ast = ast->left;
	}
	return number;
}

void fill_args(struct ast *ast, char **tab, size_t *index)
{
	if (!ast)
		return;
	while (ast)
	{
		if (!strcmp(ast->key, ";") || !strcmp(ast->key, "&"))
			return;
		tab[*index] = ast->key;
		*index += 1;
		ast = ast->left;
	}
}

int launch_exec(struct ast *ast)
{
	size_t nb_args = get_nb_elm(ast);
	char **table = malloc((nb_args + 1) * sizeof(char*));
	table[nb_args] = NULL;
	size_t index = 0;
	fill_args(ast, table, &index);
	int res = execute(table);
	free(table);
	return res;
}

struct ast *move_ast(struct ast *ast) /* move until the next separator */
{
	 while (ast && strcmp(ast->key, ";"))
	   ast = ast->left;
	 return (!ast)? NULL : ast->left;
}

int exec_rec(struct ast *ast)
{
	int res = 0;
	int res_rec = 0;
	if (!ast)
		return -2;
  if (is_valid_alias(ast))
  {
    res = environ_command(ast);
    res_rec = exec_rec(ast->left);
    return (res_rec == -2)? res : res_rec;
  }
	else if (is_builtin(ast->key)) /* call builtin */
	{
			res = call_builtin(ast);
			g_global.res = res;
			res_rec = exec_rec(move_ast(ast));
			return (res_rec == -2)? res : res_rec;
	}
	else if (is_operator(ast->key)) /* call operator */
	{
		g_global.res = call_operator(ast);
		return g_global.res;
	}

  else if (!strcmp(ast->key, ";")) /* ; operator */
  {
    g_global.res = exec_rec(ast->left);
    if (ast->right)
      g_global.res = exec_rec(ast->right);
    return g_global.res;
  }
	else /* call exec */
	{
		res = launch_exec(ast);
		g_global.res = res;
		res_rec = exec_rec(move_ast(ast));
		return (res_rec == -2)? res : res_rec;
	}
	return res;
}

int execute(char *tab[])
{
	int status;
  pid_t pid;
  pid = fork();
  if (pid == -1)
  {
     perror("fail fork");
	   exit(EXIT_FAILURE);
	}
	if (pid == 0) /* child process */
	{
     execvp(tab[0], tab);
     perror("err");
     exit(EXIT_FAILURE);
  }
  else
  {
     waitpid(pid, &status, 0);
     return (WEXITSTATUS(status) == 1 && strcmp(tab[0], "false"))? 
            127 : WEXITSTATUS(status);
	}
  return 0;
}
