#include <stdio.h>
#include <stdlib.h>
#include "parsing.h"

struct vector3* vect_init()
{
    struct vector3 *vec =  malloc(sizeof(struct vector3));
    if (!vec)
        return NULL;
    return vec;
}
struct camera* camera_init()
{
    struct camera *camera = malloc(sizeof(struct camera));
    if (!camera)
        return NULL;
    camera->pos = vect_init();
    camera->ortogonal1 = vect_init();
    camera->ortogonal2 = vect_init();
    camera->width = 0;
    camera->height = 0;
    return camera;
}

struct lights* light_init()
{
    struct lights* light = malloc(sizeof(struct lights));
    if (!light)
        return NULL;
    light->amb = vect_init();
    light->dlight_pos = senti_init();
    light->dlight_color = senti_init();
    light->plight_pos = senti_init();
    light->plight_color = senti_init();
    return light;
}

struct vertices* vertice_init()
{
    struct vertices *vert = malloc(sizeof(struct vertices));
    if (!vert)
        return NULL;
    vert->vn = senti_init();
    vert->v = senti_init();
    return vert;
}

void free_camera(struct camera *camera)
{
    free(camera->pos);
    free(camera->ortogonal1);
    free(camera->ortogonal2);
    free(camera);
}

void free_light(struct lights *light)
{
    free(light->amb);
    free_all(light->dlight_pos);
    free_all(light->dlight_color);
    free_all(light->plight_pos);
    free_all(light->plight_color);
    free(light);
}
void free_main(struct my_read_svati *myread)
{
   free_camera(myread->camera);
   free_light(myread->light);
   free_all_mat(myread->mat);
   free(myread);
}

struct my_read_svati* init_struct()
{
    struct my_read_svati *sva = malloc(sizeof(struct my_read_svati));
    if (!sva)
        return NULL;
    sva->camera = camera_init();
    sva->light = light_init();
    sva->mat = sentimat_init();
    return sva;
}
