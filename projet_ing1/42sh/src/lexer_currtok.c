/**
** \file lexer_currtok.c
** \brief This file regroups functions that get the current tok in command
** \author Odile Guillaume
**
** This file regroups functions that get the current token is the string
** comands and returns it.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"
#include "call_commands.h"

extern struct global g_global;

static char *quoted(char *command, int i)
{
  i++;
  int temp = i;
  int len = 0;
  for (; command[i] != '\0' && command[i]!= '"'; i++)
    len++;
  char *res = malloc((len + 1) * sizeof(char));
  if (!res)
    return NULL;
  strncpy(res, command + temp, len);
  res[len] = '\0';
  return res; 
}

static char *dollar(char *command, int i)
{
  int temp = i;
  int nb_par = 0;
  int len = 0;
  for (; command[i] != '\0'; i++)
  {
    if ((command[i] == '\n' || command[i] == ' ' || command[i] == ';') &&
         !nb_par)
      break;
    if (command[i] == '(')
      nb_par++;
    if (command[i] == ')')
      nb_par--;
    len++;
  }
  char *res = malloc((len + 1) * sizeof(char));
  if (!res)
    return NULL;
  strncpy(res, command + temp, len);
  res[len] = '\0';
  return res; 
}

static char *normal(char *command, int i)
{
  int len = 0;
  for (; command[i] != '\0' && command[i] != ' ' && command[i] != '\n' &&
         command[i] != '&' && command[i] != '|' && command[i] != '!' &&
         command[i] != '(' && command[i] != ')' && command[i] != '{' &&
         command[i] != '}' && command[i] != ';' && command[i] != '<' &&
         command[i] != '>'; i++)
    len += 1;

  char *res = malloc((len + 1) * sizeof(char));
  if (!res)
    return NULL;
  strncpy(res, command + (i - len), len);
  res[len] = '\0';
  return res;
}

/**
** \fn char *next_tok(char *command, int i)
** \brief get the next token of the command in function of the index
**
** \param command: represents the command written by the user in 42sh
** \param i: represents the index in the command
** \return the next token in command
*/

char *next_tok(char *command, int i)
{
  if (!command)
    return NULL;
  while (command[i] != '\0' && (command[i] == ' ' || command[i] == '\n'))
    i++;
  comments(command, &i);
  if (command[i] == '\0')
    return NULL;
  int specials = is_special(command, i);
  if (specials)
  {
    char *res = malloc((specials + 1) * sizeof(char));
    if (!res)
      return NULL;
    strncpy(res, command + i, specials);
    res[specials] = '\0';
    return res;
  }
  if (command[i] == '"')
    return quoted(command, i);
  if (command[i] == '$')
    return dollar(command, i);
  return normal(command, i);
}

