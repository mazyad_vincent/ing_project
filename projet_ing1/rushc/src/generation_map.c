#include <stdio.h>
#include <stdlib.h>
#include "generation_map.h"
#include "movement/texture.h"
#include "movement/player_control.h"

int random_numb(int height)
{
  return rand() % height;
}

int random_two_numb(int max, int min)
{
  return (rand()%(max-min)) + min;
}

FILE* create_file(struct map *map)
{
   FILE *file_map = fopen("map", "w+");
   if (!file_map)
      return NULL;
   char *data = malloc(1);
   if (!data)
      return NULL;
   for (int i = 0; i < map->height; i++)
   {
      size_t size;
      size = size;
      for (int j = 0; j < map->width; j++)
      {
         if (map->pos[i][j] == PLAYER)
            data[0] = 'P';
         else if (map->pos[i][j] == NOT)
            data[0] = '.';
         else if (map->pos[i][j] == IA)
            data[0] = 'I';
         else if (map->pos[i][j] == DECOR)
            data[0] = 'D';
         else if (map->pos[i][j] == FIRE)
            data[0] = 'F';
         else if (map->pos[i][j] == END)
            data[0] = 'E';
         size = fwrite(data, 1, 1, file_map);
      }
      size = fwrite("\n", 1, 1, file_map);
   }
   free(data);
   return file_map;
}

struct player* init_start(struct map *map)
{
  struct player *player = player_init(map->height);
  map->pos[player->pos.y][player->pos.x] = PLAYER;
  map->pos_player.x = player->pos.x;
  map->pos_player.y = player->pos.y;
  return player;
}

void generate_IA(struct map *map, SDL_Renderer *rend)
{
   int x = map->pos_player.x;  //actual pos x of the player
   int y = map->pos_player.y;  //actual pos y of the player
   int rand = random_two_numb(map->height - 1, 1);
   if (x + 20 >= map->width)
      return;
   while(map->pos[rand][x + 20] != NOT)
      rand = random_numb(y);
   map->pos[rand][x + 20] = IA;
   struct IA *ia = ia_init(x + 20, rand, rend);
   ia->pos_screen.y = rand * 30;
   ia->pos_screen.x = (x + 20) * 30;
   ia->boolean = 1;
   push_ia(map, ia);
}

void add_decoy(struct map *map)
{
  int x = map->pos_player.x; /* actual pos x of the player */
  int randx = random_two_numb(20, 8);
  int randy = random_two_numb(map->height - 1 , 1);
  if (x + 20 >= map->width)
    return;
  while(map->pos[randy][randx] != NOT)
  {
     randx = random_two_numb(20, 8);
     randy = random_two_numb(map->height, 0);
  }
  map->pos[randy][randx + x] = DECOR;
}
