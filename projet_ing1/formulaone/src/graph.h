#ifndef GRAPH_H
# define GRAPH_H

#include "control.h"

struct graph 
{
    int vertex; // number of vertices
    struct edges *head; //pointer to the first node
}; 

struct edges 
{
    struct vector2 *checkpoint; //node vector
    struct edges *next;
    struct adj_list *adj; //pointer to the first edge
};

struct adj_list
{
    struct vector2 *checkpoint; //vect of the node the edge is going to.
    struct adj_list *next; //Pointer to the next node's edge.
};

struct graph* create_graph(); //create the graph
void create_adj(struct graph *graph, struct vector2 *first_value
                           , struct vector2 *second_value); 
    //add en adj_list where second value is the val to be added

void add_edge(struct graph *graph, struct vector2 *first_pos); 
        //add an adge in the graph

void destroy_all(struct graph *graph); //free the graph
void clear_all(struct graph *graph);
int find_edge(struct graph *graph, struct vector2 *pos); //find edge in gra

struct edges* get_finish(struct graph *graph, struct vector2 *vect);
struct edges* get_link(struct graph *graph, struct edges *edge);
void print_adj(struct graph *graph);
int find_adj(struct graph *graph, struct vector2 *pos, struct vector2 *vect);
void graph_popadj(struct graph *graph, struct edges* edg, struct vector2 *vect);
#endif
