#ifndef FIFO_h
# define FIFO_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <err.h>

struct list
{
   struct list *next;
   struct list *prev;
   char *data;
};

struct fifo
{
   int size;
   struct list *head;
};

struct fifo* fifo_init();
void fifo_push(struct fifo *fifo, char *elm);
struct list* fifo_pop(struct fifo *fifo);
int fifo_find(struct fifo *fifo, char *opt);
struct list* fifo_get (struct fifo *fifo);
struct list* fifo_getindex(struct fifo *fifo, int index);
void clean_all(struct fifo *fifo);

#endif
