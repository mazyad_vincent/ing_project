/**
 ** \file ast/record-ty.hxx
 ** \brief Inline methods of ast::RecordTy.
 */

#pragma once

#include <ast/record-ty.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  inline const typename RecordTy::fields_type* RecordTy::get_vect() const
  {
     return vect_;
  }


  inline typename RecordTy::fields_type* RecordTy::get_vect()
  {
     return vect_;
  }

  inline void RecordTy::push_front(Field* field)
  {
     vect_->insert(vect_->begin(), field);
  }

} // namespace ast

