#include <stdio.h>
#include <stdlib.h>

#include "control.h"
#include "queue.h"

struct queue *create_queue(enum move action)
{
    struct queue *head = malloc(sizeof(struct queue));
    head->action = action;
    head->next = NULL;
    return head;
}

struct queue *pop_queue(struct queue **head)
{
    struct queue *temp = *head;
    *head = (*head)->next;
    return temp;
}

struct queue *add_queue(struct queue *head, struct queue *elt)
{
    struct queue *temp = head;
    while (temp->next != NULL)
        temp = temp->next;
    temp->next = elt;
    return head;
}

void destroy_queue(struct queue *head)
{
    while (head != NULL)
    {
        struct queue *temp = pop_queue(&head);
        free(temp);
    }
}

void print_queue(struct queue *head)
{
    struct queue *temp = head;
    while (temp != NULL)
    {
        if (temp->action == ACCELERATE)
            printf("ACCELERATE\n");
        if (temp->action == BRAKE)
            printf("BRAKE\n");
        if (temp->action == TURN_LEFT)
            printf("TURN_LEFT\n");
        if (temp->action == TURN_RIGHT)
            printf("TURN_RIGHT\n");
        if (temp->action == DO_NOTHING)
            printf("DO_NOTHING\n");
        temp = temp->next;
    }
}

int main(void)
{
    struct queue *head = create_queue(ACCELERATE);
    head = add_queue(head, create_queue(ACCELERATE));
    head = add_queue(head, create_queue(BRAKE));
    printf("avant\n");
    print_queue(head);
    printf("%s", pop_queue(&head)->action == ACCELERATE ?  "accelerate\n" : " ");
    pop_queue(&head);
    printf("après\n");
    print_queue(head);
    destroy_queue(head);
    return 0;
}
