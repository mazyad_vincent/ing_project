#include "iso9660.h"
#include "my_read_iso.h"


void print_tree(char *ptr, struct iso_dir *cur, int depth, char begin)
{
    char delim;
    for (int i = 0; i < 2; i++)
        cur =  add_offset(cur, sizeof (struct iso_dir) + cur->idf_len
                + (sizeof (struct iso_dir) + cur->idf_len) % 2);
    while (cur && cur->idf_len > 0)
    {
        char *name = cast_char(cur) + sizeof(struct iso_dir);
        struct iso_dir *next =  add_offset(cur, sizeof (struct iso_dir)
                + cur->idf_len + (sizeof (struct iso_dir) + cur->idf_len) % 2);
        delim = '|';
        if (cur->type == 2 || cur->type == 3)
        {
            printf("%c", begin);
            if (depth > 0)
            {
                for (int i = 0; i < depth; i++)
                {
                    for (int i = 0; i < 3; i++)
                        printf(" ");
                    printf("|");
                }
            }
            printf("-- %.*s/\n", cur->idf_len, name);
            char next = delim == '+' && depth == 0 ? ' ' : '|';
            print_tree(ptr, cast_void(ptr + cur->data_blk.le * ISO_BLOCK_SIZE),
                    depth + 1, next);
        }
        else
        {
            printf("%c", begin);
            if (depth > 0)
            {
                for (int i = 0; i < depth; i++)
                {
                    for (int i = 0; i < 3; i++)
                        printf(" ");
                    printf("|");
                }
            }
            printf("-- %.*s\n", cur->idf_len - 2, name);
        }
        cur = next; 
    }
}

void shell_tree(struct data *data, char *name)
{
    struct iso_dir *cur = data->cur;
    if (name)
        cur = find_dir(data, name);
    if (!cur)
        return;
    printf(".\n");
    print_tree(data->ptr, cur, 0, '|');
    int dir = 0;
    int file = 0;
    count_dir(data->ptr, &dir, &file, cur);
    printf("\n%d directories, %d files\n", dir, file);
}

int shell_get(struct data *data, const char *name)
{
    if (!name)
    {
        fprintf(stderr, "cat: command must take an argument\n");
        return 0;
    }
    struct iso_dir *cur = find_file(data, name);
    if (!cur)
        return 0;
    char filename[32] =
    {
        0
    };
    char *cur_name = cast_char(cur) + sizeof(struct iso_dir);
    for (int i = 0; i < cur->idf_len - 2; i++)
        filename[i] = cur_name[i];
    char *data_file = data->ptr + cur->data_blk.le * ISO_BLOCK_SIZE;
    FILE *f = fopen(filename, "w");
    if (!f)
        return 0;
    fwrite(data_file, sizeof (char), cur->file_size.le, f);
    fclose(f);
    return 1;
}


int shell_cat(struct data *data, const char *name)
{
    if (!name)
    {
        fprintf(stderr, "cat: command must take an argument\n");
        return 0;
    }
    struct iso_dir *cur = find_file(data, name);
    if (!cur)
        return 0;
    char *data_file = data->ptr + cur->data_blk.le * ISO_BLOCK_SIZE;
    fwrite(data_file, sizeof (char), cur->file_size.le, stdout);
    return 1;
}

void shell_pwd(char *ptr, struct iso_path_table_le *path_table,
        struct iso_path_table_le *root)
{
    if (path_table == root)
    {
        printf("/");
        return;
    }
    shell_pwd(ptr, get_n_path(ptr, path_table->parent_dir), root);
    printf("%.*s/", path_table->idf_len, cast_char(path_table)
            + sizeof(struct iso_path_table_le));
}
