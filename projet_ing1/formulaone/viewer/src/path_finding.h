#ifndef PATH_FINDING_H
# define PATH_FINDING_H

#include "graph.h"
#include "control.h"
#include "list.h"
#include "../tests/test.h"

void path_finding(struct map *map, struct vector2 *pos, struct graph *graph);
int get_path(struct map *map, struct graph *graph, struct vector2 *start, 
             struct sentinelle *senti);
struct vector2* find_pos(struct map *map) ;
#endif
