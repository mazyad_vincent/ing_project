#!/bin/python
import sys, subprocess, os
from pathlib import Path

#INIT
#SCRIPT OPTIONS
cmd  = './42sh '
verbose = 0
RED    = "\033[1;31m"
BLUE   = "\033[1;34m"
L_BLUE = "\033[0;34m"
CYAN   = "\033[1;36m"
GREEN  = "\033[1;32m"
RESET  = "\033[0;0m"
BOLD   = "\033[;1m"

#DEF FUNCTIONS
def print_color(string, color):
  sys.stdout.write(color)
  print(string)
  sys.stdout.write(RESET)

def parse_options():
  option = 0
  argc = len(sys.argv)
  category = "" 
  for i in range(0, argc):
    if sys.argv[i] == "--verbose" or sys.argv[i] == "-v":
      option |= 1
    if sys.argv[i] == "--category" or sys.argv[i] == "-c":
      option |= 2
      category = sys.argv[i + 1]
      i += 1
    if sys.argv[i] == "--list" or sys.argv[i] == "-l":
      option |= 4
    if sys.argv[i] == "--sanity" or sys.argv[i] == "-s":
       option |= 8
  return option, category

def print_result(my_res, sh_res):
  if my_res.returncode == sh_res.returncode:
    print_color("[ OK ]", GREEN)
    return 1
  else:
    print_color("[ KO ]", RED)
    return 0

def print_percent(percent):
  if percent == 100:
    print_color(str(int(percent)) + "%\n", GREEN)
  else:
    print_color(str(int(percent)) + "%\n", RED)

def verbose(my_res, sh_res, script_opt):
  if script_opt & 1:
    print_color("Ours: " + str(my_res.returncode), L_BLUE)
    if my_res.stdout:
      print (my_res.stdout.strip().decode("utf-8"))
    if my_res.stderr:
      print (my_res.stderr.strip().decode("utf-8"))
    print_color("Bash: " + str(sh_res.returncode), L_BLUE)
    if my_res.stdout:
      print(sh_res.stdout.strip().decode("utf-8"))
    if my_res.stderr:
      print(sh_res.stderr.strip().decode("utf-8"))

def sanity():
  print()

def test(options, script_opt, expected):
  newcmd = cmd + options
  print(newcmd)
  my_res = subprocess.run(newcmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  if script_opt & 8:
    sanity = subprocess.run("valgrind --error-exitcode =1 -q " + newcmd, stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, shell=True)
    print(sanity.returncode)
  sh_res = my_res
  if not expected:
    sh_res = subprocess.run("bash " + "--posix " + options, stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE, shell=True)
  verbose(my_res, sh_res, script_opt)
  return print_result(my_res, sh_res)

def parse_file(path, script_opt, s_category):
    f = open(path + "/commands", "r")
    c = f.read()
    ligns = c.split('\n')
    category = ligns[0]
    if s_category.upper() == category or s_category == "":
      print_color(category, CYAN)
      nb_test = int(ligns[1])
      nb_succeeded = 0
      if not script_opt & 4:
        for i in range(1, nb_test + 1):
          f_test = open(path + str(i), "r+")
          c_test = f_test.read()
          l_test = c_test.split('\n')
          nb_succeeded += test(l_test[0], script_opt, l_test[2])
          f_test.close()
        print_percent(nb_succeeded / nb_test * 100)
    f.close()

#START
r_path = Path(__file__).resolve().parent
os.chdir(r_path)
script_opt, category = parse_options()
print_color("*****42SH TESTSUITE *****\n", BLUE)

parse_file("simple/", script_opt, category)
parse_file("opt_handler/", script_opt, category)
parse_file("parser/", script_opt, category)
