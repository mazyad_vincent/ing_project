virtualenv venv

source venv/bin/activate
pip install -r requirements.txt

echo "------------------------------"
echo "VIRTUALENV CORRECTLY INSTALLED"
echo
echo "'deactivate' to exit venv"
