#ifndef PARSING_H
# define PARSING_H

/*#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "vectors.h"*/

struct vector3 /* VECTOR3 (x, y, z) */
{
    float x;
    float y;
    float z;
};

struct sentinelle
{
    struct list *head; //sentinelle for the linked list
};

struct sentinelle_mat
{
    struct materials *head;
};

struct list 
{
    struct list *next;
    struct vector3 *data;
};

struct my_read_svati   /* STRUCT FOR PARSING */
{
    struct camera *camera; //stuct for camera
    struct lights *light; //struct for lights
    struct sentinelle_mat *mat; //struct for materials
};

struct camera          /* STRUCT FOR CAMERA */
{
   unsigned width;
   unsigned height;
   struct vector3 *pos; //pos of camera
   struct vector3 *ortogonal1; // vector orto
   struct vector3 *ortogonal2; // vector orto
   float field; //field of view in degrees
};

struct lights          /* STRUCT FOR LIGHTS*/
{
   struct vector3 *amb; /* STRUCT FOR AMBIANT COLORS*/
   struct sentinelle *dlight_pos; /*STRUCT FOR DLIGHT POS*/
   struct sentinelle *dlight_color; /*STRUCT FOR DLIGHT COLOR*/
   struct sentinelle *plight_pos; /*STRUCT FOR PLIGHT POS*/
   struct sentinelle *plight_color; /*STRUCT FOR PLIGHT COLOR*/
};

struct materials
{
    struct vector3 *ka;
    struct vector3 *ks;
    struct vector3 *kd;
    float ns;
    float nr;
    float ni;
    float d;
    int nb_elements; // nombre elements
    struct vertices *vertx;
    struct materials *next;
};

struct vertices
{
    struct sentinelle *v; /* List of ALL VERTICES */
    struct sentinelle *vn; /*List of ALL NORMALS */
};

//////////////// INITIALISATION OF STRUCTS
struct vector3* vect_init();
struct sentinelle_mat* sentimat_init(); //senti for mat
struct materials *material_init(); //create a mat
struct camera* camera_init();
struct lights* light_init();
struct my_read_svati* init_struct();
struct sentinelle* senti_init();
struct list* list_init();
struct vertices* vertice_init();
/////////////////////////////////////////


////////////////FUNCTIONS FOR LIST
void push_list(struct sentinelle *senti, struct vector3 *vect);
void free_all(struct sentinelle *senti);
void print_list(struct sentinelle *senti);
//////////////////////////////////

/////////////FUNCTIONS FOR MATERIALS
void push_mat(struct sentinelle_mat *senti, int n);
void free_all_mat(struct sentinelle_mat *senti); //free the mat
struct materials* get_lastmat(struct sentinelle_mat *senti);
void print_mat(struct my_read_svati *mat);
/////////////////////////////////

///////////////FUNCTION PARSING
void fill_struct(struct my_read_svati *parse, char *path);
void fill_v(struct my_read_svati *parse, char *line);
void fill_materN(struct my_read_svati *parse, char *line);
void fill_materK(struct my_read_svati *parse, char *line);
void fill_camera(struct my_read_svati *parse);
void fill_light(struct my_read_svati *parse, char *line);
/////////////////////////////////

void free_camera(struct camera *camera);
void free_light(struct lights *light);
void free_main(struct my_read_svati *myread);
#endif
