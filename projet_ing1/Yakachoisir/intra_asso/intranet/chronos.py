import requests
import json
from django.conf import settings


class Chronos:
    chronos_url = "https://chronos.assistants.epita.fr/Web/Services/"
    sessionToken = ""
    userId = ""
    expires = ""


def connect():

    headers = {
        'X-CRI-TOKEN': settings.CHRONOS_TOKEN
    }

    url = Chronos.chronos_url + "/Authentication/Authenticate"

    data = {
        'username': settings.CHRONOS_USERNAME,
        'password': settings.CHRONOS_PASSWORD
    }

    response = requests.post(url, headers=headers, json=data)

    if response.ok:
        data = json.loads(response.text)
        if not data['isAuthenticated']:
            return False
        Chronos.sessionToken = data['sessionToken']
        Chronos.userId = data['userId']
        Chronos.expires = data['sessionExpires']
        return True

    return False


def disconnect():
    headers = {
        'X-CRI-TOKEN': settings.CHRONOS_TOKEN,
        'X-Booked-SessionToken': Chronos.sessionToken,
        'X-Booked-UserId': Chronos.userId
    }

    url = Chronos.chronos_url + "/Authentication/SignOut"

    data = {
        'sessionToken': Chronos.sessionToken,
        'userId': Chronos.userId
    }

    r = requests.post(url, headers=headers, json=data)


def get_resources():
    headers = {
        'X-CRI-TOKEN': settings.CHRONOS_TOKEN,
        'X-Booked-SessionToken': Chronos.sessionToken,
        'X-Booked-UserId': Chronos.userId
    }

    params = {
        'availableAt': '2018-07-07T14:06:20+0200',
        'availableUntil': '2018-07-07T14:30:20+0200'
    }
    url = Chronos.chronos_url + 'Resources/'

    response = requests.get(url, headers=headers, params=params)

    if response.status_code == 200:
        data = json.loads(response.text)
        return [(res['resourceId'], res['name']) for res in data['resources']]

    return []


def get_availability(resource_id, date):
    if Chronos.sessionToken == '' or Chronos.userId == '':
        if not connect():
            return False

    headers = {
        'X-CRI-TOKEN': settings.CHRONOS_TOKEN,
        'X-Booked-SessionToken': Chronos.sessionToken,
        'X-Booked-UserId': Chronos.userId
    }

    params = {
        'dateTime': date.strftime('%Y-%m-%dT%H:%M:%S+0200')
    }

    url = Chronos.chronos_url + 'Resources/' + resource_id + '/Availability'

    response = requests.get(url, headers=headers, params=params)
    if response.status_code == 200:
        data = json.loads(response.text)
        return data['resources'][0][0]['available']

    return False

