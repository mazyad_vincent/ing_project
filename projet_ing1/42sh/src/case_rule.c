/**
** \file case_rule.c
** \brief This file regroups case rules of the parser
** \author Odile Guillaume
**
** This file regroups case rules of the parser. It's corresponding to LL
** grammar of 42sh
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

static struct ast *p_case_item2(struct ast *node)
{
  struct ast *curr = curr_tok();
  while (curr && curr->type == OR)
  {
    eat();
    node = ast_create(curr, node, NULL, NULL);
    curr = curr_tok();
    if (!curr || curr->type != WORD)
    {
      ast_free(curr);
      return free_node(node);
    }
    eat();
    node = ast_create(curr, node, NULL, NULL);
    curr = curr_tok();
  }
  while (curr && curr->type == NEWL)
  {
    eat();
    ast_free(curr);
    curr = curr_tok();
  }
  ast_free(curr);
  struct ast *node2 = p_compound_list();
  if (node2)
    node = ast_create(node2, node, NULL, NULL);
  return node;
}


/**
** \fn struct ast *p_case_item(void)
** \brief reprensents the case_item rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_case_item(void)
{
  struct ast *curr = curr_tok();
  if (!curr)
    return NULL;
  struct ast *curr2 = NULL;
  if (curr->type == LPAR)
  {
    eat();
    curr2 = curr;
    curr = curr_tok();
    if (!curr)
      return NULL;
  }
  if (curr->type != WORD)
    return NULL;
  eat();
  struct ast *node = NULL;
  if (curr2)
    node = ast_create(curr, curr2, NULL, NULL);
  else
    node = ast_create(NULL, curr, NULL, NULL);
  node = p_case_item2(node);
  return node;
}

static struct ast *p_case_clause2(struct ast *node, int *test)
{
  struct ast *curr = curr_tok();
  int brk = 0;
  while (curr && curr->type == DSEMI)
  {
    if (brk)
      return free_node(node);
    eat();
    node = ast_create(curr, node, NULL, NULL);
    curr = curr_tok();
    while (curr && curr->type == NEWL)
    {
      eat();
      ast_free(curr);
      curr = curr_tok();
    }
    struct ast *node2 = p_case_item();
    if (!node2)
      brk = 1;
    if (!node2 && *test >= 1)
      return free_node(node);
    else if (node2)
      node = ast_create(node2, node, NULL, NULL);
    *test += 1;
  }
  ast_free(curr);
  return node;
}

/**
** \fn struct ast *p_case_clause(void)
** \brief reprensents the case_clause rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_case_clause(void)
{
  struct ast *node = p_case_item();
  if (!node)
    return NULL;
  int test = 0;
  node = p_case_clause2(node, &test);
  struct ast *curr = curr_tok();
  if (!test)
  {
    while (curr && curr->type == NEWL)
    {
      eat();
      ast_free(curr);
      curr = curr_tok();
    }
  }
  ast_free(curr);
  return node;
}

static struct ast *p_rule_case2(struct ast *node)
{
  struct ast *curr = curr_tok();
  if (!curr)
    return NULL;
  while (curr->type == NEWL)
  {
    eat();
    curr = curr_tok();
    if (!curr)
      return NULL;
  }
  if (!curr || curr->type != IN)
  {
    ast_free(curr);
    return free_node(node);
  }
  eat();
  node = ast_create(curr, node, NULL, NULL);
  curr = curr_tok();
  if (!curr)
    return NULL;
  while (curr->type == NEWL)
  {
    eat();
    ast_free(curr);
    curr = curr_tok();
    if (!curr)
      return NULL;
  }
  ast_free(curr);
  return node;
}

/**
** \fn struct ast *p_rule_case(void)
** \brief reprensents the case rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_rule_case(void)
{
  struct ast *curr = curr_tok();
  if (!curr || curr->type != CASE)
    return free_node(curr);
  eat();
  struct ast *curr2 = curr_tok();
  if (!curr || curr2->type != WORD)
  {
    ast_free(curr);
    return free_node(curr2);
  }
  eat();
  struct ast *node = ast_create(curr2, curr, NULL, NULL);
  node = p_rule_case2(node);
  struct ast *node2 = p_case_clause();
  if (node2)
    node = ast_create(node2, node, NULL, NULL);
  curr = curr_tok();
  if (!curr || curr->type != ESAC)
  {
    ast_free(curr);
    return free_node(node);
  }
  eat();
  node = ast_create(curr, node, NULL, NULL);
  return node;
}
