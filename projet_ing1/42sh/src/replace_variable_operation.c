#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ast.h"
#include "hashtable.h"
#include "arithmetic.h"
#include "queue.h"
#include "parser.h"

extern struct global g_global;

static size_t size_variable(char *key)
{
  size_t i = 0;
  for (; key[i] && ((key[i] >= 'a' && key[i] <= 'z') ||
       (key[i] >= 'A' && key[i] <= 'Z')); i++);
  return i;
}

static char *get_char_hash(char *key)
{
   char *for_hash = NULL;
   size_t i = size_variable(key);
   for_hash = strndup(key, i);
   struct list *list = NULL;
   if ((list = find_hash(g_global.hash, for_hash)) != NULL)
   {
      free(for_hash);
      return list->data;
   }
   return NULL;
}

static char *copy_data(char *dest, char *src, size_t size, size_t src_size)
{
    dest = realloc(dest, sizeof(char) * (src_size + size + 1));
    size_t i = 0;
    for (; src[i] && i < src_size; i++)
        dest[size + i] = src[i];
    dest[size + i] = '\0';
    return dest;
}

char *replace_node_operation(char *key, size_t size)
{
   char *return_key = NULL;
   size_t length_return = 0;
   for (int i = 0; key[i] && size; i += 1)
   {
      if (key[i] == '$')
      {
         char *data = NULL;
         if (key[i] + 1)
            data = get_char_hash(key + i + 1);
         if (!data)
         {
            return_key = copy_data(return_key, key + i, length_return, 1);
            length_return += 1;
            size -= 1;
            continue;
         }
         size_t decalage = size_variable(key + i + 1);
         return_key = copy_data(return_key, data, length_return, strlen(data));
         length_return += strlen(data);
         i += decalage;
         size -= decalage + 1;
         continue;
      }
      size -= 1;
      return_key  = copy_data(return_key, key + i, length_return, 1);
      length_return += 1;
   }
   char *all_return = malloc(sizeof(char) * (strlen(return_key) + 4));
   all_return = memcpy(all_return, "$((", 3);
   strcpy(all_return + 3, return_key);
   free(return_key);
   return all_return;
}
