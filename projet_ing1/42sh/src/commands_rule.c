/**
** \file commands_rule.c
** \brief This file regroups the commands function of the parser
** \author Odile Guillaume
**
** This file regroups the commands function rules for the LL grammar of the
** parser.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

static struct ast *p_shell_command2(void)
{
  struct ast *curr = curr_tok();
  eat();
  struct ast *node = p_compound_list();
  if (node)
  {
    struct ast *curr2 = curr_tok();
    if (!curr2 || curr2->type != RBRACE)
      return free_node(node);
    else
    {
      eat();
      ast_create(curr, node, curr2, NULL);
    }
    return node;
  }
  else
    return NULL;
  return node;
}

static struct ast *p_shell_command3(void)
{
  struct ast *curr = curr_tok();
  eat();
  struct ast *node = p_compound_list();
  if (node)
  {
    struct ast *curr2 = curr_tok();
    if (!curr2)
      return NULL;
    if (curr2->type != RPAR)
      return free_node(node);
    else
    {
      eat();
      ast_create(curr, node, curr2, NULL);
    }
    return node;
  }
  else
    return NULL;
  return node;
}

/**
** \fn struct ast *p_shell_command(void)
** \brief reprensents the shell command rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_shell_command()
{
  struct ast *curr = curr_tok();
  if (!curr)
    return NULL;
  if (curr->type == LBRACE)
  {
    ast_free(curr);
    return p_shell_command2();
  }
  else if (curr->type == LPAR)
  {
    ast_free(curr);
    return p_shell_command3();
  }
  ast_free(curr);
  struct ast *node = p_rule_for();
  if (!node)
    node = p_rule_while();
  if (!node)
    node = p_rule_until();
  if (!node)
    node = p_rule_case();
  if (!node)
    node = p_rule_if();
  return node;
}

/**
** \fn struct ast *p_simple_command(void)
** \brief reprensents the simple command rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_simple_command(void)
{
  struct ast *node = p_prefix();
  struct ast *node2 = p_prefix();
  if (!node)
  {
    node = p_element();
    if (!node)
      return NULL;
    node2 = p_element();
    while (node2)
    {
      if (node2->type == GREAT || node2->type == LESS        ||
          node2->type == DGREAT || node2->type == GREATAND   ||
          node2->type == CLOBBER || node2->type == LESSGREAT ||
          node2->type == DLESS || node2->type == DLESSDASH)
        node = ast_create(node, node2, NULL, NULL);
      else
        node = ast_create(node2, node, NULL, NULL);
      node2 = p_element();
    }
    return node;
  }
  while(node2)
  {
    if (node2->type == GREAT || node2->type == LESS        ||
        node2->type == DGREAT || node2->type == GREATAND   ||
        node2->type == CLOBBER || node2->type == LESSGREAT ||
        node2->type == DLESS || node2->type == DLESSDASH)
      node = ast_create(node, node2, NULL, NULL);
    else
      node = ast_create(node2, node, NULL, NULL);
    node2 = p_prefix();
  }
  node2 = p_element();
  while (node2)
  {
    if (node2->type == GREAT || node2->type == LESS        ||
        node2->type == DGREAT || node2->type == GREATAND   ||
        node2->type == CLOBBER || node2->type == LESSGREAT ||
        node2->type == DLESS || node2->type == DLESSDASH)
       node = ast_create(node, node2, NULL, NULL);
     else
       node = ast_create(node2, node, NULL, NULL);
     node2 = p_element();
  }
  return node;
}

/**
** \fn struct ast *p_command(void)
** \brief reprensents the command rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_command(void)
{
  struct ast *node = p_simple_command();
  if (!node)
  {
    node = p_shell_command();
    if (!node)
      node = p_funcdec();
    if (!node)
      return NULL;
    struct ast *node2 = p_redirection();
    while (node2)
    {
      node = ast_create(node, node2, NULL, NULL);
      node2 = p_redirection();
    }
  }
  return node;
}
