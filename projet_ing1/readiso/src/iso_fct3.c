#include "iso9660.h"
#include "my_read_iso.h"


struct iso_dir *find_file(struct data *data, const char *name)
{
    char path[1024] =
    {
        0
    };
    int i = 0;
    while (next_slash(name, i))
    {
        for (; name[i] != '/'; i++)
            path[i] = name[i];
        path[i] = name[i];
        i++;
    }
    struct iso_dir *cur;
    if (i > 0)
    {
        cur = find_dir(data, path);
        if (!cur)
        {
            fprintf(stderr, "unable to find '%s' entry\n", name);
            return NULL;
        }
    }
    else
        cur = data->cur;
    char file[32] =
    {
        0
    };
    for (int j = 0; name[i]; i++, j++)
        file[j] = name[i];
    cur = find_file_dir(cur, file);
    if (!cur)
        return NULL;
    return cur;
}

int shell_help(char *arg)
{
    if (arg)
    {
        fprintf(stderr, "help: command does not take an argument\n");
        return 0;
    }
    printf("help: display command help\n\
info: display volume info\n\
ls: display the content of a directory\n\
cd: change current directory\n\
tree: display the tree of a directory\n\
get: copy file to local directory\n\
cat: display file content\n\
pwd: print current path\n\
quit: program exit\n");
    return 1;
}

int shell_info(void *ptr, char *arg)
{
    if (arg)
    {
        fprintf(stderr, "info: command does not take an argument\n");
        return 0;
    }
    struct iso_prim_voldesc *super = get_super_block(ptr);
    if (!super)
        return 0;
    printf("System Identifier: %.*s\n", ISO_SYSIDF_LEN, super->syidf);
    printf("Volume Identifier: %.*s\n", ISO_VOLIDF_LEN, super->vol_idf);
    printf("Block count: %d\n", super->vol_blk_count.le);
    printf("Block size: %d\n", ISO_BLOCK_SIZE);
    printf("Creation date: %.*s\n", ISO_LDATE_LEN, super->date_creat);
    printf("Application Identifier: %.*s\n", ISO_APP_LEN, super->app_idf);
    return 1;
}

int shell_ls(struct data *data, char *name)
{
    struct iso_dir *cur = data->cur;
    if (name)
        cur = find_dir(data, name);
    if (!cur)
        return 0;
    for (int i = 0; cur && cur->idf_len > 0; i++)
    {
        char *name;
        char type = '-';
        char flag = '-';
        int len = 1;
        if (i == 0)
            name = ".";
        else if (i == 1)
        {
            name = "..";
            len = 2;
        }
        else
        {
            name = cast_char(cur) + sizeof(struct iso_dir);
            len = cur->idf_len;
        }
        if (cur->type == 1 || cur->type == 3)
            flag = 'h';
        if (cur->type == 2 || cur->type == 3)
            type = 'd';
        else
            len -= 2;
        char *date = cur->date;
        printf("%c%c %9u %04d/%02d/%02d %02d:%02d %.*s\n", type, flag,
                cur->file_size.le, date[0] + 1900, date[1], date[2], date[3],
                date[4], len, name);
        cur = add_offset(cur, sizeof (struct iso_dir) + cur->idf_len +
                (sizeof (struct iso_dir) + cur->idf_len) % 2);
    }
    return 1;
}

struct iso_path_table_le *shell_cd(char *ptr, struct iso_path_table_le 
        *path, const char *name, const char *script)
{
    struct iso_path_table_le *path_table = path;
    int dir_len = 0;
    int i = 0;
    char dir[32];
    struct iso_prim_voldesc *super = get_super_block(ptr);
    struct iso_path_table_le *root =  cast_void(ptr
            + super->le_path_table_blk * ISO_BLOCK_SIZE);
    if (!name)
    {
        printf("Changing to '/' directory\n");
        return root;
    }
    int len = strlen(name);
    if (name[0] == '/')
    {
        path_table = root;
        i++;
    }
    while (i < len)
    {
        int j = 0;
        dir_len = 0;
        for (; i < len && name[i] != '/'; i++, j++)
        {
            dir[j] = name[i];
            dir_len++;
        }
        i++;

        if (dir_len == 1 && dir[0] == '.')
            continue;
        else if (dir_len == 2 && !strncmp(dir, "..", 2))
            path_table = get_n_path(ptr, path_table->parent_dir);
        else
        {
            path_table = get_next_path(ptr, path_table, dir);
            if (!path_table)
            {
                fprintf(stderr, "%s: unable to find '%s' directory entry\n",
                        script + 2, name);
                return path;
            }
        }
    }
    if (path_table == root)
        printf("Changing to '/' directory\n");
    else
        printf("Changing to '%.*s' directory\n", path_table->idf_len,
                cast_char(path_table) + sizeof(struct iso_path_table_le));
    return path_table;
}
