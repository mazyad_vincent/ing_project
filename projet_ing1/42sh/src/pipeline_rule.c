/**
** \file pipeline_rule.c
** \brief This file contains the pipeline rule for the parser
** \author Odile Guillaume
**
** This file contains the pipeline rule for the LL grammar of the parser
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

static struct ast *p_pipeline2(struct ast *node2, struct ast *curr)
{
  eat();
  struct ast *curr2 = curr;
  curr = curr_tok();
  while (curr && curr->type == NEWL)
  {
    eat();
    ast_free(curr);
    curr = curr_tok();
  }
  ast_free(curr);
  struct ast *temp = p_command();
  if (!temp)
  {
    ast_free(curr2);
    return free_node(node2);
  }
  node2 = ast_create(node2, curr2, temp, NULL);
  return node2;
}

/**
** \fn struct ast *p_pipeline(void)
** \brief reprensents the pipeline rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_pipeline(void)
{
  struct ast *node = NULL;
  struct ast *curr = curr_tok();
  if (curr && curr->type == BANG)
  {
    node = ast_create(NULL, curr, NULL, NULL);
    eat();
  }
  struct ast *node2 = p_command();
  if (!node2)
  {
    ast_free(curr);
    return free_node(node);
  }
  if (node)
    node2 = ast_create(node2, node, NULL, NULL);
  ast_free(curr);
  curr = curr_tok();
  if (!curr)
    return node2;
  while (curr && curr->type == OR)
  {
    node2 = p_pipeline2(node2, curr);
    curr = curr_tok();
  }
  ast_free(curr);
  return node2;
}
