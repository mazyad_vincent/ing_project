#include <stdlib.h>
#include "graph.h"
#include "control.h"
#include <stdio.h> 

struct graph* create_graph()
{
    struct graph *graph = malloc(sizeof(struct graph));
    if (!graph)
        return NULL;
    graph->vertex = 0;
    graph->head = NULL;
    return graph;
}

void create_adj(struct graph *graph, struct vector2 *first_value
                            , struct vector2 *second_value)
{
    struct adj_list *new_adj;
    if (graph->vertex > 0)
        new_adj = malloc(sizeof(struct adj_list));
    if (!new_adj)
        return;
    new_adj->checkpoint = second_value;
    new_adj->next = NULL;
    struct edges *cpy = graph->head;
    while (cpy)
    {
        if ((cpy->checkpoint->x == first_value->x) && (cpy->checkpoint->y ==
             first_value->y))
        {
            struct adj_list *adja =  cpy->adj;
            while(adja && adja->next)
                adja = adja->next;
            if (!adja)
               cpy->adj = new_adj;
            else
               adja->next = new_adj;
            return;
        }
        cpy = cpy->next;
    }
}

void add_edge(struct graph *graph, struct vector2 *first_pos)
{   
    struct edges *head = malloc(sizeof(struct edges));
    if (!head)
        return;
    head->adj = NULL;
    head->next = NULL;
    head->checkpoint = first_pos;

    if (graph->vertex == 0)
        graph->head = head;
    else 
    {
        struct edges *cpy = graph->head;
        while (cpy && cpy->next)
            cpy = cpy->next;
        cpy->next = head;
    }
    graph->vertex +=1;
}

void destroy_all(struct graph *graph)
{
    while (graph->vertex != 0)
    {
        while (graph->head->adj)
        {
            struct adj_list *free_adj = graph->head->adj;
            graph->head->adj = graph->head->adj->next;
            free(free_adj);
        }
        struct edges *free_head = graph->head;
        graph->head = graph->head->next;
        free(free_head->checkpoint);
        free(free_head);
        graph->vertex--;
    }
    free(graph);
}

void clear_all(struct graph *graph)
{
    while (graph->vertex != 0)
    {
        while (graph->head->adj)
        {
            struct adj_list *free_adj = graph->head->adj;
            graph->head->adj = graph->head->adj->next;
            free(free_adj);
        }
        struct edges *free_head = graph->head;
        graph->head = graph->head->next;
        free(free_head->checkpoint);
        free(free_head);
        graph->vertex--;
    }
}

void print_adj(struct graph *graph)
{
    struct edges *headd = graph->head;
    while (headd)
    {
        printf("%s %f %s %f\n", "VECT COORD X IS ::",
                headd->checkpoint->x,"VECT COORD Y IS:" 
                ,headd->checkpoint->y);
        struct adj_list *free_adj = headd->adj;
        printf("%s \n", "his adj are:");
        while (free_adj)
        {
           printf("%f %f\n", free_adj->checkpoint->x, free_adj->checkpoint->y);
           free_adj = free_adj->next;
        }
        headd = headd->next;
    }
}

int find_edge(struct graph *graph, struct vector2 *pos)
{
    if (graph->vertex == 0)
        return 0;
    for (struct edges *edg = graph->head; edg; edg = edg->next)
    {
        if (edg->checkpoint->x == pos->x && edg->checkpoint->y == pos->y)
            return 1;
    }
    return 0;
}

struct edges* get_finish(struct graph *graph, struct vector2 *vect)
{
    struct edges *cpy = graph->head;
    int g;
    while(cpy)
    {
        if (((g = cpy->checkpoint->x) == vect->x) && 
           ((g = cpy->checkpoint->y) == vect->y))
            return cpy;
        cpy = cpy->next;
    }
    return NULL;
}

struct edges* get_link(struct graph *graph, struct edges *edge)
{
    struct edges *cpy = graph->head;
    int c;
    int d;
    while (cpy)
    {
        struct adj_list *adj = cpy->adj;
        while (adj)
        {
            if (((c = adj->checkpoint->x) == (d = edge->checkpoint->x)) &&
                ((c = adj->checkpoint->y) == (d = edge->checkpoint->y)))
                return cpy;
            adj = adj->next;
        }
        cpy = cpy->next;
    }
    return NULL;
}

int find_adj(struct graph *graph, struct vector2 *edge, struct vector2 *vect)
{
    struct edges *edg = graph->head;
    while (edg)
    {
        if (edg->checkpoint->x == edge->x && edg->checkpoint->y == edge->y)
        {
            struct adj_list *adj = edg->adj;
            while(adj)
            {
                if ((adj->checkpoint->x == vect->x) &&
                (adj->checkpoint->y == vect->y))
                    return 1;
                 adj = adj->next;
            }
        }
        edg = edg->next;
    }
    return 0;
}

void graph_popadj(struct graph *graph, struct edges *to_pop, struct vector2
                  *vect)
{
    struct edges *cpy = graph->head;
    while(cpy)
    {
        if (cpy->checkpoint->x == to_pop->checkpoint->x && cpy->checkpoint->y
            == to_pop->checkpoint->y)
        {
            struct adj_list *adj = cpy->adj;
            if (!adj->next && adj->checkpoint->x == vect->x &&
               adj->checkpoint->y == vect->y)
            {
                free(adj->checkpoint);
                free(adj);
                cpy->next = NULL;
                return;
            }
            while (adj && adj->next)
            {
                if (adj->next->checkpoint->x == vect->x &&
                    adj->next->checkpoint->y == vect->y)
                {
                    struct adj_list *to_pop = adj->next;
                    if (!to_pop->next)
                         adj->next = NULL;
                    else
                         adj->next = adj->next->next;
                    free(to_pop->checkpoint);
                    free(to_pop);
                    return;
                }
                adj = adj->next;
            }
        }
        cpy = cpy->next;
    }
}
