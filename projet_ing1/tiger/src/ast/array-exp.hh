/**
 ** \file ast/array-exp.hh
 ** \brief Declaration of ast::ArrayExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/name-ty.hh>

namespace ast
{

  /// ArrayExp.
  class ArrayExp : public Exp
  {
    //FIXME
    public:

      ArrayExp(const Location& location, NameTy* name, Exp* into, Exp* type);
      ArrayExp(const ArrayExp&) = delete;
      ArrayExp& operator=(const ArrayExp&) = delete;
      virtual ~ArrayExp();
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      const NameTy& name_get() const;
      NameTy& name_get();
      const Exp& into_get() const;
      Exp& into_get();
      const Exp& type_get() const;
      Exp& type_get();

    protected:
      Exp* into_;
      Exp* type_;
      NameTy* name_;
  };

} // namespace ast

#include <ast/array-exp.hxx>

