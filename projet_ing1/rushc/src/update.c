#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "generation_map.h"

void create_fire(struct IA *ia)
{
  ia->fire = realloc(ia->fire, (sizeof(struct vector2) * ia->numb) + 1);
  struct vector2 new_fire;
  new_fire.x = ia->pos.x;
  new_fire.y = ia->pos.y;
  ia->fire[ia->numb] =  new_fire;
  ia->numb += 1;
}

void update_enum(struct map *map, struct IA *ia, int time)
{
   map->pos[ia->pos.y][ia->pos.x] = NOT;
   //map->pos[ia->pos.y + numb][ia->pos.x] = IA;
   if (!ia->boolean)
   {
     map->pos[ia->pos.y + 1][ia->pos.x] = IA;
     ia->pos.y = ia->pos.y + 1;
     ia->pos_screen.y += 30;
   }
   if (ia->boolean)
   {
     map->pos[ia->pos.y - 1][ia->pos.x] = IA;
     ia->pos.y = ia->pos.y - 1;
     ia->pos_screen.y -= 30;
   }
   // need a wait
   if (!(time % 7))
     create_fire(ia);
}

int is_behind(struct map *map, struct IA *ia)
{
    if (ia->pos.x < map->pos_player.x)
    {
      free_ia(ia);
      return 1;
    }
    return 0;
}

int is_behind_fire(struct vector2 pos_fire, struct map *map)
{
  map = map;
    if (pos_fire.x < 0)
      return 1;
    return 0;
}

void conditions_IA(struct map *map, struct IA *ia)
{
  if (ia->pos.y == 2)
    ia->boolean = 0; /* need to go in the bottom */
  else if (ia->pos.y == map->height - 3)
    ia->boolean = 1;
}

void update_fire(struct IA *ia, struct map *map)
{
   for (size_t index = 0; index < ia->numb; index++)
   {
      if (!is_behind_fire(ia->fire[index], map))
      {
          update_fire_map(map, ia, index, 0);
          ia->fire[index].x -= 1;
          update_fire_map(map, ia, index, 1);
      }
   }
}
void update_ia(struct map *map, int time)
{
   struct IA *head_ia = map->ia;
   struct IA *copy = head_ia;
   while (copy)
   {
      conditions_IA(map, copy);
      update_enum(map, copy, time); /* moove IA and update map */
      update_fire(copy, map);
      copy = copy->next;
   }
}
