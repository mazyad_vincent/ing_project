/**
 ** \file ast/pretty-printer.cc
 ** \brief Implementation of ast::PrettyPrinter.
 */

#include <misc/escape.hh>
#include <misc/indent.hh>
#include <misc/separator.hh>
#include <misc/pair.hh>
#include <ast/all.hh>
#include <ast/pretty-printer.hh>
#include <ast/libast.hh>

#include <type/class.hh>

namespace ast
{

  // Anonymous namespace: these functions are private to this file.
  namespace
  {
    /// Output \a e on \a ostr.
    inline
    std::ostream&
    operator<<(std::ostream& ostr, const Escapable& e)
    {
      if (escapes_display(ostr)
  // FIXME: Some code was deleted here.
          )
        ostr << "/* escaping */ ";

      return ostr;
    }

    /// \brief Output \a e on \a ostr.
    ///
    /// Used to factor the output of the name declared,
    /// and its possible additional attributes.
    inline
    std::ostream&
    operator<<(std::ostream& ostr, const Dec& e)
    {
      ostr << e.name_get();
      if (bindings_display(ostr))
        ostr << " /* " << &e << " */ ";
      return ostr;
    }
  }

  PrettyPrinter::PrettyPrinter(std::ostream& ostr)
    : ostr_(ostr)
  {}


  void
  PrettyPrinter::operator()(const SimpleVar& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_))
      ostr_ << " /* " << e.def_get() << " */ ";
  }

  void
  PrettyPrinter::operator()(const FieldVar& e)
  {
     ostr_ << e.var_get() << "." << e.field_get() ;
 
  /* FIXME */
  }

  /* Foo[10]. */
  void
  PrettyPrinter::operator()(const SubscriptVar& e)
  {
    ostr_ << e.var_get() << '[' << misc::incindent << e.index_get()
          << misc::decindent << ']';
  }

  void
  PrettyPrinter::operator()(const CastVar& e)
  {
    ostr_ << "_cast(" << e.var_get() << ", " << e.ty_get() << ')';
  }

  void
  PrettyPrinter::operator()(const CastExp& e)
  {
    ostr_ << "_cast(" << e.exp_get() << ", " << e.ty_get() << ')';
  }

  // FIXME: Some code was deleted here.

  /* | function id ( tyfields ) [ : type-id ] = exp */
  void
  PrettyPrinter::operator()(const FunctionDec& e)
  {
    if (!e.body_get())
      ostr_ << "primitive " << e.name_get();
    else
      ostr_ << "function " << e.name_get();
     if (bindings_display(ostr_))
       ostr_ << " /* " << &e << " */ ";
     ostr_ << "(";

    auto sz = e.formals_get().decs_get().size();
    size_t ind = 0;

    for (auto i = e.formals_get().decs_get().begin(); i !=
    e.formals_get().decs_get().end(); i++)
    {
      if (ind != sz - 1)
        ostr_ << (*i)->name_get() << " : " << *((*i)->type_name_get()) << ", ";
      else
        ostr_ << (*i)->name_get() << " : " << *((*i)->type_name_get());
	ind++;
    }
    ostr_ << ") ";
    if (e.result_get())
      ostr_ << ": " << *(e.result_get());
    if (e.body_get())
      ostr_ << " = " << misc::incendl << *(e.body_get()) << misc::iendl;
     }

  void
  PrettyPrinter::operator()(const VarDec& e)
  {
    ostr_ << "var " << e.name_get();
    if (e.type_name_get())
      ostr_ << " : " << *(e.type_name_get());
    if (e.init_get())
      ostr_ << " := " << *(e.init_get());
    if (bindings_display(ostr_))
       ostr_ << " /* " << &e << " */ ";
  }

  /*| type-id [ exp ] of exp  */
  void
  PrettyPrinter::operator()(const ArrayExp& e)
  {
    ostr_ << e.name_get()  <<" [" << e.type_get() << "] of " << e.into_get() ;
  }

  /*  | lvalue := exp */
  void
  PrettyPrinter::operator()(const AssignExp& e)
  {
    ostr_ << e.lval_get() << " := " << e.exp_get();

  }

  /* break */
  void
  PrettyPrinter::operator()(const BreakExp& e)
  {
    ostr_ << "break";
    if (bindings_display(ostr_))
       ostr_ << " /* " << e.def_get() << " */ ";

  }
  /* | id ( [ exp { , exp }] ) */
  void
  PrettyPrinter::operator()(const CallExp& e)
  {
    ostr_ << e.get_func();
    if (bindings_display(ostr_))
      ostr_ << " /* " << e.def_get() << " */ ";

    ostr_ << " (";
    auto sz =  e.get_vect()->size();
    size_t ind = 0;
    for (auto i = e.get_vect()->begin(); i != e.get_vect()->end() ; i++)
    {
      if (sz - 1 != ind)
      {
        ostr_ << (**i) << ", ";
      }
      else
        ostr_ << (**i);
      ind++;
    }
    ostr_ << ")" ;
    if (bindings_display(ostr_))
       ostr_ << " /*" << e.def_get() << "*/ ";

  }

  /* # Method call.
  | lvalue . id ( [ exp { , exp }] )*/
  void
  PrettyPrinter::operator()(const MethodCallExp& e)
  {
    ostr_ << e.var_get() << " . " <<e.get_func() << " ( ";
    auto sz =  e.get_vect().size();
    size_t ind = 0;
    for (auto i = e.get_vect().begin(); i != e.get_vect().end() ; i++)
    {
      if (sz != ind)
      {
        ostr_ << (**i) <<  ", ";
	ind++;
      }
      else
        ostr_ << (**i);
    }
    ostr_ << " )" ;
  }


  /* | for id := exp to exp do exp */
  void
  PrettyPrinter::operator()(const ForExp& e)
  {
     ostr_ << "for ";
     if (bindings_display(ostr_))
       ostr_ << " /* " << &e << " */ ";

     ostr_ << e.vardec_get().name_get();
     if (bindings_display(ostr_))
       ostr_ << " /* " << &e.vardec_get() << " */ ";
     ostr_ << " := "  << *(e.vardec_get().init_get()) << " to " << e.hi_get() <<
     " do (" << misc::incindent << e.body_get() << ")" <<  misc::decindent ;
      }

  /* | if exp then exp [else exp] */
  void
  PrettyPrinter::operator()(const IfExp& e)
  {
    ostr_ << "if ";
    if (bindings_display(ostr_))
       ostr_ << " /* " << &e << " */ ";

    ostr_ << *(e.cond_get()) ;
    if (e.body_get())
      ostr_ << misc::incendl << " then "<< *(e.body_get()) << misc::decendl;
    if (e.else_get())
      ostr_ << " else " << misc::incindent << *(e.else_get()) <<
      misc::decindent;
  }

  /* | integer */
  void
  PrettyPrinter::operator()(const IntExp& e)
  {
    ostr_ << e.value_get();
  }

  /*| let decs in exps end */
  void
  PrettyPrinter::operator()(const LetExp& e)
  {
    ostr_ << "let";
    size_t ind = 0;
    auto size = e.list_get()->size();

    for (auto i = e.dec_get().decs_get().begin(); i != 
    e.dec_get().decs_get().end(); ++i)
    {
      if (size != ind)
        ostr_ << misc::incendl << (**i) << misc::decindent;
      else
        ostr_ << misc::incendl << (**i) << misc::decindent;
      ind++;
    }
    ind = 0;
    ostr_ << misc::iendl << "in";
    for (auto i = e.list_get()->begin(); i != e.list_get()->end() ; ++i)
    {
      if (size - 1 != ind)
        ostr_ << misc::incendl << (**i) << ";" << misc::decindent;
      else
         ostr_ << misc::incendl << (**i) << misc::decendl;
      ind++;
    }
    ostr_ <<  "end";
  }

  /*  nil */
  void
  PrettyPrinter::operator()(const NilExp& e)
  { 
    ostr_ << "nil";
  }

  /* | new type-id */
  void
  PrettyPrinter::operator()(const ObjectExp& e)
  {
    ostr_ << "new " << e.name_get();
  }

  /* | exp op exp*/
  void
  PrettyPrinter::operator()(const OpExp& e)
  {
    std::string tmp;
    if (e.oper_get() == OpExp::Oper::add)
      tmp = "+";
    else if (e.oper_get() == OpExp::Oper::sub)
      tmp = "-";
    else if (e.oper_get() == OpExp::Oper::mul)
      tmp = "*";
    else if (e.oper_get() == OpExp::Oper::div)
      tmp = "/";
    else if (e.oper_get() == OpExp::Oper::eq)
      tmp = "=";
    else if (e.oper_get() == OpExp::Oper::ne)
      tmp = "<>";
    else if (e.oper_get() == OpExp::Oper::lt)
      tmp = "<";
    else if (e.oper_get() == OpExp::Oper::le)
      tmp = "<=";
    else if (e.oper_get() == OpExp::Oper::gt)
      tmp = ">";
    else if (e.oper_get() == OpExp::Oper::ge)
      tmp = ">=";

     ostr_ << e.left_get() << " " << tmp  << " " << e.right_get();
  }

  /* | type-id {[ id = exp { , id = exp } ] } */
  void
  PrettyPrinter::operator()(const RecordExp& e)
  {
    ostr_ << e.get_type() << " {";
    size_t index = 0;
    size_t size = e.get_vect()->size();
    for (auto i = e.get_vect()->begin(); i != e.get_vect()->end() ; i++)
    {
        if (size - 1 > index)
        {
          ostr_ << (*i)->name_get() <<  "=" << (*i)->init_get() << ", ";

        }
        else
          ostr_ << (*i)->name_get() <<  "=" <<  (*i)->init_get();
       index++;
    }
    ostr_ << "}";
  }

  /*  */
  void
  PrettyPrinter::operator()(const SeqExp& e)
  {
    ostr_ << "(";
    auto sz =  e.get_vect()->size();
    for (auto i = e.get_vect()->begin(); i !=  e.get_vect()->end() ; i++)
    {
      ostr_ << **i; //FIXME
      if (i != e.get_vect()->end() - 1)
        ostr_ << ";" << misc::iendl;
    }
    ostr_ << ")";
    ostr_ << misc::iendl;
  }

  /* | string */
  void
  PrettyPrinter::operator()(const StringExp& e)
  {
    ostr_ << "\"" << e.str_get() << "\"";
  }

  /* | while exp do exp */
  void
  PrettyPrinter::operator()(const WhileExp& e)
  {
    ostr_ << "while ";
    if (bindings_display(ostr_))
      ostr_ << " /* " << &e << " */ ";

    ostr_ << e.test_get() << " do" << misc::incendl << e.body_get();
    ostr_ << misc::decindent;
      }

  /*  | array of type-id */
  void
  PrettyPrinter::operator()(const ArrayTy& e)
  {

    ostr_ << "array of " << e.base_type_get();
  }

  /* class [ extends type-id ] { classfields } */
  void
  PrettyPrinter::operator()(const ClassTy& e)
  {
    ostr_ << "class " ;
    ostr_ << "extends " << " [ " << e.super_get() << " ] ";
    auto size = e.decs_get().decs_get().size();
    size_t ind = 0;
    for (auto i = e.decs_get().decs_get().begin(); 
    i != e.decs_get().decs_get().end(); i++)
    {
      if (ind != size)
      {
        ostr_ << **i << ", ";
        ind++;
      }
      else
         ostr_ << **i; 
    }
      ostr_ << " }";
  /* if (e.decs_get().decs_get().size() != 0)
     ostr_ << " decs list";*/
  }

  /*  */
  void
  PrettyPrinter::operator()(const NameTy& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_))
       ostr_ << " /* " << e.def_get() << " */ ";

  }

  /* | { tyfields  } */
  void
  PrettyPrinter::operator()(const RecordTy& e)
  {
    int index = 0;
    auto sz =  e.get_vect()->size();
   /* int b = 0;
    if ((e.get_vect())->size() > 1)
    {
      ostr_ << "{";
      b = 1;
    }*/
    ostr_ << "{";
    for (auto i = e.get_vect()->begin(); i != e.get_vect()->end(); i++)
    {
      if (index < sz - 1)
        ostr_ << (*i)->name_get() << " : " << (*i)->type_name_get() <<
	", ";
      else
        ostr_  << (*i)->name_get() << " : " << (*i)->type_name_get(); 
      index++;
    }
    ostr_ << "}";
    //if (b == 1)
     // ostr_ << "}";
  }

  void
  PrettyPrinter::operator()(const Field& e)
  {
    ostr_ << e.type_name_get() << " , " << e.name_get();
  }

  void
  PrettyPrinter::operator()(const TypeDec& e)
  {
    ostr_ << "type " << e.name_get();
     if (bindings_display(ostr_))
       ostr_ << " /* " << &e << " */ ";


    ostr_ << " = " << e.ty_get();
    if (bindings_display(ostr_))
      ostr_ << " /* " << &e << " */ ";
  }

  /* | method id ( tyfields ) [ : type-id ] = exp */
  void
  PrettyPrinter::operator()(const FieldInit& e)
  {
    ostr_ << ", " << e.name_get()  << " = " << e.init_get() ;
  }

  void
  PrettyPrinter::operator()(const FunctionDecs& e)
  {
    for (auto i = e.decs_get().begin(); i != e.decs_get().end(); i++)
    {
      operator()(**i);
      ostr_ << misc::iendl;
    }
    // ostr_ << misc::separate(e.decs_get(),misc::iendl);
  }


} // namespace ast
