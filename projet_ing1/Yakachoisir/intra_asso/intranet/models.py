from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.exceptions import ValidationError
from django.utils.text import slugify
from django.contrib.auth import get_user_model
from guardian.shortcuts import assign_perm
from django.core.mail import EmailMessage
from django.utils.deconstruct import deconstructible
import datetime

# Create your models here.

User = get_user_model()

accepted = 'a'
denied = 'd'
waiting = 'w'

Statut = (
    (accepted, "Accepté"),
    (denied, "Refusé"),
    (waiting, "En attente"),
)


class Association(models.Model):
    name = models.CharField(max_length=100, null=False, primary_key=True)
    chemin_statut = models.FileField(upload_to="files/", null=True)
    email = models.CharField(max_length=100, null=False)
    website = models.URLField(null=True, blank=True)
    description = models.TextField(null=False)
    logo = models.ImageField(upload_to="logo/", null=True, default="logo-epita.png")
    motivation_letter = models.BooleanField()
    slug = models.SlugField(max_length=100, unique=True, null=True, blank=True)
    nb_event = models.IntegerField(null=False, default=0)

    login_pres = models.ForeignKey(
        User, on_delete=models.PROTECT, null=False, related_name='%(class)s_pres', default=0)

    comm_pres = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(25)])
    work_pres = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(50)])
    asso_life_pres = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(25)])


    login_vice_pres = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, related_name='%(class)s_vice_pres')

    comm_vice_pres = models.IntegerField(null=True, default=0, validators=[MinValueValidator(0), MaxValueValidator(25)])
    work_vice_pres = models.IntegerField(null=True, default=0, validators=[MinValueValidator(0), MaxValueValidator(50)])
    asso_life_vice_pres = models.IntegerField(null=True, default=0, validators=[MinValueValidator(0), MaxValueValidator(25)])

    login_tres = models.ForeignKey(
        User, on_delete=models.PROTECT, null=False, related_name='%(class)s_tres', default=0)

    comm_tres = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(25)])
    work_tres = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(50)])
    asso_life_tres = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(25)])

    login_secr = models.ForeignKey(
        User, on_delete=models.PROTECT, null=False, related_name='%(class)s_secr', default=0)

    comm_secr = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(25)])
    work_secr = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(50)])
    asso_life_secr = models.IntegerField(null=False, default=0, validators=[MinValueValidator(0), MaxValueValidator(25)])

    members = models.ManyToManyField(User, through='IsMemberOfAssociation')

    def get_board(self):
        return {self.login_pres, self.login_tres, self.login_secr, self.login_vice_pres}

    def __str__(self):
        return self.name

    def _get_unique_slug(self):
        slug = slugify(self.name)
        unique_slug = slug
        num = 1
        while Association.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug or self.slug == "":
            self.slug = self._get_unique_slug()
        if self.login_pres == self.login_tres \
                or self.login_pres == self.login_secr \
                or self.login_secr == self.login_tres:
            raise ValidationError("La dictature n'est pas autorisée!")

        super(Association, self).save(*args, **kwargs)


class Club(Association):
    depend = models.ForeignKey(
        Association,
        on_delete=models.CASCADE,
        related_name='%(class)s_asso'
    )


class DemandeDeBudget(models.Model):
    emis = models.ForeignKey(
        Association, on_delete=models.CASCADE, null=False)
    fichier_presentation_chemin = models.FileField(upload_to='files/', null=False)
    amount = models.IntegerField(null=False, default=1,
                                 validators=[MinValueValidator(1)]
                                 )
    date = models.DateField(auto_now_add=True)
    dateAccepted = models.DateField(null=True)

    statut = models.CharField(
        max_length=1,
        choices=Statut,
        default=waiting,
        null=False,
    )


class DeclarationDepense(models.Model):
    emis = models.ForeignKey(
        Association, on_delete=models.CASCADE, null=False)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=False)
    fichier_just_chemin = models.FileField(upload_to='files/', null=False)
    date = models.DateTimeField(null=False)
    amount = models.IntegerField(null=False, default=1,
                                 validators=[MinValueValidator(1)]
                                 )
    libelle = models.CharField(null=False, max_length=100)


class DemandeCreationAssociation(models.Model):
    nom_asso = models.CharField(max_length=100, null=False)
    createur = models.ForeignKey(
        User, on_delete=models.CASCADE, null=False)
    file = models.FileField(upload_to='files/', null=True)
    date = models.DateTimeField(auto_now_add=True, null=True)

    waiting_cia = 'c'
    waiting_responsable = 'r'

    Statut_Demande = (
        (accepted, "Accepté"),
        (waiting_cia, "En attente(CIA)"),
        (waiting_responsable, "En attente(reponsable)"),
        (denied, "Refusé"),
    )

    statut = models.CharField(
        max_length=1,
        choices=Statut_Demande,
        default=waiting_responsable,
        null=False,
    )

    def changeStatus(self, accepted):
        if self.statut == 'r':
            self.statut = 'c' if accepted else 'd'
        else:
            self.statut = 'a' if accepted else 'd'
        self.save()


class Fichier(models.Model):
    nom_fichier = models.CharField(max_length=100, null=False)
    chemin_fichier = models.FileField(max_length=100, null=False)
    contenu_association = models.ForeignKey(
        Association, on_delete=models.CASCADE)
    contenu_demande_association = models.ForeignKey(
        DemandeCreationAssociation, on_delete=models.CASCADE)


class Reunion(models.Model):
    organise = models.ForeignKey(
        Association, on_delete=models.CASCADE, null=False)
    convoque = models.ManyToManyField(User)
    horaire = models.DateTimeField(null=False)
    lieu = models.CharField(max_length=100, null=False)
    public = models.BooleanField()
    ordre_du_jour_chemin = models.CharField(max_length=100, null=False)
    compte_rendu_chemin = models.CharField(max_length=100)


class IsMemberOfAssociation(models.Model):
    asso = models.ForeignKey('Association', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    nb_participation = models.IntegerField(null=False, default=0)
    vice_president = 'p'
    vice_secretaire = 's'
    vice_tresorier = 't'
    defa = 'd'

    Poste = (
        (vice_president, 'vice_président'),
        (vice_secretaire, 'vice_secretaire'),
        (vice_tresorier, 'vice_tresorier'),
        (defa, 'defaut'),
    )
    poste = models.CharField(
        max_length=1,
        choices=Poste,
        default=defa,
        null=False,
    )

    class Meta:
        unique_together = ('asso', 'user')
        permissions = (
            ('view_member', "View Member"),
        )


class IsApplyingTo(models.Model):
    asso = models.ForeignKey('Association', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    motivation_letter = models.TextField(max_length=1000, blank=True)

    statut = models.CharField(
        max_length=1,
        choices=Statut,
        default=waiting,
        null=False,
    )

    def apply_perm(self, user):
        assign_perm('change_isapplyingto', user, self)
        assign_perm('delete_isapplyingto', user, self)
        assign_perm('view_apply', user, self)

    class Meta:
        permissions = (
            ('view_apply', "View Application"),
        )


class ProxyUser(User):

    def is_board(self):
        pres = self.username in [asso.login_pres.username for asso in Association.objects.all()]
        tres = self.username in [asso.login_tres.username for asso in Association.objects.all()]
        secr = self.username in [asso.login_secr.username for asso in Association.objects.all()]
        return pres or tres or secr

    class Meta:
        proxy = True


class ResponsableAsso(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ena_open = models.BooleanField(null=False, default=False)

    @staticmethod
    def is_respo(user):
        return ResponsableAsso.objects.all().first().user == user

    @staticmethod
    def is_enac_open():
        return ResponsableAsso.objects.first().ena_open

    @staticmethod
    def open_enac():
        respo = ResponsableAsso.objects.first()
        if (respo.ena_open):
            return "La demande de formulaire de points ENACS est déjà en cours"
        respo.ena_open = True
        respo.save()
        body = (
                    "Bonjour, la répartition des points ENACS est ouverte. Vous pouvez remplir la page correspondante")

        mails = Association.objects.all().values_list('login_pres').values_list('email')
        mails = [i[0] for i in mails]
        print(mails)
        email = EmailMessage("Demande de création d'association", body, to=mails)
        email.send()

    @staticmethod
    def close_enac():
        respo = ResponsableAsso.objects.first()
        if (not respo.ena_open):
            return "La demande de formulaire de points ENACS n'est pas ouverte"
        respo.ena_open = False
        respo.save()


"""
class Member(models.Model):
    login = models.CharField(max_length=50, primary_key=True, null=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
"""

"""
class Bureau(models.Model):
    assoc_name = models.OneToOneField(Association, on_delete=models.CASCADE)

    login_pres = models.ForeignKey(
        Member, on_delete=models.PROTECT, null=False, related_name='%(class)s_pres', default=0)
    login_tres = models.ForeignKey(
        Member, on_delete=models.PROTECT, null=False, related_name='%(class)s_tres', default=0)
    login_secr = models.ForeignKey(
        Member, on_delete=models.PROTECT, null=False, related_name='%(class)s_secr', default=0)

    login_vpres = models.ForeignKey(
        Member, on_delete=models.PROTECT, null=True, related_name='%(class)s_vpres', default=0)
    login_vtres = models.ForeignKey(
        Member, on_delete=models.PROTECT, null=True, related_name='%(class)s_vtres', default=0)
    login_vsecr = models.ForeignKey(
        Member, on_delete=models.PROTECT, null=True, related_name='%(class)s_vsecr', default=0)
"""
