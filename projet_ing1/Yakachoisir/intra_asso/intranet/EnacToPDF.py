from django.conf import settings
from openpyxl import Workbook
from openpyxl import load_workbook
from .models import Association, IsMemberOfAssociation, ProxyUser


l_start_board = 7
l_start_member = 26
c_login = 1
c_name = 2
c_firstname = 3
c_nb_asso_event = 7
c_nb_event_pres = 8
c_comm_board = 7


def set_info(user, row, ws):
    ws.cell(row=row, column=c_login, value=user.username)
    ws.cell(row=row, column=c_name, value=user.last_name)
    ws.cell(row=row, column=c_firstname, value=user.first_name)

def set_board_member(user, mark, row, ws):
    set_info(user, row, ws)
    ws.cell(row=row, column=c_nb_asso_event, value=mark.comm)
    ws.cell(row=row, column=c_nb_asso_event + 1, value=mark.work)
    ws.cell(row=row, column=c_nb_asso_event + 2, value=mark.asso_life)

def set_board(asso, ws):
    set_info(asso.login_pres, l_start_board, ws)

    if asso.login_vice_pres is not None:
        set_info(asso.login_vice_pres, l_start_board + 1, ws)
        ws.cell(row=l_start_board + 1, column=c_nb_asso_event, value=asso.comm_vice_pres / 100)
        ws.cell(row=l_start_board + 1, column=c_nb_asso_event + 1, value=asso.work_vice_pres / 100)
        ws.cell(row=l_start_board + 1, column=c_nb_asso_event + 2, value=asso.asso_life_vice_pres / 100)

    set_info(asso.login_secr, l_start_board + 2, ws)
    ws.cell(row=l_start_board + 2, column=c_nb_asso_event, value=asso.comm_secr / 100)
    ws.cell(row=l_start_board + 2, column=c_nb_asso_event + 1, value=asso.work_secr / 100)
    ws.cell(row=l_start_board + 2, column=c_nb_asso_event + 2, value=asso.asso_life_secr / 100)

    set_info(asso.login_tres, l_start_board + 4, ws)
    ws.cell(row=l_start_board + 4, column=c_nb_asso_event, value=asso.comm_tres / 100)
    ws.cell(row=l_start_board + 4, column=c_nb_asso_event + 1, value=asso.work_tres / 100)
    ws.cell(row=l_start_board + 4, column=c_nb_asso_event + 2, value=asso.asso_life_tres / 100)


def exportENAC(asso):
    if (asso.nb_event <= 0):
        print("no event")
    cpy = load_workbook(filename=settings.STATIC_FILES_ROOT + "/template_points_ENACS.xlsx");
    if (cpy == None):
        print("no copy")
        return False
    write = Workbook(cpy)
    ws1 = cpy.active
    if (ws1 == None):
        print("no write copy")
        return False

    ws1.cell(row=1, column=1, value="Nom Association : " + asso.name)

    set_board(asso, ws1)

    pos = l_start_member
    for isMember in asso.ismemberofassociation_set.all():
        if (isMember.user in asso.get_board()):
            continue
        set_info(isMember.user, pos, ws1)
        ws1.cell(row=pos, column=c_nb_asso_event, value=asso.nb_event)
        ws1.cell(row=pos, column=c_nb_event_pres, value=isMember.nb_participation)
        pos += 1

    cpy.save(settings.MEDIA_ROOT + "files/points_ENACS" + "_" + asso.name + ".xlsx")
    return True

