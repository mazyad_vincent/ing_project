/**
 ** \file ast/record-exp.hxx
 ** \brief Inline methods of ast::RecordExp.
 */

#pragma once

#include <ast/record-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  inline const typename RecordExp::fieldinits_type* RecordExp::get_vect() const
  {
    return vect_;
  }

  inline typename RecordExp::fieldinits_type* RecordExp::get_vect()
  {
    return vect_;
  }

  inline const NameTy& RecordExp::get_type() const
  {
    return *type_;
  }

  inline NameTy& RecordExp::get_type()
  {
    return *type_;
  }

  inline void RecordExp::push_front(FieldInit* field)
  {
    vect_->insert(vect_->begin(), field);
  }
} // namespace ast

