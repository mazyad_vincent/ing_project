/**
 ** \file bind/binder.cc
 ** \brief Implementation for bind/binder.hh.
 */

#include <ast/all.hh>
#include <bind/binder.hh>

#include <misc/contract.hh>

namespace bind
{

  /*-----------------.
  | Error handling.  |
  `-----------------*/

  /// The error handler.
  const misc::error&
  Binder::error_get() const
  {
    return error_;
  }

  // FIXME: Some code was deleted here (Error reporting).


  /*----------------.
  | Symbol tables.  |
  `----------------*/


  void
  Binder::scope_begin()
  {
  // CHANGE:FIXME
    scope_map_.scope_begin();
    scope_type_.scope_begin();
    scope_func_.scope_begin();
  }

  void
  Binder::scope_end()
  {
  // CHANGED:FIXME
    scope_map_.scope_end();
    scope_type_.scope_end();
    scope_func_.scope_end();

  }

  /*---------.
  | Visits.  |
  `---------*/

  // FIXME: Some code was deleted here.
  /* get value and key and add them in scope */
  void
  Binder::operator()(ast::LetExp& e)
  {
    // FIXME: Some code was deleted here.
    scope_begin();
    super_type::operator()(e);
    scope_end();
  }


  /*-------------------.
  | Visiting VarDecs.  |
  `-------------------*/
  void
  Binder::operator()(ast::VarDecs& e)
  {
    decs_visit<ast::VarDec>(e);
  }
  // FIXME

  void
  Binder::operator()(ast::VarDec& e)
  {
    scope_map_.put(e.name_get(), &e);
  }


  /*------------------------.
  | Visiting FunctionDecs.  |
  `------------------------*/
  void
  Binder::operator()(ast::FunctionDecs& e)
  {
     decs_visit<ast::FunctionDec>(e);
  }

  void
  Binder::operator()(ast::FunctionDec& e)
  {
    scope_func_.put(e.name_get(), &e);
  }

  // FIXME

  /*--------------------.
  | Visiting TypeDecs.  |
  `--------------------*/
  void
  Binder::operator()(ast::TypeDecs& e)
  {
    decs_visit<ast::TypeDec>(e);
  }

  void
  Binder::operator()(ast::TypeDec& e)
  {
  //  if (scope_type_.get(e.name_get()))
    // redefinition(scope_type_.get(e.name_get()), &e);
   //super_type::operator()(e);
   scope_type_.put(e.name_get(), &e);
  }


  void
  Binder::operator()(ast::SimpleVar& e)
  {
    auto it = scope_map_.get(e.name_get());
    if (!it)
      undeclared(e.name_get(), e);
    else
      e.def_set(it);
  }

  void
  Binder::operator()(ast::WhileExp& e)
  {
    scope_begin();
    loop_.push_back(&e);
    super_type::operator()(e);
    loop_.pop_back();
    scope_end();
  }

  void
  Binder::operator()(ast::ForExp& e)
  {
    scope_begin();
    loop_.push_back(&e);
    super_type::operator()(e);
    loop_.pop_back();
    scope_end();
  }

  void
  Binder::operator()(ast::NameTy& e)
  {
    auto it = scope_type_.get(e.name_get());
    if (!it
    && e.name_get() != "int" && e.name_get() != "string")
      undeclared(e.name_get(), e);
    else
      e.def_set(it);
  }

  void
  Binder::operator()(ast::BreakExp& e)
  {
    if (loop_.size())
      e.def_set(loop_.at(loop_.size() - 1));
    else
      error(e, "Break outside of loop");
  }

  void
  Binder::operator()(ast::CallExp& e)
  {
    auto it = scope_func_.get(e.get_func());
    if (!it)
     undeclared(e.get_func(), e);
    else
     e.def_set(it);
  }

  // FIXME

} // namespace bind
