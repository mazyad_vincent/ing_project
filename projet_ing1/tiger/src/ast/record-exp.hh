/**
 ** \file ast/record-exp.hh
 ** \brief Declaration of ast::RecordExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/field-init.hh>
#include <ast/name-ty.hh>
#include <ast/fwd.hh>

namespace ast
{

  /// RecordExp.
  class RecordExp : public Exp
  {
  // FIXME: Some code was deleted here.
     public:

      using fieldinits_type = std::vector<FieldInit*>;
      RecordExp(const Location& location, NameTy* type,
               fieldinits_type* vect);
      RecordExp(const RecordExp&) = delete;
      RecordExp& operator=(const RecordExp&) = delete;
      virtual ~RecordExp();
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      const fieldinits_type* get_vect() const;
      fieldinits_type* get_vect();
      const NameTy& get_type() const;
      NameTy& get_type();
      void push_front(FieldInit* field);

     protected:
      fieldinits_type* vect_;
      NameTy* type_;
  };

} // namespace ast

#include <ast/record-exp.hxx>

