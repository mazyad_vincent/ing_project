#ifndef MEMORY_LIST_H
# define MEMORY_LIST_H

struct mem_node
{
  void *data;
  struct mem_node *next;
};

struct mem_list
{
  size_t size;
  struct mem_node *head;
};


struct mem_list *init_mem();
void push_mem(struct mem_list *list, void *data);
void pop_mem(struct mem_list *list);
void clear_mem(struct mem_list *list);
void destroy_mem(struct mem_list *list);

void my_err(char *error);

# endif
