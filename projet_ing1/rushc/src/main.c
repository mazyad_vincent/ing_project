#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "generation_map.h"

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600

static void put_end(struct map *map)
{
  int x = map->width;
  int y = map->height;
  int randy = random_two_numb(y, 1);
  map->pos[randy][x] = END;
  map->end.x = x;
  map->end.y = randy;
}

static SDL_Window* window_init()
{
  SDL_Window *gWindow = NULL;
  if(SDL_Init(SDL_INIT_VIDEO) < 0)
    printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
  else
  {
    if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
      printf("Warning: Linear texture filtering not enabled!");
    gWindow = SDL_CreateWindow("SDL Tutorial",
              SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
              SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if(gWindow == NULL)
      printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
  }
  return gWindow;
}

static SDL_Renderer* render_init(SDL_Window *gWindow)
{
  SDL_Renderer *renderer = NULL;
  renderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED |
      SDL_RENDERER_PRESENTVSYNC);
  if(renderer == NULL)
    printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
  else
  {
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    int imgFlags = IMG_INIT_PNG;
    if(!(IMG_Init(imgFlags)))
      printf("SDL_image could not initialize! SDL_image Error: %s\n",
            IMG_GetError());
  }
  return renderer;
}

int main(void)
{
  struct map *map = map_init(); /* initialise a map */
  struct player *player = init_start(map); /* put the player into the map */
  SDL_Window* wind = window_init();
  SDL_Renderer *rend = render_init(wind);
  while (1)
  {
    SDL_RenderPresent(rend);
  }
  put_end(map);
  /*for (int i = 0; i < 3; ++i)
  {
    add_decoy(map);
    generate_IA(map);
  }
  FILE *file = create_file(map);
  fclose(file);
  free_all(player, map);*/
}
