/** \file misc/scoped-map.hxx
 ** \brief Implementation of misc::scoped_map.
 */

#pragma once

#include <sstream>
#include <stdexcept>
#include <type_traits>
#include <utility>

#include <misc/algorithm.hh>
#include <misc/contract.hh>
#include <misc/indent.hh>

#include "misc/scoped-map.hh"

namespace misc
{
  template <typename Key, typename Data>
  scoped_map<Key, Data>::scoped_map()
  {
    stack_.push_back(std::map<Key, Data>{});
  }
  // CHANGED:FIXME
  template <typename Key, typename Data>
  void scoped_map<Key, Data>::put (const Key& key, const Data& value)
  {
    int index = stack_.size() - 1;
    //stack_[index].insert(std::pair<Key, Data>(key, value));
    misc::put(stack_[index], key, value);
  }

  template <typename Key, typename Data>
  Data scoped_map<Key, Data>::get (const Key& key) const
  {
    int index = stack_.size() - 1;
    auto it = stack_[index].find(key);
    if (it == stack_[index].end())
    {
      if constexpr (!(std::is_pointer_v<Data>)) // constexpr removed
        throw std::invalid_argument("Data not a pointer");
      else
        return nullptr;
    }
    else
      return it->second;
  }

  template <typename Key, typename Data>
  std::ostream& scoped_map<Key, Data>::dump (std::ostream& ostr) const
  {
    for (auto i = stack_.begin(); i != stack_.end(); ++i)
      ostr << i->first << " --> " << i->second->name_get() << "\n";
    return ostr;
  }

  template <typename Key, typename Data>
  void scoped_map<Key, Data>::scope_begin()
  {
    // Add value from the previous scope in the new scope
    int index = stack_.size() - 1;
    auto  map_add = stack_[index];
    stack_.push_back(map_add);
  }

  template <typename Key, typename Data>
  void scoped_map<Key, Data>::scope_end()
  {
    if (stack_.size() > 0)
      stack_.pop_back();
  }

  template <typename Key, typename Data>
  inline std::ostream&
  operator<<(std::ostream& ostr, const scoped_map<Key, Data>& tbl)
  {
    return tbl.dump(ostr);
  }

  template <typename Key, typename Data>
  std::vector<std::map<Key, Data>> scoped_map<Key,
  Data>::get_vector()
  {
    return stack_;
  }

} // namespace misc
