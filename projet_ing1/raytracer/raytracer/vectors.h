#ifndef VECTOR_H
# define VECTOR_H

#include "parsing.h"

struct vector3* normalize(struct vector3 *vect);
float dot_product(struct vector3 *a, struct vector3 *b);
struct vector3* distance(struct vector3 *a, struct vector3 *b);
struct vector3* cross_product(struct vector3 *a, struct vector3 *b);
struct vector3* add_vect(struct vector3 *a, struct vector3 *b);
struct vector3* add_vect(struct vector3 *a, struct vector3 *b);
struct vector3* const_prod(struct vector3 *a, float b);
float get_d(struct list *v, struct list *vn);
int is_intersection(struct vector3 *ray, struct my_read_svati *svat, struct list
                      *v, struct list *vn);
int solve_equations(struct vector3 *p, struct vector3 *a, struct vector3 *b,
                    struct vector3 *c);
void basic_camera(struct my_read_svati *svat, struct pixel **image);
#endif
