/**
 ** \file ast/let-exp.hxx
 ** \brief Inline methods of ast::LetExp.
 */

#pragma once

#include <ast/let-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  inline const std::vector<Exp*>* LetExp::list_get() const
  {
     return vect_;
  }

  inline std::vector<Exp*>* LetExp::list_get()
  {
     return vect_;
  }

  inline const DecsList& LetExp::dec_get() const
  {
    return *dec_;
  }

  inline DecsList& LetExp::dec_get()
  {
    return *dec_;
  }

  inline void LetExp::push_front(Exp* exp)
  {
    vect_->insert(vect_->begin(), exp);
  }

} // namespace ast

