#include "operators.h"
#include "execute.h"
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

int is_operator(const char *key)
{
	if (!strcmp(key, "&&") || !strcmp(key, "||") || !strcmp(key, ">") ||
     !strcmp(key, "<") || !strcmp(key, "<<") || !strcmp(key, ">>")
     || !strcmp(key, "|"))
		return 1;
	return 0;
}

void swap_ast(struct ast *ast)
{
  struct ast *copy = ast;
  while (copy && copy->left && (!strcmp(copy->left->key, ">") ||
         !strcmp(copy->left->key, ">>")))
      copy = copy->left;
  if (!copy)
    return;
  if (!strcmp(copy->key, ">") || !strcmp(copy->key, ">>"))
  {
    struct ast *copy_right = copy->right;
    copy->right = ast->right;
    ast->right = copy_right;
  }
}

int call_operator(struct ast *ast)
{
	if (!strcmp(ast->key, "&&"))
		return operator_and(ast);
	else if (!strcmp(ast->key, "||"))
	  return operator_or(ast);
  else if (!strcmp(ast->key, "<"))
    return left_redirection(ast);
  else if (!strcmp(ast->key, ">"))
    return right_redirection(ast, 0);
  else if (!strcmp(ast->key, ">>"))
    return right_redirection(ast, 1);
  else if (!strcmp(ast->key, "<<"))
    return double_left_redirection(ast);
  else if (!strcmp(ast->key, "|"))
    return pipe_rule(ast);
	return 0;
}

int operator_and(struct ast *ast)
{
	if (exec_rec(ast->left))
		return 1; /* error */
	return exec_rec(ast->right);
}

int operator_or(struct ast *ast)
{
	if (!exec_rec(ast->left))
		return 0; /* true */
	return exec_rec(ast->right);
}
