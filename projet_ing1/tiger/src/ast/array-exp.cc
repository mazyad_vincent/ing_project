/**
 ** \file ast/array-exp.cc
 ** \brief Implementation of ast::ArrayExp.
 */

#include <ast/visitor.hh>
#include <ast/array-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  ArrayExp::ArrayExp(const Location& location, NameTy* name, Exp* type, Exp*
                     into)
   : Exp(location)
   , into_(into)
   , type_(type)
   , name_(name)
   {}

   ArrayExp::~ArrayExp()
   {
     delete name_;
     delete into_;
     delete type_;
   }

   void
   ArrayExp::accept(ConstVisitor& v) const
   {
       v(*this);
   }

   void
   ArrayExp::accept(Visitor& v)
   {
       v(*this);
   }

} // namespace ast

