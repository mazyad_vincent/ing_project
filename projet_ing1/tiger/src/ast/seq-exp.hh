/**
 ** \file ast/seq-exp.hh
 ** \brief Declaration of ast::SeqExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/fwd.hh>

namespace ast
{

  /// SeqExp.
  class SeqExp : public Exp
  {
  // FIXME: Some code was deleted here.
     public:

        using exps_type = std::vector<Exp*>;
        SeqExp(const Location& location, SeqExp::exps_type* vect);
        SeqExp(const SeqExp&) = delete;
        SeqExp& operator=(const SeqExp&) = delete;
        virtual ~SeqExp();
        const exps_type* get_vect() const;
        exps_type* get_vect();
        void accept(ConstVisitor& v) const override;
        void accept(Visitor& v) override;
        void push_front(Exp* exp);

     protected:
      SeqExp::exps_type* vect_;
  };

} // namespace ast

#include <ast/seq-exp.hxx>

