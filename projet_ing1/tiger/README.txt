---- TIGER TC4 PROJECT ----

NAME: TC-0 (PTHL): Naive Scanner and Parser

AUTHORS: 
  @de_mar_n
  @mazyad_v
  @belmok_s

DESCRIPTION:
The project is divided in two big parts: a scanner and a parser.
It can be generated with the tools Flex and Bison, which are respectevly a scanner
code generator and a parser code generator.
Of course, no conflict are expected.

Language used: C/C++

BUILD:
In order to build the binary, you can use the makefile generate by Autotools.
Here are the commands:
./bootstrap
./configure
make -j4


If you want to compile the .tig file you can use the command ./tc <.tig file>
It is possible to test command line in interactiv mode, you just have to execute
the command ./tc and then type the command line.

FLEX:

This part enable to use the parser. To generate the lexer we use lex, we manage
all the tokens declared in the file .yy. The sub state enable to choose which 
token we want to recognize and to consider valide, so we use this feature for 
the comment and the strings. In the case where comments are imbricated we have 
a loop to recognize all sub comments. 

BISON:

For Bison, we took each tokens from the flex file, and with it, we created
the tiger syntax grammar. Then, if a token doesn't match with the grammar, 
it means that the user miss syntaxed it file.
We've written an error method to show the user where it failed and why.

TESTSUITE:
You can launch the test suite with the command "make test".
The binary is regenerated and all test are printed.
If "OK" is under the .tig test file, then the test pass otherwise it fail.
It is possible to add test to the test suite by modifying the file test_suite.sh
in the tests/ directory.
