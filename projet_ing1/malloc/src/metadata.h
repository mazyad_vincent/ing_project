#ifndef METADATA_H
# define METADATA_H

#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

struct metadata
{
  short free; /* check if the metadata is free or not 1 = free */
  size_t size; /* size of the block */
  struct metadata *next; /* pointer to the next metadata */
  struct block *blk; /* reference on the block */
};

struct block
{
  size_t size; /* size left of the block */
  struct metadata *head;
  struct block *next;
  struct block *prev; /* prev block */
};

struct block *cur_block; /* current block */

void *cast_void(size_t off, void *ptr);
size_t word_align(size_t val);
struct block *init_block();
struct block *size_block();
struct metadata *alloc(size_t size);
void *malloc(size_t size);
void free(void *ptr);
void *calloc(size_t size, size_t size2);
void *realloc(void *tr, size_t size);


/* linkedlist file */
struct block *get_last_block(struct block *block);
struct metadata *add_meta(struct block *blk, size_t size);
int is_full_free(struct block *head); /* all metadate in block free */

#endif
