/**
 ** \file ast/if-exp.hh
 ** \brief Declaration of ast::IfExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/seq-exp.hh>
#include <ast/name-ty.hh>

namespace ast
{

  /// IfExp./
  class IfExp : public Exp
  {
    public:
      IfExp(const Location& location, Exp* cond, Exp* body, Exp* els);
      IfExp(const IfExp&) = delete;
      IfExp& operator=(const IfExp&) = delete;
      virtual ~IfExp();
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      const Exp* cond_get() const;
      Exp* cond_get();
      const Exp* body_get() const;
      Exp* body_get();
      const Exp* else_get() const;
      Exp* else_get();

    protected:
       Exp* cond_;
       Exp* body_;
       Exp* els_;
 };

} // namespace ast

#include <ast/if-exp.hxx>

