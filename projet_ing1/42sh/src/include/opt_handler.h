#ifndef OPT_HANDLER_H
# define OPT_HANDLER_H

struct options
{
    int option;
    char *command;
};

enum option
{
    SHOP = 1,
    NORC = 2,
    ASTP = 4,
    VERS = 8
};

enum shopts
{
    ASTS = 0, //ast-print
    DOTG,     //dotglob
    EXPA,     //expansion_aliases
    EXTG,     //extglob
    NOCG,     //nocaseglob
    NULG,     //nullglob
    SRCP,     //sourcepath
    XPGE      //xpg_echo
};

struct options *option_handler(int argc, char **argv);

#endif /* !OPT_HANDLER_H */
