#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "opt_commands.h"
#include "call_commands.h"
#include "ast.h"
#include "parser.h"

extern struct global g_global;

int select_exec(struct ast *node)
{
  if (!strcmp(node->key, "&&"))
    return commande_and(node);
  else if (!strcmp(node->key, "||"))
    return boolean_command(node);
  else if (!strcmp(node->key, ";"))
    return separate_command(node);
  else if (!strcmp(node->key, "&"))
    return separate_command(node);
  else if (!strcmp(node->key, "if"))
    return condition_command(node);
  else if (!strcmp(node->key, "while"))
    return while_command(node);
  else if (!strcmp(node->key, "|"))
    return pipe_command(node);
  else if (!strcmp(node->key, ">"))
    return redirection_right(node);
  else if (!strcmp(node->key, "<"))
    return redirection_left(node);
  else if (!strcmp(node->key, ";"))
    return separate_command(node);
  else if (!strcmp(node->key, "for"))
    return for_command(node);
  return -1;
}
