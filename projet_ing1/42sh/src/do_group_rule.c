/**
** \file do_group_rule.c
** \brief This file contain the do_group rule of the parser
** \author Odile Guillaume
**
** This file contains do_group of the LL grammar for the parser.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

/**
** \fn struct ast *p_do_group(void)
** \brief reprensents the do_group rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_do_group(void)
{
  struct ast *curr = curr_tok();
  if (!curr || curr->type != DO)
    return free_node(curr);
  eat();
  struct ast *node = p_compound_list();
  if (!node)
    return free_node(curr);
  node = ast_create(node, curr, NULL, NULL);
  curr = curr_tok();
  if (!curr || curr->type != DONE)
  {
    ast_free(node);
    return free_node(curr);
  }
  eat();
  node = ast_create(curr, node, NULL, NULL);
  return node;
}
