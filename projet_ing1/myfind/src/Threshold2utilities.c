#include "Threshold.h"

int is_expression(int argc, char **path, struct fifo *options, 
                  struct fifo *expressions)
{
    int i = 1;
    while (i < argc && path[i][0] == '-')
        i++;
    for (; i < argc; ++i)
    {
        if (path[i][0] && path[i][0] == '-')
        {
            if (my_strmcpy(path[i], "-name") || my_strmcpy(path[i], "-type"))        
               return 1;
            else
            {   
                fprintf(stderr, "%s %s\n", "myfind: cannot do -opt:"
                       ,"unvalid option");
                clean_all(options);
                clean_all(expressions);
                exit(1);
            }
        }
    }
    return 0;
}

int is_expression_arg(int i, char **path)
{
    if (my_strmcpy(path[i], "-name") || my_strmcpy(path[i], "-type"))
        return 1;
    return 0;
}

void put_expression(struct fifo *fifo, struct fifo *options, 
                    char **path, int argc)
{
    int i = 1;
    for (; i < argc && !is_expression_arg(i, path); ++i);
    while (i < argc)
    {
        char *opt = new_concatenation("", path[i]);
        fifo_push(fifo, opt);
        if (!path[i+1])
        {
          fprintf(stderr, "%s %s\n", "myfind: cannot do -name opt:"
                 ,"no name given");
          clean_all(options);
          clean_all(fifo);
          exit(1);
        }

        char *name = new_concatenation("", path[i+1]); 
        fifo_push(fifo, name);
        i+=2;
    }
}
