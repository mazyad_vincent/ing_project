#include "Threshold.h"

void empty_size(struct fifo *options, struct fifo *expressions, 
                struct stat st, char *path)
{
    if (options->size == 0)   //no options
    {
        if (S_ISREG(st.st_mode)) //regular files
        {
            int fd = open(path, O_RDONLY);
            if (fd == -1)
            {
                fprintf(stderr, "%s %s\n", "myfind: cannot do open_file:" 
                        , "the file may not exist or is not valid.");
                clean_all(options);
                clean_all(expressions);
                exit(1);
            }
            printf("%s\n", path);
            close(fd);
        }
        else if (S_ISLNK(st.st_mode))
            printf("%s\n", path);
        else if (S_ISDIR(st.st_mode))
        {
            char *copy = malloc(sizeof(char) * my_strlen(path) + 1);
            copy = copy_string(copy, path);
            copy = print_dir(copy);
            free(copy);
        }
    }
}

void with_size(struct fifo *options, struct stat st, char *path)
{
    if (options->size)
    {
        struct list *list_first = fifo_get(options);
        char *copy = malloc(sizeof(char) * (my_strlen(path) + 1));
        if (!copy)
        {
            clean_all(options);
            return;
        }
        copy = copy_string(copy, path);
        if (fifo_find(options, "d")) //if option -d
        {
            struct queue *queue = queue_init();
            if (options->size == 1) //only d
                redirect_opt('d', copy, queue, st);
            else
                redirect_opt(list_first->data[0], copy, queue, st);
            queue_destroy(queue);
        }
        else 
            redirect_flag(list_first->data, copy, st);
    }
}

void redirect_flag(char *c, char *copy,  struct stat st)
{
    if (my_strmcpy(c, "H"))
    {
        if (is_symb(copy) || S_ISDIR(st.st_mode))
           copy =  print_dir(copy);
        else if (S_ISREG(st.st_mode))
            printf("%s\n", copy);
    }
    else if (my_strmcpy(c, "L"))
    {
        if (is_symb(copy) || S_ISDIR(st.st_mode))
            copy = print_dir_syml(copy);
        else if (S_ISREG(st.st_mode))
            printf("%s\n", copy);
    }
    else if (my_strmcpy(c, "P"))
    {
        if (S_ISDIR(st.st_mode))
           copy = print_dir(copy);
        else if (S_ISREG(st.st_mode) || is_symb(copy))
            printf("%s\n", copy);
    }
    free(copy);
}


void no_arg(struct fifo *options, struct fifo *expressions, 
            struct stat st, int argc, char **argv)
{

    if (!is_arg(argc, argv)) // options without arg
    {
        struct list *list_first = fifo_get(options);
        char *copy = malloc(sizeof(char) + 1);
        copy = create_arg(copy); 
        if (lstat(copy, &st) == -1)
        {
            fprintf(stderr, "%s %s\n", "myfind: cannot do open_file:" 
                    , "the file or directory may not exist or is not valid.");
            clean_all(options);
            clean_all(expressions);
            exit(1);
        }
        if (fifo_find(options, "d"))
        {
            struct queue *queue = queue_init();
            if (options->size == 1) //only d
                redirect_opt('d', copy, queue, st);
            else
                redirect_opt(list_first->data[0], copy, queue, st);
            queue_destroy(queue);
        }
        else 
            redirect_flag(list_first->data, copy, st);
        free(copy); 
    }
}
