#!/bin/sh

cd ~
cd `mktemp -d`
cmake ~/afs/projets/cendri_n-formula-one/
make formulaone
make check
echo "===== SIMPLE MAPS ====="
echo " * MY_TEST *"
echo "   $(./check ~/afs/projets/cendri_n-formula-one/tests/test.frc)"
echo " * LVL1_SIMPLE *"
echo "   $(./check ~/afs/projets/cendri_n-formula-one/tests/lvl1_simple.frc)"
echo " * LVL1_SIN *"
echo "   $(./check ~/afs/projets/cendri_n-formula-one/tests/lvl1_sin.frc)"
echo " * LVL_DIAGONAL *"
echo "   $(./check ~/afs/projets/cendri_n-formula-one/tests/lvl1_diagonal.frc)"
