#include <err.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <curl/curl.h>
#include "tool_parse.h"
#include "tool_tracker.h"

char *port()
{
  char *por = malloc(sizeof(char) * 11);
  if (!por)
    errx(1, "fail allocation");
  por = memcpy(por, "&port=6881&", 11);
  return por;
}

char *get_peer_id() //peerid
{
    char *peer = malloc(sizeof(char) * 29);
    if (!peer)
      errx(1, "fail allocation");
    peer = memcpy(peer, "peer_id=-MB2020-", 29);
    int alph = 97;
    for (int i = 16; i < 28; ++i)
    {
        peer[i] = alph;
        alph++;
    }
    peer[28] = '&';
   return peer;
}

char *get_url(struct b_data *bdict)
{
    struct b_dict_list_node *copy = bdict->my_value->my_b_dict_list->head;
    while(copy && strcmp(copy->my_key, "announce") != 0)
        copy = copy->next;
    if (!strcmp(copy->my_key, "announce"))
    {
        char *url = malloc(sizeof(char) * copy->data->length);
        if (!url)
            errx(1, "fail allocation");
        url = memcpy(url, copy->data->my_value->string, copy->data->length);
        url = concatenation(url, "?", copy->data->length, 1);
        return url;
    }
    return NULL;
}

char* decode(char *encrypted, size_t length_info)
{
    unsigned char md_value[SHA_DIGEST_LENGTH];
    SHA1((unsigned char*)encrypted, length_info, md_value);
    char *x = malloc(sizeof(char) * 20);  
    if (!x)
      errx(1, "fail allocation");
    x = memcpy(x, md_value, 20);
    return x;
}
char *full_hash(char *infohash)
{
  char *full_hash = malloc(sizeof(char) * 10);
  full_hash = memcpy(full_hash, "info_hash=", 10);
  full_hash = concatenation(full_hash, infohash, 10, strlen(infohash));
  return full_hash;
}

char *opts_tra()
{
  char *opt = malloc(sizeof(char) * 34);
  if (!opt)
    return NULL;
  memcpy(opt, "left=0&downloaded=100&uploaded=0&", 34);
  return opt;
}

char* get_compact()
{
  char *comp = malloc(sizeof(char) * 10);
  if (!comp)
    return NULL;
  comp = memcpy(comp, "compact=1", 10);
  return comp;
}
