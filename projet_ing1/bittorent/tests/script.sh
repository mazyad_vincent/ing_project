#!/bin/sh

old_IFS=${IFS} IFS='\n'

for torrent in ./torrents/*.torrent
do
  ./../my-bittorrent --pretty-print-torrent-file "${torrent}" > "${torrent}_print.txt"
  cat "${torrent}_print.txt" | python -m json.tool
  res=$(echo $?)
  if [ $res -eq 0 ]
  then
    echo "JSON ${torrent}: SUCCESS" >> recap_print.txt
  else
    echo "JSON ${torrent}: FAIL"
  fi
done


cat recap_print.txt
mv recap_print.txt ./result

for results in ./torrents/*_print.txt
do
  mv "${results}" ./result
done

IFS=${old_IFS}
