#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "memory_list.h"

void my_err(char *error)
{
  fprintf(stderr, "%s\n", error);
  exit(1);
}

struct mem_list *init_mem()
{
  struct mem_list *list = malloc(sizeof(struct mem_list));
  if (!list)
    my_err("Can't malloc mem_list");
  list->size = 0;
  list->head = NULL;
  return list;
}

void push_mem(struct mem_list *list, void *data)
{
  struct mem_node *node = malloc(sizeof(struct mem_node));
  if (!node)
    my_err("Can't malloc mem_node");
  node->data = data;
  struct mem_node *tmp = list->head;
  list->head = node;
  node->next = tmp;
  list->size += 1;
}

void pop_mem(struct mem_list *list)
{
  if (!list)
    return;
  if (!list->head)
    return;
  struct mem_node *tmp = list->head;
  list->head = tmp->next;
  tmp->next = NULL;
  free(tmp->data);
  free(tmp);
  list->size -= 1;
}

void clear_mem(struct mem_list *list)
{
  if (!list)
    return;
  while (list->size > 0)
    pop_mem(list);
}

void destroy_mem(struct mem_list *list)
{
  if (!list)
    return;
  clear_mem(list);
  free(list);
}
