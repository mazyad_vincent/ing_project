 /**
 ** \file export.c
 ** \brief This file regroups the exec of builtin export
 ** \author Mazyad Vincent
 **
 ** This file regroups the export builtin operations
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "ast.h"
#include "call_commands.h"
#include "builtins.h"

extern char **environ;

/**
** \fn char* fiill_char(char *data)
** \brief return the char* filled with data
**
** \param char* 
** \return return the string
*/

char* fill_char(char *data)
{
   char *to_return = NULL;
   int i = 0;
   for (; data[i] && data[i] != '='; i++);
   to_return = malloc(sizeof(char) * (i + 1));
   if (!to_return)
      return NULL;
   strncpy(to_return, data, i);
   to_return[i] = '\0';
   return to_return;
}

/**
** \fn int export(struct ast *ast)
** \brief represents the exec of export builtin
**
** \param *ast
** \return return 0 if well executed 1 if not
*/

int export_builtin(struct ast *ast)
{
   size_t numbers = nb_ast_elements(ast->left);
   if (!numbers)
      return 0;
   if (numbers == 1)
   {
      if (!strcmp(ast->left->key, "-p"))
      {
        for (int i = 0; environ[i]; i++)
          printf("%s\n", environ[i]);
      }
      else
      {
        char *data = strdup(ast->left->key);
        char *before_eq = fill_char(data);
        int i = 0;
        while (data[i] && data[i] != '=')
            i++;
        char *after_eq = fill_char(data + i + 1);
        int res = setenv(before_eq, after_eq, 1);
        free(after_eq);
        free(before_eq);
        free(data);
        return res;
      }
   }
   if (numbers == 2 && !strcmp(ast->left->key, "-n"))
   {
        char *data = ast->left->left->key;
        return unsetenv(data);
   }
   return 1;
}
