#include <stdio.h>
#include <math.h>
#include "control.h"
#include "queue.h"

float find_angle(struct vector2 *src, struct vector2 *dst)
{
    float srclen = sqrt(pow(src->x, 2) + pow(src->y, 2));
    float dstlen = sqrt(pow(dst->x, 2) + pow(dst->y, 2));
    float cos = (src->x * dst->x + src->y * dst->y) / (srclen * dstlen);
    return acos(cos);
}

struct queue *find_pace(struct car *car, struct vector2 *dst)
{
    struct queue *instructions = NULL;
    struct car *clone = car_clone(car);
    car_move(clone, ACCELERATE);
    instructions = create_queue(ACCELERATE);
    int clonex = clone->position.x;
    int cloney = clone->position.y;
    int dstx = dst->x;
    int dsty = dst->y;
    while (clonex != dstx && cloney != dsty)
    {
        car_move(clone, ACCELERATE);
        //float speed = sqrt(pow(clone->speed.x, 2) + pow(clone->speed.y, 2));
        struct queue *new_move = create_queue(ACCELERATE);
        instructions = add_queue(instructions, new_move);
        clonex = clone->position.x;
        cloney = clone->position.y;
    }
    car_delete(clone);
    return instructions;
}
