/**
** \file compound_rulec
** \brief This file contains the compound rule of the parser
** \author Odile Guillaume
**
** This file contains the compound rule of the LL grammar for the parser
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

static struct ast *p_compound2(struct ast *node, struct ast *node2,
                               struct ast *curr)
{
  int test = 0;
  while (curr && (curr->type == SEMI || curr->type == AND ||
         curr->type == NEWL))
  {
    if (test)
    {
      ast_free(curr);
      return free_node(node);
    }
    eat();
    struct ast *curr2 = curr_tok();
    while (curr2 && curr2->type == NEWL)
    {
      eat();
      ast_free(curr2);
      curr2 = curr_tok();
    }
    ast_free(curr2);
    node2 = p_and_or();
    if (!node2)
      test = 1;
    node = ast_create(curr, node, NULL, NULL);
    node = ast_create(node2, node, NULL, NULL);
    curr = curr_tok();
  }
  ast_free(curr);
  return node;
}

/**
** \fn struct ast *p_compound_list(void)
** \brief reprensents the compound list rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_compound_list(void)
{
  struct ast *curr = curr_tok();
  while (curr && curr->type == NEWL)
  {
    eat();
    ast_free(curr);
    curr = curr_tok();
  }
  struct ast *node = p_and_or();
  if (!node)
    return free_node(curr);
  ast_free(curr);
  curr = curr_tok();
  if (!curr)
    return free_node(node);
  struct ast *node2 = NULL;
  node = p_compound2(node, node2, curr);
  return node;
}
