#include "parse.h"

extern struct global g_global;

static enum type get_type(char *c)
{
	enum type type = 1;
	if (!strcmp(c, "<") || !strcmp(c, "<<") || !strcmp(c, "|") || !strcmp(c, ">")
		 || !strcmp(c, ">>") || !strcmp(c, "&&") || !strcmp(c, "||"))
		type = SPECIAL;
	return type;
}

int check_parse(struct ast *ast)
{
	if (!ast)
		return 0;
  if (get_type(ast->key) == SPECIAL && (!ast->right || !ast->left)) /* no args after special */
  {
    fprintf(stderr, "bash: syntax error\n");
		return 2;
  }
	if (ast->right && get_type(ast->key) == SPECIAL &&
			(get_type(ast->right->key) == SPECIAL || !strcmp(ast->right->key, ";")))
    /* special after special */
	{
		fprintf(stderr, "bash: syntax error\n");
		return 2;
	}
  if (!strcmp(ast->key, ";") && ast->right && (get_type(ast->right->key) ==
      SPECIAL || !strcmp(ast->right->key, ";")))
  {
    fprintf(stderr, "bash: syntax error\n");
		return 2;
  }
	int left_res = check_parse(ast->left);
	int right_res = check_parse(ast->right);
	return (!left_res && !right_res)? 0 : 2;
}

struct ast *parse_input(struct ast *ast, char *input)
{
	char *buf = get_next_word(input);
	if (!buf || !ast)
		return g_global.ast;
	enum type type = get_type(buf);
	struct ast *new_ast = init_ast(buf);
	if (type == WORD && strcmp(buf, ";"))
	{
	  insert_node(&ast, new_ast);
		parse_input(ast, input);
	}
	else if (type == SPECIAL || !strcmp(buf, ";"))
	{
		new_ast->left = g_global.ast;
		new_ast->right = init_ast(get_next_word(input));
		g_global.ast = new_ast;
		parse_input(new_ast->right, input);
	}
	return g_global.ast;
}
