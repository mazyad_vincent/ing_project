/**
** \file hashtable.c
** \brief This file init an hashtable and a list
** \author Mazyad Vincent
**
** This file regroups the informations needed for an hashtable
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "hashtable.h"

/**
** \fn struct hashtable hash_init()
** \brief init an hashtable

** \return return an empty hashtable
*/

struct hashtable* hash_init()
{
    struct hashtable *hash = malloc(sizeof(struct hashtable));
    if (!hash)
      return NULL;
    hash->capacity = 30;
    hash->list = malloc(sizeof(struct list*) * hash->capacity);
    for (size_t i = 0; i < hash->capacity; i++)
      hash->list[i] = NULL;
    return hash;
}

/**
** \fn struct list* list_init(size_t indice, char *data, char *key, int a)
** \brief init a list with parameters

** \return return an empty list
*/

struct list* list_init(size_t indice, char *data, char *key, int alias)
{
    struct list *list = malloc(sizeof(struct list));
    if (!list)
      return NULL;
    list->data = data;
    list->indice = indice;
    list->key = key;
    list->next = NULL;
    list->alias = alias;
    return list;
}

/**
** \fn struct list* list_push(struct list *list, struct list *list_add)
** \brief push into the list

** \return return the head of the list
*/

struct list* list_push(struct list *list, struct list *list_add)
{
   if (!list)
      return list_add;
   struct list *copy = list;
   while (copy->next)
      copy++;
   copy->next = list_add;
   return list;
}

/**
** \fn void free_list(struct list *list)
** \brief free the list
** \return free the list
*/

void free_list(struct list *list)
{
  while (list)
  {
     struct list *to_free = list;
     list = list->next;
     free(to_free->data);
     free(to_free->key);
     free(to_free);
  }
}

/**
** \fn void free_hash(struct hashtable *hash)
** \brief free the hast
*/

void free_hash(struct hashtable *hash)
{
   for (size_t i = 0; i < hash->capacity; i++)
   {
      if (hash->list[i])
         free_list(hash->list[i]);
   }
   free(hash->list);
   free(hash);
}
