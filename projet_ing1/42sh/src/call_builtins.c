/**
** \file call_builtins.c
** \brief This file regroups the operations for builtin
** \author Mazyad Vincent
**
** This file regroups the informations needed for builtins
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "ast.h"
#include "builtins.h"
#include "parser.h"
#include "replace_value.h"

extern struct global g_global;

/**
** \fn int is_builtins(struct ast *ast)
** \brief check if this is a builtin
**
** \param struct ast *ast
** \return return 1 if builtin 0 if not
*/

int is_builtins(struct ast *ast)
{
  if (!strcmp(ast->key, "cd"))
     return 1;
  if (!strcmp(ast->key, "exit"))
     return 1;
  if (!strcmp(ast->key, "export"))
     return 1;
  if (!strcmp(ast->key, "unalias"))
     return 1;
  if (!strcmp(ast->key, "alias"))
     return 1;
  if (!strcmp(ast->key, "echo"))
     return 1;
  if (!strcmp(ast->key, "continue"))
     return 1;
  if (!strcmp(ast->key, "break"))
     return 1;
  if (is_equal_operation(ast) && parse_equal(ast->key))
     return 1;
  return 0;
}

/**
** \fn int call_builtins(struct ast *ast)
** \brief run the correct builtin function
**
** \param struct ast *ast
** \return return if the exec of builtin was ok else 0
*/

int call_builtins(struct ast *ast)
{
  if (!strcmp(ast->key, "cd"))
     return cd_command(ast);
  if (!strcmp(ast->key, "exit"))
     exit_builtin(ast);
  if (!strcmp(ast->key, "export"))
     return export_builtin(ast);
  if (!strcmp(ast->key, "unalias"))
     return unalias_builtin(g_global.hash, ast->left);
  if (!strcmp(ast->key, "echo"))
     return echo_builtin(ast);
  if (!strcmp(ast->key, "alias"))
  {
     if (ast->left)
        return put_string_in_hash(g_global.hash, ast->left->key, 1);
  }
  if (is_equal_operation(ast) && parse_equal(ast->key))
     return put_string_in_hash(g_global.hash, ast->key, 0);
  return 0;
}
