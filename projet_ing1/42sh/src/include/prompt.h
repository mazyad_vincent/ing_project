#ifndef PROMPT_H
# define PROMPT_H

/**
** \file prompt.h 
** \brief contains all fonctions for the prompt
**
** \author Mazyad Vincent
*/

struct hashtable *hash;
char *prompt_ps2(char *buf);/* ps2 */
int is_separator(char *buf); /* if there is a separator in the string */
int is_incomplete(char *a); /* if there is a condition for ps2 */
char *white_spaces(char *buffer);
int parse_equal(char *buffer); /* check if this a value command */

#endif
