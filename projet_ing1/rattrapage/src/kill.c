#include "ast.h"
#include "builtin.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

static int is_correct_builtin(struct ast *ast)
{
  
  size_t nb_ast = 0;
  for (struct ast *copy = ast; copy ; copy = copy->left)
    nb_ast++;
  if (nb_ast == 1) /* no signal must be specified */
  {
    if (ast->left->key[0] == '-')
    {
      fprintf(stderr, "bash: kill: %d : indication de signal non valable\n", 
              atoi(ast->left->key + 1));
      return(0);
    }
  }
  return 1;
}

static int correct_pid(char *key)
{
  for (size_t i = 0; key[i]; ++i)
  {
    if (key[i] < 48 || key[i] > 57)
    {
      fprintf(stderr, "kill: argument not correct\n");
      return 0;
    }
  }
  return 1;
}

int kill_builtin(struct ast *ast)
{
  if (!ast->left)
  {
    fprintf(stderr, "kill : utilisation : kill [-s sigspec | -n signum |-sigspec] pid | jobspec ... ou kill -l [sigspec]\n");
    return(2);
  }
  if (!is_correct_builtin(ast))
    return 1;
  int signal = (ast->left->key[0] == '-')? atoi(ast->left->key + 1) : 15; /* signal */
  if (ast->left->key[0] == '-')
    ast = ast->left;
  ast = ast->left;
  int res = 0;
  while (ast)
  {
    if (!correct_pid(ast->key))
      return 1;
    res = kill(atoi(ast->key), signal);
    if (res == -1)
    {
      perror("kill");
      return(1);
    }
    ast = ast->left; 
  }
  return 0; 
}
