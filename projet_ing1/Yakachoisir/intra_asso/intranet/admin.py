from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from intranet import models

class AssoGAdmdin(GuardedModelAdmin):
    pass


class IsMemberGAdmdin(GuardedModelAdmin):
    pass


class ClubGAdmdin(GuardedModelAdmin):
    pass


class DemandeCreationGAdmdin(GuardedModelAdmin):
    pass


class DemandeBudgetGAdmdin(GuardedModelAdmin):
    pass

class DeclarationDepenseGAdmin(GuardedModelAdmin):
    pass


class IsApplyingGAdmdin(GuardedModelAdmin):
    pass



# Register your models here.
admin.site.register(models.Association, AssoGAdmdin)
admin.site.register(models.IsMemberOfAssociation, IsMemberGAdmdin)
admin.site.register(models.Club, ClubGAdmdin)
admin.site.register(models.DemandeCreationAssociation, DemandeCreationGAdmdin)
admin.site.register(models.DemandeDeBudget, DemandeBudgetGAdmdin)
admin.site.register(models.IsApplyingTo, IsApplyingGAdmdin)
admin.site.register(models.Reunion)
admin.site.register(models.Fichier)
admin.site.register(models.ResponsableAsso)
admin.site.register(models.DeclarationDepense)