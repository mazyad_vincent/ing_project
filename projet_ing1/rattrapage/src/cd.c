#include "builtin.h"
#include "parse.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>

static int is_directory(char *ptr) /* 1 = fail 0 = works */
{
	struct stat sb;
	if (stat(ptr, &sb))
		 return 1;
	if (S_ISDIR(sb.st_mode) && !access(ptr, X_OK))
		 return 0;
	return 1;
}

int cd_builtin(struct ast *ast)
{
	size_t i = 0;
	char *path = ast->key;
	for (struct ast *copy = ast; copy && strcmp(copy->key, ";"); 
			copy = copy->left)
		i++;
	if (i > 1)
	{
		fprintf(stderr, "cd: too many args\n");
		return 1; /* too many args */
	}
	if (is_directory(path))
	{
		fprintf(stderr, "cd: %s might not be a directory\n", path);
		return 1;
	}
	int res = chdir(path);
	if (res)
		perror("cd :");
	return (!res)? 0 : 1; /* res = 0 if worked otherwise fails */
}
