/**
** \file commands_loop.c
** \brief This file regroups the commands for the while loop and if call
** \author Mazyad Vincent
**
** This file regroups the informations needed for exec and if and while loop
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include "ast.h"
#include "call_commands.h"
#include "opt_commands.h"
#include "builtins.h"

/**
** \fn int exec_while(struct ast *ast)
** \brief do the exec for the do in while
**
** \param struct ast *ast
** \return return 0 if the exec was well done else 1
*/

int exec_while(struct ast *ast) /* from do to done */
{
    int size = 0;
    while (ast && strcmp(ast->key, "do"))
      ast = ast->left;
    if (!ast)
    {
      fprintf(stderr, "while, error syntax \n");
      exit(1);
    }
    struct ast *copy = ast->left;
    if (is_builtins(copy))
      return call_builtins(copy);
    while (copy && strcmp(copy->key, "done"))
    {
      copy = copy->left;
      if (strcmp(copy->key, ";"))
        size++;
    }
    char *tab[size + 1];
    tab[size] = NULL;
    ast = ast->left;
    for (int i = 0; ast && strcmp(ast->key, "done"); ++i)
    {
      if (strcmp(ast->key, ";"))
         tab[i] = ast->key;
      ast = ast->left;
    }
    if (!ast)
    {
      fprintf(stderr, "while, error syntax \n");
      exit(1);
    }
    return execute_cmd(tab);
}

/**
** \fn int condition_while(struct ast *ast)
** \brief do the exec for while
**
** \param struct ast *ast
** \return return 0 if the exec was well done else 1
*/

int condition_while(struct ast *ast) /* from condition while to do */
{
  struct ast *copy = ast;
  if (is_builtins(copy))
    return call_builtins(copy);
  int size = 0;
  while (copy && strcmp(copy->key, "do"))
  {
    if (strcmp(copy->key, ";"))
      size ++;
    copy = copy->left;
  }
  if (!copy)
  {
    fprintf(stderr, "while, error syntax \n");
    exit(1);
  }
  char *tab[size + 1];
  tab[size] = NULL;
  for (int i = 0; ast && strcmp(ast->key, "do"); ++i)
  {
    if (strcmp(ast->key, ";"))
      tab[i] = ast->key;
    ast = ast->left;
  }
  return execute_cmd(tab);
}

/**
** \fn int bitwise_command(struct ast *ast)
** \brief do the for command
**
** \param struct ast *ast
** \return return 0 if the exec was well done else 1
*/

int for_command(struct ast *ast)
{
  return while_command(ast);
}

/**
** \fn int condition_command(struct ast *ast)
** \brief do the if command
**
** \param struct ast *ast
** \return return 0 if the exec was well done else 1
*/

int condition_command(struct ast *ast) /* ex if command */
{
  return fill_tab(ast, 1);
}

/**
** \fn int while_command(struct ast *ast)
** \brief do the entire while command
**
** \param struct ast *ast
** \return return 0 if the exec was well done else 1
*/

int while_command(struct ast *ast) /* while command */
{
  return command_while_operation(ast);
}
