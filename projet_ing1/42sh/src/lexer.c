/**
** \file lexer.c
** \brief This file is the lexer of the 42sh
** \author Odile Guillaume
**
** This file is the lexer of the 42sh. It regroups functions that get the token,
** comvert it into simple enum, eat parsed chars.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"
#include "call_commands.h"

extern struct global g_global;

/**
** \fn int is_special(char *command, int i)
** \brief Tell wether if the next token is a special one or not
**
** \param command: string of the command
** \param i: index in the string command
** \return len of the special tok
*/

int is_special(char *command, int i)
{
  if (!command)
    return 0;
  int res = 0;
  if  (command[i] == '&' || command[i] == '|' || command[i] == ';' ||
       command[i] == '(' || command[i] == ')' || command[i] == '{' ||
       command[i] == '}' || command[i] == '<' || command[i] == '>' ||
       command[i] == '!')
  {
    res++;
    i++;
  }
  if (command[i] == '&' || command[i] == '|' || command[i] == ';' ||
      command[i] == '<' || command[i] == '>')
  {
    res++;
    i++;
  }
  if (res && command[i] == '-')
    res++;
  return res;
}

/**
** \fn void comments(char *command, int *i)
** \brief this function increment the index to ignore comments
**
** \param command: string that contain the command
** \param i: index in the current command string
** \return void
*/

void comments(char *command, int *i)
{
  while (command[*i] == '#')
  {
    while (command[*i] != '\0' && command[*i] != '\n')
    {
      *i += 1;
      g_global.index++;
    }
  }
}


/**
** \fn static int operators1(char *s)
** \brief convert string in operators token is the string is an operator
**
** \param s: string to be tokenized
** \return the token matched, 0 if no matched
*/

static int operators1(char *s)
{
  enum type res = 0;
  if (!strcmp(s, "&"))
    res = AND;
  else if (!strcmp(s, ";"))
    res = SEMI;
  else if (!strcmp(s, "&&"))
    res = AND_IF;
  else if (!strcmp(s, "||"))
    res = OR_IF;
  else if (!strcmp(s, "<"))
    res = LESS;
  else if (!strcmp(s, ">"))
    res = GREAT;
  else if (!strcmp(s, ";;"))
    res = DSEMI;
  else if (!strcmp(s, "<<"))
    res = DLESS;
  else if (!strcmp(s, ">>"))
    res = DGREAT;
   else if (!strcmp(s, "<&"))
    res = LESSAND;
  else if (!strcmp(s, ">&"))
    res = GREATAND;
  return res;
}

/**
** \fn static int operators2(char *s)
** \brief convert string in operators token is the string is an operator
**
** \param s: string to be tokenized
** \return the token matched, 0 if no matched
*/

static int operators2(char *s)
{
  enum type res = 0;
  if (!strcmp(s, "<>"))
    res = LESSGREAT;
  else if (!strcmp(s, "<<-"))
    res = DLESSDASH;
  else if (!strcmp(s, "<|"))
    res = CLOBBER;
  else if (!strcmp(s, "{"))
    res = LBRACE;
  else if (!strcmp(s, "}"))
    res = RBRACE;
  else if (!strcmp(s, "!"))
    res = BANG;
  else if (!strcmp(s, "in"))
    res = IN;
  else if (!strcmp(s, "("))
    res = LPAR;
  else if (!strcmp(s, ")"))
    res = RPAR;
  else if (!strcmp(s, "|"))
    res = OR;
  return res;
}

/**
** \fn static int reserved(char *s)
** \brief convert string in reserved token is the string is reserved word
**
** \param s: string to be tokenized
** \return the token matched, 0 if no matched
*/

static int reserved(char *s)
{
  enum type res = 0;
  if (!strcmp(s, "if"))
    res = IF;
  else if (!strcmp(s, "then"))
    res = THEN;
  else if (!strcmp(s, "else"))
    res = ELSE;
  else if (!strcmp(s, "elif"))
    res = ELIF;
  else if (!strcmp(s, "fi"))
    res = FI;
  else if (!strcmp(s, "do"))
    res = DO;
  else if (!strcmp(s, "done"))
    res = DONE;
  else if (!strcmp(s, "case"))
    res = CASE;
  else if (!strcmp(s, "esac"))
    res = ESAC;
  return res;
}

/**
** \fn static int reserved2(char *s)
** \brief convert string in reserved token is the string is reserved word
**
** \param s: string to be tokenized
** \return the token matched, 0 if no matched
*/

static int reserved2(char *s)
{
  enum type res = 0;
  if (!strcmp(s, "while"))
    res = WHILE;
  else if (!strcmp(s, "until"))
    res = UNTIL;
  else if (!strcmp(s, "for"))
    res = FOR;
  else if (!strcmp(s, "\n"))
    res = NEWL;
  else if (!strcmp(s, "\0"))
    res = END_FILE;
  else if (!strcmp(s, "function"))
    res = FUNCTION;
  else if (s[0] >= '0' && s[0] <= '9')
  {
    int i = 0;
    for (; s[i] != '\0' && s[i] >= '0' && s[i] <= '9'; i++)
      continue;
    if (s[i] == '\0')
      res = IONUMBER;
  }
  return res;
}

/**
** \fn static int assigment(char *s)
** \brief convert string in assiment token is the string is assigment word
**
** \param s: string to be tokenized
** \return the token matched, 0 if no matched
*/

static int assigment(char *s)
{
  enum type res = 0;
  if (!strcmp(s, "cd") || !strcmp(s, "echo") || !strcmp(s, "exit") ||
      !strcmp(s, "shopt") || !strcmp(s, "export") || !strcmp(s, "alias") ||
      !strcmp(s, "unalias") || !strcmp(s, "continue") || !strcmp(s, "break") ||
      !strcmp(s, "source") || !strcmp(s, "history") || !strcmp(s, "ls"))
    res = ASSIGMENT_WORD;
  return res;
}

/**
** \fn int get_tok(char *s)
** \brief convert string in the corresponding token
*
** \param s: string to be tokenized
** \return the token matched, -1 if no matched
*/

int get_tok(char *s)
{
  enum type res = 0;
  if (!s)
    return -1;
  res = operators1(s);
  if (!res)
    res = operators2(s);
  if (!res)
    res = reserved(s);
  if (!res)
    res = reserved2(s);
  if (!res)
    res = assigment(s);
  if (!res)
    res = WORD; // HERE_DOC ? ASSIGMENT_WORD ?
  return res;
}

/**
** \fn struct ast *curr_tok(void)
** \brief get the current token in the command in funtion of the index
**
** \param void
** \return the corresponding ast if no errors occured, 0 otherwise
*/

struct ast *curr_tok(void)
{
  char *s = next_tok(g_global.command, g_global.index);
  int tok = get_tok(s);
  if (tok == -1)
  {
    free(s);
    return NULL;
  }
  struct ast *node = ast_init();
  node->key = malloc(sizeof(char) * (strlen(s) + 1));
  strcpy(node->key, s);
  node->key[strlen(s)] = '\0';
  node->type = tok;
  free(s);
  return node;
}
