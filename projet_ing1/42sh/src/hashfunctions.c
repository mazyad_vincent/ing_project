#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hashtable.h"

int my_pow(int a, int b)
{
  if (!b)
    return 1;
  return a * my_pow(a, b - 1);
}

size_t hash_func(char *data, size_t capacity)
{
  size_t h = 0;
	for(size_t i = 0 ; i < strlen(data) ; i++) /* On parcourt tous les caract */
		h += data[i] * my_pow(data[i], 2);
  return h % capacity;
}

void hash_push(struct hashtable *hash, char *key, char *data, int alias)
{
     if (!hash)
        return;
     struct list *to_add = list_init(hash_func(key, hash->capacity), data, key
                                    , alias);
     size_t index = to_add->indice;
     hash->list[index] = list_push(hash->list[index], to_add);
}

void hash_pop(struct hashtable *hash, char *key)
{
    int index = hash_func(key, hash->capacity);
    if (!strcmp(hash->list[index]->key, key))
    {
       struct list *list = hash->list[index];
       hash->list[index] = NULL;
       free_list(list);
    }
    else
    {
       struct list *list = hash->list[index];
       while (list && list->next && strcmp(list->next->key, key))
          list = list->next;
       if (list->next)
       {
          struct list *to_pop = list->next;
          list->next = NULL;
          free_list(to_pop);
       }
    }
}

struct list* find_hash(struct hashtable *hash, char *key)
{
   struct list *head = hash->list[hash_func(key, hash->capacity)];
   while (head && strcmp(head->key,  key))
      head = head->next;
   if (!head)
      return NULL;
   return head;
}
