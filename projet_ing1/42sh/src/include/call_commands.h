/**
** \file call_commands.h
** \brief This file regroups all functions proto for the the execution of ast
** \author Mazyad Vincent
*/

#ifndef CALL_COMMANDS_H
# define CALL_COMMANDS_H

#include <stdio.h>

struct ast *ast;

int fill_tab(struct ast *ast, int condi);
size_t nb_ast_elements(struct ast *ast); /* num of elements in ast */
void get_ast_args(struct ast *ast, char *tab[], size_t *index); /* put in tab */
size_t nb_elem(char *buffer);
void fill_args(char *buffer, char *tab[], size_t size);
int execute_cmd(char *tab[]); /* execute the ast */
int get_until_then(struct ast *ast); /* get numb of elements until key then */
char **condi_command(struct ast *ast); /* get array of elements until then */
int exec_then(struct ast *ast);  /* exec from then if condition if */
int exec_else(struct ast *ast); /* from else to fi */
int exec_while(struct ast *ast); /* from do to done */
int condition_while(struct ast *ast); /* from while to do */
int exec_for_pipe(struct ast *ast); /* exec without fork */
int is_else_arg(struct ast *ast); /* if there is an else in the tree */
int select_exec(struct ast *node);
int condition_if_command(struct ast *ast); /* depreciated exec the if cond */
int then_if_command(struct ast *ast); /* exec then */
int else_if_command(struct ast *ast); /* exec else */
int if_operation(struct ast *ast); /* launch the if operation */
int command_while_operation(struct ast *ast); /* while operation */
int for_command(struct ast *ast); /* for operation */
int redirection_right(struct ast *ast); /* redirection right */
int redirection_left(struct ast *ast);

#endif /* !CALL_COMMANDS_H */
