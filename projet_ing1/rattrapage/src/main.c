#include "ast.h"
#include "parse.h"
#include "execute.h"
#include "operators.h"
#include <readline/readline.h>
#include <readline/history.h>

struct global g_global;

static void replace_rec(struct ast *ast)
{
  if (!ast)
    return;
  if (ast->left && strlen(ast->left->key) > 1 && ast->left->key[0] == '$')
  {
    int i = (!strcmp(ast->left->key, "$?"))? 0 : 1;
    char *data = getenv(ast->left->key + i);
    if (!data)
    {
      struct ast *to_free = ast->left;
      ast->left = ast->left->left;
      free(to_free->key);
      free(to_free);
    }
    else
    {
      free(ast->left->key);
      char *to_add = malloc(strlen(data) + 1);
      memcpy(to_add, data, strlen(data) + 1);
      ast->left->key = to_add;
    }
  }
  replace_rec(ast->left);
  replace_rec(ast->right);
}

static void replace_ast(struct ast *ast) /* replace each node alias in ast */
{
  if (!ast)
    return;
  replace_rec(ast);
}

static int get_numb_words(char *command)
{
	size_t i = 0; /* number of words */
	while (command && *command)
	{
		if (*command != ' ')
		{
			while (*command && *command != ' ')
			{
				if (*command == ';')
				{
					i += 1;
					command++;
					break;
				}
				command++;
			}
			i += 1;
		}
		else
			command++;
	}
	return i;
}

static char *separator(char *command)
{
	int index = g_global.index;
	size_t i = 0;
	char buf[1024] = { 0 };
	if (command[index] == '|' && command[index + 1] && command[index + 1] == '|')
	{
		buf[i] = '|';
		buf[i+1] = '|';
		g_global.index += 2;
		i += 2;
	}

	else if (command[index] == '|' || command[index] == ';')
	{
		buf[i] = command[index];
		g_global.index += 1;
		i += 1;
	}

	else if (command[index] == '&' && command[index + 1]
					&& command[index + 1] == '&')
	{
		buf[i] = '&';
		buf[i+1] = '&';
		g_global.index += 2;
		i += 2;
	}
	buf[i] = '\0';
	char *to_ret = malloc(i + 1);
	memcpy(to_ret, buf, i + 1);
	return to_ret;
}

static int is_separator(const char c)
{
	if (c == ';' || c == '|' || c == '&')
		return 1;
	return 0;
}

char *get_next_word(char *command)
{
	while (command[g_global.index] && command[g_global.index] == ' ')
		g_global.index += 1;
	if (!command[g_global.index])
		return NULL;
	size_t i = 0;
	char buf[1024] = { 0 };
	if (is_separator(command[g_global.index]))
		return separator(command);
	else
	{
		for (; command[g_global.index] && command[g_global.index] != ' '
				&& command[g_global.index] != ';' && command[g_global.index] != '&'
				&& command[g_global.index] != '|'; ++i)
		{
			buf[i] = command[g_global.index];
	 	  g_global.index += 1;
		}
	}
	buf[i] = '\0';
	char *to_ret = malloc(i + 1);
	memcpy(to_ret, buf, i + 1);
	return to_ret;
}

static void prompt()
{
	char *command;
	g_global.res = 0;
  setenv("$?", "0", 1);
	while (!isatty(0) && (command = readline("minishell$ ")) != NULL)
	{
		add_history(command);
		g_global.index = 0; /* for parsing */
		if (!get_numb_words(command))
		{
			free(command);
			continue;
		}
		struct ast *ast = init_ast(get_next_word(command));
		g_global.ast = ast;
		struct ast *ast_parse = parse_input(ast, command);
		g_global.ast_parse = ast_parse;
		if (!check_parse(ast_parse))
    {
      swap_ast(ast_parse); /* for multiples redirections */
      replace_ast(ast_parse); /* for env variables */
	  	g_global.res = exec_rec(ast_parse);
    }
		else
			g_global.res = 2;
    char c[1024] = { 0 };
    sprintf(c, "%d", g_global.res);
    setenv("$?", c, 1);
		free(command);
		free_ast(ast_parse);
	}
}

static int exec_with_arg(char *command)
{
	g_global.res = 0;
  setenv("$?", "0", 1);
	g_global.index = 0; /* for parsing */
	struct ast *ast = init_ast(get_next_word(command));
	g_global.ast = ast;
	struct ast *ast_parse = parse_input(ast, command);
	g_global.ast_parse = ast_parse;
	if (!check_parse(ast_parse))
  {
     swap_ast(ast_parse); /* for multiples redirections */
     replace_ast(ast_parse); /* for env variables */
	   g_global.res = exec_rec(ast_parse);
  }
	else
  {
	  g_global.res = 2;
    char c[1024] = { 0 };
    sprintf(c, "%d", g_global.res);
    setenv("$?", c, 1);
  }
	free_ast(ast_parse);
  return g_global.res;
}

int main(int argc, char **argv)
{
  if (argc == 2)
  {
    if (!is_file(argv[1]))
      return 126;
    char *buff = get_input_from_file(argv[1]);
    int res = exec_with_arg(buff); /* execute the commands in file */
    free(buff);
    return res;
  }
	prompt();
	return 1;
}
