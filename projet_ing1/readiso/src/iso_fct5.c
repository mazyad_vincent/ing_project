#include "iso9660.h"
#include "my_read_iso.h"


void shell_quit(char *ptr, struct stat stat)
{
    if (munmap(ptr, stat.st_size) < 0)
        err(1, "munmap failed");
    exit(0);
}

int sanity_check(void *super, struct stat stat)
{
    size_t size = stat.st_size;
    struct iso_prim_voldesc *superblock = super;
    if (size < sizeof (struct iso_prim_voldesc))
        return 0;
    if (strncmp("CD001", superblock->std_identifier, 5))
        return 0;
    return 1;
}

struct cmd shell(void)
{
    struct cmd cmd = {
        NULL, NULL
    };
    char input[4096] = 
    {
        0
    };
    if (!fgets(input, 4096, stdin))
        return cmd;
    char command[4096] =
    {
        0
    };
    char arg[4096] =
    {
        0
    };
    int i = 0;
    for (; input[i] == ' ' || input[i] == '\t' || input[i] == '\n'; i++)
        continue;
    for (int j = 0; input[i] && input[i] != ' ' && input[i] != '\t' &&
            input[i] != '\n'; j++, i++)
        command[j] = input[i];
    cmd.cmd = command;
    for (; input[i] == ' ' || input[i] == '\t' || input[i] == '\n'; i++)
        continue;
    if (input[i])
    {
        for (int j = 0; input[i] && input[i] != ' ' && input[i] != '\t'
                && input[i] != '\n'; j++, i++)
            arg[j] = input[i];
        cmd.arg = arg;
    }
    return cmd;
}

int select_function(struct cmd command, struct data *data)
{
    if (!command.cmd)
        return 1;
    if (!strcmp(command.cmd, "help"))
        shell_help(command.arg);
    else if (!strcmp(command.cmd, "info"))
        shell_info(data->ptr, command.arg);
    else if (!strcmp(command.cmd, "ls"))
        shell_ls(data, command.arg);
    else if (!strcmp(command.cmd, "cd"))
    {
        data->path_table = shell_cd(data->ptr, data->path_table, command.arg,
                data->script);
        data->cur = cast_void(data->ptr + data->path_table->data_blk *
                ISO_BLOCK_SIZE);
    }
    else if (!strcmp(command.cmd, "cat"))
        shell_cat(data, command.arg);
    else if (!strcmp(command.cmd, "pwd"))
    {
        if (command.arg)
        {
            fprintf(stderr, "%s: %s: command does not take an argument\n",
                    data->script + 2, command.cmd);
            return 0;
        }
        shell_pwd(data->ptr, data->path_table, data->path_table_root);
        printf("\n");
    }
    else if (!strcmp(command.cmd, "get"))
        shell_get(data, command.arg);
    else if (!strcmp(command.cmd, "tree"))
        shell_tree(data, command.arg);
    else if (!strcmp(command.cmd, "quit"))
    {
        if (command.arg)
        {
            fprintf(stderr, "%s: %s: command does not take an argument\n",
                    data->script + 2, command.cmd);
            return 0;
        }
        shell_quit(data->ptr, data->stat);
    }
    else
        fprintf(stderr, "%s: %s: unknown command\n",
                data->script + 2, command.cmd);
    return 1;
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "usage : %s FILE\n", argv[0]);
        return 1; 
    }
    int fd = open(argv[1], O_RDONLY);
    if (fd < 0)
    {
        fprintf(stderr, "%s : %s: No such file or directory\n", argv[0] + 2,
            argv[1]);
        return 1;
    }
    struct stat stat;
    if (fstat(fd, &stat) < 0)
        err(1, "fstat");
    char *ptr = mmap(NULL, stat.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (ptr == MAP_FAILED)
        err(1, "mmap failed");
    close(fd);
    struct iso_prim_voldesc *super = get_super_block(ptr);
    if (!super)
        return 0;
    if (!sanity_check(super, stat))
    {
        fprintf(stderr, "%s : %s: invalid ISO9660 file\n", argv[0] + 2,
            argv[1]);
        return 1;
    }
    struct iso_dir *root = cast_void(ptr +
            super->root_dir.data_blk.le * ISO_BLOCK_SIZE);
    struct iso_dir *cur = root;
    struct iso_path_table_le *path_table_root = cast_void(ptr +
            super->le_path_table_blk * ISO_BLOCK_SIZE);
    struct iso_path_table_le *path_table = path_table_root;
    struct data data = {
        ptr, stat, root, cur, path_table_root, path_table, argv[0]
    };
    while (!isatty(0) || printf("> "))
    {
        struct cmd cmd = shell();
        if (!cmd.cmd && !isatty(0))
                shell_quit(ptr, stat);
        select_function(cmd, &data);
    }
    return 0;
}
