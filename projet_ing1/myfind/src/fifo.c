#include "fifo.h"

int my_strmcpy(char *string1, char *string2);

struct fifo* fifo_init()
{
    struct fifo *fifo = malloc(sizeof(struct fifo));
    if (!fifo)
        return NULL;
    struct list *list = malloc(sizeof(struct list));
    if (!list)
    {
        free(fifo);
        return NULL;
    }
    list->next = NULL;
    list->prev = NULL;
    list->data = NULL;
    fifo->head = list;
    fifo->size = 0;
    return fifo;
}

void fifo_push(struct fifo *fifo, char *elm)
{
    struct list *list = malloc(sizeof(struct list));
    if (!list)
        return;
    list->data = elm;
    if (fifo->size == 0)
    {
        list->prev = NULL;
        list->next = NULL;
        free(fifo->head);
        fifo->head = list;
    }
    else
    {
        struct list *list_head = fifo->head;
        list->next = list_head;
        list_head->prev = list;
        fifo->head = list;
    }
    fifo->size++;
}

struct list* fifo_pop(struct fifo *fifo)
{
   if (fifo->size == 0)
      return NULL;
   if (fifo->size == 1)
   {
        struct list *to_pop = fifo->head;
        fifo->head = NULL;
        fifo->size--;
        return to_pop;
   }
   struct list *cpy = fifo->head;
   while (cpy->next->next)
        cpy = cpy->next;
   struct list *to_pop = cpy->next;
   to_pop->prev = NULL;     
   cpy->next = NULL;
   fifo->size--;
   return to_pop;
}

int fifo_find(struct fifo *fifo, char *data)
{
    struct list *cpy = fifo->head;
    while (cpy && cpy->data)
    {
        if (my_strmcpy(cpy->data, data))
           return 1;
        cpy = cpy->next;
    }
    return 0;
}

struct list* fifo_get(struct fifo *fifo) //gethead
{
    struct list *cpy = fifo->head;
    while (cpy && my_strmcpy(cpy->data, "d"))
        cpy = cpy->next;
    return cpy;
}

struct list* fifo_getindex(struct fifo *fifo, int index) //get 
{
    struct list *cpy = fifo->head;
    while (cpy && cpy->next)
        cpy = cpy->next;
    for (int i = 0; i < index; i++)
        cpy = cpy->prev;
    return cpy;
}

void clean_all(struct fifo *fifo)
{
    while (fifo->size != 0)
    {
        struct list *list = fifo_pop(fifo);
        free(list->data);
        free(list);
    }
    free(fifo->head);
    free(fifo);
}
