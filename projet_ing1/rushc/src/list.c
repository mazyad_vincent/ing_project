#include <stdlib.h>
#include <stdio.h>
#include "generation_map.h"

void push_ia(struct map *map, struct IA *ia)
{
  if (!map->ia)
  {
    map->ia = ia;
    return;
  }
  struct IA *cpy = map->ia;
  while (cpy && cpy->next)
    cpy = cpy->next;
  cpy->next = ia;
}

void free_ia(struct IA *ia)
{
  if (!ia)
    return;
  while(ia)
  {
    struct IA *to_free = ia;
    ia = ia->next;
    free(to_free->fire);
    free(to_free);
  }
}
