#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "texture.h"
#include "../generation_map.h"
#include "player_control.h"

void rend_all(struct map *map, SDL_Renderer *renderer)
{
  struct IA *cpy = map->ia;
  while(cpy)
  {
     render(cpy->ia_texture, cpy->pos_screen.x, cpy->pos_screen.y,
            NULL, NULL, SDL_FLIP_NONE, renderer);
     fire_render(cpy, renderer);
     cpy = cpy->next;
  }
}
