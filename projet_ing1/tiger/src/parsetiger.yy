%language "C++"

%defines
%define api.value.type variant
%define api.token.constructor
%define api.token.prefix {TOK_}
%define parse.error verbose
%define parse.trace
%locations
%param { int& num_errors }
%code provides
{
  #include <iostream>
  #include <fstream>
  #include <cstdio>
  // The declaration, for the scanner.
#define YY_DECL yy::parser::symbol_type yylex(int& num_errors)

  // The declaration, for the parser.
  YY_DECL;
  extern FILE *yyin;
}

%token   ARRAY   "array"
         IF      "if"
         THEN    "then"
         ELSE    "else"
         WHILE   "while"
         FOR     "for"
         TO      "to"
         DO      "do"
         LET     "let"
         IN      "in"
         END     "end"
         OF      "of"
         BREAK   "break"
         NIL      "nil"
         FUNCTION "function"
         VAR      "var"
         TYPE     "type"
         IMPORT   "import"
         PRIMITIVE "primitive"
         CLASS     "class"
         METHOD    "method"
         EXTENDS   "extends"
         NEW       "new"
         COMA      ","
         DPOINT    ":"
         POINTV    ";"
         PARO      "("
         PARF      ")"
         CRO       "["
         CROF      "]"
         ACO       "{"
         ACF       "}"
         DEQ       ":="
         PLUS      "+"
         MINUS     "-"
         DIV       "/"
         MUL       "*"
         NOTEQ     "<>"
         EQ        "="
         LT        "<"
         GT        ">"
         GOE       ">="
         LOR       "<="
         AND       "&"
         OR        "|"
         BA        "\a"
         BB        "\b"
         BF        "\f"
         BV        "\v"
         DBLQ      "\""
         PT        "."
         EOF 0     "eof"

%token INT
%token STRING
%token ID


%nonassoc WHILE
%nonassoc OF
%nonassoc THEN
%nonassoc FOR
%nonassoc TO
%nonassoc BREAK
%nonassoc NIL
%nonassoc FUNCTION
%nonassoc TYPE
%nonassoc ARRAY
%nonassoc DO
%nonassoc LET
%nonassoc IN
%nonassoc END
%nonassoc VAR
%nonassoc IMPORT
%nonassoc PRIMITIVE
%nonassoc CLASS
%nonassoc ID
%nonassoc DEQ
%nonassoc ELSE
%nonassoc IF

%left OR
%left AND
%left LOR GOE GT LT EQ NOTEQ
%left PLUS MINUS
%left MUL DIV

%start program

%%

program :
  exp
  | decs

exp :
  NIL
  | INT
  | STRING
  | ID CRO exp CROF OF exp
  | ID ACO ID EQ exp rule1 ACF
  | ID ACO ACF

  | NEW ID
  | lvalue
  | ID PARO exp rule2 PARF
  | ID PARO PARF
  | lvalue1 PARO exp rule2 PARF
  | lvalue1 PARO PARF

  | MINUS exp
  | exp PLUS exp
  | exp MINUS exp
  | exp DIV exp
  | exp MUL exp
  | exp NOTEQ exp
  | exp EQ exp
  | exp LT exp
  | exp GT exp
  | exp GOE exp
  | exp LOR exp
  | exp AND exp
  | exp OR exp
  | PARO exps PARF

  | lvalue DEQ exp

  | IF exp THEN exp ELSE exp
  | IF exp THEN exp
  | WHILE exp DO exp
  | FOR ID DEQ exp TO exp DO exp
  | BREAK
  | LET decs IN exps END

rule1:
  %empty
  | rule1 COMA ID EQ exp

rule2:
  %empty
  | rule2 COMA exp

rule3:
  %empty
  | rule3 POINTV exp

rule4:
  %empty
  | rule4 dec

rule5:
  %empty
  | rule5 classfield

rule6:
  %empty
  | rule6 COMA ID DPOINT ID

lvalue :
  ID
  | lvalue1
  | lvalue2

lvalue1:
  ID PT ID
  | lvalue1 PT ID
  | lvalue2 PT ID

lvalue2:
  ID CRO exp CROF
  | lvalue1 CRO exp CROF
  | lvalue2 CRO exp CROF


exps:
  exp rule3
  | %empty
decs:
   rule4

dec:
  TYPE ID EQ ty
  | CLASS ID EXTENDS ID ACO classfields ACF
  | CLASS ID ACO classfields ACF
  | vardec
  | FUNCTION ID PARO tyfields PARF DPOINT ID EQ exp
  | FUNCTION ID PARO tyfields PARF EQ exp
  | PRIMITIVE ID PARO tyfields PARF DPOINT ID
  | PRIMITIVE ID PARO tyfields PARF
  | IMPORT STRING

vardec:
  VAR ID DPOINT ID DEQ exp
  | VAR ID DEQ exp

classfields :
  rule5

classfield:
  vardec
  | METHOD ID PARO tyfields PARF DPOINT ID EQ exp
  | METHOD ID PARO tyfields PARF EQ exp

ty:
  ID
  | ACO tyfields ACF
  | ARRAY OF ID
  | CLASS  EXTENDS ID ACO classfields ACF
  | CLASS ACO classfields ACF

tyfields:
     ID DPOINT ID rule6
     | %empty

%%

int main()
{
  auto num_errors = 0;
  yy::parser parser(num_errors);
  auto status = parser.parse();
  return status;
}

void yy::parser::error(const yy::location& loc, const std::string& str)
{
  num_errors += 1;
  std::cerr << loc << "" << str << '\n';
}
