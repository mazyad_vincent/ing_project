/**
 ** \file bind/renamer.hxx
 ** \brief Template methods of bind::Renamer.
 */

#pragma once

#include <tuple>
#include <bind/renamer.hh>
#include <utility>
namespace bind
{

  // FIXME

  template <class E, class Def>
  void
  Renamer::visit(E& e, const Def* def)
  {
    if (def == nullptr)
      return;
    misc::symbol symbol = new_name(*def);
    e.name_set(symbol);
    new_names_.emplace(def, symbol);
  }

  // Compute new name w/ fresh
  template <typename Def>
  misc::symbol Renamer::new_name_compute(const Def& e)
  {
    return misc::symbol::fresh(e.name_get());
  }

  // check if e has been rename and call new_name_compute
  template <typename Def>
  misc::symbol Renamer::new_name(const Def& e)
  {
    // Check if the e has already been rename
    try 
    {   
      new_names_.at(&e);
    }   
    catch(std::out_of_range exep)
    {
      return new_name_compute(e);
    }
    return e.name_get();// already renamed
  }

} // namespace bind
