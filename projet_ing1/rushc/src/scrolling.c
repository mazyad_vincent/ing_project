#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "generation_map.h"

SDL_Rect camera_init(struct vector2 pos, int height, int wid_rec, struct player
                     *player)
{
  SDL_Rect camera;
  camera.x = player->pos.x;
  camera.y = 0;
  camera.w = wid_rec;
  camera.h = height;
  return camera;
}
