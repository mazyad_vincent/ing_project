/**
 ** \file ast/seq-exp.hxx
 ** \brief Inline methods of ast::SeqExp.
 */

#pragma once

#include <ast/seq-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  inline const typename SeqExp::exps_type* SeqExp::get_vect() const
  {
    return vect_;
  }

  inline typename SeqExp::exps_type* SeqExp::get_vect()
  {
    return vect_;
  }

  inline void SeqExp::push_front(Exp* exp)
  {
    vect_->insert(vect_->begin(), exp);
  }
} // namespace ast

