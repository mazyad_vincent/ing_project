#include "parse.h"
#include <stdlib.h>

extern struct global g_global;

int exit_command(struct ast *ast)
{
		if (!ast)
		{
			printf("exit\n");
			free_ast(g_global.ast_parse);
			exit(g_global.res);
		}
		if (ast->left)
		{
		  printf("exit\n");
			fprintf(stderr, "exit: too many args for exit\n");
			return 1;
		}
		for (size_t i = 0; i < strlen(ast->key); ++i)
		{
			if (ast->key[i] < 48 || ast->key[i] > 57)
			{
				printf("exit\n");
				fprintf(stderr, "exit: numeric args necessary\n");
				free_ast(g_global.ast_parse);
				exit(2);
			}
		}
		int numeric = atoi(ast->key);
		if (numeric < 0 || numeric > 255)
			numeric = 125;
		printf("exit\n");
		free_ast(g_global.ast_parse);
		exit(numeric);
}
