#define _GNU_SOURCE
#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "opt_handler.h"
#include "parser.h"

/**
**  \file opt_handler.c
**  \brief Option handler: parses the options passed to 42sh
**  \author Nicolas CENDRIER
**  \version 1.0
**  \date 01.12.17
**  \
**/

extern struct global g_global;

/**
**  \brief Compares src to option: if src is equal to or contained in option
**  the function returns 1
**  \param src The string to compare
**  \param option Name of the option being compared
**  \return 1 or 0 whether the comparison succeeded or not
**/
int compare_option(char *src, char *option)
{
    int i = 2;
    while (src[i] != '\0' && option[i] != '\0')
    {
        if (src[i] != option[i])
          return 0;
        i++;
    }
    return src[i] == '\0' && i != 2;
}

/**
**  \brief Simple function printing an error message
**  \param option the option that failed
**  \param bin name of the executable
**  \return 2 and prints to stderr
**/
void option_warn(char *option, char *bin)
{
    free(g_global.shopts);
    errx(2, "%s: invalid option\nUsage:  %s [GNU long option] [option] "
            "...\n        %s [GNU long option] [option] script-file ...\n"
            "GNU long options:\n        --norc\n        --ast-print\n"
            "        --version\nShell options:\n        -c command\n"
            "        [-+]O shopt_option", option, bin, bin);
}

/**
**  \brief Prints the short options
**  \param argv lists of argv passed to 42sh
**  \param index position in the list of argv
**/
void print_shopt(char **argv, int index)
{
    if (argv[index][0] == '+')
    {
        printf("shopt %s ast-print\n", g_global.shopts[0] ? "-s" : "-u");
        printf("shopt %s dotglob\n", g_global.shopts[1] ? "-s" : "-u");
        printf("shopt %s expansion_aliases\n", 
               g_global.shopts[2] ? "-s" : "-u");
        printf("shopt %s extglob\n", g_global.shopts[3] ? "-s" : "-u");
        printf("shopt %s nocaseglob\n", g_global.shopts[4] ? "-s" : "-u");
        printf("shopt %s nullglob\n", g_global.shopts[5] ? "-s" : "-u");
        printf("shopt %s sourcepath\n", g_global.shopts[6] ? "-s" : "-u");
        printf("shopt %s xpg_echo\n", g_global.shopts[7] ? "-s" : "-u");
    }
    else
    {
        printf("ast-print %*s\n", 8, g_global.shopts[0] ? "on" : "off");
        printf("dotglob %*s\n", 10, g_global.shopts[1] ? "on" : "off");
        printf("expansion_aliases %*s\n", 0, g_global.shopts[2] ? "on" : "off");
        printf("extglob %*s\n", 10, g_global.shopts[3] ? "on" : "off");
        printf("nocaseglob %*s\n", 7, g_global.shopts[4] ? "on" : "off");
        printf("nullglob %*s\n", 9, g_global.shopts[5] ? "on" : "off");
        printf("sourcepath %*s\n", 7, g_global.shopts[6] ? "on" : "off");
        printf("xpg_echo %*s\n", 9, g_global.shopts[7] ? "on" : "off");
    }
}

/**
**  \brief Handles the setting and unsetting of shopt passed to 42sh
**  \param argv lists of args passed to 42sh
**  \param i index position in the list of argv
**/
void shopt_handler(char **argv, int i)
{
    if (!strcmp("ast-print", argv[i + 1]))
        g_global.shopts[ASTS] = argv[i][0] == '-' ? 1 : 0;
    else if (!strcmp("dotglob", argv[i + 1]))
        g_global.shopts[DOTG] = argv[i][0] == '-' ? 1 : 0;
    else if (!strcmp("expend_aliases", argv[i + 1]))
        g_global.shopts[EXPA] = argv[i][0] == '-' ? 1 : 0;
    else if (!strcmp("extglob", argv[i + 1]))
        g_global.shopts[EXTG] = argv[i][0] == '-' ? 1 : 0;
    else if (!strcmp("nocaseglob", argv[i + 1]))
        g_global.shopts[NOCG] = argv[i][0] == '-' ? 1 : 0;
    else if (!strcmp("nullglob", argv[i + 1]))
        g_global.shopts[NULG] = argv[i][0] == '-' ? 1 : 0;
    else if (!strcmp("sourcepath", argv[i + 1]))
        g_global.shopts[SRCP] = argv[i][0] == '-' ? 1 : 0;
    else if (!strcmp("xpg_echo", argv[i + 1]))
        g_global.shopts[XPGE] = argv[i][0] == '-' ? 1 : 0;
    else
    {
        free(g_global.shopts);
        errx(2, "%s: invalid shell option name", argv[i + 1]);
    }
}

/**
**  \brief Handles the long options passed to 42sh
**  \param i pointer to the position in the list of args passed to 42sh
**  \param argc number of args
**  \param argv lists of argv
**  \return bitwise int representing 
**/
int long_option_handler(int *i, int argc, char **argv)
{
    char option = 0;
    while (*i < argc && !strncmp(argv[*i], "--", 2))
    {
        if (compare_option(argv[*i], "--version"))
            option |= VERS;
        else if (compare_option(argv[*i], "--ast-print"))
            option |= ASTP;
        else if (compare_option(argv[*i], "--norc"))
            option |= NORC;
        else
            option_warn(argv[*i], argv[0]);
        (*i)++;
    }
    return option;
}

/**
**  \brief retrieves the file passed as argument and copies its content
**  \param path path to the file
**  \return the content of the file
**/
char *get_command_file(char *path)
{
    int fd = open(path, O_RDONLY);
    if (fd == -1)
    {
        free(g_global.shopts);
        errx(2, "%s: No such file or directory", path);
    }
    struct stat buf;
    fstat(fd, &buf);
    char *buffer = malloc(sizeof(char) * (buf.st_size + 1));
    read(fd, buffer, buf.st_size);
    close(fd);
    return buffer;
}

/**
**  \brief retrieves the command line passed after the -c arg
**  \param argv list of args
**  \param index index in the list of args
**  \return command line
**/
char *get_command_line(char **argv, int index)
{
    char *buffer;
    if (argv[index][0] != '"')
        asprintf(&buffer, "%s", argv[index]);
    else
        for (int i = index; argv[i] != NULL; i++)
            asprintf(&buffer, "%s%s", buffer, argv[i]);
    return buffer;
}

/**
**  \brief handles the -c option
**  \param argv list of args
**  \param index index in the list of args
**  \param argc number of args
**  \return returns the command line after the -c
**/
char *c_handle(char **argv, int *index, int argc)
{
    char *buffer = NULL;
    if (argv[*index + 1] && argv[*index + 1][0] != '-')
        buffer = get_command_line(argv, *index + 1);
    else
    {
        free(g_global.shopts);
        errx(2, "%s: option requires an argument", argv[*index]);
    }
    *index = argc;
    return buffer;
}

/**
** \brief returns the struct options, initialized with the right values
** \param option bitwise int representing the long options passed to 42sh
** \param command command line passed to 42sh
** \return returns the struct
**/
struct options *init_options(int option, char *command)
{
    struct options *options = malloc(sizeof(struct options));
    options->option = option;
    options->command = command;
    return options;
}

/**
** \brief main function of the option handler; iterates on all the args passed
** to 42sh and calls the right functions depending on the option
** \param argc number of args
** \param argv list of args
** \return returns the struct initialized with the right values
**/
struct options *option_handler(int argc, char **argv)
{
    int i = 1;
    int option = long_option_handler(&i, argc, argv);
    char *save = NULL;
    g_global.shopts = calloc(8, sizeof(int));
    for (int j = i; j < argc; j++)
    {
        if (argv[j][0] != '-' && argv[j][0] != '+')
            save = get_command_file(argv[j]);
        else if (!strncmp(argv[j], "--", 2))
        {
            free(g_global.shopts);
            errx(2, "%s: %s: invalid shell option name", argv[0], argv[j]);
        }
        else if (!strcmp(argv[j], "-O") || !strcmp(argv[j], "+O"))
        {
            if (argv[j + 1] && argv[j + 1][0] != '-')
                shopt_handler(argv, j++); //Set or unset option argv[j + 1]
            else
                print_shopt(argv, j); //Print shopts
        }
        else if (!strcmp(argv[j], "-c"))
            save = c_handle(argv, &j, argc); //-c case and get command line
        else
            option_warn(argv[j], argv[0]); //no case matched
    }
    return init_options(option, save);
}
