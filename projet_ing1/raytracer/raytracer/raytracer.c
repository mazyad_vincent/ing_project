#include <err.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "light.h"
#include "parsing.h"
#include "ppm.h"
#include "vectors.h"

struct vector3* normalize(struct vector3 *vect) /*normalize*/
{
    float x = vect->x;
    float y = vect->y;
    float z = vect->z;
    vect->x = x / sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    vect->y = y / sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    vect->z = z / sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    return vect;
}

float dot_product(struct vector3 *a, struct vector3 *b)
{
    return a->x * b->x + a->y * b->y + a->z * b->z;
}


struct vector3* distance(struct vector3 *a, struct vector3 *b)
{
    struct vector3 *c = vect_init();
    c->x = b->x - a->x;
    c->y = b->y - a->y;
    c->z = b->z - a->z;
    return c;
}

struct vector3* cross_product(struct vector3 *a, struct vector3 *b) /*cross*/
{
    struct vector3 *result = vect_init();
    result->x = a->y * b->z - a->z * b->y;
    result->y = a->z * b->x - a->x * b->z;
    result->z = a->x * b->y - a->y * b->x;
    return result;
}

struct vector3* add_vect(struct vector3 *a, struct vector3 *b) /*add two vectors*/
{
    struct vector3 *c = vect_init();
    c->x = a->x + b->x;
    c->y = a->y + b->y;
    c->z = a->z + b->z;
    return c;
}

struct vector3* const_prod(struct vector3 *a, float b) /*vect * const */
{
    struct vector3 *c = vect_init();
    c->x = a->x * b;
    c->y = a->y * b;
    c->z = a->z * b;
    return c;
}

float get_d(struct list *v, struct list *vn)
{   
    struct vector3 *vec = v->data; /*vertice*/
    struct vector3 *norm = vn->data; /*normal vect*/
    return -(norm->x * vec->x) - (vec->y * norm->y) - (norm->z * vec->z);
}

int is_intersection(struct vector3 *ray, struct my_read_svati *svat, struct list
                     *v, struct list *vn)
{
    struct vector3 *A = v->data;
    struct vector3 *B = v->next->data;           /* points for triang */
    struct vector3 *C = v->next->next->data;
    float d = get_d(v, vn);
    int i;
    if (!dot_product(vn->data, ray))
        return 0;
    float dot_p_cam = dot_product(vn->data, svat->camera->pos);
    float dot_p_ray = dot_product(vn->data, ray);
    float to = -(dot_p_cam + d) / dot_p_ray;
    struct vector3 *const_prod_ray = const_prod(ray, to);
    struct vector3 *p = add_vect(svat->camera->pos, const_prod_ray);
    if (to < 0)
        return 0;
    i = solve_equations(p, A, B, C);
    if (i == 0)
    {
        free(const_prod_ray);
        free(p);
        return 0;
    }
    i = solve_equations(p, B, C, A);
    if (i == 0)
    {
        free(const_prod_ray);
        free(p);
        return 0;
    }
    i = solve_equations(p, C, A, B);
    free(const_prod_ray);
    free(p);
    return i;
}

int solve_equations(struct vector3 *p, struct vector3 *a, 
                    struct vector3 *b, struct vector3 *c) //equation 2 inc
{
    struct vector3 *ac = distance(a, c);
    struct vector3 *ab = distance(a, b);
    struct vector3 *acp = distance(a, p);
    struct vector3 *crossac = cross_product(ac, ab);
    struct vector3 *crossap = cross_product(acp, ab);
    
    int res = dot_product(crossac, crossap) > 0;
    free(ac);
    free(ab);
    free(acp);  
    free(crossac);
    free(crossap);
    return res;
}

void basic_camera(struct my_read_svati *svat, struct pixel **image)
{
    struct vector3 *u = svat->camera->ortogonal1;
    struct vector3 *v = svat->camera->ortogonal2;
    u = normalize(u);
    v = normalize(v);
    struct vector3 *t = cross_product(u, v); //free
    struct vector3 *w = const_prod(t, -1);   //free
    w = normalize(w);
    float L = (svat->camera->width / 2) / tan((svat->camera->field * M_PI /
    360));
    struct vector3 C;
    C.x = svat->camera->pos->x + L * w->x;
    C.y = svat->camera->pos->y + L * w->y;
    C.z = svat->camera->pos->z + L * w->z;
    int width = svat->camera->width / 2;
    int height = svat->camera->height / 2;
    struct materials *my_mat;
    for (int i = -width; i < width; i++)
    {
        for (int j = -height; j < height; j++)
        {
            /* traverser TOUTE notre stucture  */
            struct vector3 *point = vect_init();
            point->x = C.x + (u->x * i + v->x * j);
            point->y = C.y + (u->y * i + v->y * j);
            point->z = C.z + (u->z * i + v->z * j);
            struct vector3 *vect = vect_init(); /* rayon */
            vect->x = point->x - svat->camera->pos->x;
            vect->y = point->y - svat->camera->pos->y;
            vect->z = point->z - svat->camera->pos->z;
            vect = normalize(vect);
            float min_dist = INT_MAX;
            my_mat = material_init();
            int is_intersect = 0;
            for (struct materials *cpy = svat->mat->head; cpy; cpy = cpy->next)
            {
                struct list *vn = cpy->vertx->vn->head;
                for (struct list *list_v = cpy->vertx->v->head; list_v;
                     list_v = list_v->next->next->next)
                {
                     is_intersect = is_intersection(vect, svat, list_v, vn);
                     if (is_intersect)
                     {
                        float d = get_d(list_v, vn);
                        float to = -((dot_product(vn->data, svat->camera->pos)
                                   + d) / dot_product(vn->data, vect));
                        if (to < min_dist)
                        {
                            min_dist = to;
                            my_mat = cpy;
                        }
                     }
                     vn = vn->next->next->next;
                }
            }
            struct pixel pixel = ambient_lighting(svat->light, my_mat);
          //pixel = directionnal_lighting(svat->light, my_mat, *vn->data, pixel);
            image[j + height][i + width] = pixel;
            if (!is_intersect)
                free(my_mat);
            free(point);
            free(vect);
        }
    }
    free(my_mat);
    free(t);
    free(w);
}

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        warnx("%s arguments: Usage: %s <*.svati> <*.ppm>", argc < 2 ? "Not enough" : "Too many", argv[0]);
        return 1;
    }
    /*
    if (!fnmatch("*.svati", argv[1]) || !fnmatch("*.ppm", argv[2]))
    {
        warnx("Wrong arguments: Usage: %s <*.svati> <*.ppm>", argv[0]);
        return 1;
    } */
    struct my_read_svati *parse = init_struct();
    fill_struct(parse, argv[1]);
    int width = parse->camera->width;
    int height = parse->camera->height;

    struct ppm *ppm = malloc(sizeof(struct ppm));
    ppm->color_store = 255;
    ppm->width = width;
    ppm->height = height;

    struct pixel **image = init_image(ppm->height, ppm->width);

    ppm->image = image;

    basic_camera(parse, ppm->image);
    create_ppmfile(ppm, argv[2]);
    free_main(parse);
    //free_image(image, ppm->height);
    free(image);
    free(ppm);
    return 0;
}
