def get_list(budgets, depenses):
    result = []
    total = 0
    while len(budgets) or len(depenses):
        budget = None
        depense = None

        if len(budgets):
            budget = budgets[0]

        if len(depenses):
            depense = depenses[0]

        if not depense or (budget and depense['date'].date() > budget['dateAccepted']):
            total += budget['amount']
            result.append({'type': 'budget',
                           'value': total,
                           'date': budget['dateAccepted']})
            budgets.pop(0)
        else:
            total -= depense['amount']
            result.append({'type': 'depense',
                           'value': total,
                           'date': depense['date'].date(),
                           'user': depense['user__username']})
            depenses.pop(0)

    return result
