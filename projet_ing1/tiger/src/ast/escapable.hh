/**
 ** \file ast/escapable.hh
 ** \brief Declaration of ast::Escapable.
 */

#pragma once

#include <ast/fwd.hh>

namespace ast
{

  /// Escapable.
  class Escapable
  {
    public:
      Escapable();
      bool is_escap_get();
      void is_escap_set(bool value);

    protected:
      bool is_escapable = false;
  };

} // namespace ast

#include <ast/escapable.hxx>

