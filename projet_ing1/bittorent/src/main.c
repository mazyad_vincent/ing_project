#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "tool_parse.h"
#include "memory_list.h"
#include "parsing.h"
#include "tool_tracker.h"
#include "pretty_print.h"
#include "get_size_char.h"

int print_option(struct mem_list *memory, struct b_data *node, FILE *torrent)
{
  print_dict(node, 0);
  printf("\n");
  destroy_mem(memory);
  fclose(torrent);
  return 0;
}

int main(int argc, char **argv)
{
  if (argc < 2)
    my_err("Too few arguments");
  int is_dump = 0;  //dump option
  int is_verbose = 0; //verbose option
  int print = 0;
  struct mem_list *memory = init_mem();
  FILE *torrent = NULL;
  for (int i = 1; i < argc; i++)
  {
    if (strcmp(argv[i], "--pretty-print-torrent-file") == 0)
       print = 1;
    else if (!strcmp(argv[i], "--dump-peers"))
      is_dump = 1;
    else if (!strcmp(argv[i], "--verbose"))
      is_verbose = 1;
    else
    {
      torrent = fopen(argv[i], "r");
      break;
    }
  }
  is_dump = is_dump;
  is_verbose = is_verbose;
  if (!torrent)
    my_err("Couldn't open torrent");
  int type_value = fgetc(torrent);
  struct b_data *torrent_parsed = get_dict(memory, torrent, type_value);
  if (print == 1)
    print_option(memory, torrent_parsed, torrent);
  struct info *info = get_info(memory, torrent_parsed);
  int i = link_tracker(torrent_parsed, info->string, info->size, is_dump,
                     is_verbose);
  destroy_mem(memory);
  fclose(torrent);
  return i;
}
