#include "Threshold.h"

void err_type(char *c)
{
    if (!my_strmcpy(c, "b") && !my_strmcpy(c, "c") && !my_strmcpy(c, "d") &&
        !my_strmcpy(c, "f") && !my_strmcpy(c, "l") && !my_strmcpy(c, "p")
        && !my_strmcpy(c, "s"))
        {
          fprintf(stderr, "%s %s\n", "myfind: cannot do type option:"
          , "parameter of type is not valid");
          exit(1);
       }
}


int print_name_expr(struct fifo *fifo, char *path)
{
   int istyp = 0;
   for (int i = 0; i < fifo->size; i+=2)
   {
        char *get_char = fifo_getindex(fifo, i)->data;
        if (my_strmcpy(get_char, "-name"))
             {
                 char *copy = malloc(sizeof(char) * my_strlen(path) + 1);
                 copy = copy_string(copy, path); 
                 copy = print_dir_expr(copy, fifo_getindex(fifo, i+1)->data);
                 free(copy);
             }
        else if (my_strmcpy(get_char, "-type"))
           {
             err_type(fifo_getindex(fifo, i+1)->data);
             char *copy = malloc(sizeof(char) * my_strlen(path) + 1);
             copy = copy_string(copy, path);
             copy = print_dir_type(copy, fifo_getindex(fifo, i+1)->data);
             istyp = is_type(path, fifo_getindex(fifo, i+1)->data);
             free(copy);
           }
   }
   return istyp;
}
int same_type(char *path, char* path2)
{
   struct stat st;
   struct stat st1;
   if (lstat(path, &st) == -1)
   {
       fprintf(stderr,"%s %s\n", "myfind: cannot do open_directory:"
               , "the directory may not exist or is not valid");
       exit(1);
   }
   if (lstat(path2, &st1) == -1)
   {
       fprintf(stderr,"%s %s\n", "myfind: cannot do open_directory:"
               , "the directory may not exist or is not valid");
       exit(1);
   }
   if (st.st_mode == st1.st_mode)       
        return 1;
   return 0;
}

char* print_dir_expr(char *path, char *name) //basique name
{ 
    DIR *dir = opendir(path); //open directory of path
    if (!dir)
    {
        fprintf(stderr, "%s %s\n", "myfind: cannot do open_directory:"
        , "the directory may not exist or is not valid");
        free(path);
        exit(1);
    }
    struct dirent *entry = readdir(dir);
    char *print_dire = NULL;
    if (path[my_strlen(path) - 1] != '/')
        path = concatenation(path, "/");
    for (; entry; entry = readdir(dir))
    {
       while (entry && (my_strmcpy(entry->d_name, ".") || 
              my_strmcpy(entry->d_name, "..")))
       {
           entry = readdir(dir);
           continue;
       }
       if (entry && (entry->d_type == DT_DIR))      //directory
       { 
            print_dire = new_concatenation(path, entry->d_name);
            print_dire = print_dir_expr(print_dire, name);
            free(print_dire);
       }
       else if (entry && (entry->d_type == DT_REG))
       {
           char *print_files = new_concatenation(path, entry->d_name);
           if (!fnmatch(name, entry->d_name, 0))
                printf("%s\n", print_files);
           free(print_files);
       }
    }
    closedir(dir);
    return path;
}

int is_type(char *path, char *type)
{
    struct stat st;
    if (lstat(path, &st) == -1)
    {
        fprintf(stderr, "%s %s\n", "myfind: cannot do type:"
                , "the file or directory may not exist or is not valid.");
        exit(1);
    }
    if ((S_ISREG(st.st_mode) && my_strmcpy(type, "f")))
        return 1;
    else if ((S_ISDIR(st.st_mode) && my_strmcpy(type, "d")))
        return 1;
    else if ((S_ISBLK(st.st_mode) && my_strmcpy(type, "b")))
        return 1;
    else if ((S_ISCHR(st.st_mode) && my_strmcpy(type, "c")))
        return 1;
    else if ((S_ISFIFO(st.st_mode) && my_strmcpy(type, "p")))
        return 1;
    else if ((S_ISLNK(st.st_mode) && my_strmcpy(type, "l")))
        return 1;
    else if ((S_ISSOCK(st.st_mode) && my_strmcpy(type, "s")))
        return 1;
    return 0;
}

void condition_type(char *type, char *print_files)
{
    struct stat sb;
    if (lstat(print_files, &sb) == -1)
    {
        fprintf(stderr, "%s %s\n", "myfind: cannot do type:"
                , "the file or directory may not exist or is not valid.");
        exit(1);
    }
   if (my_strmcpy(type, "f") && S_ISREG(sb.st_mode))
         printf("%s\n", print_files);
   else if (my_strmcpy(type, "l") && S_ISLNK(sb.st_mode))
         printf("%s\n", print_files);
   else if (my_strmcpy(type, "b") && S_ISBLK(sb.st_mode))
         printf("%s\n", print_files);
   else if (my_strmcpy(type, "c") && S_ISCHR(sb.st_mode))
         printf("%s\n", print_files);
   else if (my_strmcpy(type, "p") && S_ISFIFO(sb.st_mode))
         printf("%s\n", print_files);
   else if (my_strmcpy(type, "s") && S_ISSOCK(sb.st_mode))
         printf("%s\n", print_files);

}

char* print_dir_type(char *path, char *type)
{
    struct stat st;
    if (lstat(path, &st) == -1)
    {
        fprintf(stderr, "%s %s\n", "myfind: cannot do type:"
                , "the file or directory may not exist or is not valid.");
        exit(1);
    }
    DIR *dir;
    if ((S_ISDIR(st.st_mode)))
    {
      dir = opendir(path); //open directory of path
      if (!dir)
      {
        fprintf(stderr, "%s %s\n", "myfind: cannot do open_directory:"
        , "the directory may not exist or is not valid");
        exit(1);
      }
      struct dirent *entry = readdir(dir);
      char *print_dire = NULL;
      if (path[my_strlen(path) - 1] != '/')
        path = concatenation(path, "/");
      for (; entry; entry = readdir(dir))
      {
       while (entry && (my_strmcpy(entry->d_name, ".") || 
              my_strmcpy(entry->d_name, "..")))
       {
            entry = readdir(dir);
            continue;
       }
       if (entry && entry->d_type == DT_DIR)      //directory
       { 
            print_dire = new_concatenation(path, entry->d_name);
            if (my_strmcpy(type, "d") && entry->d_type == DT_DIR)
                printf("%s\n", print_dire);
            print_dire = print_dir_type(print_dire, type);
            free(print_dire);
       }
       else if (entry && entry->d_type != DT_DIR)
       {
           char *print_files = new_concatenation(path, entry->d_name);
           condition_type(type, print_files);
           free(print_files);
       }
      }
    }
    closedir(dir);
    return path;
}
