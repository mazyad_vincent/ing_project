/**
 ** \file ast/object-visitor.hxx
 ** \brief Implementation for ast/object-visitor.hh.
 */

#pragma once

#include <misc/contract.hh>
#include <ast/all.hh>
#include <ast/object-visitor.hh>

namespace ast
{

  template <template <typename> class Const>
  GenObjectVisitor<Const>::GenObjectVisitor()
    : GenVisitor<Const>()
  {}

  template <template <typename> class Const>
  GenObjectVisitor<Const>::~GenObjectVisitor()
  {}


  /*-------------------------------.
  | Object-related visit methods.  |
  `-------------------------------*/

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<ClassTy>& e)
  {
  // FIXME: Some code was deleted here.
    e.super_get().accept(*this);
    e.decs_get().accept(*this);
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodDecs>& e)
  {
  // FIXME: Some code was deleted here.
    for (auto i = e.decs_get().begin(); i != e.decs_get().end(); ++i)
    {
     (*i)->accept(*this);
    }
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodDec>& e)
  {
  // FIXME: Some code was deleted here.
    if (e.body_get())
      e.body_get()->accept(*this);
    if (e.result_get())
      e.result_get()->accept(*this);
    e.formals_get().accept(*this);
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodCallExp>& e)
  {
  // FIXME: Some code was deleted here.
    e.var_get().accept(*this);
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<ObjectExp>& e)
  {
  // FIXME: Some code was deleted here.
    e.name_get().accept(*this);
  }


} // namespace ast
