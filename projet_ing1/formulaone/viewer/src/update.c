#include "control.h"

int is_first = 1;

enum move update(struct car *car)
{
    if (is_first)
    {
        static float angle = find_finish(car);
        static float startx = map_get_start_x(car->map);
        static float starty = map_get_start_y(car->map);
        is_first = 0;
    }
    if (car->position.x == startx && car->position.y == starty
        && angle != 0);
    {
        angle -= car->direction_angle - angle;
        return TURN_RIGHT;
    }
}
