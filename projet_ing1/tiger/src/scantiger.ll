%option noyywrap

%{
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <regex>
#include <stdexcept>
#include "parsetiger.hh"

int nb = 0;

yy::parser::location_type loc;

#define TOKEN(Type)                                       \
  yy::parser::make_ ## Type(loc)
#define yyterminate() return TOKEN(EOF);
#define YY_USER_ACTION                          \
  do {                                          \
    loc.columns(yyleng);                        \
  } while (false);
%}

loc.step();
%x CMT
%x STR

int [0-9]+
ID [a-zA-Z][0-9a-zA-Z_]*
ESCAPE "\\"[afbnrtv]
BACKS "\\"
OCTAL "\\"[0-3][0-7][0-7]
HEXA "\\"[0-9a-fA-F][0-9a-fA-F]
SPACE [ \t]

%%

"\"" BEGIN(STR);
"/*" {nb++; BEGIN(CMT);}
{int} return TOKEN(INT);
"\n\r" loc.lines();
"\r\n" loc.lines();
"\r" loc.lines();
"\n" loc.lines();
"array" return TOKEN(ARRAY);
"if" return TOKEN(IF);
"then" return TOKEN(THEN);
"else" return TOKEN(ELSE);
"while" return TOKEN(WHILE);
"for" return TOKEN(FOR);
"to" return TOKEN(TO);
"do" return TOKEN(DO);
"let" return TOKEN(LET);
"in" return TOKEN(IN);
"end" return TOKEN(END);
"of" return TOKEN(OF);
"break" return TOKEN(BREAK);
"nil" return TOKEN(NIL);
"function" return TOKEN(FUNCTION);
"var" return TOKEN(VAR);
"type" return TOKEN(TYPE);
"import" return TOKEN(IMPORT);
"primitive" return TOKEN(PRIMITIVE);
"class" return TOKEN(CLASS);
"method" return TOKEN(METHOD);
"extends" return TOKEN(EXTENDS);
"new" return TOKEN(NEW);
"." return TOKEN(PT);
"," return TOKEN(COMA);
":" return TOKEN(DPOINT);
";" return TOKEN(POINTV);
"(" return TOKEN(PARO);
")" return TOKEN(PARF);
"[" return TOKEN(CRO);
"]" return TOKEN(CROF);
"{" return TOKEN(ACO);
"}" return TOKEN(ACF);
":=" return TOKEN(DEQ);
"+" return TOKEN(PLUS);
"-" return TOKEN(MINUS);
"/" return TOKEN(DIV);
"*" return TOKEN(MUL);
"<>" return TOKEN(NOTEQ);
"=" return TOKEN(EQ);
"<" return TOKEN(LT);
">" return TOKEN(GT);
">=" return TOKEN(GOE);
"<=" return TOKEN(LOR);
"&" return TOKEN(AND);
"|" return TOKEN(OR);

{ID} return TOKEN(ID);
{BACKS} {}
{ESCAPE} {}
{OCTAL} {}
{HEXA} {}
{SPACE} {}

<<EOF>> {if (nb) std::cerr << "error commentary " << std::endl; return yy::parser::make_EOF(loc);}

<CMT>{
"/*" nb++;
"*/" {nb--; if (!nb) BEGIN (INITIAL);}
.  {}
}

<STR>{
"\"" {BEGIN(INITIAL); return TOKEN(STRING);}
. {}
}

. { std::cerr << loc << ": unexpected character: " << yytext << '\n';
num_errors +=1;
}

%%
