/**
 ** \file ast/record-exp.cc
 ** \brief Implementation of ast::RecordExp.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/record-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  RecordExp::RecordExp(const Location& location, NameTy* type,
                      typename RecordExp::fieldinits_type* vect)
    : Exp(location)
    , vect_(vect)
    , type_(type)
  {}

  RecordExp::~RecordExp()
  {
    delete type_;
    for (auto it = vect_->begin(); it != vect_->end(); ++it)
      delete *it;
    delete vect_;
  }

  void
  RecordExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  RecordExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

