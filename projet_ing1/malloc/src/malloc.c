#include "metadata.h"

static pthread_mutex_t thread_m = PTHREAD_MUTEX_INITIALIZER;

 __attribute__((__visibility__("hidden")))
struct metadata *alloc(size_t size)
{
  struct metadata *meta = add_meta(cur_block, size);
  if (!meta)
  {
    struct block *blk = size_block(size);
    return add_meta(blk, size);
  }
  return meta;
}

void *malloc(size_t size)
{
  if (!size)
    return NULL;
  pthread_mutex_lock(&thread_m);
  size = word_align(size);
  if (!cur_block)
     cur_block = init_block();
  struct metadata *to_ret = alloc(size);
  pthread_mutex_unlock(&thread_m);
  return cast_void(sizeof(struct metadata), to_ret);
}

void *calloc(size_t size, size_t size2)
{
  if (size * size2 == 0 || (size * size2) / size2 != size)                               
      return NULL;
  void *point = malloc(size * size2);
  if (!point)
    return NULL;
  pthread_mutex_lock(&thread_m);
  memset(point, 0, word_align(size * size2));
  pthread_mutex_unlock(&thread_m);
  return point;
}
