#!/bin/sh
echo
echo "Outputs values are saved into Two files : test_input and test_output"
echo 
echo "TEST Threshold 0, argument file src (./myfind src): "
echo
find src > tests/test_input
test1="./myfind src"

eval $test1 > tests/test_output
diff -s tests/test_input tests/test_output


echo
echo "Threshold 0, with argument test_dir/ (./myfind test_dir/):"
echo
find tests/test_dir/ > tests/test_input
test2="./myfind tests/test_dir/"
eval $test2 > tests/test_output
diff -s tests/test_input tests/test_output
echo
echo "OUTPUT OF MYFIND:"
while read -r line; do
    echo $line
done < tests/test_output 


echo
echo "Threshold 0, without argument (./myfind):"
echo
find > tests/test_input
test3="./myfind"
eval $test3 > tests/test_output
diff -s tests/test_input tests/test_output
echo

echo 
echo "Threshold 1, using -d option (./myfind -d tests/test_dir):"
echo
find tests/test_dir -depth > tests/test_input
test4="./myfind -d tests/test_dir"
eval $test4 > tests/test_output
diff -s tests/test_input tests/test_output
echo
echo "After using -d: "
echo
while read -r line; do
    echo $line
done < tests/test_output 
echo 



echo 
echo "Threshold 2, using -name option (./myfind tests/test_dir -name hehe):"
echo
find tests/test_dir -name hehe> tests/test_input
test5="./myfind tests/test_dir -name hehe"
eval $test5 > tests/test_output
diff -s tests/test_input tests/test_output
echo
echo "list of files containing 'hehe'"
echo
while read -r line; do
    echo $line
done < tests/test_output 
echo

 

echo 
echo "Threshold 2, using -type option (./myfind tests/test_dir -name hehe):"
echo
find tests/test_dir -type f> tests/test_input
test6="./myfind tests/test_dir -type f"
eval $test6 > tests/test_output
diff -s tests/test_input tests/test_output
echo
echo "list of files: (using type -f for regular files)"
echo
while read -r line; do
    echo $line
done < tests/test_output 
echo
