#include <stdlib.h>
#include <stdio.h>
#include "ppm.h"

void create_ppmfile(struct ppm *ppm, char *filename)
{
    FILE *output = fopen(filename, "w");
    fprintf(output, "P3\n");
    fprintf(output, "%d %d\n", ppm->width, ppm->height);
    fprintf(output, "%d\n", ppm->color_store);
    
    for (int i = 0; i < ppm->height; i++)
    {
        for (int j = 0; j < ppm->width; j++)
        {
            struct pixel pixel = ppm->image[i][j];
            int R = pixel.r * ppm->color_store;
            int G = pixel.g * ppm->color_store;
            int B = pixel.b * ppm->color_store;
            fprintf(output, "%-3d %-3d %-3d  ", R, G, B);
        }
        fprintf(output, "\n");
    }
    fclose(output);
}

struct pixel init_pixel(float r, float g, float b)
{
    struct pixel px;
    px.r = r;
    px.g = g;
    px.b = b;
    return px;
}

struct pixel **init_image(int height, int width)
{
    struct pixel **image = malloc(sizeof(struct pixel*) * height);
    for (int i = 0; i < height; i++)
    {
        image[i] = malloc(sizeof(struct pixel) * width);
    }
    return image;
}

void free_image(struct pixel **image, int height)
{
    for (int i = 0; i < height; i++)
        free(image[i]);
    free(image);
}

/*
int main(void)
{
    struct ppm *ppm = malloc(sizeof(struct ppm));
    ppm->color_store = 255;
    ppm->width = 2;
    ppm->height = 2;
    struct pixel black = init_pixel(0, 0, 0);
    struct pixel white = init_pixel(1, 1, 1);
    struct pixel **image = init_image(ppm->height, ppm->width);
    image[0][0] = black;
    image[0][1] = white;
    image[1][0] = white;
    image[1][1] = black;
    ppm->image = image;
    create_ppmfile(ppm, "test.ppm");
    free_image(image, ppm->height);
    free(ppm);
    return 0;
}*/
