#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <stdio.h>
#include <stdbool.h>

#include "sdl_screen.h"
#include "get_image.h"
#include "../generation_map.h"
#include "../scrollin/scroling.h"
#include "../movement/display_map2.h"

static const int width = 810;
static const int height = 600;

int main(void)
{
  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;
  SDL_Texture *text = NULL;

  window = SDL_CreateWindow("My Star Citizen v0.5", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            width,
                            height,
                            SDL_WINDOW_SHOWN);

  if (!window)
  {
    fprintf(stderr, "Erreur de création de la fenêtre: %s\n", SDL_GetError());
    return -1;
  }

  renderer = SDL_CreateRenderer(window, -1,
                                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) //gestion des erreurs
  {
    printf("Erreur lors de la creation d'un renderer : %s", SDL_GetError());
    return EXIT_FAILURE;
  }

  bool isRunning = true;
  SDL_Event ev;
  struct image *screen_img = get_image("screen.png", renderer, 0, 0, 0, 0, 0);
  struct image *start_img = get_image("StartButtonred.png", renderer, width, height, 0, 1, 40);
  struct image *exit_img = get_image("Exitbutton.png", renderer, 0, height, 0, 0, 40);
  SDL_RenderCopy(renderer, screen_img->texture, NULL, screen_img->pos);
  SDL_RenderCopy(renderer, start_img->texture, NULL, start_img->pos);
  SDL_RenderCopy(renderer, exit_img->texture, NULL, exit_img->pos);
  SDL_RenderPresent(renderer);

  struct map *map = map_init();
  for (int b = 0; b < 3; b++)
    add_decoy(map);
  map->pos_player.x = 0;
  map->pos_player.y = map->height / 2;
  while (isRunning && !sog.end && map->pos_player.x < map->width)
  {
    if (SDL_PollEvent(&ev) != 0)
    {
      if (ev.type == SDL_QUIT)
        isRunning = false;
      if (ev.type == SDL_MOUSEBUTTONDOWN)
      {
        if (sog.debut && start_img->pos->x <= ev.motion.x && ev.motion.x <= start_img->pos->x + start_img->pos->w)
        {
          if (start_img->pos->y <= ev.motion.y && ev.motion.y <= start_img->pos->y + start_img->pos->h)
          {
            if(game(renderer) == 0)
              sog.end = true;
            if (sog.end)
              isRunning = false;
          }
        }

        if (sog.debut && exit_img->pos->x <= ev.motion.x && ev.motion.x <= exit_img->pos->x + exit_img->pos->w)
        {
          if (exit_img->pos->y <= ev.motion.y && ev.motion.y <= exit_img->pos->y + exit_img->pos->h)
          {
            isRunning = false;
          }
        }
      }
    }
    if (!sog.debut)
    {
      if(game(renderer) == 0)
        sog.end = true;
    }
    if (sog.end)
    {
      isRunning = true;
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
      SDL_RenderClear(renderer);
      SDL_RenderPresent(renderer);
      sog.debut = true;
      sog.end = false;
      SDL_RenderCopy(renderer, screen_img->texture, NULL, screen_img->pos);
      SDL_RenderCopy(renderer, start_img->texture, NULL, start_img->pos);
      SDL_RenderCopy(renderer, exit_img->texture, NULL, exit_img->pos);
      SDL_RenderPresent(renderer);
    }
  }

  SDL_DestroyTexture(text);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}
