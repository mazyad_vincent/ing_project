#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

#include "scroling.h"
#include "../generation_map.h"

struct state_of_game sog = {true, false};

void start_game(SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *mario, SDL_Rect *mario_pos, struct map *map, int i)
{
  if (sog.debut)
  {
    sog.debut = false;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    for (int y = 0; y < map->height; y++)
    {
      int x = i;
      for (int a = 0; a < 800; x++, a++)
      {
        if (map->pos[y][x] == DECOR)
        {
          SDL_Rect *tmp = mario_pos;
          tmp->x = 800 - a;
          tmp->y = y - mario_pos->h;
          SDL_RenderCopy(renderer, mario, NULL, tmp);
          SDL_RenderPresent(renderer);
        }
      }
    }
  }
  else
  {
    for (int y = 0; y < map->height; y++)
    {
      int x = i;
      for (int a = 0; a < 800; a++)
      {
        if (map->pos[y][x] == DECOR)
        {
          SDL_RenderClear(renderer);
          SDL_Rect *tmp = mario_pos;
          tmp->x = 800 - a;
          tmp->y = y - mario_pos->h;
          SDL_RenderCopy(renderer, mario, NULL, tmp);
          SDL_RenderPresent(renderer);
        }
      }
    }
  }
  if (i == map->width - 1 || map->pos_player.x == map->width - 1)
  {
    sog.end = true;
  }
  return;
}