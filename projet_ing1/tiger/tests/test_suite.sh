#!/bin/sh

echo -e "\033[1;30mBEGIN OF THE TESTS\033[0m \n"

error=0
nbtest=0
code=0

# Correct files

files=$(echo tests/good/*)
for file in $files; do
  echo -e File: "\033[1;33m$file\033[0m"
    ./../src/tc -XbBA $file >& test.tig
    ./../src/tc -XbBA test.tig > /dev/null
  var=$?
  if [ $code -ne 0 ]
  then
    ./../src/tc -XbBA test.tig  
  fi
  if [ $var -eq 0 ]
  then
    echo -e "$var [\033[1;32mOK\033[0m]"
  else
    error=$(($error+1))
    echo -e "$var [\033[1;31mKO\033[0m]"
  fi
  nbtest=$(($nbtest+1))

done 

files=$(echo tests/good1/*)
for file in $files; do
  echo -e File: "\033[1;33m$file\033[0m"
    ./../src/tc -XA $file >& test.tig
    ./../src/tc -XA test.tig > /dev/null
  var=$?
  if [ $code -ne 0 ]
  then
    ./../src/tc -XA test.tig  
  fi
  if [ $var -eq 0 ]
  then
    echo -e "$var [\033[1;32mOK\033[0m]"
  else
    error=$(($error+1))
    echo -e "$var [\033[1;31mKO\033[0m]"
  fi
  nbtest=$(($nbtest+1))

done 



# Incorrect files

# ------------- Scanning errors -------------

files=$(echo tests/bad/scanning-error/*)
for file in $files; do
  echo -e File: "\033[1;33m$file\033[0m"
  ./../../src/tc -XbBA $file >& test.tig
  ./../../src/tc -XbBA test.tig > /dev/null
 var=$?
 if [ $var -eq 2 ] 
  then
    echo -e "$var [\033[1;32mOK\033[0m]"
  else
    error=$(($error+1))
    echo -e "$var [\033[1;31mKO\033[0m]"
  fi  
  nbtest=$(($nbtest+1))
done

echo -e "\nNumber of error: \033[1;31m$error\033[0m / \033[1;36m$nbtest\033[0m"

# ------------- Parse errors -------------

files=$(echo tests/bad/parse-error/*)
for file in $files; do
  echo -e File: "\033[1;33m$file\033[0m"
  ./../src/tc -XbBA $file >& test.tig
  ./../src/tc -XbBA test.tig > /dev/null
 var=$?
 if [ $var -eq 3 ] 
  then
    echo -e "$var [\033[1;32mOK\033[0m]"
  else
    error=$(($error+1))
    echo -e "$var [\033[1;31mKO\033[0m]"
  fi  
  nbtest=$(($nbtest+1))
done

echo -e "\nNumber of error: \033[1;31m$error\033[0m / \033[1;36m$nbtest\033[0m"


# ------------- Binding errors -------------

files=$(echo tests/bad/binding-error/*)
for file in $files; do
  echo -e File: "\033[1;33m$file\033[0m"
  ./../src/tc -XbBA $file >& test.tig
  ./../src/tc -XbBA test.tig > /dev/null
 var=$?
 if [ $var -eq 4 ] 
  then
    echo -e "$var [\033[1;32mOK\033[0m]"
  else
    error=$(($error+1))
    echo -e "$var [\033[1;31mKO\033[0m]"
  fi  
  nbtest=$(($nbtest+1))
done

echo -e "\nNumber of error: \033[1;31m$error\033[0m / \033[1;36m$nbtest\033[0m"


# ------------- Type-checking errors -------------

files=$(echo tests/bad/type-checking-error/*)
for file in $files; do
  echo -e File: "\033[1;33m$file\033[0m"
  ./../src/tc -XbBA $file >& test.tig
  ./../src/tc -XbBA test.tig > /dev/null
 var=$?
 if [ $var -eq 5 ] 
  then
    echo -e "$var [\033[1;32mOK\033[0m]"
  else
    error=$(($error+1))
    echo -e "$var [\033[1;31mKO\033[0m]"
  fi  
  nbtest=$(($nbtest+1))
done

echo -e "\nNumber of error: \033[1;31m$error\033[0m / \033[1;36m$nbtest\033[0m"

# ------------- Other errors -------------

files=$(echo tests/bad/other-error/*)
for file in $files; do
  echo -e File: "\033[1;33m$file\033[0m"
  ./../src/tc -XbBA $file >& test.tig
  ./../src/tc -XbBA test.tig > /dev/null
 var=$?
 if [ $var -eq 1 ] 
  then
    echo -e "$var [\033[1;32mOK\033[0m]"
  else
    error=$(($error+1))
    echo -e "$var [\033[1;31mKO\033[0m]"
  fi  
  nbtest=$(($nbtest+1))
done

echo -e "\nNumber of error: \033[1;31m$error\033[0m / \033[1;36m$nbtest\033[0m"



echo -e "\n\033[1;30mEND OF THE TESTS\033[0m"
