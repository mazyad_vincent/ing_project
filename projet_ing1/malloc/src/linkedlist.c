#include "metadata.h"

__attribute__((__visibility__("hidden")))
struct block *get_last_block(struct block *block)
{
  if (!block)
    return NULL;
  struct block *copy = block;  
  while (copy && copy->next)
    copy = copy->next;
  return copy;
}

__attribute__((__visibility__("hidden")))
struct metadata *add_meta(struct block *block, size_t size)
{
  while (block)
  {
     struct metadata *head = block->head;
     while (head && !head->free && head->size < size)
        head = head->next; 
     if (head && head->free && head->size >= size + sizeof(struct metadata))
     {
        head->free = 0;
        if (head->next)
          head->size = size;
        else
        {
          size_t old_size = head->size;
          head->size = size;
          struct metadata *add_meta = cast_void(sizeof(struct metadata) + 
                                      size, head);
          head->next = add_meta;
          add_meta->next = NULL;
          add_meta->free = 1;
          add_meta->blk = block;
          add_meta->size = old_size - (sizeof(struct metadata) + size);
        }
        return head;
     }
     else if (head && head->free && head->size == size)
     {
        head->free = 0;
        return head;
     }
     block = block->next;
  }
  return NULL;
}

__attribute__((__visibility__("hidden")))
int is_full_free(struct block *blk)
{
  struct metadata *head = blk->head;
  while (head && head->free == 1)
    head = head->next;
  return (!head)? 1 : 0;
}
