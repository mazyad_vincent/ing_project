/**
 ** \file ast/method-call-exp.cc
 ** \brief Implementation of ast::MethodCallExp.
 */

#include <ast/visitor.hh>
#include <ast/method-call-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  MethodCallExp::MethodCallExp(const Location& location, Var* var,
                              std::vector<Exp*>* liste, misc::symbol& name)
    : CallExp(location, name, liste)
    , var_(var)
    , vect_(*liste)
    , func_(name)
  {}

  MethodCallExp::~MethodCallExp()
  {
    delete var_;
    for (auto it = vect_.begin(); it != vect_.end(); ++it)
      delete *it;
  }

  void
  MethodCallExp::accept(ConstVisitor& v) const
  {
   v(*this);
  }

  void
  MethodCallExp::accept(Visitor& v)
  {
   v(*this);
  }

} // namespace ast

