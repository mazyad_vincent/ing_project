#include <stdlib.h>
#include <stdio.h>
#include "ast.h"
#include "arithmetic.h"
#include "queue.h"

struct queue* init_queue()
{
  struct queue *queue = malloc(sizeof(struct queue));
  queue->size = 0;
  queue->list = NULL;
  return queue;
}

struct liste* init_liste(int val)
{
  struct liste *list = malloc(sizeof(struct liste));
  list->next = NULL;
  list->val = val;
  list->prec = NULL;
  list->head = NULL;
  return list;
}

struct queue* queue_push(struct queue *queue, struct liste *list)
{
   if (!queue->list)
      queue->list = list;
   else
   {
      struct liste *head = queue->list;
      queue->list = list;
      list->next = head;
   }
   queue->size += 1;
   return queue;
}

struct liste* liste_push(struct liste *head, struct liste *list)
{
   if (!head)
     return list;
   struct liste *copy = head;
   while (copy && copy->next)
     copy = copy->next;
   copy->next = list;
   list->prec = copy;
   list->next = NULL;
   return head;
}

struct liste* queue_pop(struct queue *queue)
{
   if (!queue->size)
      return NULL;
   struct liste *list = queue->list;
   queue->list = list->next;
   queue->size -= 1;
   return list;
}

struct liste* list_pop(struct liste *list, struct liste *addr)
{
   struct liste *copy = list;
   if (list == addr)
   {
      struct liste *to_free = list;
      list = list->next;
      free(to_free);
      list->prec = NULL;
      return list;
   }
   while (copy)
   {
      if (copy->next && copy->next == addr)
      {
         struct liste *to_free = copy->next;
         if (to_free->next)
         {
            copy->next = to_free->next;
            to_free->next->prec = copy;
         }
         else
            copy->next = NULL;
         free(to_free);
         return list;
      }
      copy = copy->next;
   }
   return list;
}

void free_all_list(struct liste *list)
{
  while (list)
  {
    struct liste *to_free = list;
    list = list->next;
    free(to_free);
  }
}
