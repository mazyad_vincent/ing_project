/**
** \file exit.c
** \brief file for builtin exit
** \author Mazyad Vincent
**
*/

#include <stdlib.h>
#include <stdio.h>
#include "include/ast.h"

/**
** \fn int exit_builtins(struct ast *ast)
** \brief execute the exit builtin
**
** \param struct ast *ast
** \return return the exit value
*/

int exit_builtin(struct ast *ast)
{
   if (ast->left)
   {
      char *copy = ast->left->key;
      for (int i = 0; copy[i]; i++)
      {
        if (copy[i] < '0' || copy[i] > '9')
        {
          fprintf(stderr, "exit need sumeric arg\n");
          exit(2);
        }
      }
      int res = atoi(ast->left->key);
      if (res < 0 || res > 255)
         exit (125);
      exit(atoi(ast->left->key));
   }
   exit(0);
}
