/**
 ** \file ast/assign-exp.hxx
 ** \brief Inline methods of ast::AssignExp.
 */

#pragma once

#include <ast/assign-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  inline const Exp& AssignExp::exp_get() const
  {
     return *exp_;
  }


  inline Exp& AssignExp::exp_get()
  {
     return *exp_;
  }

  inline const Var& AssignExp::lval_get() const
  {
     return *lval_;
  }

  inline Var& AssignExp::lval_get()
  {
     return *lval_;
  }

} // namespace ast

