#include "Threshold.h"

char *copy_string(char *a, char *b)
{
    char *cpy = a;
    char *cpy2 = b;
    while (*cpy2 != '\0')
    {
        *cpy = *cpy2;
        cpy++;
        cpy2++; 
    }
    *cpy = '\0';
    return a;
}

void err_args(char **arg, int i, struct fifo *options, struct fifo *expression)
{
  if (arg[i][1] != 'd' && arg[i][1] != 'H' && arg[i][1] != 'L' && arg[i][1] !=
     'P')
  {
          fprintf(stderr, "%s %s\n", "myfind: cannot do find command:" 
                 ,"option is not valid");
          clean_all(options);
          clean_all(expression);
          exit(1);
  }
}
int is_arg(int argc, char **arg)
{
    for (int i = 1; i < argc; ++i)
    {
        if (arg[i] && arg[i][0] == '-')
           continue;
        else
           return 1;
    }
    return 0;
}

struct stat lstate(char* argv, struct stat st)
{
     if (lstat(argv, &st) == -1)
     {
       fprintf(stderr, "%s %s\n", "myfind: cannot do open_file:" 
               , "the file or directory may not exist or is not valid.");
       exit(1);
     }
     return st;
}
int main(int argc, char **argv)
{
    struct stat st; //struct for stat
    int istyp = 0;
    if (argc == 1)
    {
        char *buf = malloc(sizeof(char) + 1);
        buf = create_arg(buf);
        buf = print_dir(buf);
        free(buf);
    }
    else
    {
        struct fifo *options = fifo_init(); //struct for options
        struct fifo *expressions = fifo_init(); //struct expression
        if (is_expression(argc, argv, options, expressions))
             put_expression(expressions, options, argv, argc);
        int is_expr = 0;
        for (int i = 1; i < argc && !is_expr; ++i)
        {
            if (!is_expression(argc, argv, options, expressions)) 
            {
               if (add_options(options, expressions, argv, i))
                   continue;
               if (lstat(argv[i], &st) == -1)
               {
                 fprintf(stderr, "%s %s\n", "myfind: cannot do open_file:" 
                 , "the file or directory may not exist or is not valid.");
                 clean_all(options);
                 clean_all(expressions);
                 exit(1);
               }
               empty_size(options, expressions, st, argv[i]);  //no options           
               with_size(options, st, argv[i]);   // options
               no_arg(options, expressions, st, argc, argv);  //no arg
            }
            else
            {
                if (is_expression_arg(i, argv))
                {
                    is_expr = 1;
                    continue;
                }
                if (add_options(options, expressions, argv, i))
                   continue;
                if (!is_arg(argc, argv))
                {
                    char *no_argum = malloc(sizeof(char) + 1);
                    no_argum = create_arg(no_argum);
                    istyp = print_name_expr(expressions, no_argum);
                    free(no_argum);
                }
                else
                  istyp = print_name_expr(expressions, argv[i]); //if -name or -type
            }
         }
         if (!is_arg(argc, argv))
         {
             st = lstate(".", st);
             char *no_ar = malloc(sizeof(char) + 1);
             if (!no_ar)
                exit(1);
             no_ar = create_arg(no_ar);
             with_size(options, st, no_ar);
             free(no_ar);
         }
         clean_all(options);
         clean_all(expressions);
         return istyp;
    }
}
