/**
** \file opt_command.h 
** \brief contains all fonctions for basic operations
**
** \author Mazyad Vincent
*/


#ifndef OPT_COMMANDS_H
# define OPT_COMMANDS_H

struct ast *ast;
int commande_and(struct ast *ast);
int boolean_command(struct ast *ast);
int condition_command(struct ast *ast);
int while_command(struct ast *ast);
int pipe_command(struct ast *ast);
int separate_command(struct ast *ast);
int bitwise_command(struct ast *ast);

#endif
