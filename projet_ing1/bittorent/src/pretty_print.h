#ifndef PRETTY_PRINT_H
# define PRETTY_PRINT_H

#include "tool_parse.h"

void print_list(struct b_data *list, int spaces);
void print_dict(struct b_data *dict, int spaces);

#endif
