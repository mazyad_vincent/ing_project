# Generated by Django 2.0.4 on 2018-05-15 19:14

import django.contrib.auth.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
        ('intranet', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProxyUser',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterField(
            model_name='isapplyingto',
            name='motivation_letter',
            field=models.TextField(max_length=1000),
        ),
    ]
