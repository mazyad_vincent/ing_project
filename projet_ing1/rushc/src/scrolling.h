#ifndef SCROLLING_H
# define SCROLLING_H

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image>

#include "generation_map.h"

SDL_Rect camera_init(struct vector2 pos, int height, int wid_rec);

#endif
