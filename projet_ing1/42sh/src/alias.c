 /**
 ** \file alias.c
 ** \brief This file exec the alias builtin / unalias builtin
 ** \author Mazyad Vincent
 **
 ** This file exec the alias builtin / unalias
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ast.h"
#include "hashtable.h"
#include "builtins.h"
#include "replace_value.h"
#include "prompt.h"
#include "parser.h"

extern struct global g_global;

char* get_unalias_args(char *buff)
{
   if (!buff)
      return NULL;
   char *copy = buff;;
   while (copy && *copy && *copy != ' ')
      copy++;
   if (!copy)
      return NULL;
   while(copy && *copy && *copy == ' ')
      copy++;
   if (!copy)
      return NULL;
   size_t i = 0;
   while (copy[i] && copy[i] != ' ')
      i++;
   return strndup(copy, i);
}

/**
** \fn void unalias_builtin (struct hashtable *hash, struct ast *ast)
** \brief unalias the buf
**
** \param struct hashtable *hash char *buff
*/

int unalias_builtin(struct hashtable *hash, struct ast *ast)
{
   if (!ast)
   {
      fprintf(stderr, "unalias: need arg\n");
      return 1;
   }
   char *buff = ast->key;
   if (!find_hash(hash, buff))
   {
      fprintf(stderr, "bash: unalias: %s: not found\n", buff);
      return 1;
   }
   else
   {
      hash_pop(hash, buff);
      return 0;
   }
   return 1;
}
