/**
 ** \file ast/break-exp.hxx
 ** \brief Inline methods of ast::BreakExp.
 */

#pragma once

#include <ast/break-exp.hh>

// Hint: this needs to be done at TC-3.


namespace ast
{
  inline void BreakExp::def_set(Exp* exp)
  {
    exp_ = exp;
  }

  inline Exp* BreakExp::def_get()
  {
    return exp_;
  }

  inline const Exp* BreakExp::def_get() const
  {
    return exp_;
  }

  // FIXME

} // namespace ast

