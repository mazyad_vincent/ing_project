/**
** \file  conditions.c
** \brief get the number of arg in the ast
** \author Mazyad Vincent
**
** \return this file is for the if and else operations
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ast.h"
#include "call_commands.h"
#include "builtins.h"

/**
** \fn int get_until_then(struct ast *ast)
** \brief get the numb of args until then when if operation
**
** \param ast
** \return the numb of elements
*/

int get_until_then(struct ast *ast)
{
   int numb = 0;
   while (ast && strcmp(ast->key, ";") && strcmp(ast->key, "&") &&
         strcmp(ast->key, "fi"))
   {
       numb++;
       ast = ast->left;
   }
   return numb;
}

/**
** \fn exec_then(struct ast *ast)
** \brief exec the then  when if operation
**
** \param ast
** \return if the exec was well (0) or not (1)
*/

int exec_then(struct ast *ast)
{
   int numb = 0;
   while (ast && strcmp(ast->key, "then"))
      ast = ast->left;
   if (!ast)
   {
      fprintf(stderr, "missing then argument\n");
      return 1;
   }
   struct ast *copy = ast->left;
   if (is_builtins(copy))
      return call_builtins(copy);
   while (copy && (strcmp(copy->key, "else") && strcmp(copy->key, "fi")))
   {
      copy = copy->left;
      if (strcmp(copy->key, ";"))
         numb++;
   }
   if (!copy)
   {
      fprintf(stderr, "missing argument\n");
      return 1;
   }
   char *tab[numb + 1];
   tab[numb] = NULL;
   ast = ast->left;
   for (int i = 0; i < numb; ++i)
   {
     if (strcmp(ast->key, ";"))
        tab[i] = ast->key;
     ast = ast->left;
   }
   return execute_cmd(tab);
}

/**
** \fn exec_else(struct ast *ast)
** \brief exec the else  when if operation
**
** \param ast
** \return if the exec was well (0) or not (1)
*/

int exec_else(struct ast *ast)
{
  while (ast && strcmp(ast->key, "else"))
      ast = ast->left;
  if (!ast)
  {
     fprintf(stderr, "missing else arg\n");
     return 1;
  }
  ast = ast->left;
  struct ast *copy = ast;
  if (is_builtins(copy))
    return call_builtins(copy);
  int number_args = 0; /* numb of arg between else and fi */
  while (copy && strcmp(copy->key, "fi"))
  {
    if (strcmp(";", copy->key))
        number_args++;
    copy = copy->left;
  }
  if (!copy)
  {
     fprintf(stderr, "missing fi args\n");
     return 1;
  }
  char *tab[number_args + 1];
  tab[number_args] = NULL;
  for (int i = 0; ast && strcmp(ast->key, "fi"); i++)
  {
    if (strcmp(ast->key, ";"))
       tab[i] = ast->key;
    ast = ast->left;
  }
  return execute_cmd(tab);
}

/**
** \fn char ** condi_command(struct ast *ast)
** \brief return a tab of string for exec
**
** \param ast
** \return if the tab
*/

char** condi_command(struct ast *ast)
{
   int size = get_until_then(ast);
   if (!size)
   {
      fprintf(stderr, "invalid command\n");
      return NULL;
   }
   char **tab = malloc(sizeof(char*) * (size + 1));
   tab[size] = NULL;
   int index = 0;
   while (ast && strcmp(ast->key, "fi") && strcmp(ast->key, "done"))
   {
      if (strcmp(ast->key, ";") && strcmp(ast->key, "&"))
         tab[index] = ast->key;
      ast = ast->left;
   }
   char **copy_tab = tab;
   return copy_tab;
}

/**
** \fn int is_else_arg(struct ast *ast)
** \brief return 1 if there is an else after if operation
**
** \param ast
** \return 1 if true 0 if false
*/

int is_else_arg(struct ast *ast)
{
  while (ast)
  {
    if (!strcmp(ast->key, "else"))
      return 1;
    ast = ast->left;
  }
  return 0;
}
