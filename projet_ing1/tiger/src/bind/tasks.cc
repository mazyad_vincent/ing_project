/**
 ** \file bind/tasks.cc
 ** \brief Bind module tasks implementation.
 */

// CHANGED:FIXME 
#include <bind/libbind.hh>
#include <ast/libast.hh>
#include <misc/contract.hh>
#include <ast/tasks.hh>
#define DEFINE_TASKS 1
#include <bind/tasks.hh>
#undef DEFINE_TASKS
#include <bind/renamer.hh>


namespace bind
{
  namespace tasks
  {
    void bindings_compute()
    {
      precondition(ast::tasks::the_program);
      task_error() <<  bind(*ast::tasks::the_program);
      task_error().exit_on_error();
    }

    void bindings_display()
    {
      ast::bindings_display(std::cout) = true;
    }

    void rename_compute()
    {
      //precondition(ast::tasks::the_program);
      Renamer r;
      r(*ast::tasks::the_program);
    }
  }
}
