#ifndef MOVEMENTS_H
# define MOVEMENTS_H

float find_angle(struct vector2 *src, struct vector2 *dst);
struct queue *find_pace(struct car *car, struct vector2 *dst);

#endif
