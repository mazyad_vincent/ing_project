/**
 ** \file ast/call-exp.hxx
 ** \brief Inline methods of ast::CallExp.
 */

#pragma once

#include <ast/call-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  inline const typename CallExp::exps_type* CallExp::get_vect() const
  {
    return vect_;
  }

  inline typename CallExp::exps_type* CallExp::get_vect()
  {
    return vect_;
  }

  inline const typename misc::symbol& CallExp::get_func() const
  {
    return func_;
  }

  inline typename misc::symbol& CallExp::get_func()
  {
    return func_;
  }

  inline void CallExp::push_front(Exp* exp)
  {
    vect_->insert(vect_->begin(), exp);
  }
 

  inline const FunctionDec* CallExp::def_get() const
  {
    return f_;
  }

  inline FunctionDec* CallExp::def_get()
  {
    return f_;
  }

  inline void CallExp::def_set(ast::FunctionDec* f)
  {
    f_ = f;
  }

  // For bind/rename
  inline const typename misc::symbol& CallExp::name_get() const
  {
    return func_;
  }

  inline typename misc::symbol& CallExp::name_get()
  {
    return func_;
  }

  inline void CallExp::name_set(typename misc::symbol elm)
  {
    func_ = elm;
  }

} // namespace ast

