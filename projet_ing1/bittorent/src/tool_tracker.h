#ifndef TOOL_TRACKER_H
# define TOOL_TRACKER_H

#include "tool_parse.h"
#include "struct_peers.h"
#include <stdint.h>

/* not all but almost in tool_concatenation.c */
char *concatenation(char *a, char *b, size_t sizeea, size_t sizeb);
int length(int num, int base);
char *itoa(int num, int base);
char *traversal_dict(char *path, struct b_dict_list_node *dict);
char *traversal_list(char *path, struct b_list_node *list);
char *get_url(struct b_data *data);
char *port(); //get port
char *get_peer_id(); //get peerid
char *opts_tra(); //get left=...
char* decode(char *encrypted, size_t length_info); //sha1
char *get_compact(); //compact=0
char *full_hash(char *infohash); //infohash=sha1
int link_tracker(struct b_data *data, char *info_hash, size_t length_info,
                  int print, int verbose); //send to tracker


/* file print_ip .c */
struct sock_senti *get_peers(struct b_data *peers, int print);
char *build_ip(char *buf);

struct sock_senti* get_ip(char *buf, int print, size_t size);
void print_ip(char *buf, uint16_t port);

/* file peers_com.c */
char *craft_url(char *url, size_t size, char *hash);
void peer_request(struct sock_senti *senti, char *hash, char *url, int verbose);
void print_request(char *info_hash, char *url); /* --verbose option first line*/
void print_connected(char *info_hash, struct list_sock *sock); /* --verbose opt*/
void print_disconnected(char *info_hash, struct list_sock *sock);
void epoll_com(int tab[], size_t length, char *info_hash);
void sent_handshake(int fd, char *info_hash);

#endif
