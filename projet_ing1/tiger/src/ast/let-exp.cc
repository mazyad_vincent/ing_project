/**
 ** \file ast/let-exp.cc
 ** \brief Implementation of ast::LetExp.
 */

#include <ast/visitor.hh>
#include <ast/let-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  LetExp::LetExp(const Location& location, std::vector<Exp*>* vect, DecsList* dec)
    :  Exp(location)
    ,  vect_(vect)
    ,  dec_(dec)
  {}

  LetExp::~LetExp()
  {
    for (auto it = vect_->begin(); it != vect_->end(); ++it)
       delete *it;
    dec_->~DecsList();
    delete vect_;
  }

  void
  LetExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  LetExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

