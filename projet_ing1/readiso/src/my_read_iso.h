#ifndef MY_READ_ISO_H
# define MY_READ_ISO_H

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define SUPER_BLOCK_OFFSET (ISO_BLOCK_SIZE * ISO_PRIM_VOLDESC_BLOCK)

struct cmd {
    char *cmd;
    char *arg;
};

struct data {
    char *ptr;
    struct stat stat;
    struct iso_dir *root;
    struct iso_dir *cur;
    struct iso_path_table_le *path_table_root;
    struct iso_path_table_le *path_table;
    char *script;
};

void *cast_void(void *ptr);
char *cast_char(void *ptr);
void *add_offset(void *ptr, int offset);

int next_slash(const char *str, int i);

void *get_super_block(void *ptr);
void count_dir(char *ptr, int *dir, int *file, struct iso_dir *cur);

struct iso_path_table_le *get_n_path(char *ptr, int n);
struct iso_path_table_le *get_next_path(char *ptr, struct iso_path_table_le
        *path_table, const char *name);

struct iso_dir *find_dir(struct data *data, const char *name);
struct iso_dir *find_file_dir(struct iso_dir *cur, const char *name);
struct iso_dir *find_file(struct data *data, const char *name);

int shell_help(char *arg);
int shell_info(void* ptr, char *arg);
int shell_ls(struct data *data, char *name);
struct iso_path_table_le* shell_cd(char* ptr, struct iso_path_table_le*
    path, const char* name, const char* script);
void print_tree(char *ptr, struct iso_dir *cur, int depth, char begin);
void shell_tree(struct data *data, char *name);
int shell_get(struct data *data, const char *name);
int shell_cat(struct data *data, const char *name);
void shell_pwd(char *ptr, struct iso_path_table_le* path_table,
        struct iso_path_table_le* root);
void shell_quit(char* ptr, struct stat stat);

int sanity_check(void* ptr, struct stat stat);

struct cmd shell(void);
int select_function(struct cmd command, struct data *data);

#endif
