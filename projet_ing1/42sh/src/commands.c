/**
** \file commands.h
** \brief This file regroups all functions for operations commands
** \author Mazyad Vincent
*/

#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include "ast.h"
#include "call_commands.h"
#include "opt_commands.h"

/**
** \fn int commande_and(struct ast *ast)
** \brief call the operation and
**
** \param struct ast *ast
** \return the value of the execution
*/

int commande_and(struct ast *ast) /* && */
{
   if (fill_tab(ast->left, 0) != 0)
      return 1;
   return (!fill_tab(ast->right, 0))? 0 : 1;
}

/**
** \fn int boolean_command(struct ast *ast)
** \brief call the boolean operation
**
** \param struct ast *ast
** \return the value of the execution
*/

int boolean_command(struct ast *ast)
{
   if (fill_tab(ast->left, 0) != 0)
        return (!fill_tab(ast->right, 0))? 0 : 1;
   else
      return 0;
}

/**
** \fn int separate_command(struct ast *ast)
** \brief call the ";" operation
**
** \param struct ast *ast
** \return the value of the execution
*/

int separate_command(struct ast *ast)
{
  int res;
  fill_tab(ast->left, 0);
  res = fill_tab(ast->right, 0);
  return res;
}

/**
** \fn int exec_for_pipe(struct ast *ast)
** \brief do an exec for the pipe function
**
** \param struct ast *ast
** \return the value of the execution
*/

int exec_for_pipe(struct ast *ast)
{
   int i = 0;
   struct ast *copy = ast;
   for (; copy && strcmp(copy->key, ";") && strcmp(copy->key, "&"); ++i)
      copy = copy->left;
   char *tab[i + 1];
   tab[i] = NULL;
   for (int index = 0; ast; index++)
   {
      tab[index] = ast->key;
      ast = ast->left;
   }
   execvp(tab[0], tab);
   perror("error");
   exit(EXIT_FAILURE);
}

/**
** \fn int pipe_command(struct ast *ast)
** \brief do the pipe operation
**
** \param struct ast *ast
** \return the value of the execution
*/

int pipe_command(struct ast *ast)
{
   int fd[2];
   pid_t childpid;
   pid_t lastpid;
   int status; /* status of exec */
   if (pipe(fd) == -1)
   {
     perror("error");/* error */
     exit(1);
   }
   if ((childpid = fork()) == -1)
   {
     perror("error");/* error */
     exit(1);
   }
   else if (childpid == 0) /* child process */
   {
     close(fd[0]);
     if (dup2(fd[1], 1) == -1) /* connect the write with stdout */
        perror("error dup");
     exec_for_pipe(ast->left); /* exec */
     exit(EXIT_FAILURE);
   }
   else /* parents */
   {
     if ((lastpid = fork()) == -1)
     {
        perror("error"); /*  error */
        exit(1);
     }
     if (lastpid == 0)
     {
       close(fd[1]); /* close write side */
       dup2(fd[0], 0); /* connecte the read with stdin */
       exec_for_pipe(ast->right);
     }
     else
     {
       close(fd[0]);
       close(fd[1]);
       waitpid(childpid, &status, 0);
       waitpid(lastpid, &status, 0);
       return WEXITSTATUS(status);
     }
   }
   return 1;
}
