#include "parse.h"
#include <stdio.h>
#include <stdlib.h>

int is_file(const char *file)
{
  FILE *input = fopen(file, "r");
  if (!input)
    return 0;
  fclose(input);
  return 1;
}

char *get_input_from_file(char *file)
{
  FILE *input = fopen(file, "r+");
  fflush(input); 
  char c;
  char *buff = malloc(1);
  buff[0] = '\0';
  while ((c = fgetc(input)) != EOF)
  { 
    if (c == '\n')
      c = ';';
    size_t old_length = strlen(buff);
    buff = realloc(buff, old_length + 2);
    buff[old_length] = c;
    buff[old_length + 1] = '\0';
  }
  fclose(input);
  return buff;
}
