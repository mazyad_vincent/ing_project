#ifndef GET_IMAGE_H
# define GET_IMAGE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

struct image
{
  SDL_Rect *pos;
  SDL_Texture *texture;
};

struct image *get_image(char *path, SDL_Renderer *renderer, int x, int y, int flag_render, int flag_width_end, int flag_height_end);

# endif /* !GET_IMAGE_H_ */
