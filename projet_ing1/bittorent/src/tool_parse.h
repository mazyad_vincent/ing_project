#ifndef TOOL_PARSE_H
# define TOOL_PARSE_H

#include "memory_list.h"

enum type
{
  CHAR,
  INT,
  B_LIST,
  B_DICT_LIST
};

struct b_data
{
  union value *my_value;
  enum type my_type;
  size_t length;
};

struct b_list_node
{
  struct b_data *data;
  struct b_list_node *next;
  struct b_list_node *prev;
};

struct b_list
{
  struct b_list_node *head;
  struct b_list_node *tail;
  size_t size;
};

struct b_dict_list_node
{
  char *my_key;
  struct b_data *data;
  struct b_dict_list_node *next;
  struct b_dict_list_node *prev;
};

struct b_dict_list
{
  struct b_dict_list_node *head;
  struct b_dict_list_node *tail;
  size_t size;
};

union value
{
  char *string;
  int integer;
  struct b_list *my_b_list;
  struct b_dict_list *my_b_dict_list;
};

struct b_dict_list *dict_list_init(struct mem_list *memory);
void dict_list_push(struct b_dict_list *list, struct b_dict_list_node *node);

struct b_list *b_list_init(struct mem_list *memory);
void b_list_push(struct b_list *list, struct b_list_node *node);

#endif
