/**
 ** \file misc/symbol.hxx
 ** \brief Inline implementation of misc::symbol.
 */

#pragma once

#include <misc/symbol.hh>
#include <string>
namespace misc
{

  inline symbol&
  symbol::operator=(const symbol& rhs)
  {
    //FIXME
    this->obj_ = rhs.obj_;
    return *this;
  }

  inline bool
  symbol::operator==(const symbol& rhs) const
  {
    //FIXME
    return this->obj_ == rhs.obj_;
  }

  inline bool
  symbol::operator!=(const symbol& rhs) const
  {
    //FIXME
    return this->obj_ != rhs.obj_;
  }

  inline std::ostream&
  operator<<(std::ostream& ostr, const symbol& the)
  {
    //FIXME
    ostr << the.get();
    return ostr;
  }

}
