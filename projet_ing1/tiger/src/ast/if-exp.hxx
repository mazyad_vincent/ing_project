/**
 ** \file ast/if-exp.hxx
 ** \brief Inline methods of ast::IfExp.
 */

#pragma once

#include <ast/if-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  inline const Exp* IfExp::cond_get() const
  {
    return cond_;
  }


  inline Exp* IfExp::cond_get()
  {
    return cond_;
  }

  inline const Exp* IfExp::body_get() const
  {
    return body_;
  }

  inline Exp* IfExp::body_get()
  {
    return body_;
  }

  inline const Exp* IfExp::else_get() const
  {
    return els_;
  }

  inline Exp* IfExp::else_get()
  {
    return els_;
  }

} // namespace ast

