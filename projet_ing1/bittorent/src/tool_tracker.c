#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "tool_tracker.h"

char *concatenation(char *a, char *b, size_t a_size, size_t b_size)
{
  a = realloc(a, sizeof(char) * (a_size + b_size) + 1);
  size_t i = 0;
  while (i < b_size)
  {
    *(a + a_size + i) = *(b + i);
    i++;
  }
  *(a + a_size + i) = '\0';
  return a;
}
int length(int num, int base)
{
    int i = 0;
    if (num < 0)
    {
        i++;
        num = -num;
    }
    while(num > 0)
    {
        i++;
        num = num/base;
    }
    return i;
}

char* itoa(int num, int base)
{
    int end = length(num, base);
    int i = end;
    char *str = malloc(sizeof(char) * (i + 1));
    if (base < 2 || base > 32)
        return str;
    if (num < 0) 
    {   
        str[0] = '-';
        num = -num;
    }
    while(num > 0)
    {
       str[i - 1] = num % base + '0';
       num = num / base;
       i--;
    }
    str[end] = '\0';
    return str;
}
