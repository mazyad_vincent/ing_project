/**
** \file builtins.h  
** \brief contains all fonctions for builtins
**
** \author Mazyad Vincent
*/

#ifndef BUILTINS_H
# define BUILTINS_H

struct hashtable *hash;
struct ast *ast;
int is_builtins(struct ast *ast); /* check if this is a builtin */
int call_builtins(struct ast *ast); /* call the right builtin */
int parse_equal(char *string); /* check if the input is correct */
int cd_command(struct ast *ast); /* cd builtin */
int exit_builtin(struct ast *ast);
int export_builtin(struct ast *ast); /* export builtin */
void alias_builtin(struct hashtable *hash, struct ast *ast); /* alias built */
int unalias_builtin(struct hashtable *hash, struct ast *ast);
int echo_builtin(struct ast *ast); /* echo builtin */

#endif
