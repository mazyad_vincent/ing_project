#include <stdlib.h>
#include <stdio.h>
#include "generation_map.h"
#include "movement/texture.h"

# define MAP_WIDTH 100
# define MAP_HEIGHT 20

struct map* map_init()
{
   struct map *map = malloc(sizeof(struct map));
   map->width = MAP_WIDTH; /* random val */
   map->height = MAP_HEIGHT; /* rand val */
   enum type **position = malloc(sizeof(enum type*) * map->height);
   for (int i = 0; i < map->height; ++i)
   {
      position[i] = malloc(sizeof(enum type) * map->width);
      for (int j = 0; j < map->width; ++j)
      {
         if (i == 0)
           position[i][j] = TOP;
         else if (i == map->height - 1)
           position[i][j] = GROUND;
         else
           position[i][j] = NOT;
      }
   }
   map->pos = position;
   map->pos_player.x = 0;
   map->pos_player.y = 0;
   map->ia = NULL;
   return map;
}

struct player* player_init(int height)
{
   struct vector2 vec;
   vec.x = 0;
   vec.y = height / 2;
   struct player *player = malloc(sizeof(struct player));
   player->pos = vec;
   player->life = 20; /* life of player */
   return player;
}

struct IA* ia_init(int x, int y, SDL_Renderer *renderer)
{
  struct IA *ia = malloc(sizeof(struct IA));
  ia->ia_texture = texture_init("../texture/ship.png", renderer);
  ia->fire_texture = texture_init("../texture/laser.png", renderer);
  ia->pos.x = x;
  ia->pos.y = y;
  ia->next = NULL;
  struct vector2 *vec = malloc(sizeof(struct vector2));
  vec->x = x;
  vec->y = y;
  ia->fire = vec;
  ia->numb = 1;
  return ia;
}

void free_all(struct player *player, struct map *map)
{
  free(player);
  for (int i = 0; i < map->height; ++i)
    free(map->pos[i]);
  free(map->pos);
  free(map);
}
