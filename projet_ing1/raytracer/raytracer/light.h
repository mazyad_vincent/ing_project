#ifndef AMBIENT_LIGHT_H
# define AMBIENT_LIGHT_H

#include "parsing.h"

struct pixel ambient_lighting(struct lights *lights, struct materials *mat);
struct pixel directionnal_lighting(struct lights *lights, struct materials *mat,
                                   struct vector3 N, struct pixel pixel);
struct pixel point_lighting(struct lights *lights, struct materials *mat,
                            struct vector3 N, float distance, struct pixel pixel);

#endif
