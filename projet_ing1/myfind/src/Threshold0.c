#include "Threshold.h"

char *concatenation(char *a, char *b)
{
    int length_b = my_strlen(b);
    int length_a = my_strlen(a);
    a = realloc(a, sizeof(char) * (length_a + length_b + 1));
    if (!a)
        return NULL;
    char *cpy = a;
    char *cpy_b = b;
    while (*cpy != '\0')
        cpy++;
    while (*cpy_b != '\0')
        *(cpy)++ = *(cpy_b)++;
    *(cpy) = '\0';
   return a;
}

char *new_concatenation(char *a, char *b)
{
    
    int length_b = my_strlen(b);
    int length_a = my_strlen(a);
    char *new_char = calloc((length_b + length_a + 1), sizeof(char));
    if (!new_char)
        return NULL;
    char *cpy = a;
    char *cpy_b = b;
    char *cpy_new = new_char;
    while(*cpy)
    {
       *cpy_new = *cpy;    
       cpy_new++;
       cpy++; 
    }
    while(*cpy_b)
    {
       *cpy_new = *cpy_b;
       cpy_new++;
       cpy_b++;
    }
    *cpy_new = '\0';
    return new_char;
}


int is_symb(char *path)
{
   struct stat st;  
   if (lstat(path, &st) == -1)
   {
         fprintf(stderr, "%s %s\n", "myfind: cannot do open_file:" 
         , "the file or directory may not exist or is not valid.");
          exit(1);
   }
   return (S_ISLNK(st.st_mode))? 1 : 0;
}
    
char* print_dir(char *path)
{ 
    DIR *dir = opendir(path); //open directory of path
    if (!dir)
    {
        fprintf(stderr, "%s %s\n", "myfind: cannot do open_directory:"
        , "the directory may not exist or is not valid");
        exit(1);
    }
    struct dirent *entry = readdir(dir);
    char *print_dire = NULL;
    printf("%s\n",path);
    if (path[my_strlen(path) - 1] != '/')
        path = concatenation(path, "/");
    char *g = NULL;
    for (; entry; entry = readdir(dir))
    {
       while (entry && (my_strmcpy(entry->d_name, ".")
              || my_strmcpy(entry->d_name, "..")))
        {
                entry = readdir(dir);
                continue;
        }
       if (entry)
          g = new_concatenation(path, entry->d_name);
       if (entry && entry->d_type == DT_DIR)      //directory
       { 
            print_dire = new_concatenation(path, entry->d_name);
            print_dire = print_dir(print_dire);
            free(print_dire);
       }
       else if (entry && (entry->d_type == DT_REG || is_symb(g)))
       {
           char *print_files = new_concatenation(path, entry->d_name);
           printf("%s\n", print_files);
           free(print_files);
       }
       if (entry)
          free(g);
    }
    closedir(dir);
    return path;
}
