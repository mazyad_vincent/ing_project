#ifndef AST_H
# define AST_H

#include <stdio.h>

struct ast
{
  char *key;
  struct ast *right;
  struct ast *left;
};

struct ast *init_ast(char *key); /* init an ast */
void insert_node(struct ast **ast_main, struct ast *to_insert);
void free_ast(struct ast *ast);
void print_all(struct ast *ast);
struct ast *copy_ast(struct ast *ast); /* copy an ast */

#endif
