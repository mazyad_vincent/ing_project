/**
** \file  ps2.c
** \brief launch the ps2
** \author Mazyad Vincent
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "opt_handler.h"
#include "call_commands.h"
#include "prompt.h"
#include "ast.h"
#include "parser.h"
#include "opt_commands.h"

extern struct global g_global;

/**
** \fn int is_separator(char *a)
** \brief check if there is an ';' in the string
**
** \param char *a
** \return  return 1 if there is 0 if not
*/

int is_separator(char *a)
{
  while (a && *a)
  {
    if (*a == ';')
      return 1;
    a++;
  }
  return 0;
}

/**
** \fn int is_incomplete(char *a)
** \brief check if we have to launch the interactif mode
**
** \param char *a
** \return 1 is user called the prompt with while / if ...
*/

int is_incomplete(char *a)
{
   int i = 0;
   for (; a[i] && a[i] != ' '; i++);
   char tab[i+1];
   char *copy_tab = tab;
   copy_tab = memcpy(copy_tab, a, i);
   copy_tab[i] = '\0';
   if (!strcmp(copy_tab, "while") || (!strcmp(copy_tab, "if")))
      return 1;
   return 0;
}

/**
** \fn char *concatenation(char *a, char *b, size_t a_size, size_t b_size
** \brief concatenate two strings
**
** \param the strings to concatenate with their corresponding size
** \return the string concatenate
*/

char *concatenation(char *a, char *b, size_t a_size, size_t b_size)
{
  a = realloc(a, sizeof(char) * (a_size + b_size + 2));
  size_t i = 0;
  *(a + a_size) = ' '; /* add a space */
  while (i < b_size)
  {
    *(a + a_size + i + 1) = *(b + i);
    i++;
  }
  *(a + a_size + i + 1) = '\0';
  return a;
}

/**
** \fn char* prompt_ps2(char *buf)
** \brief launch the interactif mod
**
** \param the first buff the user types as input
** \return the whole string written until fi or done
*/

char* prompt_ps2(char *buf)
{
  char *new_buf;
  char *err = malloc(sizeof(char) * (strlen(buf) + 1));
  err = memcpy(err, buf, strlen(buf));
  err[strlen(buf)] = '\0';
  while (((new_buf = readline("> ")) != NULL))
  {
     char *buf_copy = white_spaces(new_buf);
     err = concatenation(err, buf_copy, strlen(err), strlen(buf_copy));
     if (!strcmp(buf_copy, "fi") || !strcmp(buf_copy, "done"))
     {
        free(new_buf);
        break;
     }
     free(new_buf);
  }
  return err;
}
