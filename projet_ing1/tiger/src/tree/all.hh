/**
 ** \file tree/all.hh
 ** \brief Intermediate representation: all the sources.
 **/

#pragma once

#include <tree/binop.hh>
#include <tree/call.hh>
#include <tree/cjump.hh>
#include <tree/const.hh>
#include <tree/eseq.hh>
#include <tree/exp.hh>
#include <tree/iterator.hh>
#include <tree/jump.hh>
#include <tree/label.hh>
#include <tree/mem.hh>
#include <tree/move.hh>
#include <tree/name.hh>
#include <tree/seq.hh>
#include <tree/stm.hh>
#include <tree/sxp.hh>
#include <tree/temp.hh>
#include <tree/tree.hh>
#include <tree/trees.hh>
