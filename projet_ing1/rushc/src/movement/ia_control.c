#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "../generation_map.h"
#include "texture.h"

void update_enum(struct map *map, struct IA *ia, int numb)
{
   map->pos[ia->pos.y][ia->pos.x] = NOT;
   map->pos[ia->pos.y + numb][ia->pos.x] = IA;
   ia->pos.y = ia->pos.y + numb;
   ia->pos_screen += 30 * numb;
}

int is_behind(struct map *map, struct IA *ia)
{
    if (ia->pos.x < map->pos_player.x)
    {
      free_ia(ia);
      return 1;
    }
    return 0;
}

int is_behind_fire(struct vector2 pos_fire)
{
    if (pos_fire.x - 1  < 0)
      return 1;
    return 0;
}

int conditions_IA(struct map *map, struct IA *ia, int y)
{
  int height = map->height;
  if (is_behind(map, ia))
     return 0;
  if (ia->pos.y + 1 >= height && ia->pos.y - 1 < 0)
     return 0;
  if (ia->pos.y + 1 >= height)
  {
     if (ia->pos.y - 1 >= 0 && map->pos[ia->pos.y - 1][ia->pos.x] == NOT)
        update_enum(map, ia, -1);
  }
  else if (ia->pos.y - 1  < 0)
  {
     if (ia->pos.y + 1 < height && map->pos[ia->pos.y + 1][ia->pos.x] == NOT)
        update_enum(map, ia, 1);
  }
  else if (ia->pos.y - 1 >= 0 && ia->pos.x < map->width)
  {
     int pos_y = ia->pos.y;
     if ((pos_y < y) && (map->pos[ia->pos.y + 1][ia->pos.x] == NOT))
        update_enum(map, ia, 1);
     else if ((pos_y > y) && (map->pos[ia->pos.y - 1][ia->pos.x] == NOT))
        update_enum(map, ia, -1);
  }
  return 1;
}

void create_fire(struct IA *ia)
{
  ia->fire = realloc(ia->fire, (sizeof(struct vector2) * (ia->numb + 1)));
  struct vector2 new_fire;
  /*new_fire.x = ia->pos.x - 15;
  new_fire.y = ia->ia_texture->height / 4 + ia->pos.y;*/
  new_fire.x = ia->pos.x;
  new_fire.y = ia->pos.y;
  ia->fire[ia->numb] = new_fire;
  ia->numb += 1;
}

void update_fire(struct IA *ia, struct map *map)
{
   for (size_t index = 0; index < ia->numb; index++)
   {
      if (!is_behind_fire(ia->fire[index]))
      {
          update_fire_map(map, ia, index, 0);
          ia->fire[index].x -= 1;
          update_fire_map(map, ia, index, 1);
      }
   }
}
void update_ia(struct map *map)
{
   int player_y = map->pos_player.y; /* pos of player */
   struct IA *head_ia = map->ia;
   struct IA *copy = head_ia;
   while (copy)
   {
      int cond = conditions_IA(map, copy, player_y);
      SDL_Delay(1);
      if (cond)
        update_fire(copy, map);
      copy = copy->next;
   }
}
