/**
** \file cd.c
** \brief execute the cd command
** \author Mazyad Vincent
**
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "ast.h"

/**
** \fn int is_dir(char *path)
** \brief check if the path is a valid directory
**
** \param a path
** \return  return 1 if it is a direct 0 if not
*/

static int is_dir(char *path)
{
  struct stat sb;
  if (!access(path, F_OK))
  {
     if (!stat(path, &sb))
     {
        if (S_ISDIR(sb.st_mode) && !access(path, X_OK))
              return 1;
     }
     else
        return 0;
  }
  else
    fprintf(stderr, "access is wrong\n");
  return 0;
}

/**
** \fn cd_command(struct ast *ast)
** \brief do the cd operation
**
** \param an ast
** \return  return 0 if cd well done 1 if not
*/

int cd_command(struct ast *ast)
{
   int res = 0; /* result of execution */
   char *path = NULL;
   char *check_pwd = get_current_dir_name();
   if (ast->left)
      path = ast->left->key;
   if (!path)
   {
     setenv("OLDPWD", check_pwd, 1);
     res = chdir(getenv("HOME"));
     free(check_pwd);
     return (!res)? 0 : 1;
   }
   if (!is_dir(path))
   {
      fprintf(stderr, "bash: cd: %s: Not a directory\n", path);
      return 1;
   }
   setenv("OLDPWD", check_pwd, 1);
   free(check_pwd);
   res = chdir(path);
   return (!res)? 0 : 1;
}
