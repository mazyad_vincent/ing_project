#include "metadata.h"

static pthread_mutex_t thread_m = PTHREAD_MUTEX_INITIALIZER;

void free(void *ptr)
{
  if (!ptr)
    return;
  pthread_mutex_lock(&thread_m);
  struct metadata *get_meta = cast_void(-sizeof(struct metadata), ptr);
  if (!get_meta)
  {
    pthread_mutex_unlock(&thread_m);
    return;
  }
  struct block *blk = get_meta->blk;
  get_meta->free = 1;
  if (is_full_free(blk))
  {
    if (blk == cur_block)
      cur_block = cur_block->next;
    struct block *prev_block = blk->prev;
    if (prev_block) /* need to reorganise blocks */
    {
      struct block *next_blk = blk->next;
      prev_block->next = next_blk;
      if (next_blk)
        next_blk->prev = prev_block;
    }
    munmap(blk, blk->size);
  }
  pthread_mutex_unlock(&thread_m); 
}
