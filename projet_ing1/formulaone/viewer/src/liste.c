#include "list.h"

struct sentinelle* senti_init()
{
    struct sentinelle *senti = malloc(sizeof(struct sentinelle));
    if (!senti)
        return NULL;
    senti->head = NULL;
    return senti;
}

struct list* list_init()
{
    struct list *list = malloc(sizeof(struct list));
    if (!list)
        return NULL;
    list->data = NULL;
    list->next = NULL;
    list->weight = 0;
    return list;
}

void push_list(struct sentinelle *senti, struct graph *graph, int weight)
{
    struct list *cpy = senti->head;
    struct list *push = list_init();
    push->data = graph;
    push->weight = weight;
    if (!cpy)
    {
       senti->head = push;
       return;
    }
    while (cpy && cpy->next)
        cpy = cpy->next;
    cpy->next = push;
}

void free_all(struct sentinelle *senti)
{
    struct list *cpy = senti->head;
    while (cpy)
    {
        struct graph *graph = cpy->data;
        destroy_all(graph);
        struct list *to_pop = cpy;
        cpy = cpy->next;
        free(to_pop);
    }
   free(senti);
}
