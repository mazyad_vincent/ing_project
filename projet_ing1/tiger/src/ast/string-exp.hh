/**
 ** \file ast/string-exp.hh
 ** \brief Declaration of ast::StringExp.
 */

#pragma once

#include <ast/exp.hh>
#include <string>
#include <misc/symbol.hh>

namespace ast
{

  /// StringExp.
  class StringExp : public Exp
  {
  // FIXME: Some code was deleted here.
   public:
    StringExp(const Location& location, const std::string& str);
    StringExp(const StringExp&) = delete;
    StringExp& operator=(const StringExp&) = delete;
    virtual ~StringExp();
    const std::string& str_get() const;
    std::string& str_get();
    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;
   protected:
    std::string str_;
  };

} // namespace ast

#include <ast/string-exp.hxx>

