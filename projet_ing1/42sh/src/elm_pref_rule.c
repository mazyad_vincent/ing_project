/**
** \file elm_pref_rule.c
** \brief This file contains prefix and element rule of the parser
** \author Odile Guillaume
**
** This file contains the prefix and element rules of the LL grammar for the
** parser.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

/**
** \fn struct ast *p_element(void)
** \brief reprensents the element rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_element(void)
{
  struct ast *curr = curr_tok();
  struct ast *node = NULL;
  if (curr && (curr->type == WORD || curr->type == IONUMBER))
  {
    eat();
    node = ast_create(NULL, curr, NULL, NULL);
  }
  else
  {
    ast_free(curr);
    node = p_redirection();
  }
  return node;
}

/**
** \fn struct ast *p_prefix(void)
** \brief reprensents the prefix rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_prefix(void)
{
  struct ast *curr = curr_tok();
  struct ast *node = NULL;
  if (curr && (curr->type == ASSIGMENT_WORD || curr->type == IONUMBER))
  {
    eat();
    node = ast_create(NULL, curr, NULL, NULL);
  }
  else
  {
    ast_free(curr);
    node = p_redirection();
  }
  return node;
}
