#ifndef QUEUE_H
# define QUEUE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <err.h>

struct queue_list
{
   struct queue_list *next;
   char *data;
};

struct queue
{
   int size;
   struct queue_list *head;
};

struct queue* queue_init();
struct queue* queue_push(struct queue *queue, char *elm);
struct queue_list* queue_pop(struct queue *queue);
void print_queue(struct queue *queue);
void queue_destroy(struct queue *queue);
#endif
