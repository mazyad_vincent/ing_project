                                                            /* -*- C++ -*- */
%option c++
%option nounput
%option debug
%option batch

%{

#include <cerrno>
#include <climits>
#include <regex>
#include <string>
#include <iostream>
#include <cstdio>
#include <boost/lexical_cast.hpp>

#include <misc/contract.hh>
  // Using misc::escape is very useful to quote non printable characters.
  // For instance
  //
  //    std::cerr << misc::escape('\n') << '\n';
  //
  // reports about `\n' instead of an actual new-line character.
#include <misc/escape.hh>
#include <misc/symbol.hh>
#include <parse/parsetiger.hh>
#include <parse/tiger-parser.hh>
#define YY_USER_ACTION tp.location_.columns(yyleng);
  // FIXME: Some code was deleted here.

// Convenient shortcuts.
#define TOKEN_VAL(Type, Value)                  \
  parser::make_ ## Type(Value, tp.location_)

#define TOKEN(Type)                             \
  parser::make_ ## Type(tp.location_)


// Flex uses `0' for end of file.  0 is not a token_type.
#define yyterminate() return TOKEN(EOF)

# define CHECK_EXTENSION()                              \
  do {                                                  \
    if (!tp.enable_extensions_p_)                       \
      tp.error_ << misc::error::error_type::scan        \
                << tp.location_                         \
                << ": invalid identifier: `"            \
                << misc::escape(yytext) << "'\n";       \
  } while (false)

YY_FLEX_NAMESPACE_BEGIN
%}

%x SC_COMMENT SC_STRING
/* Abbreviations.  */
int             [0-9]+
ID ([a-zA-Z][0-9a-zA-Z_]*|"_main")
ESCAPE \\[afbnrtv]
BACKS "\\"
OCTAL "\\"[0-3][0-7][0-7]
HEXA \\x[0-9a-fA-F][0-9a-fA-F]
SPACE [ \t]
BACKSDOUBLE "\\\""

  /* FIXME: Some code was deleted here. */
%%
%{
  int nb = 0;
  std::string global_string = "";
  // Each time yylex is called.
  tp.location_.step();
%}

"\"" {global_string.clear(); BEGIN(SC_STRING);}
"/*" {nb++; BEGIN(SC_COMMENT);}
"\n\r" {tp.location_.lines();}
"\r\n" {tp.location_.lines();}
"\r" {}
"\n" {tp.location_.lines();}
"array" return TOKEN(ARRAY);
"if" return TOKEN(IF);
"then" return TOKEN(THEN);
"else" return TOKEN(ELSE);
"while" return TOKEN(WHILE);
"for" return TOKEN(FOR);
"to" return TOKEN(TO);
"do" return TOKEN(DO);
"let" return TOKEN(LET);
"in" return TOKEN(IN);
"end" return TOKEN(END);
"of" return TOKEN(OF);
"break" return TOKEN(BREAK);
"nil" return TOKEN(NIL);
"function" return TOKEN(FUNCTION);
"var" return TOKEN(VAR);
"type" return TOKEN(TYPE);
"import" return TOKEN(IMPORT);
"primitive" return TOKEN(PRIMITIVE);
"class" return TOKEN(CLASS);
"method" return TOKEN(METHOD);
"extends" return TOKEN(EXTENDS);
"new" return TOKEN(NEW);
"." return TOKEN(DOT);
"," return TOKEN(COMMA);
":" return TOKEN(COLON);
";" return TOKEN(SEMI);
"(" return TOKEN(LPAREN);
")" return TOKEN(RPAREN);
"[" return TOKEN(LBRACK);
"]" return TOKEN(RBRACK);
"{" return TOKEN(LBRACE);
"}" return TOKEN(RBRACE);
":=" return TOKEN(ASSIGN);
"+" return TOKEN(PLUS);
"-" return TOKEN(MINUS);
"/" return TOKEN(DIVIDE);
"*" return TOKEN(TIMES);
"<>" return TOKEN(NE);
"=" return TOKEN(EQ);
"<" return TOKEN(LT);
">" return TOKEN(GT);
">=" return TOKEN(GE);
"<=" return TOKEN(LE);
"&" return TOKEN(AND);
"|" return TOKEN(OR);
"_cast" return TOKEN(CAST);
"_exp" return TOKEN(EXP);
"_lvalue" return TOKEN(LVAL);
"_namety" return TOKEN(NAMETY);
"_decs" return TOKEN(DECS);

{SPACE} {}

{ID}   {misc::symbol id(yytext); return TOKEN_VAL(ID, id);}

<SC_STRING>{

<<EOF>> {tp.error_ << misc::error::error_type::scan
                   << tp.location_
                   << ": invalid EOF"
                   << misc::escape(yytext) << "'\n'";
         BEGIN(INITIAL);
        }
"\""   {BEGIN(INITIAL); return TOKEN_VAL(STRING, global_string);}
{OCTAL} {global_string.append(std::to_string(std::stoi(yytext + 1)));}
{HEXA}  {global_string.append(std::to_string(std::stoi(yytext + 2)));}
{ESCAPE} {global_string.append(yytext);}
{BACKS} {global_string.append(yytext);}
{BACKSDOUBLE} {global_string.append(yytext);}
\\.   {
         tp.error_ << misc::error::error_type::scan
         << tp.location_
         << " invalid string "
         << misc::escape(yytext) << "'\n'";
         BEGIN(INITIAL);
        }
.      {global_string.append(yytext);}
}

{int}         {
                int val = 0;
                val = atoi(yytext);
                if (val > INT_MAX / 2 || val <= INT_MIN)
                   tp.error_ << misc::error::error_type::scan
                             << tp.location_
                             << ": invalid int "
                             << misc::escape(yytext) << "'\n'";
                else
                   return TOKEN_VAL(INT, val);
              }

<SC_COMMENT>{

<<EOF>> {tp.error_ << misc::error::error_type::scan
                   << tp.location_
                   << ": invalid EOF"
                   << misc::escape(yytext) << "'\n'";
        }
"/*" nb++;
"*/" {nb--; if (!nb) BEGIN (INITIAL);}
.  {}
}


<<EOF>> {if (nb) tp.error_ << misc::error::error_type::scan
                             << tp.location_
                             << ": invalid comment "
                             << misc::escape(yytext) << "'\n'";
         ; return TOKEN(EOF);}


. { tp.error_ << misc::error::error_type::scan
                << tp.location_
                << ": invalid token: `"
                << misc::escape(yytext) << "'\n";
}
%%

// Do not use %option noyywrap, because then flex generates the same
// definition of yywrap, but outside the namespaces, so it defines it
// for ::yyFlexLexer instead of ::parse::yyFlexLexer.
int yyFlexLexer::yywrap() { return 1; }

void
yyFlexLexer::scan_open_(std::istream& f)
{
  yypush_buffer_state(YY_CURRENT_BUFFER);
  yy_switch_to_buffer(yy_create_buffer(&f, YY_BUF_SIZE));
}

void
yyFlexLexer::scan_close_()
{
  yypop_buffer_state();
}

YY_FLEX_NAMESPACE_END
