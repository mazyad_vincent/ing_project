from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns, static
from django.conf import settings

from . import views

urlpatterns = [
    path('', views.home_view, name='accueil'),
    path('search/', views.result_search_view, name='search'),
    path('association/<slug:slug>/', views.asso_view, name='asso'),
    path('association/', views.list_asso_view, name="l_asso"),
    path('association/<slug:slug>/enacs/export/', views.export_enac_view, name='export_enac'),
    path('association/<slug:slug>/enacs/', views.enacs_view, name='enacs'),
    path('association/<slug:slug>/finances/', views.finances_view, name='finances'),
    path('association/<slug:slug>/members/', views.member_view, name='l_member'),
    path('association/<slug:slug>/members_delete/<int:user_id>/', views.delete_member_view, name='delete_member'),
    path('association/<slug:slug>/apply_to_asso/', views.apply_to_asso_view, name='apply_asso'),
    path('association/<slug:slug>/apply_list/', views.list_apply_view, name='l_list_apply'),
    path('association/<slug:slug>/apply_resolve/<int:apply_id>/<int:accepted>', views.apply_resolve, name='apply_resolve'),
    path('association/<slug:slug>/reserve/', views.get_room_view, name='reserve'),
    path('association/<slug:slug>/get_board/', views.get_board_list, name='get_board'),
    path('creationassociation/', views.crea_asso_view, name="crea_asso"),
    path('respo/', views.respo_view, name='responsable'),
    path('respo/changestatusasso/<int:apply_id>/<int:accepted>/', views.respo_change_status_asso, name='respo_change_status_asso'),
    path('respo/acceptBudget/<int:budget_id>/<int:accepted>/', views.acceptBudget_view, name='acceptBudget'),
    path('respo/open_enac/', views.demande_open_enac, name='open_enac'),
    path('respo/close_enac/', views.demande_close_enac, name='close_enac'),
    path('user/<login>/', views.user_view, name='user'),
    path('user/', views.list_user_view, name='l_user'),
    path('login/', views.login_view, name='login'),
    path('create/', views.create_user_view, name='create_account'),
    path('disconnect/', views.disconnect, name='disconnect'),
    path('list_asso/', views.get_asso_list, name='asso_list'),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
