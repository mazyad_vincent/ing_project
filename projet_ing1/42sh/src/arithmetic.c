#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ast.h"
#include "hashtable.h"
#include "arithmetic.h"
#include "queue.h"
#include "parser.h"

extern struct global g_global;

int which_type(char a)
{
  if (a == ')')
    return -2;
  if (a == ' ')
    return -1;
  if (a == '(')
    return 0;
  if (a == '+')
    return 2;
  if (a == '*')
    return 3;
  if (a == '-')
    return 4;
  if (a == '/')
    return 5;
  if (a == '|')
    return 6;
  if (a == '&')
    return 7;
  return 1;
}

static struct liste* add_in_output(struct liste *output, int entier)
{
  if (!output)
  {
    output = init_liste(1);
    output->entier = entier;
  }
  else
  {
    struct liste *list = init_liste(1);
    list->entier = entier;
    output = liste_push(output, list);
  }
  return output;
}

static int get_size(char *key, size_t size)
{
  char *integer = strndup(key, size);
  int entier = atoi(integer);
  free(integer);
  return entier;
}

struct liste* get_polonaise_list(char *input, struct queue *queue)
{
  char *key = input;
  size_t size_key = strlen(input) - 2;
  struct liste *output = NULL;
  while (key && *key && size_key)
  {
     if (*key == ' ')
     {
        key++;
        size_key--;
        continue;
     }
     int actual = which_type(*key);
     if (actual == 1) /* integer */
     {
        size_t size = 0;
        for (int k = 0; key[k] && (which_type(key[k]) == 1); k++)
           size++;
        output = add_in_output(output, get_size(key, size));
        key += size;
        size_key -= size;
        continue;
     }
     else
     {
        if (*key == ')')
        {
           struct liste *to_pop = NULL;
           while (queue->size && queue->list->val != 0 &&
                 (to_pop = queue_pop(queue)) != NULL)
                output = liste_push(output, to_pop);
           if (queue->size && !queue->list->val) /* ( */
                free(queue_pop(queue));
           else
           {
                fprintf(stderr, "bash: syntax error: missing token ')'\n");
                return NULL;
           }
        }
        else
        {
           int top_queue = -10;
           if (queue->list)
               top_queue = queue->list->val;
           if ((actual == 2 || actual == 4) &&
              (top_queue == 3 || top_queue == 5))
           {
              struct liste *to_pop = NULL;
              while (queue->size &&
                    queue->list->val != 0 &&
                    (to_pop = queue_pop(queue)) != NULL)
                 output = liste_push(output, to_pop);
           }
           struct liste *list = init_liste(actual);
           queue = queue_push(queue, list);
        }
     }
     key += 1;
     size_key -= 1;
  }
  struct liste *to_pop = NULL;
  while ((to_pop = queue_pop(queue)) != NULL)
     output = liste_push(output, to_pop);
  return output;
}

int do_all_operation(struct ast *ast)
{
   char *key = NULL;
   size_t ast_key_size = strlen(ast->key) - 3;
   if ((key = replace_node_operation(ast->key + 3, ast_key_size)) != NULL)
   {
      free(ast->key);
      ast->key = key;
   }
   char *to_free = ast->key + 3; /* after the '(' */
   struct queue *queue = init_queue();
   struct liste *polonaise = get_polonaise_list(to_free, queue);
   if (!polonaise)
   {
      free(queue);
      return 1;
   }
   struct tree *tree = create_tree(polonaise);
   int result_operation = result(tree);
   free(ast->key);
   size_t size = result_operation < 0; /* calculate nb of decimal in result */
   for (int div = result_operation; div != 0; size++)
      div = div / 10;
   char *new_key = malloc(size + 1);
   sprintf(new_key, "%d", result_operation);
   ast->key = new_key;
   free(queue); /* free the queue */
   free_all_tree(tree);
   return 0;
}
