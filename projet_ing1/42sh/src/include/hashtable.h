/**
** \file hashtable.h 
** \brief contains all fonctions used for the hashtable
**
** \author Mazyad Vincent
*/

#ifndef HASHTABLE_H
# define HASHTABLE_H

#include <stdio.h>

/**
**\struct hashtable: regroup the elements used for an hashtable
*/

struct hashtable
{
   size_t capacity; /* capacity of the hashtable */
   struct list **list; /* list of the hashtable */
};

/**
**\struct list: regroupe elements for lists which will be inserted
** in the hashtable
*/

struct list
{
  char *data;
  char *key; /* name of the key which is hashed */
  size_t indice;
  int alias;
  struct list *next;
};

/* fonctions for hash table */
struct hashtable* hash_init(); /* initialise the hash table */
void free_hash(struct hashtable *hash); /* free the hash table */
size_t hash_func(char *data, size_t capacity); /* calculate the hash func */

/* fonctions for list */
struct list* list_init(size_t indice, char *data, char *key, int alias);
                                  /* initialise the list */
struct list* list_push(struct list *head, struct list *list_add);
                                 /* add a linked list */
void free_list(struct list *list); /* free the list */
void hash_push(struct hashtable *has, char *key, char *dat, int alias);
struct list* find_hash(struct hashtable *hash, char *key); /* find the data */
void hash_pop(struct hashtable *hash, char *key); /* pop the hash */

#endif
