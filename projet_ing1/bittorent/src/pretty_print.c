#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "memory_list.h"
#include "tool_parse.h"
#include "parsing.h"
#include "pretty_print.h"

void print_spaces(int spaces)
{
  int i = 0;
  while (i < spaces)
  {
    printf(" ");
    i++;
  }
}

void print_key(struct b_dict_list_node *node)
{
  printf("\"%s\":", node->my_key);
}

void print_int(struct b_data *data)
{
  printf("\"%d\"", data->my_value->integer);
}

void print_string(struct b_data *data)
{
  printf("\"");
  size_t i = 0;
  while (i < data->length)
  {
    int c = *(data->my_value->string + i);
    if (c < 32 || c > 126)
      printf("\\u%04X", (unsigned char)(c));
    else if (c == 34 || c == 92)
      printf("\\%c", c);
    else
      printf("%c", c);
    i++;
  }
  printf("\"");
}

void print_value(struct b_data *data, int spaces)
{
  if (data->my_type == INT)
    print_int(data);
  else if (data->my_type == CHAR)
    print_string(data);
  else if (data->my_type == B_LIST)
    print_list(data, 2 + spaces);
  else if (data->my_type == B_DICT_LIST)
    print_dict(data, 2 + spaces);
}

void print_list(struct b_data *list, int spaces)
{
  struct b_list_node *node = list->my_value->my_b_list->head;
  printf("[\n");
  spaces += 2;
  while (node)
  {
    print_spaces(spaces);
    print_value(node->data, spaces);
    if (node->next)
      printf(",");
    printf("\n");
    node = node->next;
  }
  spaces -= 2;
  print_spaces(spaces);
  printf("]\n");
}

void print_dict(struct b_data *dict, int spaces)
{
  struct b_dict_list_node *node = dict->my_value->my_b_dict_list->head;
  printf("{\n");
  spaces += 2;
  while (node)
  {
    print_spaces(spaces);
    print_key(node);
    print_value(node->data, spaces);
    if (node->next)
      printf(",");
    printf("\n");
    node = node->next;
  }
  spaces -= 2;
  print_spaces(spaces);
  printf("}");
}
