/**
** \file if_rule.c
** \brief This file contains the if rule of the parser
** \author Odile Guillaume
**
** This file contains the if rule of the LL grammar for the parser.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

/**
** \fn struct ast *p_rule_if(void)
** \brief reprensents the if rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_rule_if(void)
{
  struct ast *curr = curr_tok();
  if (!curr || curr->type != IF)
    return free_node(curr);
  eat();
  struct ast *node = p_compound_list();
  if (!node)
    return free_node(curr);
  node = ast_create(node, curr, NULL, NULL);
  curr = curr_tok();
  if (!curr || curr->type != THEN)
  {
    ast_free(curr);
    return free_node(node);
  }
  eat();
  node = ast_create(NULL, node, curr, NULL);
  struct ast *node2 = p_compound_list();
  if (!node2)
    return free_node(node);
  node = ast_create(NULL, node, node2, NULL);
  node2 = p_else_clause();
  if (node2)
    node = ast_create(NULL, node, NULL, node2);
  curr = curr_tok();
  if (!curr || curr->type != FI)
    return free_node(node);
  eat();
  node = ast_create(curr, node, NULL, NULL);
  return node;
}
