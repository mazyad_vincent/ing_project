#ifndef EXECUTE_H
# define EXECUTE_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void fill_args(struct ast *ast, char **tab, size_t *index); /* fill tab with ast keys */
int launch_exec(struct ast *ast);	/* call execute */
int exec_rec(struct ast *ast); /* call the correct operation/builtin */
int execute(char *tab[]); /* exec the process */

#endif
