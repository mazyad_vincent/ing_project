#include <stdio.h>
#include <string.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "texture.h"

//load texture from file
struct texture *texture_init(char *path, SDL_Renderer *renderer)
{
  struct texture *t = malloc(sizeof(struct texture));
  t->width = 0;
  t->height = 0;
  t->degree = 0;
  t->m_texture = NULL;

  SDL_Texture *new_texture = NULL;
  SDL_Surface *loaded_surface = IMG_Load(path);
  if (!loaded_surface)
  {
    printf( "Unable to load image %s! SDL_image Error: %s\n",
        path, IMG_GetError() );
    return NULL;
  }
  else
  {
    SDL_SetColorKey(loaded_surface, SDL_TRUE,
        SDL_MapRGB(loaded_surface->format, 0, 0xFF, 0xFF));
    new_texture = SDL_CreateTextureFromSurface(renderer, loaded_surface);
    if(!new_texture)
    {
      printf( "Unable to create texture from %s! SDL Error: %s\n",
          path, SDL_GetError() );
      return NULL;
    }
    else
    {
      t->width = loaded_surface->w;
      t->height = loaded_surface->h;
    }
    SDL_FreeSurface(loaded_surface);
  }
  t->m_texture = new_texture;
  return t;
}

void texture_free(struct texture *t)
{
  if (t->m_texture)
  {
    SDL_DestroyTexture(t->m_texture);
    free(t);
  }
}

//Set color modulation
void setColor(struct texture *t, Uint8 r, Uint8 g, Uint8 b)
{
  SDL_SetTextureColorMod(t->m_texture, r, g, b);
}

//Set blending
void setBlendMode(struct texture *t, SDL_BlendMode blending)
{
  SDL_SetTextureBlendMode(t->m_texture, blending);
}

//Set alpha modulation
void setAlpha(struct texture *t, Uint8 alpha)
{
  SDL_SetTextureAlphaMod(t->m_texture, alpha);
}

//Renders texture at given point
void render(struct texture *t, int x, int y, SDL_Rect* clip,
    SDL_Point* center, SDL_RendererFlip flip, SDL_Renderer *renderer)
{
  if (!renderer)
    return;
  SDL_Rect render_quad = { x,  y, t->width, t->height};
  if (clip)
  {
    render_quad.w = clip->w;
    render_quad.h = clip->h;
  }
  SDL_RenderCopyEx(renderer, t->m_texture, clip, &render_quad, t->degree,
      center, flip);
}
