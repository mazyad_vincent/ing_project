/**
** \file if.c
** \brief This file regroups the operations for if operation
** \author Mazyad Vincent
**
** This file regroups the informations for if operation
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "ast.h"
#include "call_commands.h"
#include "builtins.h"

/**
** \fn int condition_if_command(struct ast *ast)
** \brief exec condi if
**
** \param struct ast *ast
** \return return 1 if fails 0 if worked
*/

int condition_if_command(struct ast *ast) /* exec condition if */
{
  ast = ast->left;
  if (is_builtins(ast))
     return call_builtins(ast);
  char **tab = condi_command(ast);
  int i = 0;
  while(tab[i])
    i++;
  char *tab_cop[i + 1];
  for(int g = 0; g < i; g++)
    tab_cop[g] = tab[g];
  tab_cop[i] = NULL;
  free(tab);
  return execute_cmd(tab_cop);
}

/**
** \fn int then_if_command(struct ast *ast)
** \brief exec condi then
**
** \param struct ast *ast
** \return return 1 if fails 0 if worked
*/

int then_if_command(struct ast *ast) /* exec then */
{
  ast = ast->right;
  struct ast *copy = ast;
  int res = 0;
  while (copy)
  {
    copy = copy->left;
    if (copy && is_builtins(copy))
    {
      if (!strcmp(copy->key, "continue"))
         return 0;
      if (!strcmp(copy->key, "break"))
         return -2; 
      res = call_builtins(copy);
      while (copy && strcmp(copy->key, ";") && strcmp(copy->key, "&"))
            copy = copy->left;
      continue;
    }
    if (copy && strcmp(copy->key, ";") && strcmp(copy->key, "&"))
    {  
      int numb = get_until_then(copy);
      char *tab[numb + 1];
      tab[numb] = NULL;
      for (int i = 0; i < numb; ++i)
      {
        if (strcmp(copy->key, ";") && strcmp(copy->key, "&"))
          tab[i] = copy->key;
        copy = copy->left;
      }
      res = execute_cmd(tab);
    }
  }
  return res;
}

/**
** \fn int else_if_command(struct ast *ast)
** \brief exec condi else
**
** \param struct ast *ast
** \return return 1 if fails 0 if worked
*/

int else_if_command(struct ast *ast)
{
  ast = ast->third;
  struct ast *copy = ast;
  int res = 0;
  while (copy)
  {
    copy = copy->left;
    if (copy && is_builtins(copy))
    {
      if (!strcmp(copy->key, "continue"))
         return 0;
      if (!strcmp(copy->key, "break"))
         return -2; 
      res = call_builtins(copy);
      while (copy && strcmp(copy->key, ";") && strcmp(copy->key, "&"))
        copy = copy->left;
      continue;
    }
    if (copy && strcmp(copy->key, ";") && strcmp(copy->key, "&"))
    {
      int numb = get_until_then(copy);
      char *tab[numb + 1];
      tab[numb] = NULL;
      for (int i = 0; i < numb; ++i)
      {
        if (strcmp(copy->key, ";") && strcmp(copy->key, "&"))
          tab[i] = copy->key;
        copy = copy->left;
      }
      res = execute_cmd(tab);
    }
  }
  return res;
}

/**
 ** \fn int if_operation(struct ast *ast)
 ** \brief exec if operation
 **
 ** \param struct ast *ast
 ** \return return 1 if fails 0 if worked
 */

int if_operation(struct ast *ast)
{
  int res = select_exec(ast->left);
  if (!res)
    return then_if_command(ast);
  if (res == 1 && ast->third)
    return else_if_command(ast);
  if (res != 1 && !condition_if_command(ast))
    return then_if_command(ast);
  else if (ast->third)
    return else_if_command(ast);
  return 1;
}
