/**
 ** \file ast/call-exp.hh
 ** \brief Declaration of ast::CallExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/function-dec.hh>
#include <misc/symbol.hh>
#include <ast/fwd.hh>

namespace ast
{

  /// CallExp.
  class CallExp : public Exp
  {
  // FIXME: Some code was deleted here.
     public:
       using exps_type = std::vector<Exp*>;
       CallExp(const Location& location, const misc::symbol& name,
               exps_type* vect);
       CallExp(const CallExp&) = delete;
       CallExp& operator=(const CallExp&) = delete;
       virtual ~CallExp();
       const exps_type* get_vect() const;
       exps_type* get_vect();
       const misc::symbol& get_func() const;
       misc::symbol& get_func();
       void accept(ConstVisitor& v) const override;
       void accept(Visitor& v) override;
       void push_front(Exp* exp);

       const FunctionDec* def_get() const;

       FunctionDec* def_get();
       void def_set(FunctionDec* f);

       // For bind/rename
       const misc::symbol& name_get() const;
       misc::symbol& name_get();

       void name_set(misc::symbol elm);
       
  

    protected:
       exps_type* vect_;
       misc::symbol func_;
       FunctionDec* f_ = nullptr;
  };

} // namespace ast

#include <ast/call-exp.hxx>

