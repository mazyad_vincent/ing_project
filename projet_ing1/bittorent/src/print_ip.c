#define _DEFAULT_SOURCE
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "parsing.h"
#include "tool_tracker.h"
#include "struct_peers.h"

struct sock_senti* get_peers(struct b_data *peers, int print)
{
   struct b_dict_list *bdict = peers->my_value->my_b_dict_list;
   struct b_dict_list_node *copy = bdict->head;
   while (copy && strcmp(copy->my_key, "peers") != 0)
       copy = copy->next;
   if (!strcmp(copy->my_key, "peers"))
        return get_ip(copy->data->my_value->string, print, copy->data->length);
   return NULL;
}

struct sock_senti* get_ip(char *buf, int print, size_t size)
{
  char *stringip = malloc(sizeof(char) * 5); /* to stock an ip */
  uint16_t port; /* to stock a port */
  struct sock_senti *senti_ip = sock_senti_init();
  for (size_t i = 0; i < size; i += 6) 
  {
      struct sockaddr_in res;
      memcpy(stringip, &buf[i], 4); /* ip */
      stringip[4] = '\0';
      memcpy(&port, &buf[i] + 4, 2); /*port*/
      if (print == 1)
          print_ip(stringip, port); /*print if option */
      char *craft_ip = build_ip(stringip); /* return a craft ip to stock*/
      res.sin_family = AF_INET;
      res.sin_port = htons(port);
      inet_aton(craft_ip, &res.sin_addr);
      free(craft_ip);
      struct list_sock *in_struct = list_sock_init(res); /*initailise the head*/
      sockaddr_push(senti_ip, in_struct); /*push into the struct */
  }
  free(stringip);
  return senti_ip;
}

char* build_ip(char *buf)
{
  char *ip = malloc(sizeof(char) * 16); /*ip to add in struct*/
  uint8_t first = buf[0];
  uint8_t second = buf[1];
  uint8_t third = buf[2];
  uint8_t fourth = buf[3];
  sprintf(ip, "%d%c%d%c%d%c%d", first, '.', second, '.', third, '.', fourth);
  return ip;
}

void print_ip(char *buf, uint16_t port)
{
  for (int i = 0; i < 4; i++)
  {
    uint8_t t = buf[i];
    printf("%d", t);
    if (i != 3)
       putchar('.');
  }
  port = ntohs(port);
  printf("%c%d\n", ':', port);
}
