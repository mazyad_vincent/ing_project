#include "queue.h"

struct queue* queue_init()
{
    struct queue *queue = malloc(sizeof(struct queue));
    if (!queue)
        return NULL;
    struct queue_list *list = malloc(sizeof(struct queue_list));
    if (!list)
    {
        free(queue);
        return NULL;
    }
    list->next = NULL;
    list->data = NULL;
    queue->head = list;
    queue->size = 0;
    return queue;
}

struct queue* queue_push(struct queue *queue, char *elm)
{
    struct queue_list *list = malloc(sizeof(struct queue_list));
    if (!list)
        return queue;
    list->data = elm;
    if (queue->size == 0)
    {
        free(queue->head);
        list->next = NULL;
        queue->head = list;
    }
    else
    {
        list->next = queue->head;
        queue->head = list;
    }
    queue->size++;
    return queue;
}

struct queue_list* queue_pop(struct queue *queue)
{
   if (queue->size == 0)
      return NULL;
   if (queue->size == 1)
   {
        struct queue_list *to_pop = queue->head;
        queue->head = NULL;
        queue->size--;
        return to_pop;
   }
   struct queue_list *cpy = queue->head;
   queue->head = cpy->next;
   queue->size--;
   return cpy;
}

void print_queue(struct queue *queue)
{
   while (queue->size != 0)
   {
      struct queue_list *list  = queue_pop(queue);
      printf("%s\n", list->data);
      free(list);
   }
}

void queue_destroy(struct queue *queue)
{
   while (queue->size != 0) 
   {
      struct queue_list *list = queue_pop(queue);
      free(list->data);
      free(list);
   }
   free(queue->head);
   free(queue);    
}
