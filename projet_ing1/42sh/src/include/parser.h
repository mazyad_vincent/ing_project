/**
** \file parser.h
** \brief This file regroups all functions proto for the lexer and the parser
** \author Odile Guillaume
*/

#ifndef PARSER_H
# define PARSER_H

/**
** \struct global: struct that contains globales variables
*/

struct hashtable *hash;

struct global
{
  int index;
  char *command;
  struct hashtable *hash;
  int *shopts;
  struct ast *ast;
};

/* lexer */
void eat(void);
struct ast *curr_tok(void);
char *next_tok(char *command, int i);
int is_special(char *command, int i);
void comments(char *command, int *i);

/* parser */
struct ast *free_node(struct ast *node);
struct ast *p_input(void);
struct ast *p_list(void);
struct ast *p_and_or(void);
struct ast *p_pipeline(void);
struct ast *p_command(void);
struct ast *p_simple_command(void);
struct ast *p_shell_command(void);
struct ast *p_funcdec(void);
struct ast *p_redirection(void);
struct ast *p_prefix(void);
struct ast *p_element(void);
struct ast *p_compound_list(void);
struct ast *p_rule_for(void);
struct ast *p_rule_while(void);
struct ast *p_rule_until(void);
struct ast *p_rule_case(void);
struct ast *p_rule_if(void);
struct ast *p_else_clause(void);
struct ast *p_do_group(void);
struct ast *p_case_clause(void);
struct ast *p_case_item(void);

#endif /* !PARSER_H */
