/**
** \file main.c
** \brief This file is the start of the programm, call the prompt
** \author Mazyad Vincent
**
** This file regroups the main functions
*/

#define _GNU_SOURCE
#include <err.h>
#include <string.h>
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>

#include "opt_handler.h"
#include "call_commands.h"
#include "prompt.h"
#include "ast.h"
#include "parser.h"
#include "opt_commands.h"
#include "hashtable.h"
#include "replace_value.h"
#include "builtins.h"

struct global g_global;

/**
** \fn char *white_spaces(char *buffer)
** \brief return a buffer without the first spaces
**
** \param char *buffer
** \return the buffer
*/

char *white_spaces(char *buffer)
{
   char *buf = buffer;
   while (buf && *buf == ' ' && *buf != '\0')
        buf++;
   return buf;
}

/**
** \fn static void prompt(int opt)
** \brief call the prompt
**
** \param int opt
** \return nothing
*/

static int prompt(int opt)
{
  char *command;
  char *interactif = NULL;
  struct hashtable *hash = hash_init();
  g_global.hash = hash;
  int res = 0;
  while ((command = readline("42sh$ ")) != NULL)
  {
      add_history(command);
      char *buf_copy = white_spaces(command);
      if (is_incomplete(buf_copy) && !is_separator(buf_copy)) /* launch ps2 */
      {
          interactif = prompt_ps2(buf_copy);
          g_global.command = interactif;
      }
      else
        g_global.command = command;
      struct ast *node = p_input();
      if (!node)
      {
         res = 1;
         fprintf(stderr, "Wrong syntax\n");
      }
      else
      {
         g_global.ast = node;
         replace_all_nodes(hash, node);
         g_global.command = buf_copy;
         if (opt == ASTP)
           to_dot(node);
         if (select_exec(node) == -1) /* check the value of node->key */
            fill_tab(node, 0); /* simple command without operation */
         ast_free(node);
      }
      g_global.index = 0;
      if (interactif)
        free(interactif);
      free(command);
  }
  free_hash(hash); /* free the hashtable */
  return res;
}

/**
** \fn int main(int argc, char **argv)
** \brief call the main
**
** \param int argc, char **argv
** \return a random int
*/

int main(int argc, char **argv)
{
    int res = 0;
    struct options *options = option_handler(argc, argv);
    if ((options->option & VERS) == VERS)
    {
        printf("Version 1.0\n");
        free(options->command);
        free(options);
        free(g_global.shopts);
        return 0;
    }
    if (options->command == NULL && isatty(STDIN_FILENO))
          res = prompt(options->option);
    else
    {
        int isstdin = 0;
        if (options->command == NULL)
        {
            char *buffer = malloc(sizeof(char) * 256);
            read(STDIN_FILENO, buffer, 256);
            options->command = buffer;
            isstdin = 1;
        }
        g_global.command = options->command;
        struct ast *node = p_input();
        if (!node && isstdin)
        {
          fprintf(stderr, "Wrong Syntax\n");
          return 2;
        }
        if (!node)
        {
           fprintf(stderr, "Wrong Syntax\n");
           return 1;
        }
        else
        {
            if ((options->option & ASTP) == ASTP)
                to_dot(node);
            res = select_exec(node);
            if (res == -1)
                res = fill_tab(node, 0);
            ast_free(node);
            g_global.index = 0;
        }
    }
    free(options->command);
    free(options);
    free(g_global.shopts);
    return res;
}
