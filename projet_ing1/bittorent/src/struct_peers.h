#ifndef STRUCT_PEERS_H
# define STRUCT_PEERS_H

#include <netinet/in.h>

struct sock_senti
{
   struct list_sock *head;
   size_t size;
};
struct list_sock
{
  struct sockaddr_in head;
  struct list_sock *next;
};

struct sock_senti* sock_senti_init();
struct list_sock* list_sock_init(struct sockaddr_in head);
void sockaddr_push(struct sock_senti *senti, struct list_sock *to_push);
void free_all(struct sock_senti *senti);

#endif
