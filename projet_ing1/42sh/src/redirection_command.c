/**
** \file commands.h
** \brief This file regroups all functions for operations commands
** \author Mazyad Vincent
*/ 
#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "ast.h"
#include "call_commands.h"
#include "opt_commands.h"

int redirection_right(struct ast *ast)
{
  int redir_file = open(ast->right->key, O_CREAT|O_WRONLY|O_TRUNC, 0666); 
  pid_t childpid;
  int status;
  if (redir_file == -1)
  {
     fprintf(stderr, "can't open file\n");
     return 1;
  }
  if ((childpid = fork()) == -1)
  {
     perror("error");
     exit(1);
  }
  else if (childpid == 0) /* child process */
  {
    if (dup2(redir_file, 1) == -1) /* connect file with stdout */ 
    {
       perror("error dup");
       return 1;
    }
    exec_for_pipe(ast->left);
    close(redir_file);
    exit(EXIT_FAILURE);
  }
  else /* parents */
  {
    waitpid(childpid, &status, 0);
    return WEXITSTATUS(status);
  }
  return 1;
}

int redirection_left(struct ast *ast)
{
    int redir_file = open(ast->right->key, O_CREAT|O_RDONLY, 0666); 
    pid_t childpid;
    int status;
    if (redir_file == -1)
    {
      fprintf(stderr, "can't open file\n");
      return 1;
    }
    if ((childpid = fork()) == -1)
    {
      perror("error");
      exit(1);
    }
    else if (childpid == 0) /* child process */
    {
      if (dup2(redir_file, 0) == -1)
      {
         perror("error dup");
         return 1; 
      }
      close (redir_file);
      exec_for_pipe(ast->left);
      exit(EXIT_FAILURE);
    }
    else
    {
      waitpid(childpid, &status, 0);
      return WEXITSTATUS(status);
    }
    return 1;
}   
