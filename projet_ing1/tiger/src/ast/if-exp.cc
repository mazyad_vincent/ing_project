/**
 ** \file ast/if-exp.cc
 ** \brief Implementation of ast::IfExp.
 */

#include <ast/visitor.hh>
#include <ast/if-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
  IfExp::IfExp(const Location& location, Exp* cond, Exp* body, Exp* els)
      : Exp(location)
      , cond_(cond)
      , body_(body)
      , els_(els)
  {}
  IfExp::~IfExp()
  {
    delete cond_;
    delete body_;
    delete els_;
  }

 void IfExp::accept(ConstVisitor& v) const
 {
   v(*this);
 }

 void IfExp::accept(Visitor& v)
 {
   v(*this);
 }

} // namespace ast

