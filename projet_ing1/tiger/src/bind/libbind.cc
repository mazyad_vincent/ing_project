/**
 ** \file bind/libbind.cc
 ** \brief Define exported bind functions.
 */

  // FIXME

#include <bind/libbind.hh>

namespace bind
{
  namespace tasks
  {
    misc::error bind(ast::Ast& tree)
    {
      bind::Binder binder; 
      binder(tree);
      auto e = binder.error_get();
      return e;
    }
  }
}
