/**
 ** \file ast/assign-exp.cc
 ** \brief Implementation of ast::AssignExp.
 */

#include <ast/visitor.hh>
#include <ast/assign-exp.hh>

namespace ast
{

  // FIXME: Some code was deleted here.
    AssignExp::AssignExp(const Location& location,Var* lval,  Exp* exp)
      :  Exp(location)
      ,  lval_(lval)
      ,  exp_(exp)
    {}

    AssignExp::~AssignExp()
    {
      delete exp_;
      delete lval_;
    }

    void AssignExp::accept(ConstVisitor& v) const
    {
      v(*this);
    }
    void AssignExp::accept(Visitor& v)
    {
      v(*this);
    }


} // namespace ast

