/**
** \file ast.h: header of ast.c
** \brief contains all fonctions, enum or struct used by the ast
**
** \author Odile Guillaume
*/

#ifndef AST_H
# define AST_H

/**
** \enum type: regroups all the token available in the 42sh
*/

enum type
{
  WORD = 1,
  HEREDOC,
  ASSIGMENT_WORD,
  NEWL,         /* \n */
  END_FILE,        /* EOF */
  IONUMBER,        /* number */
  FUNCTION,        /* 'function' */

/* operators */

  AND,             /* & */
  SEMI,            /* ; */
  AND_IF,          /* && */
  OR_IF,           /* || */
  OR,              /* | */
  LESS,            /* < */
  GREAT,           /* > */
  DSEMI,           /* ;; */
  DLESS,           /* << */
  DGREAT,          /* >> */
  LESSAND,         /* <& */
  GREATAND,        /* >& */
  LESSGREAT,       /* <> */
  DLESSDASH,       /* <<- */
  CLOBBER,         /* >| */

/* reserved words */

  IF,              /* if */
  THEN,            /* then */
  ELSE,            /* else */
  ELIF,            /* elif */
  FI,              /* fi */
  DO,              /* do */
  DONE,            /* done */
  CASE,            /* case */
  ESAC,            /* esac */
  WHILE,           /* while */
  UNTIL,           /* until */
  FOR,             /* for */

/* spcecials */

  LBRACE,          /* { */
  RBRACE,          /* } */
  BANG,            /* ! */
  IN,              /* in */
  LPAR,            /* ( */
  RPAR,            /* ) */
};

/**
** \struct ast: struct of the ast. ast->key = key of the current node,
** ast->type = token of the current key ast->left/right/third = pointer to child
*/

struct ast
{
  char *key;
  int type;
  struct ast *left;
  struct ast *right;
  struct ast *third;
};

struct ast *ast_init(void);
struct ast *ast_create(struct ast *node1, struct ast *node2,
                       struct ast *node3, struct ast *node4);
void ast_free(struct ast *ast);
void to_dot(struct ast *node);

#endif /* !AST_H */
