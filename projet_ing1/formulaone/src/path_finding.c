#include "path_finding.h"


void path_finding(struct map *map, struct vector2 *pos, struct graph *graph)
{
    struct vector2 *new_edge = vector2_new();
    new_edge->x = pos->x;
    new_edge->y = pos->y;
    add_edge(graph, new_edge);
    for (int i = - 1; i < 2; i++)
    {
        for (int j = - 1; j < 2; j++)
        {
            if (i == 0 && j == 0)
                continue;
            struct vector2 *vect = vector2_new();
            vect->x = pos->x + i;
            vect->y = pos->y + j;
            enum floortype curr_floor = map_get_floor(map, vect->x, vect->y);
            if (curr_floor != BLOCK)
            {
                create_adj(graph, pos, vect);
                if (!find_edge(graph, vect))
                    path_finding(map, vect, graph);
            }
            else 
               vector2_delete(vect);
        }
    }
}


struct vector2* find_pos(struct map *map)
{
    for (float i = 0; i < map->height; i+= 1)
    {
        for (float j = 0; j < map->width; j+= 1)
        {
            if (map_get_floor(map, j, i) == FINISH)
            {
                struct vector2 *vect = vector2_new();
                vect->x = j;
                vect->y = i;
                return vect;
            }
        }
    }
    return NULL;
}

int get_path(struct map *map, struct graph *graph,
              struct vector2 *start, struct sentinelle *senti)
{ 
    struct graph *cpy_graph = create_graph();
    int weight = 0;
    struct vector2 *finish_pos = find_pos(map);
    struct edges *finish = get_finish(graph, finish_pos);
    if (!finish)
        return 0;
    struct vector2 *vect = vector2_new();
    vect->x = finish->checkpoint->x;
    vect->y = finish->checkpoint->y;
    add_edge(cpy_graph, vect);
    struct edges *get_linked;
    int i = 0;
    struct edges *fin = get_finish(graph, finish_pos);
    while (get_link(graph, finish) &&
           ((get_linked = get_link(graph, finish))->checkpoint->x != start->x
           || get_linked->checkpoint->y != start->y) && !find_edge(cpy_graph,
           get_linked->checkpoint))
    {
        struct vector2 *vect1 = vector2_new();
        vect1->x = get_linked->checkpoint->x;
        vect1->y = get_linked->checkpoint->y;
    
        add_edge(cpy_graph, vect1);
        struct edges *new = get_linked;
        finish = new;
        weight++;
        i = 1;
    }
    struct vector2 *vect3 = vector2_new();
    vect3->x = start->x;
    vect3->y = start->y;
    add_edge(cpy_graph, vect3);
    if (i)
    {
        push_list(senti, cpy_graph, weight);
        graph_popadj(graph, get_link(graph, fin), fin->checkpoint);
    }
    return i;
}
