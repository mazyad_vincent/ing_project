#include <stdio.h>
#include <stdlib.h>
#include "struct_peers.h"

struct sock_senti* sock_senti_init()
{
  struct sock_senti *head = malloc(sizeof(struct sock_senti));
  if (!head)
    return NULL;
  head->head = NULL;
  head->size = 0;
  return head;
}


struct list_sock* list_sock_init(struct sockaddr_in head)
{
    struct list_sock *list = malloc(sizeof(struct list_sock));
    if (!list)
      return NULL;
    list->head = head;
    list->next = NULL;
    return list;
}

void sockaddr_push(struct sock_senti *senti, struct list_sock *to_push)
{
    if (!senti->size)
    {
       senti->head = to_push;
       senti->size += 1;
       return;
    }
    if (senti->size == 1)
       senti->head->next = to_push;
    else
    {
        struct list_sock *cpy = senti->head;
        while (cpy && cpy->next)
            cpy = cpy->next;
        cpy->next = to_push;
    }
    senti->size += 1;  
}

void free_all(struct sock_senti *senti)
{
   struct list_sock *head = senti->head;
   while (senti->size != 0 && head)
   {
      struct list_sock *to_free = head;
      head = head->next;
      free(to_free);
      senti->size -= 1;
   }
   free(senti); 
}
