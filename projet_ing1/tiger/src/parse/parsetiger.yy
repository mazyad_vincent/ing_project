                                                                // -*- C++ -*-
%require "3.0"
%language "C++"
// Set the namespace name to `parse', instead of `yy'.
%name-prefix "parse"
%define api.value.type variant
%define api.token.constructor
%skeleton "glr.cc"
%glr-parser
%expect-rr 0
%expect 2
%error-verbose
%defines
%debug
// Prefix all the tokens with TOK_ to avoid colisions.
%define api.token.prefix {TOK_}

/* We use pointers to store the filename in the locations.  This saves
   space (pointers), time (no deep copy), but leaves the problem of
   deallocation.  This would be a perfect job for a misc::symbol
   object (passed by reference), however Bison locations require the
   filename to be passed as a pointer, thus forcing us to handle the
   allocation and deallocation of this object.

   Nevertheless, all is not lost: we can still use a misc::symbol
   object to allocate a flyweight (constant) string in the pool of
   symbols, extract it from the misc::symbol object, and use it to
   initialize the location.  The allocated data will be freed at the
   end of the program (see the documentation of misc::symbol and
   misc::unique).  */
%define filename_type {const std::string}
%locations

// The parsing context.
%param { ::parse::TigerParser& tp }

/*---------------------.
| Support for tokens.  |
`---------------------*/
%code requires
{
#include <string>
#include <misc/algorithm.hh>
#include <misc/separator.hh>
#include <misc/symbol.hh>
#include <parse/fwd.hh>

  // Pre-declare parse::parse to allow a ``reentrant'' parsing within
  // the parser.
  namespace parse
  {
    ast_type parse(Tweast& input);
  }
}

%code provides
{
  // Announce to Flex the prototype we want for lexing (member) function.
  # define YY_DECL_(Prefix)                               \
    ::parse::parser::symbol_type                          \
    (Prefix parselex)(::parse::TigerParser& tp)
  # define YY_DECL YY_DECL_(yyFlexLexer::)
}

%printer { yyo << $$; } <int> <std::string> <misc::symbol>;

%token <std::string>    STRING "string"
%token <misc::symbol>   ID     "identifier"
%token <int>            INT    "integer"


/*--------------------------------.
| Support for the non-terminals.  |
`--------------------------------*/

%code requires
{
# include <ast/fwd.hh>
// Provide the declarations of the following classes for the
// %destructor clauses below to work properly.
# include <ast/exp.hh>
# include <ast/var.hh>
# include <ast/ty.hh>
# include <ast/name-ty.hh>
# include <ast/field.hh>
# include <ast/field-init.hh>
# include <ast/function-dec.hh>
# include <ast/type-dec.hh>
# include <ast/var-dec.hh>
# include <ast/any-decs.hh>
# include <ast/decs-list.hh>
}

/*-----------------------------------------.
| Code output in the implementation file.  |
`-----------------------------------------*/

%code
{
# include <parse/tiger-parser.hh>
# include <parse/scantiger.hh>
# include <parse/tweast.hh>
# include <misc/separator.hh>
# include <misc/symbol.hh>
# include <ast/all.hh>
# include <ast/libast.hh>

  namespace
  {

    /// Get the metavar from the specified map.
    template <typename T>
    T*
    metavar(parse::TigerParser& tp, unsigned key)
    {
      parse::Tweast* input = tp.input_;
      return input->template take<T>(key);
    }

  }

  /// Use our local scanner object.
  inline
  ::parse::parser::symbol_type
  parselex(parse::TigerParser& tp)
  {
    return tp.scanner_->parselex(tp);
  }
}

// Definition of the tokens, and their pretty-printing.
%token AND          "&"
       ARRAY        "array"
       ASSIGN       ":="
       BREAK        "break"
       CAST         "_cast"
       CLASS        "class"
       COLON        ":"
       COMMA        ","
       DIVIDE       "/"
       DO           "do"
       DOT          "."
       ELSE         "else"
       END          "end"
       EQ           "="
       EXTENDS      "extends"
       FOR          "for"
       FUNCTION     "function"
       GE           ">="
       GT           ">"
       IF           "if"
       IMPORT       "import"
       IN           "in"
       LBRACE       "{"
       LBRACK       "["
       LE           "<="
       LET          "let"
       LPAREN       "("
       LT           "<"
       MINUS        "-"
       METHOD       "method"
       NE           "<>"
       NEW          "new"
       NIL          "nil"
       OF           "of"
       OR           "|"
       PLUS         "+"
       PRIMITIVE    "primitive"
       RBRACE       "}"
       RBRACK       "]"
       RPAREN       ")"
       SEMI         ";"
       THEN         "then"
       TIMES        "*"
       TO           "to"
       TYPE         "type"
       VAR          "var"
       WHILE        "while"
       EXP          "_exp"
       LVAL         "_lvalue"
       NAMETY       "_namety"
       DECS         "_decs"
       EOF 0        "end of file"

/* BEGIN HEAD */
%nonassoc func-dec-chunk-low
%nonassoc type-dec-chunk-low
%nonassoc WHILE
%nonassoc OF
%nonassoc THEN
%nonassoc FOR
%nonassoc TO
%nonassoc BREAK
%nonassoc NIL
%nonassoc FUNCTION
%nonassoc TYPE
%nonassoc ARRAY
%nonassoc DO
%nonassoc LET
%nonassoc IN
%nonassoc END
%nonassoc VAR
%nonassoc IMPORT
%nonassoc PRIMITIVE
%nonassoc CLASS
%nonassoc ASSIGN
%nonassoc ELSE
%nonassoc IF
/*%nonassoc method-dec-chunk-low*/

%type <ast::Exp*> exp
%type <ast::DecsList*> decs
%type <std::vector<ast::FieldInit*>*> array_creation
%type <std::vector<ast::Exp*>*> func_call
%type <std::vector<ast::Exp*>*> suite_exp
%type <std::vector<ast::Field*>*> var_type
%type <ast::VarDec*> rule7
%type <ast::Var*> lvalue
%type <ast::Var*> lvalue1
%type <ast::SubscriptVar*> lvalue2
%type <std::vector<ast::Exp*>*> exps
%type <ast::VarDec*> vardec
%type <ast::DecsList*> classfields
%type <ast::Ty*> ty
%type <std::vector<ast::Field*>*> tyfields
%type <ast::NameTy*> type-id
%type <ast::FunctionDecs*> funct-dec-chunk
%type <ast::FunctionDec*> func-chunk
%type <ast::VarDecs*> var-dec-chunk
%type <ast::TypeDecs*> type-dec-chunk
%type <ast::TypeDec*> type-chunk
%type <ast::VarDecs*> var_type_copy
%type <ast::VarDecs*> tyfieldscopy
/*%type <ast::MethodDecs*> method-dec-chunk
%type <ast::MethodDecs*> method-chunk*/

/* ENNNND*/

%left OR
%left AND
%left EQ GE GT LE LT NE
%left PLUS MINUS
%left TIMES DIVIDE
%start program

%destructor { misc::deep_clear(*$$); delete $$; } <std::vector<ast::FieldInit*>*>
%destructor { misc::deep_clear(*$$); delete $$; } <std::vector<ast::Exp*>*>
%destructor { misc::deep_clear(*$$); delete $$; } <std::vector<ast::Field*>*>
%destructor { delete $$; } <ast::Exp*>
%destructor { delete $$; } <ast::DecsList*>
%destructor { delete $$; } <ast::VarDec*>
%destructor { delete $$; } <ast::Var*>
%destructor { delete $$; } <ast::Ty*>
%destructor { delete $$; } <ast::NameTy*>
%destructor { delete $$; } <ast::FunctionDecs*>
%destructor { delete $$; } <ast::FunctionDec*>
%destructor { delete $$; } <ast::VarDecs*>
%destructor { delete $$; } <ast::TypeDecs*>
%destructor { delete $$; } <ast::TypeDec*>

%printer { yyo << $$; } <ast::Exp*> <ast::DecsList*> <ast::VarDec*> <ast::Var*>
<ast::SubscriptVar*> <ast::Ty*> <ast::NameTy*> <ast::FunctionDecs*>
<ast::FunctionDec*> <ast::VarDecs*> <ast::TypeDecs*> <ast::TypeDec*>;

%%
program:
   /* Parsing a source program.  */
  exp { tp.ast_ = $1; }
| /* Parsing an imported file.  */
  decs { tp.ast_ = $1; }
;
/* --------------------------- EXP --------------------------- */
exp:
  INT { $$ = new ast::IntExp(@$, $1); }
  | NIL { $$ = new ast::NilExp(@$); }
  | STRING { $$ = new ast::StringExp(@$, $1); }
  | type-id LBRACK exp RBRACK OF exp { $$ = new ast::ArrayExp(@$, $1, $3, $6) ; }
  | type-id LBRACE ID EQ exp array_creation RBRACE {$6->insert($6->begin(), new ast::FieldInit(@$, $3, $5));
                                                    $$ = new ast::RecordExp(@$, $1, $6) ;}
  | type-id LBRACE RBRACE {auto c = new std::vector<ast::FieldInit*>{}; $$ = new
  ast::RecordExp(@$, $1, c) ;}
  | NEW ID { $$ = new ast::SimpleVar(@$, $2) ;}
  | lvalue { $$ = $1 ;}
  | ID LPAREN exp func_call RPAREN { $4->insert($4->begin(), $3); $$ = new ast::CallExp(@$, $1, $4) ;}
  | ID LPAREN RPAREN  { auto c = new std::vector<ast::Exp*>{}; $$ = new ast::CallExp(@$, $1, c) ;}

/* ------------------- METHOD CALL, OBJECT OPTIONNAL -------------------------*/
  /*| ID DOT ID LPAREN exp rule2 RPAREN {$6.push_front($5); $$ = new ast::MethodCallExp(@$, new ast::SimpleVar(@$, $1), $6, $3) ;} FIX */
  /*| ID DOT ID LPAREN RPAREN { auto c = std::list<ast::Exp*>{}; $$ = new ast::MethodCallExp(@$, new ast::SimpleVar(@$, $1), c, $3) ;} */

  | MINUS exp { $$ = new ast::OpExp(@$, new ast::IntExp(@$, 0), ast::OpExp::Oper::sub, $2); }
  | exp PLUS exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::add, $3) ;}
  | exp MINUS exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::sub, $3) ;}
  | exp DIVIDE exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::div, $3) ;}
  | exp TIMES exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::mul, $3) ;}
  | exp NE exp  { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::ne, $3) ;}
  | exp EQ exp  { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::eq, $3) ;}
  | exp LT exp  { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::lt, $3) ;}
  | exp GT exp  { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::gt, $3) ;}
  | exp GE exp  { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::ge, $3) ;}
  | exp LE exp  { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::le, $3) ;}
  | exp AND exp  { $$ = parse::parse(parse::Tweast()
                  << "if " << $1 << " then " << $3 << "<> 0 else 0"); }
  | exp OR exp  { $$ = parse::parse(parse::Tweast()
                  << "if " << $1 << " then " << $3 << "= 0 else 1"); } /* FIX */
  | LPAREN exps RPAREN { $$ = new ast::SeqExp(@$, $2) ;}
  | lvalue ASSIGN exp { $$ = new ast::AssignExp(@$, $1, $3);}
  | IF exp THEN exp ELSE exp { $$ = new ast::IfExp(@$, $2, $4, $6);}
  | IF exp THEN exp { $$ = new ast::IfExp(@$, $2, $4, nullptr) ;}
  | WHILE exp DO exp { $$ = new ast::WhileExp(@$, $2, $4) ;}
  | FOR rule7 TO exp DO exp { $$ = new ast::ForExp(@$, $2, $4, $6);}
  | BREAK { $$ = new ast::BreakExp(@$) ;}
  | LET decs IN exps END { $$ = new ast::LetExp(@$, $4, $2); }
  | CAST LPAREN exp COMMA ty RPAREN { $$ = new ast::CastExp(@$, $3, $5) ;}
  | EXP LPAREN INT RPAREN { $$ = metavar<ast::Exp>(tp, $3) ;}

/* --------------------------- END EXP -------------------------- */

array_creation:
  %empty { $$ = new std::vector<ast::FieldInit*>{} ;}
  | array_creation COMMA ID EQ exp {$1->insert($1->begin(), new ast::FieldInit(@$, $3, $5));
                           $$ = $1 ;}

func_call:
  %empty { $$ = new std::vector<ast::Exp*>{}; }
  | func_call COMMA exp {$1->insert($1->begin(), $3) ; $$ = $1;}

suite_exp:
  %empty { $$ = new std::vector<ast::Exp*>{} ;}
  | suite_exp SEMI exp { $1->insert($1->begin(), $3) ; $$ = $1;}

var_type:
  %empty { $$ = new std::vector<ast::Field*>{} ; }
  | var_type COMMA ID COLON type-id {$1->insert($1->begin(), new ast::Field(@$, $3, $5));
                                 $$ = $1 ;}

rule7:
  ID ASSIGN exp { $$ = new ast::VarDec(@$, $1, nullptr, $3) ;}

lvalue:
  ID { $$ = new ast::SimpleVar(@$, $1) ;}
  | lvalue1 { $$ = $1 ;}
  | lvalue2 { $$ = $1 ;}
  | CAST LPAREN lvalue COMMA ty RPAREN {$$ = new ast::CastVar(@$, $3, $5) ;}
  | LVAL LPAREN INT RPAREN { $$ = metavar<ast::Var>(tp, $3) ;}

lvalue1:
  ID DOT ID { $$ = new ast::FieldVar(@$, new ast::SimpleVar(@$, $1), $3) ;}
  | lvalue1 DOT ID { $$ = new ast::FieldVar(@$, $1, $3) ;}
  | lvalue2 DOT ID { $$ = new ast::SubscriptVar(@$, $1, new ast::SimpleVar(@$, $3)) ;}

lvalue2:
  ID LBRACK exp RBRACK { $$ = new ast::SubscriptVar(@$, new ast::SimpleVar(@$,
                       $1), $3) ;}
  | lvalue1 LBRACK exp RBRACK {$$ = new ast::SubscriptVar(@$, $1, $3) ;}
  | lvalue2 LBRACK exp RBRACK { $$ = new ast::SubscriptVar(@$, $1, $3) ;}

exps:
  exp suite_exp { $2->insert($2->begin(), $1) ; $$ = $2 ;}
  | %empty { $$ = new std::vector<ast::Exp*>{} ;}

 /*  | IMPORT STRING { $$ = new ast::VarDec(@$, $2, nullptr, nullptr) ;} */

/* --------------------------- CHUNK ----------------------------*/

funct-dec-chunk:
  func-chunk funct-dec-chunk { $2->push_front(*$1); $$ = $2;}
  | func-chunk %prec func-dec-chunk-low{auto c = new ast::FunctionDecs(@$); c->push_front(*$1); $$ = c;}

func-chunk:
   FUNCTION ID LPAREN tyfieldscopy RPAREN COLON type-id EQ exp {$$ = new ast::FunctionDec(@$, $2, $4, $7, $9) ;}
  | FUNCTION ID LPAREN tyfieldscopy RPAREN EQ exp {$$ = new ast::FunctionDec(@$, $2, $4, nullptr, $7) ;}
  | PRIMITIVE ID LPAREN tyfieldscopy RPAREN COLON type-id {$$ = new ast::FunctionDec(@$, $2, $4, $7, nullptr) ;}
  | PRIMITIVE ID LPAREN tyfieldscopy RPAREN {$$ = new ast::FunctionDec(@$, $2, $4, nullptr, nullptr) ;}


var-dec-chunk:
  vardec { auto c = new ast::VarDecs(@$); c->push_front(*$1) ; $$ = c ;}

type-dec-chunk:
   type-chunk type-dec-chunk { $2->push_front(*$1); $$ = $2 ;}
  | type-chunk %prec type-dec-chunk-low { auto c = new ast::TypeDecs(@$) ; c->push_front(*$1) ; $$ = c;}

type-chunk:
  TYPE ID EQ ty  { $$ = new ast::TypeDec(@$, $2, $4) ;}
  | CLASS ID EXTENDS type-id LBRACE classfields RBRACE { 
  $$ = new ast::TypeDec(@$, $2, new ast::ClassTy(@$, $4, $6)) ;}

/*method-dec-chunk:
   method-chunk method-dec-chunk {$2->push_front(*$1); $$ = $2 ;}
  | method-chunk %prec method-dec-chunk-low { auto c = new ast::MethodDecs(@$); c->push_front(*$1) ; $$ = c ;}

method-chunk:
  CLASS ID EXTENDS type-id LBRACE classfields RBRACE {$$ = new ast::MethodDec(@$, $2, nullptr, $4, nullptr) ;}
  | CLASS ID LBRACE classfields RBRACE {$$ = new ast::MethodDec(@$, $2, nullptr,
nullptr, nullptr) ;}*/

vardec:
  VAR ID COLON type-id ASSIGN exp {$$ = new ast::VarDec(@$, $2, $4, $6) ;}
  | VAR ID ASSIGN exp { $$ = new ast::VarDec(@$, $2, nullptr, $4) ; }

/* ------------------------------ END CHUNK ------------------------ */

/* CLASS FIELDS */

classfields:
   var-dec-chunk classfields { $2->push_front($1) ; $$ = $2 ;}
  | %empty { auto c = ast::DecsList::decs_type{} ; $$ = new ast::DecsList(@$, c);}

/* ---- end ---- */

ty:
  type-id { $$ = $1;}
  | LBRACE tyfields RBRACE { $$ = new ast::RecordTy(@$, $2) ;}
  | ARRAY OF type-id { $$ = new ast::ArrayTy(@$, $3) ;}
  | CLASS EXTENDS type-id LBRACE classfields RBRACE { $$ = new ast::ClassTy(@$, $3,
                                               $5) ;}

tyfields:
  ID COLON type-id var_type { $4->insert($4->begin(), new ast::Field(@$, $1, $3)); $$ = $4; }
  | %empty { $$ = new std::vector<ast::Field*>{} ;}

tyfieldscopy:
   ID COLON type-id var_type_copy { $4->push_front(*(new ast::VarDec(@$, $1, $3, nullptr))); $$ = $4 ;}
  | %empty { $$ = new ast::VarDecs(@$) ;}

var_type_copy:
 %empty { $$ = new ast::VarDecs(@$); }
 | var_type_copy COMMA ID COLON type-id {$1->push_front(*(new ast::VarDec(@$, $3, $5, nullptr))); $$ = $1;}
type-id:
  ID {$$ = new ast::NameTy(@$, $1) ;}
  | NAMETY LPAREN INT RPAREN { $$ = metavar<ast::NameTy>(tp, $3); }

/*---------------.
| Declarations.  |
`---------------*/

decs:
  %empty { auto c = ast::DecsList::decs_type{} ;$$ = new ast::DecsList(@$, c) ;}
  | funct-dec-chunk decs { $2->push_front($1); $$ = $2; }
  | var-dec-chunk decs { $2->push_front($1); $$ = $2; }
  | type-dec-chunk decs { $2->push_front($1); $$ = $2;  }
 /* | method-dec-chunk decs { $2->push_front($1); $$ = $2; }*/
  | DECS LPAREN INT RPAREN decs { $5->splice_front(*metavar<ast::DecsList>(tp, $3)) ; $$ = $5;}
%%

void
parse::parser::error(const location_type& l, const std::string& m)
{
   tp.error_ << misc::error::error_type::parse
             << l
             << m
             << "'\n'";
}
