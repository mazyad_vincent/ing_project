#ifndef ENV_H
# define ENV_H

#include "parse.h"

int check_equal(char *key); /* check if there is an equal for alias */
int is_valid_alias(struct ast *ast); /* check if the alias is valid, if not
                                     return 0 */
char *get_alias(char *key); /* return alias name */
char *get_val(char *key); /* return alias value */
int environ_command(struct ast *ast); /* set the env variable */

#endif
