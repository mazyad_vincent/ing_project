/**
 ** \file ast/let-exp.hh
 ** \brief Declaration of ast::LetExp.
 */

#pragma once

#include <ast/decs-list.hh>
#include <ast/exp.hh>
#include <misc/contract.hh>
#include <ast/fwd.hh>

namespace ast
{

  /// LetExp.
  class LetExp : public Exp
  {
    public:
      LetExp(const Location& location, std::vector<Exp*>* vect, DecsList* dec);
      LetExp(const LetExp&) = delete;
      LetExp& operator=(const LetExp&) = delete;
      virtual ~LetExp();
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      const std::vector<Exp*>* list_get() const;
      std::vector<Exp*>* list_get();
      const DecsList& dec_get() const;
      DecsList& dec_get();
      void push_front(Exp* exp);

    protected:
      std::vector<Exp*>* vect_;
      DecsList* dec_;
  // FIXME: Some code was deleted here.
  };

} // namespace ast

#include <ast/let-exp.hxx>

