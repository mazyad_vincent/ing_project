/**
 ** \file bind/binder.hxx
 ** \brief Inline methods of bind::Binder.
 **/

#pragma once

#include <bind/binder.hh>

namespace bind
{

  /*-----------------.
  | Error handling.  |
  `-----------------*/

  // CHANGED:FIXME

  inline void Binder::check_main(const ast::FunctionDec& e)
  {
    if (e.name_get() == "_main")
    {
      if (def_main_ == true && is_main_ != 0)
      {
         error(e, std::string("multiple definition of main"));
         def_main_set(false);
      }
      else
        def_main_set(true);
    }
  }

 inline  void Binder::error(const ast::Ast& loc, const std::string& msg)
  {
    error_ << loc.location_get() <<  msg << '\n' // Set the error message
     << misc::error::error_type::bind; // Set the return value
  }

  template <typename T>
  void Binder::undeclared(const std::string& k, const T& e)
  {
    std::string msg = "variable " + k + " is undeclared";
    error(e, msg);
  }

  template <typename T>
  void Binder::redefinition(const T& e1, const T& e2)
  {
    std::string msg0 = std::string(": redefinition: ") + e2.name_get().get();
    error(e2, msg0);
    std::string msg = std::string(": first definition");
    error(e1, msg);
  }

  /*-------------------.
  | Definition sites.  |
  `-------------------*/

  template <typename NodeType, typename DefType>
  void Binder::def_default(NodeType& e, DefType* def)
  {
     if (!e.def_get())
       e.def_set(def);
  }

  // CHANGED:FIXME

 // template <typename NodeType, typename DefType>
 // void Binder::def_default(NodeType& e, DefType* def);

  /*------------------.
  | Visiting /Decs/.  |
  `------------------*/

  template <class D>
  void
  Binder::decs_visit(ast::AnyDecs<D>& e)
  {
    // Shorthand.
    auto vect_dec =  e.decs_get();
    std::map<misc::symbol, D*> mapping;
    for (size_t i = 0; i < vect_dec.size(); ++i)
    {
      auto it = mapping.begin();
      if ((it = mapping.find(vect_dec[i]->name_get())) != mapping.end())
      	  redefinition(*vect_dec[i], *it->second);
      else
     	 mapping.insert(std::pair<misc::symbol, D*>(vect_dec[i]->name_get(), vect_dec[i]));
    }
    for (auto it = vect_dec.begin(); it != vect_dec.end(); ++it)
       visit_dec_header(**it);
    for (auto it = vect_dec.begin(); it != vect_dec.end(); ++it)
       visit_dec_body(**it);
  // FIXME: Some code was deleted here (Two passes: once on headers, then on bodies).
  }

  /* These specializations are in bind/binder.hxx, so that derived
     visitors can use them (otherwise, they wouldn't see them).  */

  // CHANGED:FIXME
  template <>
  inline void
  Binder::visit_dec_header(ast::FunctionDec& e)
  {
    misc::symbol key = e.name_get(); /* add func name in scoped map */
    check_main(e);
    scope_func_.put(key, &e);
  }

  template <>
  inline void
  Binder::visit_dec_header(ast::TypeDec& e)
  {
    misc::symbol key = e.name_get(); /* add the type name in the scope */
    scope_type_.put(key, &e);
  }

  template <>
  inline void
  Binder::visit_dec_header(ast::VarDec& e)
  {
    scope_map_.put(e.name_get(), &e);
  }

  template <>
  inline void Binder::visit_dec_body(ast::FunctionDec& e)
  {
    scope_begin();
    super_type::operator()(e);
    scope_end();
  }

  template <>
  inline void Binder::visit_dec_body(ast::TypeDec& e)
  {
    super_type::operator()(e);
  }

  template <>
  inline void Binder::visit_dec_body(ast::VarDec& e)
  {
    super_type::operator()(e);
  }

  inline bool Binder::def_main_get()
  {
    return def_main_;
  }

  inline void Binder::def_main_set(bool new_def)
  {
    if (!is_main_ && new_def == true)
    {
      def_main_ = new_def;
      is_main_ = 1;
    }
    else if (is_main_ && new_def == false)
      def_main_ = new_def;
  }

} // namespace bind
