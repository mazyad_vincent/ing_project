from django import forms
from django.forms import modelformset_factory
from django.core.validators import FileExtensionValidator
from .models import IsMemberOfAssociation, Association, DemandeDeBudget, User, DeclarationDepense
import datetime


class EnacBoardForm(forms.ModelForm):
    comm_pres = forms.IntegerField(required=False)
    work_pres = forms.IntegerField(required=False)
    asso_life_pres = forms.IntegerField(required=False)
    comm_vice_pres = forms.IntegerField(required=False)
    work_vice_pres = forms.IntegerField(required=False)
    asso_life_vice_pres = forms.IntegerField(required=False)

    def __init__(self, *args, **kwargs):
        super(EnacBoardForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Association
        fields = ['comm_pres', 'work_pres', 'asso_life_pres',
                  'comm_vice_pres', 'work_vice_pres', 'asso_life_vice_pres',
                  'comm_secr', 'work_secr', 'asso_life_secr',
                  'comm_tres', 'work_tres', 'asso_life_tres', 'nb_event']


class EnacMemberForm(forms.ModelForm):
    nb_participation = forms.IntegerField()

    class Meta:
        model = IsMemberOfAssociation
        fields = ('nb_participation',)

    def __init__(self, *args, **kwargs):
        super(EnacMemberForm, self).__init__(*args, **kwargs)

    def clean_status(self):
        return self.instance.status


enacFormSet = modelformset_factory(IsMemberOfAssociation, form=EnacMemberForm, extra=0)


class CreateUserForm(forms.Form):
    login = forms.CharField(max_length=30)
    firstname = forms.CharField(max_length=50)
    lastname = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())
    pass_confirm = forms.CharField(widget=forms.PasswordInput())
    email = forms.EmailField()

    def clean(self):
        cleaned_data = super(CreateUserForm, self).clean()
        password = cleaned_data.get('password')
        pass_conf = cleaned_data.get('pass_confirm')

        if pass_conf and password:
            if password != pass_conf:
                raise forms.ValidationError(
                    "Les mots de passe sont différents"
                )
        return cleaned_data


class LoginForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)


class ApplyForm(forms.Form):
    motivation_letter = forms.CharField(widget=forms.Textarea, label="Motivation", max_length=1000)


class ChangeUserForm(forms.Form):
    firstname = forms.CharField(max_length=50)
    lastname = forms.CharField(max_length=50)
    email = forms.EmailField()


class ChangeAssoForm(forms.Form):
    email = forms.EmailField()
    website = forms.URLField()


class DemandeCreateAssoForm(forms.Form):
    name = forms.CharField(label="Nom de l'association", max_length=100)
    file = forms.FileField(label="Fichier",
                           validators=[FileExtensionValidator(allowed_extensions=["pdf"])])


class MemberEnacsForm(forms.Form):
    username = forms.CharField(label="username", max_length=30)
    enacs = forms.IntegerField(label="Nombre de points ENACS")


class ReunionForm(forms.Form):
    date = forms.SplitDateTimeField(input_date_formats="%d/%m/%Y",
                                    input_time_formats="%H:%M",
                                    widget=forms.SplitDateTimeWidget(
                                        date_format="%d/%m/%Y",
                                        time_format="%H:%M"))
    lieu = forms.CharField()


class DemandeBudgetForm(forms.ModelForm):
    fichier_presentation_chemin = forms.FileField(label="Fichier",
                                                  validators=[FileExtensionValidator(allowed_extensions=["pdf"])])
    amount = forms.IntegerField(label="Montant")
    emis = forms.ModelChoiceField(widget=forms.HiddenInput(), queryset=Association.objects.all())

    class Meta:
        model = DemandeDeBudget
        fields = ['fichier_presentation_chemin', 'amount', 'emis']


class DeclareSpent(forms.ModelForm):
    emis = forms.ModelChoiceField(widget=forms.HiddenInput(), queryset=Association.objects.all())
    user = forms.ModelChoiceField(queryset=User.objects.all())
    amount = forms.IntegerField(label="Montant")
    libelle = forms.CharField(label="Motif", max_length=100)
    date = forms.DateField(label="Date", initial=datetime.date.today, widget=forms.SelectDateWidget())
    fichier_just_chemin = forms.FileField(label="Justificatif",
                                          validators=[FileExtensionValidator(allowed_extensions=["pdf"])])

    class Meta:
        model = DeclarationDepense
        fields = ['fichier_just_chemin', 'amount', 'emis', 'user', 'libelle', 'date']

class AcceptBudgetForm(forms.ModelForm):
    dateAccepted = forms.DateField(label="Date de transfert", initial=datetime.date.today, widget=forms.SelectDateWidget())
    amount = forms.IntegerField(label="Montant")
    
    class Meta:
        model = DemandeDeBudget
        fields = ['dateAccepted', 'amount']
