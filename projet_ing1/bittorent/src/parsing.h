#ifndef PARSING_H
# define PARSING_H

# include "memory_list.h"
# include "tool_parse.h"

struct info
{
  char *string;
  size_t size;
};

char *get_key(struct mem_list *memory, FILE *torrent, int type_value);
struct b_data *get_string(struct mem_list *memory, FILE *torrent,
                          int type_value);
struct b_data *get_integer(struct mem_list *memory, FILE *torrent);
struct b_data *get_list(struct mem_list *memory, FILE *torrent, int type_value);
struct b_data *get_dict(struct mem_list *memory, FILE *torrent, int type_value);



void get_info_list(struct b_list_node *node, struct info *info);
void get_info_dict(struct b_dict_list_node *node, struct info *info);
struct info *get_info(struct mem_list *memory, struct b_data *data);

#endif
