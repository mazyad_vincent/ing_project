/**
** \file else_rule.c
** \brief This file contains the else rule of the parser.
** \author Odile Guillaume
**
** This file contains the else rule of the LL grammar for the parser.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "ast.h"

/**
** \fn struct ast *p_else_clause(void)
** \brief reprensents the else_clause rule of 42sh
**
** \param void
** \return the corresponding ast
*/

struct ast *p_else_clause(void)
{
  struct ast *curr = curr_tok();
  if (!curr || (curr->type != ELSE && curr->type != ELIF))
    return free_node(curr);
  eat();
  struct ast *node = p_compound_list();
  if (!node)
    return free_node(curr);
  node = ast_create(node, curr, NULL, NULL);
  curr = curr_tok();
  if (curr && curr->type == THEN)
  {
    eat();
    node = ast_create(curr, node, NULL, NULL);
    struct ast *node2 = p_compound_list();
    if (!node2)
    {
      ast_free(curr);
      return free_node(node);
    }
    node = ast_create(node2, node, NULL, NULL);
    ast_free(node2);
    node2 = p_else_clause();
    if (node2)
    {
      node = ast_create(node2, node, NULL, NULL);
      ast_free(node2);
    }
  }
  else
    ast_free(curr);
  return node;
}
