#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <errno.h>
#include <err.h>
#include <stdio.h>
#include <fcntl.h>
#include "struct_peers.h"

void print_request(char *info_hash, char *url)
{
  for (int i = 0; i < 3; i++)
    printf("%02x", info_hash[i] & 0xff);
  printf(": tracker: requesting peers to %s \n", url);
}

void print_connected(char *info_hash, struct list_sock *sock)
{
  for (int i = 0; i < 3; i++)
    printf("%02x", info_hash[i] & 0xff);
  printf(": peers: connect: %s:%d\n", inet_ntoa(sock->head.sin_addr), 
         (int)(sock->head.sin_port)); 
}

void print_disconnected(char *info_hash, struct list_sock *senti)
{
   struct list_sock *copy = senti;
   while (copy)
   {
      for (int i = 0; i < 3; i++)
        printf("%02x", info_hash[i] & 0xff);
      printf(": peers: disconnect: %s:%d\n", inet_ntoa(copy->head.sin_addr), 
      (int)(copy->head.sin_port));
      copy = copy->next;
   }  
}

void peer_request(struct sock_senti *senti, char *info_hash, char *url, int
                  verbose)
{
  int optval = 1;
  size_t length = senti->size;
  if (length == 0)
     return;
  int index = 0; //index of  tab_fd
  if (verbose == 1)
      print_request(info_hash, url); /* print verbose requesting */
  for (struct list_sock *sock = senti->head; length; sock = sock->next)
  { 
     int fd = socket(AF_INET, SOCK_STREAM, 0);
     fcntl(fd, F_SETFL, O_NONBLOCK);
     int error = setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &optval, 
                            sizeof(optval));
     if (error == -1)
        errx(1, "error socket");
     connect(fd, (struct sockaddr*)&(sock->head), sizeof(sock->head));
     if (verbose == 1)
         print_connected(info_hash, sock); /*print connected */
     length--;
     index++;
  }
  if (verbose == 1)
     print_disconnected(info_hash, senti->head);            
}
/*
void epoll_com(int tab[], size_t length, char *info_hash)
{
  int fd_ep = epoll_create(1);
  struct epoll_event evp[length]; // to stock fd from epoll_wait //
  for (int i = 0; i < length; i++)
  {
      struct epoll_event ep;
      ep.events = EPOLLIN | EPOLLOUT;
      ep.data.fd = tab[i];
      ep.data.u32 = 0LL;
      ep.data.u64 = 0LL;
      int res = epoll_ctl(fd_ep, EPOLL_CTL_ADD, tab[i], &ep);
      if (res == -1)
        errx(1, "fail epoll");
  }
  int boolean = 0; // to leave epollout //
  while (1)
  {
    int wait = epoll_wait(fd_ep, &evp, length, 5000);
    for (int i = 0; i < wait; i++)
    {
      if (evp[i].events & EPOLLIN)
      {
        
      }
      if ((evp[i].events & EPOLLOUT) && !boolean)
        sent_handshake(evp[i].data.fd, info_hash);
    }
  }
}

void sent_handshake(int fd, char *info_hash)
{
  char *hand = malloc(sizeof(char) * 70);
  sprintf(hand, "%c%s%s%s%s", (char)19, "BitTorrent protocol", "\0\0\0\0\0\0\0\0
          ", info_hash, "-MB2020-abcdefghijkl");
  int res = send(fd, hand, 70, MSG_CONFIRM);
  if (res == -1)
     errx(1, "error sending handshake");
  free(hand);
}

void recieve(int fd)
{
   char x[100]; 
   recv(fd, &x, 100, MSG_PEEK);
   if (x[4] == 1)
      return;
   if 
   
}*/
