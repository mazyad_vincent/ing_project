#include <stdio.h>
#include <string.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "texture.h"
#include "player_control.h"

# define TURN_ANGLE 40;
# define GRAVITY_FALL_RATE 2
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;


int player_update(struct player *p, struct map *map)
{
  struct texture *t = p->p_texture;
  //Event handler
  SDL_Event e;


  //Handle events on queue
  while(SDL_PollEvent(&e) != 0)
  {
    if(e.type == SDL_QUIT)
      return -1;
    if(e.type == SDL_KEYDOWN)
    {
      switch(e.key.keysym.sym)
      {
        case SDLK_UP:
          if (p->pos.y - 30 >= 0) //map->height)
          {
            p->pos.y -= 30;
            update_pos_player(map, p, 0, -1);
          }
          break;
        case SDLK_DOWN:
          if (p->pos.y + 30 < SCREEN_HEIGHT)
          {
            p->pos.y += 30;
            update_pos_player(map, p, 0, 1);
          }
          break;
        case SDLK_a:
          if (t->degree == 0)
            t->degree -= TURN_ANGLE;
          break;
        case SDLK_d:
          if (t->degree == 0)
            t->degree += TURN_ANGLE;
          break;
      }
    }
    else if (e.type == SDL_KEYUP)
    {
      if (t->degree > 0)
        t->degree -= TURN_ANGLE;
      if (t->degree < 0)
        t->degree += TURN_ANGLE;
    }
  }

  return 0;
}


void fire_render(struct IA *ia, SDL_Renderer *renderer)
{
  for (size_t i = 0; i < ia->numb; i++)
  {
    //ia->fire[i].x = 0.5;
    render(ia->fire_texture, ia->fire[i].x * 30, ia->fire[i].y * 30, NULL, NULL,
        SDL_FLIP_NONE, renderer);
  }
}

void update_ia_1(struct IA *ia, struct player *player)
{
  SDL_Delay(2);
  if (ia->pos.y == player->pos.y)
    return;
  else if (ia->pos.y - player->pos.y < 0)
    ia->pos.y += 20;
  else
    ia->pos.y -= 20;
}
