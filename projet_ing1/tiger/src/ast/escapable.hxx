/**
 ** \file ast/escapable.hxx
 ** \brief Inline methods of ast::Escapable.
 */

#pragma once

#include <ast/visitor.hh>
#include <ast/escapable.hh>

namespace ast
{

  // FIXME

  inline bool Escapable::is_escap_get()
  {
    return is_escapable;
  }

  inline void Escapable::is_escap_set(bool value)
  {
    is_escapable = value;
  }

} // namespace ast

