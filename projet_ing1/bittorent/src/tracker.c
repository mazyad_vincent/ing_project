#include <err.h>
#include <curl/curl.h> 
#include <string.h>
#include <stdio.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include "tool_tracker.h"
#include "tool_parse.h"
#include "parsing.h"
#include "get_size_char.h"

struct MemoryStruct{
  char *memory;
  size_t size;
};

static size_t write_callback(void *ptr, size_t size, size_t nmemb, void *userdata)
{
   size_t realsize = size * nmemb;
   struct MemoryStruct *mem = (struct MemoryStruct *)userdata;
   mem->memory = realloc(mem->memory, mem->size + realsize + 1);
   if(mem->memory == NULL) {
    errx(1, "not enough memory (realloc returned NULL)\n");
    return 0;
   }
   memcpy(&(mem->memory[mem->size]), ptr, realsize);
   mem->size += realsize;
   mem->memory[mem->size] = '\0';
   return realsize; 
}

char *craft_url(char *url, size_t size_actu, char *info_hash)
{
    char *peer = get_peer_id();
    char *por = port(); 
    char *opts = opts_tra();  
    char *compct = get_compact();
    url = concatenation(url, peer, size_actu, 29);//peer id
    size_actu += 29;
    char *hashing = full_hash(info_hash); 
    url = concatenation(url, hashing, size_actu, strlen(hashing)); //info hash
    size_actu += strlen(hashing);  
    url = concatenation(url, por, size_actu, 11); //port
    size_actu += 11;
    url = concatenation(url, opts, size_actu, 33);
    size_actu += 33;
    url = concatenation(url, compct, size_actu, 10);
    size_actu += 10;
    free(hashing);
    free(peer);
    free(por);
    free(opts);
    free(compct);
    return url;
}

int link_tracker(struct b_data *data, char *infoo, size_t length_info, int
                  print, int verbose)
{
    CURL *handle = curl_easy_init();
    char *hash_free = decode(infoo, length_info);
    char *hash_peers = decode(infoo, length_info); //free it
    char *info_hash = curl_easy_escape(handle, (char*)hash_free, 20);
    char *url = get_url(data);
    char *full_url = malloc(sizeof(char) * strlen(url));
    int ret = 0; /* return value */
    full_url = memcpy(full_url, url, strlen(url) - 1);
    full_url[strlen(url) - 1] = '\0';
    url = craft_url(url, strlen(url), info_hash);
    struct MemoryStruct chunk;
    chunk.memory = malloc(sizeof(char));
    chunk.size = 0;
    char *errbuff[CURL_ERROR_SIZE];
    curl_easy_setopt(handle, CURLOPT_URL, url);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_callback);
    curl_easy_setopt(handle, CURLOPT_WRITEDATA,(void *)&chunk);
    curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, &errbuff);
    CURLcode res = curl_easy_perform(handle); //do the file transfer
    FILE *file_track = fopen("tracker.torrent", "w+");
    file_track = get_tracker_into_file(file_track, chunk.memory);
    struct mem_list *cleanleak = init_mem();
    int type_value = fgetc(file_track);
    struct b_data *info = get_dict(cleanleak, file_track, type_value);
    if (res != CURLE_OK)
      errx(1, "tracker gives no answer");
    struct sock_senti *all_ip = get_peers(info, print); /* get all ip */
    if (!all_ip->size)
      ret = 1;
    peer_request(all_ip, hash_peers, full_url, verbose); /* communicate with peers */
    curl_easy_cleanup(handle); /* to clean */
    curl_global_cleanup();
    destroy_mem(cleanleak);
    fclose(file_track);
    free_all(all_ip);
    free(url);
    free(full_url);
    free(hash_peers);
    free(hash_free);
    free(info_hash);
    free(chunk.memory);
    return ret;
}

