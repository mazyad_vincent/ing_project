#ifndef GENERATION_MAP_H
# define GENERATION_MAP_H
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

enum type
{
  PLAYER, /* if it's the main player */
  IA,  /* IA */
  DECOR, /* ELEMENT OF THE DECOR */
  NOT,  /* NOTHING, THE PLAYER CAN GO THERE WITHOUT DYING */
  END, /* END POS */
  FIRE, /* if there is a fire in the cell */
  GROUND, /* enum for bot */
  TOP /* enum for top */
};

struct vector2
{
  int x;
  int y;
};

struct player
{
  struct vector2 pos; /* position of the player */
  struct texture *p_texture; /* texture of the player */
  int life; /* hp of the player */
};

struct map
{
  enum type **pos; /* map of enum */
  int width;
  struct vector2 pos_player; /* pos of the player */
  struct IA *ia; /* list of ia */
  int height;
  struct vector2 end;
  struct texture *ground_texture;
  struct texture *decoy_texture;
  struct texture *top_texture;
};

struct IA
{
  struct IA *next;
  size_t numb; /* number of fire */
  struct vector2 *fire; /* pos of fire */
  struct vector2 pos; /* pos of IA */
  struct vector2 pos_screen; /* pos of IA on the screen */
  struct texture *ia_texture; /* texture of IA */
  struct texture *fire_texture; /* texture of Fire */
  int boolean;
};

/* functions for map */
struct map* map_init();  /* generate the map */
struct player* init_start(struct map *map); /* create the player */
FILE* create_file(struct map *map); /* write the map into a file */

/* functions for player */
struct player* player_init(int width);
struct vector2 get_pos_player(struct player *player);
void losing_hp(struct map *map, struct player *player);
int update_pos_player(struct map *map, struct player *player, int x, int y);
/* upd in map */

/* functions for IA */
struct IA* ia_init(int x, int y, SDL_Renderer *r); /* init an IA */
void generate_IA(struct map *map, SDL_Renderer *r); /* generate en IA */
void push_ia(struct map *map, struct IA *ia); /* push into the list */
void update_enum(struct map *map, struct IA *ia, int time); /* update the map */
void conditions_IA(struct map *map, struct IA *ia); /* move the ia */
void update_ia(struct map *map, int timing); /* updating everything */
void update_fire(struct IA *ia, struct map *map);
void update_fire_map(struct map *map, struct IA *ia, size_t index, int cond);
void create_fire(struct IA *ia); /* create fire */
int is_behind(struct map *map, struct IA *ia); /* if ia is behind the player */
int is_behind_fire(struct vector2 pos, struct map *map);

/* utilities functions */
int random_num(int height); /* return a random num < height */
int random_two_numb(int max, int min); /* rand between two values */
void free_all(struct player *player, struct map *map);
void free_ia(struct IA *ia); /* free the list of IA */
int is_win(struct map *map, struct player *player); /* is win */
void rend_all(struct map *map, SDL_Renderer *renderer); /* rend ia and flames */

/*function for decoy */
void add_decoy(struct map *map);

#endif
