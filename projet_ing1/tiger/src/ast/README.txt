* README

Tiger Abstract Syntax Tree nodes with their principal members.
Incomplete classes are tagged with a `*'.

/Ast/               (Location location)
  /Dec/             (symbol name)
    FunctionDec     (VarDecs formals, NameTy result, Exp body)
      MethodDec     ()
    TypeDec         (Ty ty)
    VarDec          (NameTy type_name, Exp init)

  /Exp/             ()
    /Var/           ()
      CastVar       (Var var, Ty ty)
*     FieldVar
      SimpleVar     (symbol name)
      SubscriptVar  (Var var, Exp index)

*   ArrayExp
*   AssignExp
*   BreakExp
*   CallExp
*     MethodCallExp
    CastExp         (Exp exp, Ty ty)
    ForExp          (VarDec vardec, Exp hi, Exp body)
*   IfExp
    IntExp          (int value)
*   LetExp
    NilExp          ()
*   ObjectExp
    OpExp           (Exp left, Oper oper, Exp right)
*   RecordExp
*   SeqExp
*   StringExp
    WhileExp        (Exp test, Exp body)

  /Ty/              ()
    ArrayTy         (NameTy base_type)
    ClassTy         (NameTy super, DecsList decs)
    NameTy          (symbol name)
*   RecordTy

  DecsList          (decs_type decs)

  Field             (symbol name, NameTy type_name)

  FieldInit         (symbol name, Exp init)

Some of these classes also inherit from other classes.

/Escapable/
  VarDec            (NameTy type_name, Exp init)

/* a supprimer */


------------------------- TC3 ------------------------------

~Fieldvar  /* accèder a l'element d'un record ex class */ Var Field
~ArrayExp  EXP EXP NameTy
~BreakExp Rien
~ AssignExp Var Exp
~ CallExp  symbol, vect<exp> -> a verifier    point
~ IfExp EXP EXP EXP NameTy              point
~ LetExp  EXP -> a verifier     point vecteur
~ ObjectExp NameTy   point veecteur 
~ RecordExp NameTy symbol /* list d'exp */ -> point vecteur
~ SeqExp  -> a verifier   
~StringExp symbol
~ REcordTy  symbol symbol [list] -> a verifier point vecteur
~ Methodcallexp Var , Callexp 



~ = done


PRETTY PRINTER

Else case? Cmt le gerer

For borne sup ??

Dans #operation
Une seule regle gerée ds le pretty print : exp op exp

----
A finir (vince)
-> let node
-> operation and or
-> subrègles

ADDING STRING IN UNIQUE



DEF_SET AND DEF_GET
callExp
Break Exp
simple var

--------------------------------- TC4 -----------------------------------
/Typable/
  /Dec/             (symbol name)
  /Exp/             ()
  /Ty/              ()

/TypeConstructor/
  /Ty/              ()
  FunctionDec       (VarDecs formals, NameTy result, Exp body)
  NilExp            ()
  TypeDec           (Ty ty)



----

Number of suppressed lines, suppressed chunks, average chunks size:
.                             : 4760 303  15
    lib                       :  249  14  17
        misc                  :  249  14  17
            scoped-map.hh     :   66   2  33
            scoped-map.hxx    :  167   1 167
            symbol.hxx        :    6   4   1
            test-symbol.cc    :    1   1   1
            unique.hxx        :    8   5   1
            variant.hxx       :    1   1   1
    src                       : 4511 289  15
        ast                   : 1573  70  22
            array-exp.cc      :   26   1  26
            array-exp.hh      :   43   1  43
            array-exp.hxx     :   34   1  34
            assign-exp.cc     :   23   1  23
            assign-exp.hh     :   36   1  36
            assign-exp.hxx    :   23   1  23
            break-exp.cc      :   16   1  16
            break-exp.hh      :   30   1  30
            break-exp.hxx     :   12   1  12
            call-exp.cc       :   24   1  24
            call-exp.hh       :   43   1  43
            call-exp.hxx      :   34   1  34
            default-visitor.hxx:   20  11   1
            escapable.cc      :    3   1   3
            escapable.hh      :   30   1  30
            escapable.hxx     :   12   1  12
            field-var.cc      :   23   1  23
            field-var.hh      :   42   1  42
            field-var.hxx     :   34   1  34
            function-dec.hh~  :    2   1   2
            if-exp.cc         :   26   1  26
            if-exp.hh         :   51   1  51
            if-exp.hxx        :   34   1  34
            let-exp.cc        :   23   1  23
            let-exp.hh        :   36   1  36
            let-exp.hxx       :   23   1  23
            method-call-exp.cc:   23   1  23
            method-call-exp.hh:   37   1  37
            method-call-exp.hxx:   23   1  23
            object-exp.cc     :   17   1  17
            object-exp.hh     :   30   1  30
            object-exp.hxx    :   12   1  12
            object-visitor.hxx:    9   5   1
            op-exp.cc         :   14   1  14
            pretty-printer.cc :  313   3 104
            pretty-printer.hh :   30   1  30
            record-exp.cc     :   25   1  25
            record-exp.hh     :   37   1  37
            record-exp.hxx    :   23   1  23
            record-ty.cc      :   22   1  22
            record-ty.hh      :   30   1  30
            record-ty.hxx     :   12   1  12
            seq-exp.cc        :   22   1  22
            seq-exp.hh        :   30   1  30
            seq-exp.hxx       :   12   1  12
            string-exp.cc     :   17   1  17
            string-exp.hh     :   30   1  30
            string-exp.hxx    :   12   1  12
            typable.cc        :    2   1   2
            typable.hh        :   30   1  30
            typable.hxx       :   11   1  11
 type-constructor.cc:    6   1   6
            type-constructor.hh:   30   1  30
            type-constructor.hxx:   11   1  11
        astclone              :   53  17   3
            cloner.cc         :   53  17   3
        bind                  :  488  25  19
            binder.cc         :  188   9  20
            binder.hh         :   37   3  12
            binder.hxx        :   65   4  16
            libbind.cc        :   16   1  16
            libbind.hh        :   16   1  16
            renamer.cc        :   43   1  43
            renamer.hh        :    6   2   3
            renamer.hxx       :   43   2  21
            tasks.cc          :   34   1  34
            tasks.hh          :   40   1  40
        callgraph             :    4   1   4
            call-graph-visitor.cc:    4   1   4
        desugar               :  230   9  25
            bounds-checking-visitor.cc:  139   1 139
            bounds-checking-visitor.hh:    7   1   7
            desugar-visitor.cc:   64   2  32
            libdesugar.cc     :    1   1   1
            tasks.cc          :   10   2   5
            tasks.hh          :    9   2   4
        escapes               :   47   2  23
            escapes-visitor.cc:   34   1  34
            escapes-visitor.hh:   13   1  13
        inlining              :  193   8  24
            inliner.cc        :   78   1  78
            inliner.hh        :    1   1   1
            pruner.cc         :   87   1  87
            pruner.hh         :    9   1   9
            tasks.cc          :   10   2   5
            tasks.hh          :    8   2   4
        object                :  458  73   6
            binder.cc         :   32   3  10
            desugar-visitor.cc:  148  39   3
            libobject.cc      :    8   2   4
            libobject.hh      :    5   1   5
            overfun-binder.cc :   11   1  11
            overfun-binder.hh :   17   1  17
            overfun-type-checker.cc:    4   1   4
            overfun-type-checker.hh:   15   1  15
            renamer.cc        :   33   6   5
            tasks.cc          :   17   2   8
            tasks.hh          :    4   1   4
            type-checker.cc   :  164  15  10
        overload              :  142   4  35
            binder.cc         :   17   2   8
            type-checker.cc   :  114   1 114
            type-checker.hh   :   11   1  11
        parse                 :  447  12  37
            parsetiger.yy     :  421   7  60
            scantiger.ll      :   18   3   6
            tasks.cc          :    1   1   1
            tiger-parser.cc   :    7   1   7
        type                  :  876  68  12
            array.cc          :   15   1  15
            array.hh          :   23   1  23
            array.hxx         :    5   1   5
            attribute.hxx     :    1   1   1
            builtin-types.cc  :   68   1  68
            builtin-types.hh  :   89   1  89
            class.cc          :   45   6   7
            class.hh          :    1   1   1
            default-visitor.hxx:   11   5   2
            function.cc       :   27   1  27
            function.hh       :    1   1   1
            method.cc         :   11   1  11
            method.hh         :    1   1   1
            method.hxx        :    1   1   1
            named.cc          :   32   3  10
            named.hxx         :    2   1   2
            nil.cc            :    2   1   2
            pretty-printer.cc :   55   7   7
            record.cc         :   28   2  14
            record.hh         :    1   1   1
            type-checker.cc   :  422  21  20
            type-checker.hh   :   13   2   6
            type-checker.hxx  :   20   5   4
            type.hxx          :    2   2   1

------------------------------------ TC 5 ----------------------------------

Number of suppressed lines, suppressed chunks, average chunks size:
.                             : 5393 400  13
    lib                       :  249  14  17
        misc                  :  249  14  17
            scoped-map.hh     :   66   2  33
            scoped-map.hxx    :  167   1 167
            symbol.hxx        :    6   4   1
            test-symbol.cc    :    1   1   1
            unique.hxx        :    8   5   1
            variant.hxx       :    1   1   1
    src                       : 5144 386  13
        ast                   : 1573  70  22
            array-exp.cc      :   26   1  26
            array-exp.hh      :   43   1  43
            array-exp.hxx     :   34   1  34
            assign-exp.cc     :   23   1  23
            assign-exp.hh     :   36   1  36
            assign-exp.hxx    :   23   1  23
            break-exp.cc      :   16   1  16
            break-exp.hh      :   30   1  30
            break-exp.hxx     :   12   1  12
            call-exp.cc       :   24   1  24
            call-exp.hh       :   43   1  43
            call-exp.hxx      :   34   1  34
            default-visitor.hxx:   20  11   1
            escapable.cc      :    3   1   3
            escapable.hh      :   30   1  30
            escapable.hxx     :   12   1  12
            field-var.cc      :   23   1  23
            field-var.hh      :   42   1  42
            field-var.hxx     :   34   1  34
            function-dec.hh   :    2   1   2
            if-exp.cc         :   26   1  26
            if-exp.hh         :   51   1  51
            if-exp.hxx        :   34   1  34
            let-exp.cc        :   23   1  23
            let-exp.hh        :   36   1  36
            let-exp.hxx       :   23   1  23
            method-call-exp.cc:   23   1  23
            method-call-exp.hh:   37   1  37
            method-call-exp.hxx:   23   1  23
            object-exp.cc     :   17   1  17
            object-exp.hh     :   30   1  30
            object-exp.hxx    :   12   1  12
            object-visitor.hxx:    9   5   1
            op-exp.cc         :   14   1  14
            pretty-printer.cc :  313   3 104
            pretty-printer.hh :   30   1  30
            record-exp.cc     :   25   1  25
            record-exp.hh     :   37   1  37
            record-exp.hxx    :   23   1  23
            record-ty.cc      :   22   1  22
            record-ty.hh      :   30   1  30
            record-ty.hxx     :   12   1  12
            seq-exp.cc        :   22   1  22
            seq-exp.hh        :   30   1  30
            seq-exp.hxx       :   12   1  12
            string-exp.cc     :   17   1  17
            string-exp.hh     :   30   1  30
            string-exp.hxx    :   12   1  12
            typable.cc        :    2   1   2
            typable.hh        :   30   1  30
            typable.hxx       :   11   1  11
type-constructor.cc:    6   1   6
            type-constructor.hh:   30   1  30
            type-constructor.hxx:   11   1  11
        astclone              :   53  17   3
            cloner.cc         :   53  17   3
        bind                  :  488  25  19
            binder.cc         :  188   9  20
            binder.hh         :   37   3  12
            binder.hxx        :   65   4  16
            libbind.cc        :   16   1  16
            libbind.hh        :   16   1  16
            renamer.cc        :   43   1  43
            renamer.hh        :    6   2   3
            renamer.hxx       :   43   2  21
            tasks.cc          :   34   1  34
            tasks.hh          :   40   1  40
        callgraph             :    4   1   4
            call-graph-visitor.cc:    4   1   4
        desugar               :  230   9  25
            bounds-checking-visitor.cc:  139   1 139
            bounds-checking-visitor.hh:    7   1   7
            desugar-visitor.cc:   64   2  32
            libdesugar.cc     :    1   1   1
            tasks.cc          :   10   2   5
            tasks.hh          :    9   2   4
        escapes               :   47   2  23
            escapes-visitor.cc:   34   1  34
            escapes-visitor.hh:   13   1  13
        inlining              :  193   8  24
            inliner.cc        :   78   1  78
            inliner.hh        :    1   1   1
            pruner.cc         :   87   1  87
            pruner.hh         :    9   1   9
            tasks.cc          :   10   2   5
            tasks.hh          :    8   2   4
        llvmtranslate         :  249  42   5
            escapes-collector.cc:   38   8   4
            llvm-type-visitor.cc:    5   4   1
            translator.cc     :  206  30   6
        object                :  458  73   6
            binder.cc         :   32   3  10
            desugar-visitor.cc:  148  39   3
            libobject.cc      :    8   2   4
            libobject.hh      :    5   1   5
            overfun-binder.cc :   11   1  11
            overfun-binder.hh :   17   1  17
            overfun-type-checker.cc:    4   1   4
            overfun-type-checker.hh:   15   1  15
            renamer.cc        :   33   6   5
            tasks.cc          :   17   2   8
            tasks.hh          :    4   1   4
            type-checker.cc   :  164  15  10
        overload              :  142   4  35
            binder.cc         :   17   2   8
            type-checker.cc   :  114   1 114
            type-checker.hh   :   11   1  11
parse                 :  447  12  37
            parsetiger.yy     :  421   7  60
            scantiger.ll      :   18   3   6
            tasks.cc          :    1   1   1
            tiger-parser.cc   :    7   1   7
        temp                  :   57  16   3
            identifier.hxx    :   57  16   3
        translate             :  274  38   7
            exp.cc            :   47   6   7
            level.cc          :    3   1   3
            translation.cc    :  118  10  11
            translation.hh    :    6   1   6
            translator.cc     :  100  20   5
        tree                  :   53   1  53
            fragment.cc       :   53   1  53
        type                  :  876  68  12
            array.cc          :   15   1  15
            array.hh          :   23   1  23
            array.hxx         :    5   1   5
            attribute.hxx     :    1   1   1
            builtin-types.cc  :   68   1  68
            builtin-types.hh  :   89   1  89
            class.cc          :   45   6   7
            class.hh          :    1   1   1
            default-visitor.hxx:   11   5   2
            function.cc       :   27   1  27
            function.hh       :    1   1   1
            method.cc         :   11   1  11
            method.hh         :    1   1   1
            method.hxx        :    1   1   1
            named.cc          :   32   3  10
            named.hxx         :    2   1   2
            nil.cc            :    2   1   2
            pretty-printer.cc :   55   7   7
            record.cc         :   28   2  14
            record.hh         :    1   1   1
            type-checker.cc   :  422  21  20
            type-checker.hh   :   13   2   6
            type-checker.hxx  :   20   5   4
            type.hxx          :    2   2   1

