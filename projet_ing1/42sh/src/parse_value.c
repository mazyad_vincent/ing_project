/**
** \file parse_value.c
** \brief This file regroups the commands to store a value
** \author Mazyad Vincent
**
** This file regroups the informations needed for storing a value
*/
#define _GNU_SOURCE
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "prompt.h"
#include "hashtable.h"
#include "replace_value.h"
#include "parser.h"
#include "ast.h"

extern struct global g_global;

/**
** \fn static int count_word(char *v)
** \brief count the number of words in the char*
**
** \param char*
** \return the numb of words
*/

int count_word(char *v, int boolean)
{
   int numbers = 0;
   int string = 0;
   char x = ' ';
   for (int i = 0; v && v[i]; i++)
   {
      if (v[i] == '"' && !string)
      {
         if (string == 1)
         {
            string = 0;
            numbers++;
         }
         else
         {
            numbers++;
            string = 1;
         }
      }
      if (v[i] != ' ' && x == ' ' && !string)
      {
         numbers++;
         x = v[i];
      }
      else if (v[i] == ' ')
      {
         if (!boolean) /* before the '=' */
            numbers++;
         if (boolean && !numbers && !string)
            numbers++;
         x = v[i];
      }
   }
   return numbers;
}

/**
** \fn int parse_equal(char *v)
** \brief check if the parsing is correct
**
** \param char*
** \return return 1 if correct and 0 if not
*/

int parse_equal(char *string) /* check if the input is correct */
{
  char *v = strdup(string); /* need to be free */
  char *copy = white_spaces(v);
  char *x = strtok(copy, "=");
  if (count_word(x, 0) != 1) /* count the numb of word spaces before = */
  {
    free(v);
    return 0;
  }
  x = strtok(NULL, "\0");
  if (count_word(x, 1) != 1) /* count numb of word + spaces after = */
  {
     free(v);
     return 0;
  }
  free(v);
  return 1;
}

/**
** \fn static char *escape_blanks(char *buff)
** \brief remove the blanks characters of buff
**
** \param char *buff
** \return return the new char* without the useless characters
*/

char *escape_blanks(char *buff)
{
  size_t length = 0; /* length of malloc */
  char *x = malloc(1);
  for (int i = 0; buff[i] && buff[i] != ' '; i++)
  {
    length++;
    x = realloc(x, length + 1);
    x[i] = buff[i];
  }
  x[length] = '\0';
  return x;
}

/**
** \fn is_equal_operation(struct ast *ast)
** \brief check if there is an '='
**
** \param struct ast *ast
** \return 1 if yes 0 if not
*/

int is_equal_operation(struct ast *ast)
{
  char *key = ast->key;
  for (int i = 0; key && key[i]; i++)
  {
    if (key[i] == '=')
      return (key[i+1] && key[i+1] != '=')? 1 : 0;
  }
  return 0;
}

/**
** \fn put_string_in_hash(struct hashtable *hash, char *string)
** \brief put the string in the hashtable
**
** \param struct hashtable *hash, char *string
*/

int put_string_in_hash(struct hashtable *hash, char *string, int alias)
{
  char *x = strtok(string, "=");
  char *key = strdup(x);
  x = strtok(NULL, "\0");
  char *copy = strdup(x);
  char *data = NULL;
  if (copy[0] != '"')
    data = escape_blanks(copy);
  else
    data = strdup(copy);
  if (!find_hash(hash, key))
     hash_push(hash, key, data, alias);
  else
     hash->list[hash_func(key, hash->capacity)]->data = data;
  free(copy);
  replace_all_nodes(hash, g_global.ast); /* replace all variables by its val */
  return 0;
}
