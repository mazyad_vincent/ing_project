#include "iso9660.h"
#include "my_read_iso.h"

void count_dir(char *ptr, int *dir, int *file, struct iso_dir *cur)
{
    for (int i = 0; i < 2; i++)
        cur = add_offset(cur, sizeof (struct iso_dir) + cur->idf_len +
                (sizeof (struct iso_dir) + cur->idf_len) % 2);

    while (cur && cur->idf_len > 0)
    {
        if (cur->type == 2 || cur->type == 3)
        {
            (*dir)++;
            count_dir(ptr, dir, file, cast_void(ptr + cur->data_blk.le
                        * ISO_BLOCK_SIZE));
        }
        else
            (*file)++;
        cur = add_offset(cur, sizeof (struct iso_dir) + cur->idf_len +
                (sizeof (struct iso_dir) + cur->idf_len) % 2);
    }
}

struct iso_path_table_le* get_n_path(char *ptr, int n)
{
    struct iso_prim_voldesc *super = get_super_block(ptr);
    struct iso_path_table_le *path_table = cast_void(ptr +
            super->le_path_table_blk * ISO_BLOCK_SIZE);
    for (int i = 1; i < n; i++)
        path_table = add_offset(path_table, sizeof (struct iso_path_table_le)
                + path_table->idf_len + (sizeof (struct iso_path_table_le)
                    + path_table->idf_len) % 2);
    return path_table;
}

struct iso_path_table_le *get_next_path(char *ptr, struct iso_path_table_le 
        *path_table, const char *name)
{
    char *dir_name;
    struct iso_path_table_le *origin = path_table;
    path_table = add_offset(path_table, sizeof (struct iso_path_table_le)
            + path_table->idf_len + (sizeof (struct iso_path_table_le)
                + path_table->idf_len) % 2);
    while (path_table && path_table->idf_len > 0)
    {
        dir_name = cast_char(path_table) + sizeof(struct iso_path_table_le);
        if (!strncmp(name, dir_name, path_table->idf_len) && get_n_path(ptr,
                    path_table->parent_dir) == origin)
            break;
        path_table = add_offset(path_table, sizeof (struct iso_path_table_le)
                + path_table->idf_len + (sizeof (struct iso_path_table_le)
                    + path_table->idf_len) % 2);
    }
    return path_table->idf_len > 0 ? path_table : NULL;
}

struct iso_dir *find_dir(struct data *data, const char *name)
{
    struct iso_path_table_le *path_table = data->path_table;
    int dir_len = 0;
    int i = 0;
    char dir[32];
    int len = strlen(name);
    if (name[0] == '/')
    {
        path_table = data->path_table_root;
        i++;
    }
    while (i < len)
    {
        int j = 0;
        dir_len = 0;
        for (; i < len && name[i] != '/'; i++, j++)
        {
            dir[j] = name[i];
            dir_len++;
        }
        i++;
        if (dir_len == 1 && dir[0] == '.')
            continue;
        else if (dir_len == 2 && !strncmp(dir, "..", 2))
            path_table = get_n_path(data->ptr, path_table->parent_dir);
        else
        {
            path_table = get_next_path(data->ptr, path_table, dir);
            if (!path_table)
            {
                fprintf(stderr, "%s: unable to find '%s' directory entry\n",
                        data->script + 2, name);
                return NULL;
            }
        }
    }
    return cast_void(data->ptr + path_table->data_blk
            * ISO_BLOCK_SIZE);
}

struct iso_dir *find_file_dir(struct iso_dir *cur, const char *name)
{
    char *filename;
    while (cur && cur->idf_len > 0)
    {
        filename = cast_char(cur) + sizeof(struct iso_dir);
        if (!strncmp(name, filename, cur->idf_len - 2))
            break;
        cur = add_offset(cur, sizeof (struct iso_dir) + cur->idf_len +
                (sizeof (struct iso_dir) + cur->idf_len) % 2);
    }
    if (cur->idf_len == 0 || cur->type == 2 || cur->type == 3)
    {
        fprintf(stderr, "unable to find '%s' file\n", name);
        return NULL;
    }
    return cur;
}
