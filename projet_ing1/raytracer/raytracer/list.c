#include <stdio.h>
#include <stdlib.h>
#include "parsing.h"

struct sentinelle* senti_init()
{
    struct sentinelle *senti = malloc(sizeof(struct sentinelle));
    if (!senti)
        return NULL;
    senti->head = NULL;
    return senti;
}

struct list* list_init()
{
    struct list *list = malloc(sizeof(struct list));
    if (!list)
        return NULL;
    list->data = NULL;
    list->next = NULL;
    return list;
}

void push_list(struct sentinelle *senti, struct vector3 *vect)
{
    struct list *cpy = senti->head;
    struct list *push = list_init();
    push->data = vect;
    if (!cpy)
    {
       senti->head = push; 
       return;
    }
    while (cpy && cpy->next)
        cpy = cpy->next;
    cpy->next = push;
}

void print_list(struct sentinelle *senti)
{
    struct list *cpy = senti->head;
    while(cpy)
    {
        printf("%f %f %f\n", cpy->data->x, cpy->data->y, cpy->data->z);
        cpy = cpy->next;
    }
}

void free_all(struct sentinelle *senti)
{
    struct list *cpy = senti->head;
    while (cpy)
    {
        struct list *to_pop = cpy;
        cpy = cpy->next;
        struct vector3 *vec = to_pop->data;
        free(vec);
        free(to_pop);
    }
   free(senti);
}

struct list* get_list(struct sentinelle *senti)
{
    struct list *cpy = senti->head;
    if (!cpy)
        return NULL;
    while(cpy && cpy->next)
        cpy = cpy->next;
    return cpy;
}
