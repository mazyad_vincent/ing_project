/**
** \file expansion.h
** \brief contains all functions and enum used by the expansion
** \author OdileGuillaume
**
*/

#ifndef EXPANSION_H
# define EXPANSION_H

/**
** \enum variable: regroups all the possible names of variables in shell
*/

enum variable
{
  HOME = 1,
  PWD,
  OLDPWD
};

/* tilde_expand.c */
char *expand_tilde(char *s);

/* var_expand.c */
char *expand_var(int var);

#endif /* !EXPANSION_H */
