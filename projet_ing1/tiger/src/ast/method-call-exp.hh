/**
 ** \file ast/method-call-exp.hh
 ** \brief Declaration of ast::MethodCallExp.
 */

#pragma once

#include <ast/call-exp.hh>
#include <ast/method-dec.hh>
#include <ast/var.hh>

namespace ast
{

  /** \class ast::MethodCallExp
   ** \brief Method call.
   **
   ** A method call is \em not a function call in the strict sense
   ** of object-oriented programming.  Inheritance is used as a
   ** factoring tool here.
   */

  class MethodCallExp : public CallExp
  {
  // FIXME: Some code was deleted here.
    public:

     MethodCallExp(const Location& location, Var* var, std::vector<Exp*>* liste,
                  misc::symbol& name);
     MethodCallExp(const MethodCallExp&) = delete;
     MethodCallExp& operator=(const MethodCallExp&) = delete;
     virtual ~MethodCallExp();
     void accept(ConstVisitor& v) const override;
     void accept(Visitor& v) override;
     const Var& var_get() const;
     Var& var_get();

     const misc::symbol& get_func() const;
     misc::symbol& get_func();
     const std::vector<Exp*>& get_vect() const;
     std::vector<Exp*>& get_vect();
     void push_front(Exp* exp);

    protected:
     Var* var_;
     std::vector<Exp*> vect_;
     misc::symbol func_;
  };

} // namespace ast

#include <ast/method-call-exp.hxx>

