#include <stdio.h>
#include <string.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "../generation_map.h"
//Texture wrapper class
struct texture
{
  size_t width;
  size_t height;
  double degree;
  SDL_Texture *m_texture;
};

//load texture from file
struct texture *texture_init(char *path, SDL_Renderer *renderer);

void texture_free(struct texture *t);

//Set color modulation
void setColor(struct texture *t, Uint8 r, Uint8 g, Uint8 b);

//Set blending
void setBlendMode(struct texture *t, SDL_BlendMode blending);

//Set alpha modulation
void setAlpha(struct texture *t, Uint8 alpha);

//Renders texture at given point
void render(struct texture *t, int x, int y, SDL_Rect* clip,
    SDL_Point* center, SDL_RendererFlip flip, SDL_Renderer *renderer);
